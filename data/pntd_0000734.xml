<doc>
<field name="uid">doi:10.1371/journal.pntd.0000734</field>
<field name="doi">10.1371/journal.pntd.0000734</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Sanoussi Bamani, Jonathan D. King, Mamadou Dembele, Famolo Coulibaly, Dieudonne Sankara, Yaya Kamissoko, Jim Ting, Lisa A. Rotondo, Paul M. Emerson</field>
<field name="title">Where Do We Go from Here? Prevalence of Trachoma Three Years after Stopping Mass Distribution of Antibiotics in the Regions of Kayes and Koulikoro, Mali</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">7</field>
<field name="pages">e734</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Objectives

A national survey in 1997 demonstrated that trachoma was endemic in Mali.
Interventions to control trachoma including mass drug administration (MDA)
with azithromycin were launched in the regions of Kayes and Koulikoro in 2003.
MDA was discontinued after three annual rounds in 2006, and an impact survey
conducted. We resurveyed all districts in Kayes and Koulikoro in 2009 to
reassess trachoma prevalence and determine intervention objectives for the
future. In this paper we present findings from both the 2006 and 2009 surveys.

Methods

Population-based cluster surveys were conducted in each of the nine districts
in Koulikoro in 2006 and 2009, whilst in Kayes, four of seven districts in
2006 and all seven districts in 2009 were surveyed. Household members present
were examined for clinical signs of trachoma.

Results

Overall, 29,179 persons from 2,528 compounds, in 260 clusters were examined in
2006 and 32,918 from 7,533 households in 320 clusters in 2009. The prevalence
of TF in children aged 1–9 years in Kayes and Koulikoro was 3.9% (95%CI
2.9–5.0%, range by district 1.2–5.4%) and 2.7% (95%CI 2.3–3.1%, range by
district 0.1–5.0%) respectively in 2006. In 2009 TF prevalence was 7.26%
(95%CI 6.2–8.2%, range by district 2.5–15.4%) in Kayes and 8.19% (95%CI
7.3–9.1%, range by district 1.7–17.2%) in Koulikoro among children of the same
age group. TT in adults 15 years of age and older was 2.37% (95%CI 1.66–3.07%,
range by district 0.30–3.54%) in 2006 and 1.37% (95%CI 1.02–1.72%, range by
district 0.37–1.87%) in 2009 in Kayes and 1.75% (95%CI 1.31–2.23%, range by
district 1.06–2.49%) in 2006 and 1.08% (95%CI 0.86–1.30%, range by district
0.34–1.78%) in 2009 in Koulikoro.

Conclusions

Using WHO guidelines for decision making, four districts, Bafoulabe in Kayes
Region; and Banamba, Kolokani and Koulikoro in Koulikoro Region, still meet
criteria for district-wide implementation of the full SAFE strategy as TF in
children exceeds 10%. A community-by-community approach to trachoma control
may now be required in the other twelve districts. Trichiasis surgery
provision remains a need in all districts and should be enhanced in six
districts in Kayes and five in Koulikoro where the prevalence exceeded 1.0% in
adults. Since 1997 great progress has been observed in the fight against
blinding trachoma; however, greater effort is required to meet the elimination
target of 2015.

Author Summary

Trachoma, a blinding bacterial disease, is targeted for elimination by 2020.
To achieve the elimination target, the World Health Organization (WHO)
recommends member states implement the SAFE strategy; surgery, mass
administration of antibiotics, promotion of hygiene and facial cleanliness and
water and sanitation as environmental improvements. We present results from
evaluation surveys conducted in 2006 and 2009 from the regions of Kayes and
Koulikoro, Mali. Prevalence of active trachoma in 2006 was below baseline
intervention thresholds in all surveyed districts and the national program
stopped antibiotic distribution. The prevalence of trachoma in 2009 remained
well below levels in 1998. However, in 8 of 13 distr</field></doc>
