<doc>
<field name="uid">doi:10.1371/journal.pntd.0001007</field>
<field name="doi">10.1371/journal.pntd.0001007</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Pere P. Simarro, Abdoulaye Diarra, Jose A. Ruiz Postigo, José R. Franco, Jean G. Jannin</field>
<field name="title">The Human African Trypanosomiasis Control and Surveillance Programme of the World Health Organization 2000–2009: The Way Forward</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">2</field>
<field name="pages">e1007</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Background

One century ago human African trypanosomiasis (HAT), also known as sleeping
sickness, was believed to curb the development of colonial territories. As
soon as the cause of the disease was clearly identified, colonial authorities
established extensive control operations, fearing an unpopulated continent and
a shortage of human labour to exploit natural resources.

Systematic screening, treatment, and patient follow-up was established in
western and central Africa for the gambiense form of the disease while, animal
reservoir and vector control was mainly implemented in eastern and southern
Africa for the rhodesiense form.

By the 1960s, transmission was practically interrupted in all endemic areas,
providing evidence that the elimination of the disease as a public health
problem was feasible and could be achieved with basic tools. Thereafter, the
rarity of cases led to a loss of interest in sustained surveillance, and the
risk of re-emergence of the disease was overlooked. Thus in the 1980s the
disease re-emerged. By the 1990s, flare-ups were observed throughout past
endemic areas, leading to a worrisome increase in the number of reported
cases. At this time, nongovernmental organizations (NGOs) played a crucial
role in the control of HAT. However, their interventions were mainly focused
on remote and insecure areas. As emergency operators, their policy
understandably excluded support to National Sleeping Sickness Control
Programmes (NSSCPs), which resulted in (i) the establishment of substitute HAT
control systems (ii), the maintenance of a large part of the population at
risk out of the umbrella of NGO projects, and (iii) the difficulty for
national programmes to sustain control achievements after the NGOs&apos;
withdrawal. Concurrently, bilateral cooperation continued to support NSSCPs in
some historically linked countries.

Concerning HAT screening, the card agglutination trypanosomiasis test (CATT)
for serological screening of populations at risk of HAT gambiense was
developed during the 1970s [1], but its large-scale production encountered
many problems, hindering its availability [2]; in addition, production of
anti-trypanosomal drugs was seriously threatened due to the lower economic
return for manufacturers.

Research for new diagnostic tools and drugs was scarce [3]. Only eflornithine,
initially developed for cancer treatment, was finally registered for the
treatment of the gambiense form of the disease in 1990 [4]. But its cost and
complex distribution and administration requirements made it inappropriate for
the under-equipped peripheral health services in remote rural areas where HAT
was prevalent. Only some well-funded NGOs were able to afford the cost of
eflornithine treatment.

During the 1990s, security constraints due to civil wars and social upheavals
complicated HAT control by preventing access to a large number of HAT-endemic
areas, leading to difficulties in reaching a large number of affected
populations and consequently to a considerable lack of epidemiological
information. The World Health Organization (WHO) Expert Committee on HAT
Control and Surveillance held in 1995, in consideration of the huge
uncertainties between the reported cases and the factual field situation,
estimated that the</field></doc>
