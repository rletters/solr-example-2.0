<doc>
<field name="uid">doi:10.1371/journal.pntd.0001207</field>
<field name="doi">10.1371/journal.pntd.0001207</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Fernando Abad-Franch, M. Celeste Vega, Miriam S. Rolón, Walter S. Santos, Antonieta Rojas de Arias</field>
<field name="title">Community Participation in Chagas Disease Vector Surveillance: Systematic Review</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">6</field>
<field name="pages">e1207</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Vector control has substantially reduced Chagas disease (ChD) incidence.
However, transmission by household-reinfesting triatomines persists,
suggesting that entomological surveillance should play a crucial role in the
long-term interruption of transmission. Yet, infestation foci become smaller
and harder to detect as vector control proceeds, and highly sensitive
surveillance methods are needed. Community participation (CP) and vector-
detection devices (VDDs) are both thought to enhance surveillance, but this
remains to be thoroughly assessed.

Methodology/Principal Findings

We searched Medline, Web of Knowledge, Scopus, LILACS, SciELO, the
bibliographies of retrieved studies, and our own records. Data from studies
describing vector control and/or surveillance interventions were extracted by
two reviewers. Outcomes of primary interest included changes in infestation
rates and the detection of infestation/reinfestation foci. Most results likely
depended on study- and site-specific conditions, precluding meta-analysis, but
we re-analysed data from studies comparing vector control and detection
methods whenever possible. Results confirm that professional, insecticide-
based vector control is highly effective, but also show that reinfestation by
native triatomines is common and widespread across Latin America. Bug
notification by householders (the simplest CP-based strategy) significantly
boosts vector detection probabilities; in comparison, both active searches and
VDDs perform poorly, although they might in some cases complement each other.

Conclusions/Significance

CP should become a strategic component of ChD surveillance, but only
professional insecticide spraying seems consistently effective at eliminating
infestation foci. Involvement of stakeholders at all process stages, from
planning to evaluation, would probably enhance such CP-based strategies.

Author Summary

Blood-sucking triatomine bugs are the vectors of Chagas disease, a potentially
fatal illness that affects millions in Latin America. With no vaccines
available, prevention heavily depends on controlling household-infesting
triatomines. Insecticide-spraying campaigns have effectively reduced
incidence, but persistent household reinfestation can result in disease re-
emergence. What, then, is the best strategy to keep houses free of triatomines
and thus interrupt disease transmission in the long run? We reviewed published
evidence to (i) assess the effectiveness of insecticide-based vector control,
gauging the importance of reinfestation; (ii) compare the efficacy of
programme-based (with households periodically visited by trained staff) and
community-based (with residents reporting suspect vectors found in their
homes) surveillance strategies; and (iii) evaluate the performance of
alternative vector-detection methods. The results confirm that insecticide-
based vector control is highly effective, but also that persistent house
reinfestation is a general trend across Latin America. Surveillance systems
are significantly more effective when householders report suspect bugs than
when programme staff search houses, either manually or using vector-detection
devices. Our results clearly support the view that long-term vector
surveillance will be necessary for sustained Chagas dis</field></doc>
