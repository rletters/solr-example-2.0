<doc>
<field name="uid">doi:10.1371/journal.pntd.0001264</field>
<field name="doi">10.1371/journal.pntd.0001264</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Ha Vinh, Vo Thi Cuc Anh, Nguyen Duc Anh, James I. Campbell, Nguyen Van Minh Hoang, Tran Vu Thieu Nga, Nguyen Thi Khanh Nhu, Pham Van Minh, Cao Thu Thuy, Pham Thanh Duy, Le Thi Phuong, Ha Thi Loan, Mai Thu Chinh, Nguyen Thi Thu Thao, Nguyen Thi Hong Tham, Bui Li Mong, Phan Van Be Bay, Jeremy N. Day, Christiane Dolecek, Nguyen Phu Huong Lan, To Song Diep, Jeremy J. Farrar, Nguyen Van Vinh Chau, Marcel Wolbers, Stephen Baker</field>
<field name="title">A Multi-Center Randomized Trial to Assess the Efficacy of Gatifloxacin versus Ciprofloxacin for the Treatment of Shigellosis in Vietnamese Children</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">8</field>
<field name="pages">e1264</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The bacterial genus Shigella is the leading cause of dysentery. There have
been significant increases in the proportion of Shigella isolated that
demonstrate resistance to nalidixic acid. While nalidixic acid is no longer
considered as a therapeutic agent for shigellosis, the fluoroquinolone
ciprofloxacin is the current recommendation of the World Health Organization.
Resistance to nalidixic acid is a marker of reduced susceptibility to older
generation fluoroquinolones, such as ciprofloxacin. We aimed to assess the
efficacy of gatifloxacin versus ciprofloxacin in the treatment of
uncomplicated shigellosis in children.

Methodology/Principal Findings

We conducted a randomized, open-label, controlled trial with two parallel arms
at two hospitals in southern Vietnam. The study was designed as a superiority
trial and children with dysentery meeting the inclusion criteria were invited
to participate. Participants received either gatifloxacin (10 mg/kg/day) in a
single daily dose for 3 days or ciprofloxacin (30 mg/kg/day) in two divided
doses for 3 days. The primary outcome measure was treatment failure; secondary
outcome measures were time to the cessation of individual symptoms. Four
hundred and ninety four patients were randomized to receive either
gatifloxacin (n  =  249) or ciprofloxacin (n  =  245), of which 107 had a
positive Shigella stool culture. We could not demonstrate superiority of
gatifloxacin and observed similar clinical failure rate in both groups
(gatifloxacin; 12.0% and ciprofloxacin; 11.0%, p  =  0.72). The median (inter-
quartile range) time from illness onset to cessation of all symptoms was 95
(66–126) hours for gatifloxacin recipients and 93 (68–120) hours for the
ciprofloxacin recipients (Hazard Ratio [95%CI]  =  0.98 [0.82–1.17], p  =
0.83).

Conclusions

We conclude that in Vietnam, where nalidixic acid resistant Shigellae are
highly prevalent, ciprofloxacin and gatifloxacin are similarly effective for
the treatment of acute shigellosis.

Trial Registration

Controlled trials number ISRCTN55945881

Author Summary

The bacterial genus Shigella is the most common cause of dysentery (diarrhea
containing blood and/or mucus) and the disease is common in developing
countries with limitations in sanitation. Children are most at risk of
infection and frequently require hospitalization and antimicrobial therapy.
The WHO currently recommends the fluoroquinolone, ciprofloxacin, for the
treatment of childhood Shigella infections. In recent years there has been a
sharp increase in the number of organisms that exhibit resistance to nalidixic
acid (an antimicrobial related to ciprofloxacin), corresponding with reduced
susceptibility to ciprofloxacin. We hypothesized that infections with Shigella
strains that demonstrate resistance to nalidixic acid may prevent effective
treatment with ciprofloxacin. We performed a randomiz</field></doc>
