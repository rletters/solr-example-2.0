<doc>
<field name="uid">doi:10.1371/journal.pntd.0001388</field>
<field name="doi">10.1371/journal.pntd.0001388</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Tiziana Lembo, on behalf of the Partners for Rabies Prevention</field>
<field name="title">The Blueprint for Rabies Prevention and Control: A Novel Operational Toolkit for Rabies Elimination</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">2</field>
<field name="pages">e1388</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Introduction

Rabies is a prime example of a neglected tropical disease that mostly affects
communities suffering from inequitable health care [1]. The false perception
that rabies impacts on society are low is due to case under-reporting and
limited awareness of the disease burden [2], [3]. Effective tools for
elimination of terrestrial rabies are available [4]. While the sustained
deployment of these tools has led to some remarkably successful interventions
[5], [6], canine rabies continues to claim lives in rabies-endemic countries
and areas of re-emergence, where &gt;95% of human deaths occur as a result of
bites by rabid domestic dogs [7], [8]. Control programs targeting dogs can
effectively reduce the risk of rabies to humans [3], [9]. However, the design
and implementation of such programs still pose considerable challenges to
local governments, and a lack of easy-to-use guidelines has been identified as
an important reason for this.

Global rabies experts from the Partners for Rabies Prevention have therefore
gathered to translate evidence-based knowledge on rabies control into user-
friendly guidelines. Existing information obtained from different sources,
including previously published guidelines by international health and animal
welfare organizations and scientific findings, has been packaged into a novel
online document, the Blueprint for Rabies Prevention and Control
(http://www.rabiesblueprint.com), which we describe herewith.

The Website

The ultimate goal of the Rabies Blueprint is to provide relevant authorities
and personnel in rabies-affected areas with a standard operating procedure
(“Blueprint”) to develop their own programs for preventing human rabies
through canine rabies elimination and control of wildlife rabies. This
document is not meant to replace national legislation or existing guidelines.
Rather, it brings all relevant information into one accessible document to
make the design of rabies management programs easier globally, both in areas
where rabies is endemic or has been re-introduced.

This toolkit has been developed based on the following principles: (1) create
a multi-disciplinary working group combining all disciplines/institutions
required for formulating comprehensive guidelines for a “One Health” approach
to controlling rabies [10]; (2) build upon research-based material to
summarize critical information and provide key take-home messages, with links
to more detailed information; (3) provide country-specific examples that
address common misperceptions and illustrate successful rabies control
programs in a range of settings to encourage appropriate investment and
efforts elsewhere; (4) follow a question-and-answer approach, reflecting the
most frequently asked questions on rabies prevention and control; (5) use
language that is understandable by a wide range of users, including
professionals and field personnel; (6) use an openly accessible Internet-based
format; and (7) develop a low-resolution website to allow users with low
Internet speeds or a mobile phone connection to navigate the system.

The current version of the Rabies Blueprint focuses on dogs as reservoirs, in
recognition of the amplified risk to human health from canine rabies. The
Rabies Blueprint provides step-by-step guidelines to prevent human rabies by
eliminating</field></doc>
