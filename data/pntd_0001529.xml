<doc>
<field name="uid">doi:10.1371/journal.pntd.0001529</field>
<field name="doi">10.1371/journal.pntd.0001529</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Kimberly K. Gray, Melissa N. Worthy, Terry L. Juelich, Stacy L. Agar, Allison Poussard, Dan Ragland, Alexander N. Freiberg, Michael R. Holbrook</field>
<field name="title">Chemotactic and Inflammatory Responses in the Liver and Brain Are Associated with Pathogenesis of Rift Valley Fever Virus Infection in the Mouse</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">2</field>
<field name="pages">e1529</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Rift Valley fever virus (RVFV) is a major human and animal pathogen associated
with severe disease including hemorrhagic fever or encephalitis. RVFV is
endemic to parts of Africa and the Arabian Peninsula, but there is significant
concern regarding its introduction into non-endemic regions and the
potentially devastating effect to livestock populations with concurrent
infections of humans. To date, there is little detailed data directly
comparing the host response to infection with wild-type or vaccine strains of
RVFV and correlation with viral pathogenesis. Here we characterized clinical
and systemic immune responses to infection with wild-type strain ZH501 or IND
vaccine strain MP-12 in the C57BL/6 mouse. Animals infected with live-
attenuated MP-12 survived productive viral infection with little evidence of
clinical disease and minimal cytokine response in evaluated tissues. In
contrast, ZH501 infection was lethal, caused depletion of lymphocytes and
platelets and elicited a strong, systemic cytokine response which correlated
with high virus titers and significant tissue pathology. Lymphopenia and
platelet depletion were indicators of disease onset with indications of
lymphocyte recovery correlating with increases in G-CSF production. RVFV is
hepatotropic and in these studies significant clinical and histological data
supported these findings; however, significant evidence of a pro-inflammatory
response in the liver was not apparent. Rather, viral infection resulted in a
chemokine response indicating infiltration of immunoreactive cells, such as
neutrophils, which was supported by histological data. In brains of ZH501
infected mice, a significant chemokine and pro-inflammatory cytokine response
was evident, but with little pathology indicating meningoencephalitis. These
data suggest that RVFV pathogenesis in mice is associated with a loss of liver
function due to liver necrosis and hepatitis yet the long-term course of
disease for those that might survive the initial hepatitis is neurologic in
nature which is supported by observations of human disease and the BALB/c
mouse model.

Author Summary

Rift Valley fever virus (RVFV) is a deadly virus that is found primarily in
sub-Saharan Africa. However, mosquitoes that are capable of transmitting RVFV
have a worldwide distribution. RVF affects a broad range of animal species and
is known to cause encephalitis or hemorrhagic fever in humans with survivors
having long-term health effects such as a loss of vision. There is no vaccine
currently approved for use in humans. In this study we evaluated the host
response to infection with either a RVFV vaccine strain or a wild-type strain
to identify similarities or differences that could be exploited for
development of therapeutics or improved vaccines. While mice infected with the
vaccine strain did not develop disease and survived, infection with the wild-
type strain caused severe disease with a fatal outcome. Analyzing multiple
clinical factors and the host immune response over the course of infection
allowed us to identify potential host factors associated with disease
progression during wild-type virus infection. This work also provides support
for a</field></doc>
