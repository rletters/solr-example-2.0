<doc>
<field name="uid">doi:10.1371/journal.pntd.0000666</field>
<field name="doi">10.1371/journal.pntd.0000666</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Matthew T. Aliota, Jeremy F. Fuchs, Thomas A. Rocheleau, Amanda K. Clark, Julián F. Hillyer, Cheng-Chen Chen, Bruce M. Christensen</field>
<field name="title">Mosquito Transcriptome Profiles and Filarial Worm Susceptibility in Armigeres subalbatus</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">4</field>
<field name="pages">e666</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Armigeres subalbatus is a natural vector of the filarial worm Brugia pahangi,
but it kills Brugia malayi microfilariae by melanotic encapsulation. Because
B. malayi and B. pahangi are morphologically and biologically similar,
comparing Ar. subalbatus-B. pahangi susceptibility and Ar. subalbatus-B.
malayi refractoriness could provide significant insight into recognition
mechanisms required to mount an effective anti-filarial worm immune response
in the mosquito, as well as provide considerable detail into the molecular
components involved in vector competence. Previously, we assessed the
transcriptional response of Ar. subalbatus to B. malayi, and now we report
transcriptome profiling studies of Ar. subalbatus in relation to filarial worm
infection to provide information on the molecular components involved in B.
pahangi susceptibility.

Methodology/Principal Findings

Utilizing microarrays, comparisons were made between mosquitoes exposed to B.
pahangi, B. malayi, and uninfected bloodmeals. The time course chosen
facilitated an examination of key events in the development of the parasite,
beginning with the very start of filarial worm infection and spanning to well
after parasites had developed to the infective stage in the mosquito. At 1, 3,
6, 12, 24 h post infection and 2–3, 5–6, 8–9, and 13–14 days post challenge
there were 31, 75, 113, 76, 54, 5, 3, 13, and 2 detectable transcripts,
respectively, with significant differences in transcript abundance (increase
or decrease) as a result of parasite development.

Conclusions/Significance

Herein, we demonstrate that filarial worm susceptibility in a laboratory
strain of the natural vector Ar. subalbatus involves many factors of both
known and unknown function that most likely are associated with filarial worm
penetration through the midgut, invasion into thoracic muscle cells, and
maintenance of homeostasis in the hemolymph environment. The data show that
there are distinct and separate transcriptional patterns associated with
filarial worm susceptibility as compared to refractoriness, and that an
infection response in Ar. subalbatus can differ significantly from that
observed in Ae. aegypti, a common laboratory model.

Author Summary

In general, organisms can use two different strategies when confronted with
pathogens, tolerance and/or resistance. Resistance reduces the fitness of the
invading pathogen, whereas tolerance reduces the damage caused by the pathogen
to the host. Mosquitoes that transmit the parasites that cause human lymphatic
filariasis generally are tolerant to the parasite, whereas those that do not
transmit the parasite are resistant. We examined the effects of filarial worm
tolerance and resistance on Armigeres subalbatus by analyzing changes in
mosquito gene expression at key stages of parasite development and
destruction. Because the gene expression data showed few mosquito
transcriptional changes associated with parasite development, we
morphologically examined mosquito flight muscle to see if we could identify
damage associated with parasite infection. The research described in this
manuscript provides a better understanding of the molecular components
involved in compatible and incompatible relationships between mosquit</field></doc>
