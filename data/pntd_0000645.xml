<doc>
<field name="uid">doi:10.1371/journal.pntd.0000645</field>
<field name="doi">10.1371/journal.pntd.0000645</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Joseli Lannes-Vieira, Tania C. de Araújo-Jorge, Maria de Nazaré Correia Soeiro, Paulo Gadelha, Rodrigo Corrêa-Oliveira</field>
<field name="title">The Centennial of the Discovery of Chagas Disease: Facing the Current Challenges</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">6</field>
<field name="pages">e645</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Chagas Disease: Then and Now

One hundred years after Carlos Chagas&apos; discovery [1] WHO defines Chagas
disease as one of the most important infectious diseases of poverty. Beyond
its biological determinants (interplays among the parasite, vector, and
human), the social determinants of Chagas disease are of utmost importance;
poor housing and working conditions, low salaries, and malnutrition are
directly linked to Chagas disease in Latin American [2]. This article
highlights the current state on research and innovation related to control and
care of Chagas disease, as well as the challenges for the next decade.

Confronting Reality and Keeping Transmission Control in Focus

In 2007, Latin America [3] had an estimated 8 million to 15 million
Trypanosoma cruzi-infected individuals in 15 afflicted countries, with an
incidence of vector transmission greater than 40,000 cases per year and the
congenital transmission rate higher than 14,000 cases per year. These numbers
reflect approximately 2 million infected women of fertile age and almost 30
million people at risk in endemic areas. The prevalence of infected blood in
blood banks is 1.28%, and 1.8 million individuals have Chagas disease
cardiopathy [4], with several hundred thousand infected people living in other
nations as a result of migration [5].

Considering the feasibility of Chagas disease control, several points must be
discussed, namely:

  1. Permanent surveillance for new outbreaks due to oral transmission;

  2. Domiciliary invasion by sylvatic triatomines, due to the complex parasite behaviour and ecological contexts;

  3. Lack of effective treatment, immunoprophylaxis, and feasible markers for disease progression; and

  4. Continuous circulation of T. cruzi isolates largely spread in sylvatic ecotopes across the American continent, showing that elimination of human disease transmission has to be seen in a new context [6] that is dependent on local transmission characteristics and prevalence [4], [5].

Epidemiological parameters have shown that the success of vector control
policies are due to efforts of large interventions such as those in the
Southern Cone, Andes, Central America, Mexico, and Amazon, which have largely
reduced the morbidity and transmission of Chagas disease [3], [4]. In Brazil,
for example, the success of vector control reduced the incidence of new cases
from 100,000 in 1980 to less than 500 notified cases per year from 2001 to
2006 [7]. Uruguay, Chile, and Brazil received the Pan American Health
Organization (PAHO)/World Health Organization (WHO) certification for
interruption of Triatoma infestans vector transmission [3]. However,
sustainability depends on control of autochthonous triatomines by permanent
epidemiological and entomological surveillance, as annual oscillating
frequencies of house infestation within the same region have been noted,
particularly in some hotspots in the Argentinean Gran Chaco [8]. Limited
effectiveness of pyrethroid insecticides outdoors and in peridomiciliar
structures, development of insecticide resistance, and sylvatic and semi-
domestic vectors might contribute to residual vector foci even after
insecticide spraying [8], implying a risk of re-emergence of acute Chagas
disease. Additionally, blood bank control still reveal</field></doc>
