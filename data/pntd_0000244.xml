<doc>
<field name="uid">doi:10.1371/journal.pntd.0000244</field>
<field name="doi">10.1371/journal.pntd.0000244</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Robert Bergquist, Jürg Utzinger, Donald P. McManus</field>
<field name="title">Trick or Treat: The Role of Vaccines in Integrated Schistosomiasis Control</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">6</field>
<field name="pages">e244</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Neglected and Underappreciated—The Case of Schistosomiasis

Recent systematic reviews [1],[2] indicate that the geographic extent and
burden of schistosomiasis exceeds official estimates. The risk for infection
is particularly pronounced in sub-Saharan Africa, but also exists in many
parts of South America, the Middle East, and Southeast Asia. Collectively,
close to 800 million individuals are at risk of schistosomiasis, and over 200
million people are infected [2]. The disease can be controlled with vigorous
political and financial commitment, but local elimination has proved difficult
in reality. The cost of treatment with the only available
drug—praziquantel—has become affordable, and hence preventive chemotherapy is
being advocated on a large scale [3]. However, a strongly biased approach,
devoid of any emphasis on prevention, access to clean water and improved
sanitation, and snail control, demands indefinite drug distribution [4],[5].
Although praziquantel-based morbidity control has proved successful, it has an
intrinsic weakness, which is intimately related to its inadequate impact on
transmission even when chemotherapy is provided according to schedules
carefully adjusted to the local setting [6].

Genuine change of the disease spectrum in endemic settings demands lasting
results which can only be obtained by long-term protection involving
vaccination. An entirely vaccine-based approach to schistosomiasis control is
unrealistic, but we advocate that acceptable protection could be achieved by
chemotherapy followed by vaccination aimed at reducing, or markedly delaying,
the development of pathology [7]. Thus, the issue is not vaccines versus
chemotherapy, but how to graft a vaccine approach onto current schistosomiasis
control programs. This, we believe, would fit well with the scope of the
“Schistosomiasis Research Agenda”, particularly in the areas of “Basic
Science”, “Tools and Interventions”, and “Disease Burden” advanced by Colley
and Secor in their recent Policy Platform article in PLoS Neglected Tropical
Diseases [8] and the linked Expert Commentary [9].

The True Impact of Schistosomiasis

Currently, vaccines do not figure prominently in the context of
schistosomiasis control. In fact, neither vaccines nor new drug development
are pursued with high priority by funding bodies, industry, or academia. There
are two important reasons for this. First, praziquantel is safe, efficacious,
and inexpensive (recently even donated free of charge in some control
programs) and, as yet, there are no clear indications of drug resistance.
Second, a low rank is awarded to schistosomiasis by the disability-adjusted
life year (DALY) metric, an approach guiding the policies of most
organizations active in the public health area, including the World Health
Organization (WHO) [10],[11].

Figure 1 illustrates the impact according to the DALY metric of the ten
diseases supported by the UNICEF/UNDP/World Bank/WHO Special Programme for
Research and Training in Tropical Diseases (TDR) in relation to research
spending for the biennium 2001–2002 [7] and 2007–2008 [12]. The difference
between the two graphs in Figure 1 seems to indicate a move away from a
rigorous adherence to the DALY metric as the key to funding that was evident
at the beginning of the new millennium. Indeed, there are several “outlier</field></doc>
