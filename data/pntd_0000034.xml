<doc>
<field name="uid">doi:10.1371/journal.pntd.0000034</field>
<field name="doi">10.1371/journal.pntd.0000034</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Hervé Lecoeur, Pierre Buffet, Gloria Morizot, Sophie Goyard, Ghislaine Guigon, Geneviève Milon, Thierry Lang</field>
<field name="title">Optimization of Topical Therapy for Leishmania major Localized Cutaneous Leishmaniasis Using a Reliable C57BL/6 Model</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2007</field>
<field name="volume">1</field>
<field name="number">2</field>
<field name="pages">e34</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Because topical therapy is easy and usually painless, it is an attractive
first-line option for the treatment of localized cutaneous leishmaniasis
(LCL). Promising ointments are in the final stages of development. One main
objective was to help optimize the treatment modalities of human LCL with
WR279396, a topical formulation of aminoglycosides that was recently proven to
be efficient and safe for use in humans.

Methodology/Principal Findings

C57BL/6 mice were inoculated in the ear with luciferase transgenic L. major
and then treated with WR279396. The treatment period spanned lesion onset, and
the evolution of clinical signs and bioluminescent parasite loads could be
followed for several months without killing the mice. As judged by clinical
healing and a 1.5-3 log parasite load decrease in less than 2 weeks, the 94%
efficacy of 10 daily applications of WR279396 in mice was very similar to what
had been previously observed in clinical trials. When WR279396 was applied
with an occlusive dressing, parasitological and clinical efficacy was
significantly increased and no rebound of parasite load was observed. In
addition, 5 applications under occlusion were more efficient when done every
other day for 10 days than daily for 5 days, showing that length of therapy is
a more important determinant of treatment efficacy than the total dose
topically applied.

Conclusions/Significance

Occlusion has a significant adjuvant effect on aminoglycoside ointment therapy
of experimental cutaneaous leishmaniasis (CL), a concept that might apply to
other antileishmanial or antimicrobial ointments. Generated in a laboratory
mouse-based model that closely mimics the course of LCL in humans, our results
support a schedule based on discontinuous applications for a few weeks rather
than several daily applications for a few days.

Author Summary

When initiating the cutaneous disease named cutaneous leishmaniasis (CL),
Leishmania parasites develop within the parasitophorous vacuoles of phagocytes
residing in and/or recruited to the dermis, a process leading to more or less
chronic dermis and epidermis-damaging inflammatory processes. Topical
treatment of CL could be a mainstay in its management. Any improvements of
topicals, such as new vehicles and shorter optimal contact regimes, could
facilitate their use as an ambulatory treatment. Recently, WR279396, a third-
generation aminoglycoside ointment, was designed with the aim to provide
stability and optimal bioavailability for the molecules expected to target
intracellular Leishmania. Two endpoints were expected to be reached: i)
accelerated clearance of the maximal number of parasites, and ii) accelerated
and stable repair processes without scars. A mouse model of CL was designed:
it relies on the intradermal inoculation of luciferase-expressing Leishmania,
allowing for in vivo bioluminescence imaging of the parasite load fluctuation,
which can then be quantified simultaneously with the onset and resolution of
clinical signs. These quantitative readout assays, deployed in real time,
provide robust methods to rapidly assess efficacy of drugs/compounds i) to
screen treatment modalities and ii) allow standardized comparison of different
therapeutic agents.

Introductio</field></doc>
