<doc>
<field name="uid">doi:10.1371/journal.pntd.0000306</field>
<field name="doi">10.1371/journal.pntd.0000306</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Aura A. Andreasen, Matthew J. Burton, Martin J. Holland, Spencer Polley, Nkoyo Faal, David C.W. Mabey, Robin L. Bailey</field>
<field name="title">Chlamydia trachomatis ompA Variants in Trachoma: What Do They Tell Us?</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">9</field>
<field name="pages">e306</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Trachoma, caused by Chlamydia trachomatis (Ct), is the leading infectious
cause of blindness. Sequence-based analysis of the multiple strains typically
present in endemic communities may be informative for epidemiology,
transmission, response to treatment, and understanding the host response.

Methods

Conjunctival and nasal samples from a Gambian community were evaluated before
and 2 months after mass azithromycin treatment. Samples were tested for Ct by
Amplicor, with infection load determined by quantitative PCR (qPCR). ompA
sequences were determined and their diversity analysed using frequency-based
tests of neutrality.

Results

Ninety-five of 1,319 (7.2%) individuals from 14 villages were infected with Ct
at baseline. Two genovars (A and B) and 10 distinct ompA genotypes were
detected. Two genovar A variants (A1 and A2) accounted for most infections.
There was an excess of rare ompA mutations, not sustained in the population.
Post-treatment, 76 (5.7%) individuals had Ct infection with only three ompA
genotypes present. In 12 of 14 villages, infection had cleared, while in two
it increased, probably due to mass migration. Infection qPCR loads associated
with infection were significantly greater for A1 than for A2. Seven
individuals had concurrent ocular and nasal infection, with divergent
genotypes in five.

Conclusions

The number of strains was substantially reduced after mass treatment. One
common strain was associated with higher infection loads. Discordant genotypes
in concurrent infection may indicate distinct infections at ocular and nasal
sites. Population genetic analysis suggests the fleeting appearance of rare
multiple ompA variants represents purifying selection rather than escape
variants from immune pressure. Genotyping systems accessing extra-ompA
variation may be more informative.

Author Summary

Trachoma is an important cause of blindness resulting from transmission of the
bacterium Chlamydia trachomatis. One way to understand better how this
infection is transmitted and how the human immune system controls it is to
study the strains of bacteria associated with infection. Comparing strains
before and after treatment might help us learn if someone has a new infection
or the same one as before. Identifying differences between disease-causing
strains should help us understand how infection leads to disease and how the
human host defences work. We chose to study variation in the chlamydial gene
ompA because it determines the protein MOMP, one of the leading candidates for
inclusion in a vaccine to prevent trachoma. If immunity to MOMP is important
in natural trachoma infections, we would expect to find evidence of this in
the way the strains varied. We did not find this, but instead found that two
common strains seemed to cause different types of disease. Although their
MOMPs were very slightly different, this did not really explain the
differences. We conclude that methods of typing strains going beyond the ompA
gene will be needed to help us understand the interaction between Chlamydia
and its human host.

Introduction

Trachoma is the leading infectious cause of blindness worldwide [1]. Repeated
infection by Chlamydia trachomatis provokes chronic follicular conjunctivitis
(clinically active trachoma), which lea</field></doc>
