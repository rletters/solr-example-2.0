<doc>
<field name="uid">doi:10.1371/journal.pntd.0000916</field>
<field name="doi">10.1371/journal.pntd.0000916</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Bruce Y. Lee, Kristina M. Bacon, Diana L. Connor, Alyssa M. Willig, Rachel R. Bailey</field>
<field name="title">The Potential Economic Value of a Trypanosoma cruzi (Chagas Disease) Vaccine in Latin America</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">12</field>
<field name="pages">e916</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Chagas disease, caused by the parasite Trypanosoma cruzi (T. cruzi), is the
leading etiology of non-ischemic heart disease worldwide, with Latin America
bearing the majority of the burden. This substantial burden and the
limitations of current interventions have motivated efforts to develop a
vaccine against T. cruzi.

Methodology/Principal Findings

We constructed a decision analytic Markov computer simulation model to assess
the potential economic value of a T. cruzi vaccine in Latin America from the
societal perspective. Each simulation run calculated the incremental cost-
effectiveness ratio (ICER), or the cost per disability-adjusted life year
(DALY) avoided, of vaccination. Sensitivity analyses evaluated the impact of
varying key model parameters such as vaccine cost (range: $0.50–$200), vaccine
efficacy (range: 25%–75%), the cost of acute-phase drug treatment (range:
$10–$150 to account for variations in acute-phase treatment regimens), and
risk of infection (range: 1%–20%). Additional analyses determined the
incremental cost of vaccinating an individual and the cost per averted
congestive heart failure case. Vaccination was considered highly cost-
effective when the ICER was ≤1 times the GDP/capita, still cost-effective when
the ICER was between 1 and 3 times the GDP/capita, and not cost-effective when
the ICER was &gt;3 times the GDP/capita. Our results showed vaccination to be
very cost-effective and often economically dominant (i.e., saving costs as
well providing health benefits) for a wide range of scenarios, e.g., even when
risk of infection was as low as 1% and vaccine efficacy was as low as 25%.
Vaccinating an individual could likely provide net cost savings that rise
substantially as risk of infection or vaccine efficacy increase.

Conclusions/Significance

Results indicate that a T. cruzi vaccine could provide substantial economic
benefit, depending on the cost of the vaccine, and support continued efforts
to develop a human vaccine.

Author Summary

The substantial burden of Chagas disease, especially in Latin America, and the
limitations of currently available treatment and control strategies have
motivated the development of a Trypanosoma cruzi (T. cruzi) vaccine.
Evaluating a vaccine&apos;s potential economic value early in its development can
answer important questions while the vaccine&apos;s key characteristics (e.g.,
vaccine efficacy targets, price points, and target population) can still be
altered. This can assist vaccine scientists, manufacturers, policy makers, and
other decision makers in the development and implementation of the vaccine. We
developed a computational economic model to determine the cost-effectiveness
of introducing a T. cruzi vaccine in Latin America. Our results showed
vaccination to be very cost-effective, in many cases providing both cost
savings and health benefits, even at low infection risk and vaccine efficacy.
Moreover, our study suggests that a vaccine may actually “pay for itself”, as
even a relatively higher priced vaccine will generate net cost savings for a
purchaser (e.g., a country&apos;s ministry of health). These findings support
continued investments in and efforts toward the development of a human T.
cruzi vaccine.

Introduction

Chagas disease (American trypanos</field></doc>
