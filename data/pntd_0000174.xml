<doc>
<field name="uid">doi:10.1371/journal.pntd.0000174</field>
<field name="doi">10.1371/journal.pntd.0000174</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Karen A. Grépin, Michael R. Reich</field>
<field name="title">Conceptualizing Integration: A Framework for Analysis Applied to Neglected Tropical Disease Control Partnerships</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">4</field>
<field name="pages">e174</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Introduction

The timely and sustained delivery of effective health interventions to
communities in developing countries is one of the greatest challenges in
global health. Millions of the world&apos;s poorest citizens continue to be
afflicted by bacterial, viral, and parasitic infections that have persisted,
mainly in the tropics—the so-called neglected tropical diseases (NTDs)—despite
the availability of safe and cost-effective interventions for the control and
elimination of many of these diseases. Access to these interventions (or
control tools) remains low and inadequate, particularly in sub-Saharan Africa
[1],[2]. The NTDs, including onchocerciasis, schistosomiasis, lymphatic
filariasis, trachoma, and soil-transmitted helminthiasis, have been shown to
affect the poorest of the poor disproportionately. Addressing the NTDs,
therefore, will be an essential element in poverty alleviation programs
[3],[4].

A number of important international single-disease control partnerships have
been developed over the last few decades [5]–[7]. To date, however, there has
been little integration among these partnerships [3]. Integration refers to
the creation of linkages among existing programs to improve the delivery of
health interventions given existing commitments and resources. The presence of
many common elements and general arguments about economies of scale provide
strong reasons to believe that integration amongst partnerships can help
improve both efficiency and effectiveness.

Interest in integration is currently at an all-time high, due in part to new
funding for integration (the Bill &amp; Melinda Gates Foundation has announced
research grants to investigate integration of NTDs, and the United States
Agency for International Development [USAID] has awarded operational grants to
scale-up integrated NTD control programs), the creation of the Global Network
for Neglected Tropical Diseases Control (GNNTDC; http://gnntdc.sabin.org/),
and high-level political commitment to address these scourges [8]. In
addition, reports of successful national control programs for single diseases
supported by these partnerships (such as trachoma control in Morocco and
lymphatic filariasis control in Egypt) bolster the case that integration be
prioritized in affected countries.

While there has been significant discussion about the integration of single-
disease partnerships [9]–[11] and the potential usefulness of such approaches
in helping to tackle the burden of NTDs, there is limited experience in
implementing integration and even less experience in conducting systematic
analysis of these experiences. Recently, a number of articles have discussed
potential challenges and opportunities, and have estimated potential benefits,
including cost savings [12]–[14].

The lack of a common understanding of integration for disease control programs
may be a significant impediment towards implementing integration, despite
significant interest in the topic. This article presents a conceptual
framework to help guide the discussion about integration of NTD control
partnerships. It then provides specific examples of potential opportunities
and actual cases of integration of NTDs, and places these examples within the
conceptual framework. The main purpose of this article is to provide a tool
for thinking about integration</field></doc>
