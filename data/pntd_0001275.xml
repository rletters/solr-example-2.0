<doc>
<field name="uid">doi:10.1371/journal.pntd.0001275</field>
<field name="doi">10.1371/journal.pntd.0001275</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Yoshimi Tsuda, Patrizia Caposio, Christopher J. Parkins, Sara Botto, Ilhem Messaoudi, Luka Cicin-Sain, Heinz Feldmann, Michael A. Jarvis</field>
<field name="title">A Replicating Cytomegalovirus-Based Vaccine Encoding a Single Ebola Virus Nucleoprotein CTL Epitope Confers Protection against Ebola Virus</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">8</field>
<field name="pages">e1275</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Human outbreaks of Ebola virus (EBOV) are a serious human health concern in
Central Africa. Great apes (gorillas/chimpanzees) are an important source of
EBOV transmission to humans due to increased hunting of wildlife including the
‘bush-meat’ trade. Cytomegalovirus (CMV) is an highly immunogenic virus that
has shown recent utility as a vaccine platform. CMV-based vaccines also have
the unique potential to re-infect and disseminate through target populations
regardless of prior CMV immunity, which may be ideal for achieving high
vaccine coverage in inaccessible populations such as great apes.

Methodology/Principal Findings

We hypothesize that a vaccine strategy using CMV-based vectors expressing EBOV
antigens may be ideally suited for use in inaccessible wildlife populations.
To establish a ‘proof-of-concept’ for CMV-based vaccines against EBOV, we
constructed a mouse CMV (MCMV) vector expressing a CD8+ T cell epitope from
the nucleoprotein (NP) of Zaire ebolavirus (ZEBOV) (MCMV/ZEBOV-NPCTL). MCMV
/ZEBOV-NPCTL induced high levels of long-lasting (&gt;8 months) CD8+ T cells
against ZEBOV NP in mice. Importantly, all vaccinated animals were protected
against lethal ZEBOV challenge. Low levels of anti-ZEBOV antibodies were only
sporadically detected in vaccinated animals prior to ZEBOV challenge
suggesting a role, at least in part, for T cells in protection.

Conclusions/Significance

This study demonstrates the ability of a CMV-based vaccine approach to protect
against an highly virulent human pathogen, and supports the potential for
‘disseminating’ CMV-based EBOV vaccines to prevent EBOV transmission in
wildlife populations.

Author Summary

Human outbreaks of hemorrhagic disease caused by Ebola virus (EBOV) are a
serious health concern in Central Africa. Great apes (gorillas/chimpanzees)
are an important source of EBOV transmission to humans. Candidate EBOV
vaccines do not spread from the initial vaccinee. In addition to being highly
immunogenic, vaccines based on the cytomegalovirus (CMV) platform have the
unique potential to re-infect and disseminate through target populations. To
explore the utility of CMV-based vaccines against EBOV, we constructed a mouse
CMV (MCMV) vector expressing a region of nucleoprotein (NP) of Zaire
ebolavirus (ZEBOV) (MCMV/ZEBOV-NPCTL). MCMV/ZEBOV-NPCTL induced high levels of
long-lasting CD8+ T cells against ZEBOV NP in mice. Importantly, all
vaccinated animals were protected against lethal ZEBOV challenge. The absence
of ZEBOV neutralizing and only low, sporadic levels of total anti-ZEBOV IgG
antibodies in protected animals prior to ZEBOV challenge indicate a role,
albeit perhaps not exclusive, for CD8+ T cells in mediating protection. This
study demonstrates the ability of a CMV-based vaccine approach to protect
against ZEBOV, and provides a ‘proof-of-concept’ for the potential for a
‘disseminating’ CMV-based EBOV vaccine to prevent EBOV transmission in wild
animal populations.

Introduction

Ebola virus (EBOV), a member of the Filoviridae family, causes rapidly
progressing viral hemorrhagic fever culminating in multi-organ failure, shock
and death [1]. EBOV can be subdivided into four distinc</field></doc>
