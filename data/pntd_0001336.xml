<doc>
<field name="uid">doi:10.1371/journal.pntd.0001336</field>
<field name="doi">10.1371/journal.pntd.0001336</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Johan Esterhuizen, Basilio Njiru, Glyn A. Vale, Michael J. Lehane, Stephen J. Torr</field>
<field name="title">Vegetation and the Importance of Insecticide-Treated Target Siting for Control of Glossina fuscipes fuscipes</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">9</field>
<field name="pages">e1336</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Control of tsetse flies using insecticide-treated targets is often hampered by
vegetation re-growth and encroachment which obscures a target and renders it
less effective. Potentially this is of particular concern for the newly
developed small targets (0.25 high × 0.5 m wide) which show promise for cost-
efficient control of Palpalis group tsetse flies. Consequently the performance
of a small target was investigated for Glossina fuscipes fuscipes in Kenya,
when the target was obscured following the placement of vegetation to simulate
various degrees of natural bush encroachment. Catches decreased significantly
only when the target was obscured by more than 80%. Even if a small target is
underneath a very low overhanging bush (0.5 m above ground), the numbers of G.
f. fuscipes decreased by only about 30% compared to a target in the open. We
show that the efficiency of the small targets, even in small (1 m diameter)
clearings, is largely uncompromised by vegetation re-growth because G. f.
fuscipes readily enter between and under vegetation. The essential
characteristic is that there should be some openings between vegetation.

This implies that for this important vector of HAT, and possibly other
Palpalis group flies, a smaller initial clearance zone around targets can be
made and longer interval between site maintenance visits is possible both of
which will result in cost savings for large scale operations. We also
investigated and discuss other site features e.g. large solid objects and
position in relation to the water&apos;s edge in terms of the efficacy of the small
targets.

Author Summary

Sleeping Sickness (Human African Trypanosomiasis) is a serious threat to
health and development in sub-Saharan Africa. Due to lack of vaccines and
prophylactic drugs, vector control is the only method of disease prevention.
Small (0.25×0.5 m) insecticide-treated targets have been shown to be cost-
efficient for several Palpalis group tsetse flies, but there are concerns that
they may become obscured by vegetation with a subsequent reduction in
efficiency. We showed that the efficiency of the small targets was largely
uncompromised by vegetation encroachment because G. f. fuscipes readily enter
between and under vegetation to locate a small target, e.g. into small (1 m
diameter) site clearings and underneath a very low (0.5 m) canopy. This
implies that the dense vegetation, typical of the riverine habitats of
Palpalis group tsetse, will not compromise the performance of tiny targets, as
long as there are adequate openings of &gt;30 cm between vegetation. Moreover,
the maintanence of cleared areas around targets seems less important for the
control of G. f. fuscipes with consequent savings in costs for control
operations.

Introduction

The major vectors of Human African Trypanosomiasis (HAT) are in the Palpalis
group tsetse flies, especially the G. fuscipes subspecies, which are
responsible for transmission of &gt;90% of reported HAT cases [1], [2]. In the
present situation with limited drug and no vaccine availability, vector
control remains an important addition to current efforts against HAT. Tsetse
control with insecticide-treated blue/black cloth panels (c. 1–2 m wide ×1 m
high), called targets [3], have been used successfully for several Mor</field></doc>
