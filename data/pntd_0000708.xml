<doc>
<field name="uid">doi:10.1371/journal.pntd.0000708</field>
<field name="doi">10.1371/journal.pntd.0000708</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Brian K. Chu, Pamela J. Hooper, Mark H. Bradley, Deborah A. McFarland, Eric A. Ottesen</field>
<field name="title">The Economic Benefits Resulting from the First 8 Years of the Global Programme to Eliminate Lymphatic Filariasis (2000–2007)</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">6</field>
<field name="pages">e708</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Between 2000–2007, the Global Programme to Eliminate Lymphatic Filariasis
(GPELF) delivered more than 1.9 billion treatments to nearly 600 million
individuals via annual mass drug administration (MDA) of anti-filarial drugs
(albendazole, ivermectin, diethylcarbamazine) to all at-risk for 4–6 years.
Quantifying the resulting economic benefits of this significant achievement is
important not only to justify the resources invested in the GPELF but also to
more fully understand the Programme&apos;s overall impact on some of the poorest
endemic populations.

Methodology

To calculate the economic benefits, the number of clinical manifestations
averted was first quantified and the savings associated with this disease
prevention then analyzed in the context of direct treatment costs, indirect
costs of lost-labor, and costs to the health system to care for affected
individuals. Multiple data sources were reviewed, including published
literature and databases from the World Health Organization, International
Monetary Fund, and International Labour Organization

Principal Findings

An estimated US$21.8 billion of direct economic benefits will be gained over
the lifetime of 31.4 million individuals treated during the first 8 years of
the GPELF. Of this total, over US$2.3 billion is realized by the protection of
nearly 3 million newborns and other individuals from acquiring lymphatic
filariasis as a result of their being born into areas freed of LF
transmission. Similarly, more than 28 million individuals already infected
with LF benefit from GPELF&apos;s halting the progression of their disease, which
results in an associated lifetime economic benefit of approximately US$19.5
billion. In addition to these economic benefits to at-risk individuals,
decreased patient services associated with reduced LF morbidity saves the
health systems of endemic countries approximately US$2.2 billion.

Conclusions/Significance

MDA for LF offers significant economic benefits. Moreover, with favorable
program implementation costs (largely a result of the sustained commitments of
donated drugs from the pharmaceutical industry) it is clear that the economic
rate of return of the GPELF is extremely high and that this Programme
continues to prove itself an excellent investment in global health.

Author Summary

Lymphatic filariasis (LF), commonly known as ‘elephantiasis’, is one of the
world&apos;s most debilitating infectious diseases. In 83 countries worldwide, more
than 1.3 billion people are at risk of infection with an estimated 120 million
individuals already infected. A recent publication reviewing the health impact
of the first 8 years of the Global Programme to Eliminate Lymphatic Filariasis
(GPELF) demonstrated the enormous health benefits achieved in populations
receiving annual mass drug administration (MDA), as a result of infection
prevented, disease progression halted, and ancillary treatment of co-
infections. To date, however, no studies have estimated the economic value of
these health benefits, either to the individuals or the societies afflicted
with LF. Our study estimates that US$21.8 billion will be gained among
individuals benefitting from just the first 8 years of the Global Programme,
and an additional US$2.2</field></doc>
