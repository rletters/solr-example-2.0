<doc>
<field name="uid">doi:10.1371/journal.pntd.0000756</field>
<field name="doi">10.1371/journal.pntd.0000756</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Kate Mounsey, Mei-Fong Ho, Andrew Kelly, Charlene Willis, Cielo Pasay, David J. Kemp, James S. McCarthy, Katja Fischer</field>
<field name="title">A Tractable Experimental Model for Study of Human and Animal Scabies</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">7</field>
<field name="pages">e756</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Scabies is a parasitic skin infestation caused by the burrowing mite Sarcoptes
scabiei. It is common worldwide and spreads rapidly under crowded conditions,
such as those found in socially disadvantaged communities of Indigenous
populations and in developing countries. Pruritic scabies lesions facilitate
opportunistic bacterial infections, particularly Group A streptococci.
Streptococcal infections cause significant sequelae and the increased
community streptococcal burden has led to extreme levels of acute rheumatic
fever and rheumatic heart disease in Australia&apos;s Indigenous communities. In
addition, emerging resistance to currently available therapeutics emphasizes
the need to identify potential targets for novel chemotherapeutic and/or
immunological intervention. Scabies research has been severely limited by the
availability of parasites, and scabies remains a truly neglected infectious
disease. We report development of a tractable model for scabies in the pig,
Sus domestica.

Methodology/Principal Findings

Over five years and involving ten independent cohorts, we have developed a
protocol for continuous passage of Sarcoptes scabiei var. suis. To increase
intensity and duration of infestation without generating animal welfare issues
we have optimised an immunosuppression regimen utilising daily oral treatment
with 0.2mg/kg dexamethasone. Only mild, controlled side effects are observed,
and mange infection can be maintained indefinitely providing large mite
numbers (&gt;6000 mites/g skin) for molecular-based research on scabies. In pilot
experiments we explore whether any adaptation of the mite population is
reflected in genetic changes. Phylogenetic analysis was performed comparing
sets of genetic data obtained from pig mites collected from naturally infected
pigs with data from pig mites collected from the most recent cohort.

Conclusions/Significance

A reliable pig/scabies animal model will facilitate in vivo studies on host
immune responses to scabies including the relations to the associated
bacterial pathogenesis and more detailed studies of molecular evolution and
host adaption. It is a most needed tool for the further investigation of this
important and widespread parasitic disease.

Author Summary

Scabies, a neglected parasitic disease caused by the microscopic mite
Sarcoptes scabiei, is a major driving force behind bacterial skin infections
in tropical settings. Aboriginal and Torres Strait Islander peoples are nearly
twenty times more likely to die from acute rheumatic fever and rheumatic heart
disease than individuals from the wider Australian community. These conditions
are caused by bacterial pathogens such as Group A streptococci, which have
been linked to underlying scabies infestations. Community based initiatives to
reduce scabies and associated disease have expanded, but have been threatened
in recent years by emerging drug resistance. Critical biological questions
surrounding scabies remain unanswered due to a lack of biomedical research.
This has been due in part to a lack of either a suitable animal model or an in
vitro culture system for scabies mites. The pig/mite model reported here will
be a much needed resource for parasite material and will facilitate in vivo
studies on host immune responses to sc</field></doc>
