<doc>
<field name="uid">doi:10.1371/journal.pntd.0001283</field>
<field name="doi">10.1371/journal.pntd.0001283</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Oscar Franzén, Erik Arner, Marcela Ferella, Daniel Nilsson, Patricia Respuela, Piero Carninci, Yoshihide Hayashizaki, Lena Åslund, Björn Andersson, Carsten O. Daub</field>
<field name="title">The Short Non-Coding Transcriptome of the Protozoan Parasite Trypanosoma cruzi</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">8</field>
<field name="pages">e1283</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

The pathway for RNA interference is widespread in metazoans and participates
in numerous cellular tasks, from gene silencing to chromatin remodeling and
protection against retrotransposition. The unicellular eukaryote Trypanosoma
cruzi is missing the canonical RNAi pathway and is unable to induce RNAi-
related processes. To further understand alternative RNA pathways operating in
this organism, we have performed deep sequencing and genome-wide analyses of a
size-fractioned cDNA library (16–61 nt) from the epimastigote life stage. Deep
sequencing generated 582,243 short sequences of which 91% could be aligned
with the genome sequence. About 95–98% of the aligned data (depending on the
haplotype) corresponded to small RNAs derived from tRNAs, rRNAs, snRNAs and
snoRNAs. The largest class consisted of tRNA-derived small RNAs which
primarily originated from the 3′ end of tRNAs, followed by small RNAs derived
from rRNA. The remaining sequences revealed the presence of 92 novel
transcribed loci, of which 79 did not show homology to known RNA classes.

Author Summary

Chagas&apos; disease is a major health problem in Latin America and is caused by
the protozoan parasite Trypanosoma cruzi. T. cruzi lacks the pathway for RNA
interference, which is widespread among eukaryotes, and is therefore unable to
induce RNAi-related processes. In many organisms, small RNAs play an important
role in regulating gene expression and other cellular processes. In order to
understand if other small RNA pathways are operating in this organism, we
performed high throughput sequencing and genome-wide analyses of the short
transcriptome. We identified an abundance of small RNAs derived from non-
coding RNA genes, including transfer RNAs, ribosomal RNAs as well as small
nucleolar RNAs and small nuclear RNAs. Certain tRNA types were overrepresented
as precursors for small RNAs. Further, we identified 79 novel small non-coding
RNAs, not previously reported. We did not identify canonical small RNAs, like
microRNAs and small interfering RNAs, and concluded that these do not exist in
T. cruzi. This study has provided insights into the short transcriptome of a
major human pathogen and provided starting points for further functional
investigation of small RNAs and their biological roles.

Introduction

Trypanosoma cruzi is a protozoan parasite and the causative agent of Chagas&apos;
disease, which has substantial health and socioeconomic impact in Latin
America [1]. Treatment is currently restricted to a small number of drugs with
insufficient efficacy and potentially harmful side effects.

The genome of T. cruzi strain CL Brener is complex in terms of sequence
repetitiveness and is a hybrid between two diverged haplotypes, named non-
Esmeraldo-like and Esmeraldo-like: we refer to them here as non-Esmeraldo and
Esmeraldo. Taken together, both haplotypes [2] sum up to approximately 110 Mb
distributed over at least 80 chromosomes [3]. Similar to other
trypanosomatids, genes are organized into co-directional clusters that undergo
polycistronic transcription. Gene rich regions are frequently interrupted by
sequence repeats, which comprise at least 50% of the genome [2]. Several gene
variants occur in tandemly repeated copies </field></doc>
