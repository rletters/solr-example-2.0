<doc>
<field name="uid">doi:10.1371/journal.pntd.0001025</field>
<field name="doi">10.1371/journal.pntd.0001025</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Patient Pati Pyana, Ipos Ngay Lukusa, Dieudonné Mumba Ngoyi, Nick Van Reet, Marcel Kaiser, Stomy Karhemere Bin Shamamba, Philippe Büscher</field>
<field name="title">Isolation of Trypanosoma brucei gambiense from Cured and Relapsed Sleeping Sickness Patients and Adaptation to Laboratory Mice</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">4</field>
<field name="pages">e1025</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Sleeping sickness due to Trypanosoma brucei (T.b.) gambiense is still a major
public health problem in some central African countries. Historically, relapse
rates around 5% have been observed for treatment with melarsoprol, widely used
to treat second stage patients. Later, relapse rates of up to 50% have been
recorded in some isolated foci in Angola, Sudan, Uganda and Democratic
Republic of the Congo (DRC). Previous investigations are not conclusive on
whether decreased sensitivity to melarsoprol is responsible for these high
relapse rates. Therefore we aimed to establish a parasite collection isolated
from cured as well as from relapsed patients for downstream comparative drug
sensitivity profiling. A major constraint for this type of investigation is
that T.b. gambiense is particularly difficult to isolate and adapt to
classical laboratory rodents.

Methodology/Principal Findings

From 360 patients treated in Dipumba hospital, Mbuji-Mayi, D.R. Congo, blood
and cerebrospinal fluid (CSF) was collected before treatment. From patients
relapsing during the 24 months follow-up, the same specimens were collected.
Specimens with confirmed parasite presence were frozen in liquid nitrogen in a
mixture of Triladyl, egg yolk and phosphate buffered glucose solution.
Isolation was achieved by inoculation of the cryopreserved specimens in
Grammomys surdaster, Mastomys natalensis and SCID mice. Thus, 85 strains were
isolated from blood and CSF of 55 patients. Isolation success was highest in
Grammomys surdaster. Forty strains were adapted to mice. From 12 patients,
matched strains were isolated before treatment and after relapse. All strains
belong to T.b. gambiense type I.

Conclusions and Significance

We established a unique collection of T.b. gambiense from cured and relapsed
patients, isolated in the same disease focus and within a limited period. This
collection is available for genotypic and phenotypic characterisation to
investigate the mechanism behind abnormally high treatment failure rates in
Mbuji-Mayi, D.R. Congo.

Author Summary

Human African trypanosomiasis, or sleeping sickness, is still a major public
health problem in central Africa. Melarsoprol is widely used for treatment of
patients where the parasite has already reached the brain. In some regions in
Angola, Sudan, Uganda and Democratic Republic of the Congo, up to half of the
patients cannot be cured with melarsoprol. From previous investigations it is
not yet clear what causes these high relapse rates. Therefore we aimed to
establish a parasite collection isolated from cured as well as relapsed
patients for downstream comparative drug sensitivity profiling. From 360
sleeping sickness patients, blood and cerebrospinal fluid (CSF) was collected
before treatment and along the prescribed 24 months follow-up. Blood and CSF
were inoculated in thicket rats (Grammomys surdaster), Natal multimammate mice
(Mastomys natalensis) and immunodeficient laboratory mice (Mus musculus).
Thus, we established a unique collection of Trypanosoma brucei gambiense type
I parasites, isolated in the same disease focus and within a limited period,
including 12 matched strains isolated from the same patient before treatment</field></doc>
