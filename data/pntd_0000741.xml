<doc>
<field name="uid">doi:10.1371/journal.pntd.0000741</field>
<field name="doi">10.1371/journal.pntd.0000741</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">David G. Addiss</field>
<field name="title">Global Elimination of Lymphatic Filariasis: Addressing the Public Health Problem</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">6</field>
<field name="pages">e741</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Background

On July 15, 1997, two months after the World Health Assembly (WHA) passed a
resolution calling for the “global elimination of lymphatic filariasis (LF) as
a public health problem,” a small group of public health leaders and
scientists gathered at Magnetic Island, near Townsville, Australia. They were
meeting to consider the elements of a program to achieve such a lofty goal.
The evening before the meeting began I asked several of those present, all
veterans of global efforts to eradicate smallpox, polio, or Guinea worm
disease, whether the LF elimination program should concern itself with
providing care to those who already suffer the clinical manifestations of LF.
Their response was uniformly negative: LF elimination should focus solely on
interrupting transmission of the parasite. As with other disease eradication
efforts, the intended beneficiaries were future generations; saddling the LF
elimination program with responsibilities for clinical care could dilute
focus, divert resources, and complicate strategies and partnerships.

Two days later, the group unanimously endorsed a “two-pillar” strategy that
included both interrupting transmission and providing care for those with
disease [1]. Several arguments had shifted the group&apos;s position. First, there
was the ethical issue: how could one ignore the suffering of 15 million people
with lymphedema and 25 million men with urogenital disease, principally
hydrocele? Hydrocele is readily treated with surgery, and evidence was
beginning to accumulate that simple measures, including hygiene and skin care,
could help arrest the progression of lymphedema [2], [3]. Second, the “public
health problem” to which the WHA resolution referred was clinical disease; by
itself, the presence of microfilaria in the blood does not constitute a public
health problem. In affected communities, clearing the blood of microfilaria
through annual mass drug administration (MDA) can interrupt transmission of
the parasite. However, the scientific evidence remained divided on what
effect, if any, these drugs have on established disease [4]. Finally, and
perhaps most importantly, it was thought that providing care for those with
filariasis-associated morbidity could increase community acceptance of MDA.

The World Health Organization (WHO) launched the Global Programme to Eliminate
Lymphatic Filariasis (GPELF) in 2000. Ten years on, progress in scaling up MDA
has been phenomenal; 496 million persons received antifilarial drugs in 2008
[5], yielding impressive global health benefits [6]. In contrast, despite
excellent pilot programs (e.g., [7]–[9]) and some at the state and national
levels [10], morbidity management has generally languished.

What are the reasons for this imbalance? For one, the concerns expressed at
Magnetic Island had merit. The single focus of other disease elimination
programs enabled them to be streamlined and efficient. The dual goals of
interrupting transmission and managing morbidity may require different
approaches, skills, and timeframes. Given limited resources and the ambitious
goal of interrupting transmission by 2020, MDA has taken priority. Despite the
experience of those who advocated a “two-pillar” strategy, there has been no
scientific evidence that lymphedema management actually improves acceptance of
MDA. Thus, it has been difficult to dispel the notion tha</field></doc>
