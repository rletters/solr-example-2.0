<doc>
<field name="uid">doi:10.1371/journal.pntd.0001572</field>
<field name="doi">10.1371/journal.pntd.0001572</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Zablon Kithinji Njiru</field>
<field name="title">Loop-Mediated Isothermal Amplification Technology: Towards Point of Care Diagnostics</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">6</field>
<field name="pages">e1572</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Introduction

The invention of the loop-mediated isothermal amplification (LAMP) method a
decade ago has given new impetus towards development of point of care
diagnostic tests based on amplification of pathogen DNA, a technology that has
been the precinct of well-developed laboratories. The LAMP technology
amplifies DNA with high sensitivity relying on an enzyme with strand
displacement activity under isothermal conditions. Additionally, the
technology uses four to six specially designed primers recognising six to
eight regions of the target DNA sequence, hence a high specificity. The auto-
cycling reactions lead to accumulation of a large amount of the target DNA and
other reaction by-products, such as magnesium pyrophosphate, that allow rapid
detection using varied formats [1], [2]. Over the last 10 years, LAMP has been
used widely in the laboratory setting to detect pathogens of medical and
veterinary importance, plant parasitic diseases, genetically modified
products, and tumour and embryo sex identification, among other uses [3].
However, its application under field conditions has been limited, partly due
to the infancy of the technologies associated with LAMP, such as field-based
template preparation methods and product detection formats. In this Viewpoint,
the author highlights the essential technologies that require development
before the LAMP platform can be progressed into a realistic point of care
format for resource-poor endemic areas.

ASSURED Tests

Lack of effective point of care diagnostic tests applicable in resource-poor
endemic areas is a critical barrier to effective treatment and control of
infectious diseases. Indeed, this paucity is acutely demonstrated in neglected
tropical diseases (NTDs), where access to reliable diagnostic testing is
severely limited and misdiagnosis commonly occurs. Although the reasons for
the failure to prevent and control NTDs in the developing world are complex, a
major barrier to effective health care is the lack of access to reliable
diagnostic laboratory services [4]. The World Health Organization (WHO)
recommends that an ideal diagnostic test suitable for developing countries
should be Affordable, Sensitive, Specific, User-friendly (simple to perform in
a few steps with minimal training), Robust and rapid (results available in 30
min), Equipment free, and Deliverable to the end user (ASSURED) [5]. So far,
only a few rapid diagnostic test (RDT) formats fit this model, albeit with
limited sensitivity and specificity [6]. Nucleic acid (DNA) amplification
tests targeting pathogen markers have high sensitivity and specificity but
generally fail to meet the ASSURED guidelines in terms of affordability,
rapidity, and being equipment free [7]. However, with the recent invention and
advancement of isothermal technologies [8], development of ASSURED tests based
on DNA amplification seems realistic. One such potential method is the LAMP
technology, which has salient advantages over most DNA-based amplification
tests (Box 1). These characteristics make LAMP strategy a potential ASSURED
platform. However, for LAMP technology to demonstrate the performance goals
implied in the ASSURED guidelines, four technologies that are applicable with
the LAMP test need to be developed. These include template preparation
protocols, a lysophilised kit, a reliable power source, and product detection
technologi</field></doc>
