<doc>
<field name="uid">doi:10.1371/journal.pntd.0000620</field>
<field name="doi">10.1371/journal.pntd.0000620</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Fernando Abad-Franch, Gonçalo Ferraz, Ciro Campos, Francisco S. Palomeque, Mario J. Grijalva, H. Marcelo Aguilar, Michael A. Miles</field>
<field name="title">Modeling Disease Vector Occurrence when Detection Is Imperfect: Infestation of Amazonian Palm Trees by Triatomine Bugs at Three Spatial Scales</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">3</field>
<field name="pages">e620</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Failure to detect a disease agent or vector where it actually occurs
constitutes a serious drawback in epidemiology. In the pervasive situation
where no sampling technique is perfect, the explicit analytical treatment of
detection failure becomes a key step in the estimation of epidemiological
parameters. We illustrate this approach with a study of Attalea palm tree
infestation by Rhodnius spp. (Triatominae), the most important vectors of
Chagas disease (CD) in northern South America.

Methodology/Principal Findings

The probability of detecting triatomines in infested palms is estimated by
repeatedly sampling each palm. This knowledge is used to derive an unbiased
estimate of the biologically relevant probability of palm infestation. We
combine maximum-likelihood analysis and information-theoretic model selection
to test the relationships between environmental covariates and infestation of
298 Amazonian palm trees over three spatial scales: region within Amazonia,
landscape, and individual palm. Palm infestation estimates are high (40–60%)
across regions, and well above the observed infestation rate (24%). Detection
probability is higher (∼0.55 on average) in the richest-soil region than
elsewhere (∼0.08). Infestation estimates are similar in forest and rural
areas, but lower in urban landscapes. Finally, individual palm covariates
(accumulated organic matter and stem height) explain most of infestation rate
variation.

Conclusions/Significance

Individual palm attributes appear as key drivers of infestation, suggesting
that CD surveillance must incorporate local-scale knowledge and that
peridomestic palm tree management might help lower transmission risk. Vector
populations are probably denser in rich-soil sub-regions, where CD prevalence
tends to be higher; this suggests a target for research on broad-scale risk
mapping. Landscape-scale effects indicate that palm triatomine populations can
endure deforestation in rural areas, but become rarer in heavily disturbed
urban settings. Our methodological approach has wide application in infectious
disease research; by improving eco-epidemiological parameter estimation, it
can also significantly strengthen vector surveillance-control strategies.

Author Summary

Blood-sucking bugs of the genus Rhodnius are major vectors of Chagas disease.
Control and surveillance of Chagas disease transmission critically depend on
ascertaining whether households and nearby ecotopes (such as palm trees) are
infested by these vectors. However, no bug detection technique works
perfectly. Because more sensitive methods are more costly, vector searches
face a trade-off between technical prowess and sample size. We compromise by
using relatively inexpensive sampling techniques that can be applied multiple
times to a large number of palms. With these replicated results, we estimate
the probability of failing to detect bugs in a palm that is actually infested.
We incorporate this information into our analyses to derive an unbiased
estimate of palm infestation, and find it to be about 50% – twice the observed
proportion of infested palms. We are then able to model the effects of
regional, landscape, and local environmenta</field></doc>
