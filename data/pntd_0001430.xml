<doc>
<field name="uid">doi:10.1371/journal.pntd.0001430</field>
<field name="doi">10.1371/journal.pntd.0001430</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Vinicio T. S. Coelho, Jamil S. Oliveira, Diogo G. Valadares, Miguel A. Chávez-Fumagalli, Mariana C. Duarte, Paula S. Lage, Manuel Soto, Marcelo M. Santoro, Carlos A. P. Tavares, Ana Paula Fernandes, Eduardo A. F. Coelho</field>
<field name="title">Identification of Proteins in Promastigote and Amastigote-like Leishmania Using an Immunoproteomic Approach</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">1</field>
<field name="pages">e1430</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The present study aims to identify antigens in protein extracts of
promastigote and amastigote-like Leishmania (Leishmania) chagasi syn. L. (L.)
infantum recognized by antibodies present in the sera of dogs with
asymptomatic and symptomatic visceral leishmaniasis (VL).

Methodology/Principal Findings

Proteins recognized by sera samples were separated by two-dimensional
electrophoresis (2DE) and identified by mass spectrometry. A total of 550
spots were observed in the 2DE gels, and approximately 104 proteins were
identified. Several stage-specific proteins could be identified by either or
both classes of sera, including, as expected, previously known proteins
identified as diagnosis, virulence factors, drug targets, or vaccine
candidates. Three, seven, and five hypothetical proteins could be identified
in promastigote antigenic extracts; while two, eleven, and three hypothetical
proteins could be identified in amastigote-like antigenic extracts by
asymptomatic and symptomatic sera, as well as a combination of both,
respectively.

Conclusions/Significance

The present study represents a significant contribution not only in
identifying stage-specific L. infantum molecules, but also in revealing the
expression of a large number of hypothetical proteins. Moreover, when
combined, the identified proteins constitute a significant source of
information for the improvement of diagnostic tools and/or vaccine development
to VL.

Author Summary

Canine visceral leishmaniasis (CVL) is an important emerging zoonosis caused
by Leishmania (Leishmania) infantum in the Mediterranean and Middle East and
L. (L.) chagasi (syn. L. (L.) infantum) in Latin America. Due to their
genotypic relationships, these species are considered identical. The present
study focused on comparing the protein expression profiles of the promastigote
and amastigote-like stages of L. infantum, by means of a protein separation by
two-dimensional electrophoresis and identification by mass spectrometry. The
present study attempted to identify proteins recognized by antibodies present
in the sera of dogs with asymptomatic and symptomatic visceral leishmaniasis.
A total of one hundred and four proteins were identified. Of these, several
stage-specific proteins had been previously identified as diagnosis and/or
vaccine candidates. In addition, antibodies from infected dogs recognized
thirty-one proteins, which had been previously considered hypothetical,
indicating that these proteins are expressed during active infection.
Therefore, the present study reveals new potential candidates for the
improvement of diagnosis of CVL.

Introduction

Visceral leishmaniasis (VL) is an important parasitic disease, with a
worldwide distribution in 88 countries, where a total of 350 million people
may be at risk. In Brazil, the disease is an endemic zoonosis caused by the
parasitic protozoa Leishmania (Leishmania) chagasi syn. L. (L.) infantum [1].
Dogs are the main parasite domestic reservoirs, and culling of seropositive
dogs, as detected by means of serological tests using promastigote antigens,
i.e. RIFI or ELISA, is a major VL control measure adopted in B</field></doc>
