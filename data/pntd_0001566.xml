<doc>
<field name="uid">doi:10.1371/journal.pntd.0001566</field>
<field name="doi">10.1371/journal.pntd.0001566</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Daniel Menezes-Souza, Renata Guerra-Sá, Cláudia Martins Carneiro, Juliana Vitoriano-Souza, Rodolfo Cordeiro Giunchetti, Andréa Teixeira-Carvalho, Denise Silveira-Lemos, Guilherme Corrêa Oliveira, Rodrigo Corrêa-Oliveira, Alexandre Barbosa Reis</field>
<field name="title">Higher Expression of CCL2, CCL4, CCL5, CCL21, and CXCL8 Chemokines in the Skin Associated with Parasite Density in Canine Visceral Leishmaniasis</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">4</field>
<field name="pages">e1566</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The immune response in the skin of dogs infected with Leishmania infantum is
poorly understood, and limited studies have described the immunopathological
profile with regard to distinct levels of tissue parasitism and the clinical
progression of canine visceral leishmaniasis (CVL).

Methodology/Principal Findings

A detailed analysis of inflammatory cells (neutrophils, eosinophils, mast
cells, lymphocytes, and macrophages) as well as the expression of chemokines
(CCL2, CCL4, CCL5, CCL13, CCL17, CCL21, CCL24, and CXCL8) was carried out in
dermis skin samples from 35 dogs that were naturally infected with L.
infantum. The analysis was based on real-time polymerase chain reaction (PCR)
in the context of skin parasitism and the clinical status of CVL. We
demonstrated increased inflammatory infiltrate composed mainly of mononuclear
cells in the skin of animals with severe forms of CVL and high parasite
density. Analysis of the inflammatory cell profile of the skin revealed an
increase in the number of macrophages and reductions in lymphocytes,
eosinophils, and mast cells that correlated with clinical progression of the
disease. Additionally, enhanced parasite density was correlated with an
increase in macrophages and decreases in eosinophils and mast cells. The
chemokine mRNA expression demonstrated that enhanced parasite density was
positively correlated with the expression of CCL2, CCL4, CCL5, CCL21, and
CXCL8. In contrast, there was a negative correlation between parasite density
and CCL24 expression.

Conclusions/Significance

These findings represent an advance in the knowledge about skin inflammatory
infiltrates in CVL and the systemic consequences. Additionally, the findings
may contribute to the design of new and more efficient prophylactic tools and
immunological therapies against CVL.

Author Summary

Several previous studies correlated immunopathological aspects of canine
visceral leishmaniasis (CVL) with tissue parasite load and/or the clinical
status of the disease. Recently, different aspects of the immune response in
Leishmania-infected dogs have been studied, particularly the profile of
cytokines in distinct compartments. However, the role of chemokines in disease
progression or parasite burdens of the visceralising species represents an
important approach for understanding immunopathology in CVL. We found an
increase in inflammatory infiltrate, which was mainly composed of mononuclear
cells, in the skin of animals presenting severe forms of CVL and high parasite
density. Our data also demonstrated that enhanced parasite density is
positively correlated with the expression of CCL2, CCL4, CCL5, CCL21, and
CXCL8. In contrast, there was a negative correlation between parasite density
and CCL24 expression. These findings represent an advance in the knowledge of
the involvement of skin inflammatory infiltrates in CVL and the systemic
consequences and may contribute to developing a rational strategy for the
design of new and more efficient prophylactic tools and immunological
therapies against CVL.

Introduction

Visceral leis</field></doc>
