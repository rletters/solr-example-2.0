<doc>
<field name="uid">doi:10.1371/journal.pntd.0000495</field>
<field name="doi">10.1371/journal.pntd.0000495</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Suman K. Vodnala, Marcela Ferella, Hilda Lundén-Miguel, Evans Betha, Nick van Reet, Daniel Ndem Amin, Bo Öberg, Björn Andersson, Krister Kristensson, Hans Wigzell, Martin E. Rottenberg</field>
<field name="title">Preclinical Assessment of the Treatment of Second-Stage African Trypanosomiasis with Cordycepin and Deoxycoformycin</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">8</field>
<field name="pages">e495</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

There is an urgent need to substitute the highly toxic compounds still in use
for treatment of the encephalitic stage of human African trypanosomiasis
(HAT). We here assessed the treatment with the doublet cordycepin and the
deaminase inhibitor deoxycoformycin for this stage of infection with
Trypanosoma brucei (T.b.).

Methodology/Principal Findings

Cordycepin was selected as the most efficient drug from a direct parasite
viability screening of a compound library of nucleoside analogues. The minimal
number of doses and concentrations of the drugs effective for treatment of
T.b. brucei infections in mice were determined. Oral, intraperitoneal or
subcutaneous administrations of the compounds were successful for treatment.
The doublet was effective for treatment of late stage experimental infections
with human pathogenic T.b. rhodesiense and T.b. gambiense isolates. Late stage
infection treatment diminished the levels of inflammatory cytokines in brains
of infected mice. Incubation with cordycepin resulted in programmed cell death
followed by secondary necrosis of the parasites. T.b. brucei strains developed
resistance to cordycepin after culture with increasing concentrations of the
compound. However, cordycepin-resistant parasites showed diminished virulence
and were not cross-resistant to other drugs used for treatment of HAT, i.e.
pentamidine, suramin and melarsoprol. Although resistant parasites were
mutated in the gene coding for P2 nucleoside adenosine transporter, P2
knockout trypanosomes showed no altered resistance to cordycepin, indicating
that absence of the P2 transporter is not sufficient to render the
trypanosomes resistant to the drug.

Conclusions/Significance

Altogether, our data strongly support testing of treatment with a combination
of cordycepin and deoxycoformycin as an alternative for treatment of second-
stage and/or melarsoprol-resistant HAT.

Author Summary

There is an urgent need to substitute the highly toxic arsenic compounds still
in use for treatment of the encephalitic stage of African trypanosomiasis, a
disease caused by infection with Trypanosoma brucei. We exploited the
inability of trypanosomes to engage in de novo purine synthesis as a
therapeutic target. Cordycepin was selected from a trypanocidal screen of a
2200-compound library. When administered together with the adenosine deaminase
inhibitor deoxycoformycin, cordycepin cured mice inoculated with the human
pathogenic subspecies T. brucei rhodesiense or T. brucei gambiense even after
parasites had penetrated into the brain. Successful treatment was achieved by
intraperitoneal, oral or subcutaneous administration of the compounds.
Treatment with the doublet also diminished infection-induced cerebral
inflammation. Cordycepin induced programmed cell death of the parasites.
Although parasites grown in vitro with low doses of cordycepin gradually
developed resistance, the resistant parasites lost virulence and showed no
cross-resistance to trypanocidal drugs in clinical use. Our data strongly
support testing cordycepin and deoxycoformycin as an alternative for treatment
of second-stage and/or melarsoprol-resistant HAT.

Introduction
</field></doc>
