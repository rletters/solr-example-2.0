<doc>
<field name="uid">doi:10.1371/journal.pntd.0001410</field>
<field name="doi">10.1371/journal.pntd.0001410</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Mohammed Abdelfatah Alhoot, Seok Mui Wang, Shamala Devi Sekaran</field>
<field name="title">Inhibition of Dengue Virus Entry and Multiplication into Monocytes Using RNA Interference</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">11</field>
<field name="pages">e1410</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Dengue infection ranks as one of the most significant viral diseases of the
globe. Currently, there is no specific vaccine or antiviral therapy for
prevention or treatment. Monocytes/macrophages are the principal target cells
for dengue virus and are responsible for disseminating the virus after its
transmission. Dengue virus enters target cells via receptor-mediated
endocytosis after the viral envelope protein E attaches to the cell surface
receptor. This study aimed to investigate the effect of silencing the CD-14
associated molecule and clathrin-mediated endocytosis using siRNA on dengue
virus entry into monocytes.

Methodology/Principal Findings

Gene expression analysis showed a significant down-regulation of the target
genes (82.7%, 84.9 and 76.3% for CD-14 associated molecule, CLTC and DNM2
respectively) in transfected monocytes. The effect of silencing of target
genes on dengue virus entry into monocytes was investigated by infecting
silenced and non-silenced monocytes with DENV-2. Results showed a significant
reduction of infected cells (85.2%), intracellular viral RNA load (73.0%), and
extracellular viral RNA load (63.0%) in silenced monocytes as compared to non-
silenced monocytes.

Conclusions/Significance

Silencing the cell surface receptor and clathrin mediated endocytosis using
RNA interference resulted in inhibition of the dengue virus entry and
subsequently multiplication of the virus in the monocytes. This might serve as
a novel promising therapeutic target to attenuate dengue infection and thus
reduce transmission as well as progression to severe dengue hemorrhagic fever.

Author Summary

Prevention and treatment of dengue infection remain a serious global public
health priority. Extensive efforts are required toward the development of
vaccines and discovery of potential therapeutic compounds against the dengue
viruses. Dengue virus entry is a critical step for virus reproduction and
establishes the infection. Hence, the blockade of dengue virus entry into the
host cell is an interesting antiviral strategy as it represents a barrier to
suppress the onset of infection. This study was achieved by using RNA
interference to silence the cellular receptor, and the clathrin mediated
endocytosis that enhances the entry of dengue virus in monocytes. Results
showed a marked reduction of infected monocytes by flow cytometry. In
addition, both intracellular and extracellular viral RNA load was shown to be
reduced in treated monocytes when compared to untreated monocytes. Based on
these findings, this study concludes that this therapeutic strategy of
blocking the virus replication at the first stage of multiplication might
serve as a hopeful drug to mitigate the dengue symptoms, and reduction the
disease severity.

Introduction

Dengue infection ranks as one of the most clinically significant and prevalent
mosquito-borne viral diseases of the globe. It is an expanding public health
problem particularly in the tropical and subtropical areas [1]. Following an
incubation period of 3 to 14 days, fever and a variety of symptoms occur,
coinciding with the appearance of dengue virus (DENV) in blood [2].
Immunopathological studies suggest that many tissues may be involved during
dengue infection, as viral antigens are expressed in liver, lymph node, spleen
and bone m</field></doc>
