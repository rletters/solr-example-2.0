<doc>
<field name="uid">doi:10.1371/journal.pntd.0000277</field>
<field name="doi">10.1371/journal.pntd.0000277</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Ruth E. Gilbert, Katherine Freeman, Eleonor G. Lago, Lilian M. G. Bahia-Oliveira, Hooi Kuan Tan, Martine Wallon, Wilma Buffolano, Miles R. Stanford, Eskild Petersen, for The European Multicentre Study on Congenital Toxoplasmosis (EMSCOT)</field>
<field name="title">Ocular Sequelae of Congenital Toxoplasmosis in Brazil Compared with Europe</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">8</field>
<field name="pages">e277</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Toxoplasmic retinochoroiditis appears to be more severe in Brazil, where it is
a leading cause of blindness, than in Europe, but direct comparisons are
lacking. Evidence is accumulating that more virulent genotypes of Toxoplasma
gondii predominate in South America.

Methods

We compared prospective cohorts of children with congenital toxoplasmosis
identified by universal neonatal screening in Brazil and neonatal or prenatal
screening in Europe between 1992 and 2003, using the same protocol in both
continents.

Results

Three hundred and eleven (311) children had congenital toxoplasmosis: 30 in
Brazil and 281 in Europe, where 71 were identified by neonatal screening.
Median follow up was 4.1 years in Europe and 3.7 years in Brazil. Relatively
more children had retinochoroiditis during the first year in Brazil than in
Europe (15/30; 50% versus 29/281; 10%) and the risk of lesions by 4 years of
age was much higher: the hazard ratio for Brazil versus Europe was 5.36
(95%CI: 3.17, 9.08). Children in Brazil had larger lesions, which were more
likely to be multiple and to affect the posterior pole (p&lt;0.0001). In Brazil,
visual impairment (&lt;6/12 Snellen) was predicted for most affected eyes (87%,
27/31), but not in Europe (29%; 20/69, p&lt;0.0001). The size of newly detected
lesions decreased with age (p = 0.0007).

Conclusions

T. gondii causes more severe ocular disease in congenitally infected children
in Brazil compared with Europe. The marked differences in the frequency, size
and multiplicity of retinochoroidal lesions may be due to infection with more
virulent genotypes of the parasite that predominate in Brazil but are rarely
found in Europe.

Author Summary

Toxoplasma gondii is found throughout the world and is the most common
parasitic infection in humans. Infection can cause inflammatory lesions at the
back of the eye, which sometimes affect vision. These complications appear to
be more common and more severe when people acquire infection in Brazil than in
Europe or North America, but there have been no direct comparisons of patients
identified and followed up in the same way. In this report, we compare
children with congenital toxoplasmosis diagnosed at birth by universal
screening in Europe and Brazil and followed up until the age of 4. We found
that Brazilian children had a 5 times higher risk than European children of
developing eye lesions and their lesions were larger, more numerous and more
likely to affect the part of the area of the retina responsible for central
vision. Two-thirds of Brazilian children infected with congenital
toxoplasmosis had eye lesions by 4 years of age compared with 1 in 6 in
Europe. These stark differences are likely to be due to the predominance of
more virulent genotypes of the parasite in Brazil, which are rarely found in
Europe.

Introduction

Toxoplasmic retinochoroiditis appears to be more common in Brazil than in
Europe or North America and more severe. It is a leading cause of blindness in
Brazil[1] but not in Europe or North America.[2],[3] These differences are not
adequately explained by high rates of postnatal or congenital infection in
Brazil, as simila</field></doc>
