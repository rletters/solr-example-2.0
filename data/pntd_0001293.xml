<doc>
<field name="uid">doi:10.1371/journal.pntd.0001293</field>
<field name="doi">10.1371/journal.pntd.0001293</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Gustavo Mourglia-Ettlin, Juan Martín Marqués, José Alejandro Chabalgoity, Sylvia Dematteis</field>
<field name="title">Early Peritoneal Immune Response during Echinococcus granulosus Establishment Displays a Biphasic Behavior</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">8</field>
<field name="pages">e1293</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Cystic echinococcosis is a worldwide distributed helminth zoonosis caused by
the larval stage of Echinococcus granulosus. Human secondary cystic
echinococcosis is caused by dissemination of protoscoleces after accidental
rupture of fertile cysts and is due to protoscoleces ability to develop into
new metacestodes. In the experimental model of secondary cystic echinococcosis
mice react against protoscoleces producing inefficient immune responses,
allowing parasites to develop into cysts. Although the chronic phase of
infection has been analyzed in depth, early immune responses at the site of
infection establishment, e.g., peritoneal cavity, have not been well studied.
Because during early stages of infection parasites are thought to be more
susceptible to immune attack, this work focused on the study of cellular and
molecular events triggered early in the peritoneal cavity of infected mice.

Principal Findings

Data obtained showed disparate behaviors among subpopulations within the
peritoneal lymphoid compartment. Regarding B cells, there is an active
molecular process of plasma cell differentiation accompanied by significant
local production of specific IgM and IgG2b antibodies. In addition, peritoneal
NK cells showed a rapid increase with a significant percentage of activated
cells. Peritoneal T cells showed a substantial increase, with predominance in
CD4+ T lymphocytes. There was also a local increase in Treg cells. Finally,
cytokine response showed local biphasic kinetics: an early predominant
induction of Th1-type cytokines (IFN-γ, IL-2 and IL-15), followed by a shift
toward a Th2-type profile (IL-4, IL-5, IL-6, IL-10 and IL-13).

Conclusions

Results reported here open new ways to investigate the involvement of immune
effectors players in E. granulosus establishment, and also in the sequential
promotion of Th1- toward Th2-type responses in experimental secondary cystic
echinococcosis. These data would be relevant for designing rational therapies
based on stimulation of effective responses and blockade of evasion
mechanisms.

Author Summary

Cystic echinococcosis is a zoonotic disease caused by the larval stage of the
cestode Echinococcus granulosus and shows a cosmopolitan distribution with a
worldwide prevalence of roughly 6 million infected people. Human cystic
echinococcosis can develop in two types of infection. Primary infection occurs
by ingestion of oncospheres, while secondary infection is caused by
dissemination of protoscoleces after accidental rupture of fertile cysts.
Murine experimental secondary infection in Balb/c mice is the current model to
study E. granulosus-host interaction. Secondary infection can be divided into
two stages: an early stage in which protoscoleces develop into hydatid cysts
(infection establishment) and a later stage in which already differentiated
cysts grow and eventually become fertile cysts (chronic infection). During
infection establishment parasites are more susceptible to immune attack, thus
our study focused on the immunological phenomena triggered early in the
peritoneal cavity of experimentally infected mice. Our results suggest that
early and local Th2-type responses are permissive for infection establishment.

Introduction

Helminths are metazoan parasites currently</field></doc>
