<doc>
<field name="uid">doi:10.1371/journal.pntd.0001712</field>
<field name="doi">10.1371/journal.pntd.0001712</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Hemavathi Gopal, Hassan K. Hassan, Mario A. Rodríguez-Pérez, Laurent D. Toé, Sara Lustigman, Thomas R. Unnasch</field>
<field name="title">Oligonucleotide Based Magnetic Bead Capture of Onchocerca volvulus DNA for PCR Pool Screening of Vector Black Flies</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">6</field>
<field name="pages">e1712</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Entomological surveys of Simulium vectors are an important component in the
criteria used to determine if Onchocerca volvulus transmission has been
interrupted and if focal elimination of the parasite has been achieved.
However, because infection in the vector population is quite rare in areas
where control has succeeded, large numbers of flies need to be examined to
certify transmission interruption. Currently, this is accomplished through PCR
pool screening of large numbers of flies. The efficiency of this process is
limited by the size of the pools that may be screened, which is in turn
determined by the constraints imposed by the biochemistry of the assay. The
current method of DNA purification from pools of vector black flies relies
upon silica adsorption. This method can be applied to screen pools containing
a maximum of 50 individuals (from the Latin American vectors) or 100
individuals (from the African vectors).

Methodology/Principal Findings

We have evaluated an alternative method of DNA purification for pool screening
of black flies which relies upon oligonucleotide capture of Onchocerca
volvulus genomic DNA from homogenates prepared from pools of Latin American
and African vectors. The oligonucleotide capture assay was shown to reliably
detect one O. volvulus infective larva in pools containing 200 African or
Latin American flies, representing a two-four fold improvement over the
conventional assay. The capture assay requires an equivalent amount of
technical time to conduct as the conventional assay, resulting in a two-four
fold reduction in labor costs per insect assayed and reduces reagent costs to
$3.81 per pool of 200 flies, or less than $0.02 per insect assayed.

Conclusions/Significance

The oligonucleotide capture assay represents a substantial improvement in the
procedure used to detect parasite prevalence in the vector population, a major
metric employed in the process of certifying the elimination of
onchocerciasis.

Author Summary

The absence of infective larvae of Onchocerca volvulus in the black fly vector
of this parasite is a major criterion used to certify that transmission has
been eliminated in a focus. This process requires screening large numbers of
flies. Currently, this is accomplished by screening pools of flies using a
PCR-based assay. The number of flies that may be included in each pool is
currently limited by the DNA purification process to 50 flies for Latin
American vectors and 100 flies for African vectors. Here, we describe a new
method for DNA purification that relies upon a specific oligonucleotide to
capture and immobilize the parasite DNA on a magnetic bead. This method
permits the reliable detection of a single infective larva of O. volvulus in
pools containing up to 200 individual flies. The method described here will
dramatically improve the efficiency of pool screening of vector black flies,
making the process of elimination certification easier and less expensive to
implement.

Introduction

Onchocerciasis, or river blindness has historically represented one of the
most important neglected tropical diseases in the developing world as measured
as a cause of socio-economic disruption [1]. It is also considered a candidate
for elimination b</field></doc>
