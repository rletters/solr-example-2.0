<doc>
<field name="uid">doi:10.1371/journal.pntd.0000917</field>
<field name="doi">10.1371/journal.pntd.0000917</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Vincent Jamonneau, Bruno Bucheton, Jacques Kaboré, Hamidou Ilboudo, Oumou Camara, Fabrice Courtin, Philippe Solano, Dramane Kaba, Roger Kambire, Kouakou Lingue, Mamadou Camara, Rudy Baelmans, Veerle Lejon, Philippe Büscher</field>
<field name="title">Revisiting the Immune Trypanolysis Test to Optimise Epidemiological Surveillance and Control of Sleeping Sickness in West Africa</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">12</field>
<field name="pages">e917</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Because of its high sensitivity and its ease of use in the field, the card
agglutination test for trypanosomiasis (CATT) is widely used for mass
screening of sleeping sickness. However, the CATT exhibits false-positive
results (i) raising the question of whether CATT-positive subjects who are
negative in parasitology are truly exposed to infection and (ii) making it
difficult to evaluate whether Trypanosoma brucei (T.b.) gambiense is still
circulating in areas of low endemicity. The objective of this study was to
assess the value of the immune trypanolysis test (TL) in characterising the
HAT status of CATT-positive subjects and to monitor HAT elimination in West
Africa.

Methodology/Principal Findings

TL was performed on plasma collected from CATT-positive persons identified
within medical surveys in several West African HAT foci in Guinea, Côte
d&apos;Ivoire and Burkina Faso with diverse epidemiological statuses (active,
latent, or historical). All HAT cases were TL+. All subjects living in a
nonendemic area were TL−. CATT prevalence was not correlated with HAT
prevalence in the study areas, whereas a significant correlation was found
using TL.

Conclusion and Significance

TL appears to be a marker for contact with T.b. gambiense. TL can be a tool
(i) at an individual level to identify nonparasitologically confirmed CATT-
positive subjects as well as those who had contact with T.b. gambiense and
should be followed up, (ii) at a population level to identify priority areas
for intervention, and (iii) in the context of HAT elimination to identify
areas free of HAT.

Author Summary

Human African trypanosomiasis (HAT) due to Trypanosoma brucei (T.b.) gambiense
is usually diagnosed using two sequential steps: first the card agglutination
test for trypanosomiasis (CATT) used for serological screening, followed by
parasitological methods to confirm the disease. Currently, CATT will continue
to be used as a test for mass screening because of its simplicity and high
sensitivity; however, its performance as a tool of surveillance in areas where
prevalence is low is poor because of its limited specificity. Hence in the
context of HAT elimination, there is a crucial need for a better marker of
contact with T.b. gambiense in humans. We evaluated here an existing highly
specific serological tool, the trypanolysis test (TL). We evaluated TL in
active, latent and historical HAT foci in Guinea, Côte d&apos;Ivoire and Burkina
Faso. We found that TL was a marker for exposure to T.b. gambiense. We propose
that TL should be used as a surveillance tool to monitor HAT elimination.

Introduction

Human African trypanosomiasis (HAT) or sleeping sickness is caused by two
subspecies of the protozoan flagellate Trypanosoma brucei. In West and Central
Africa, T.b. gambiense causes the chronic form of sleeping sickness, while in
East Africa, T.b. rhodesiense causes the more fulminant form [1]. T.b. brucei
is normally not infectious to humans, like other species causing animal
African trypanosomiasis (AAT) such as T. evansi, T. congolense, T. vivax and
T. equiperdum.

After the successful contr</field></doc>
