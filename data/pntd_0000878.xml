<doc>
<field name="uid">doi:10.1371/journal.pntd.0000878</field>
<field name="doi">10.1371/journal.pntd.0000878</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Kimberly A. Bishop-Lilly, Michael J. Turell, Kristin M. Willner, Amy Butani, Nichole M. E. Nolan, Shannon M. Lentz, Arya Akmal, Al Mateczun, Trupti N. Brahmbhatt, Shanmuga Sozhamannan, Chris A. Whitehouse, Timothy D. Read</field>
<field name="title">Arbovirus Detection in Insect Vectors by Rapid, High-Throughput Pyrosequencing</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">11</field>
<field name="pages">e878</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Despite the global threat caused by arthropod-borne viruses, there is not an
efficient method for screening vector populations to detect novel viral
sequences. Current viral detection and surveillance methods based on culture
can be costly and time consuming and are predicated on prior knowledge of the
etiologic agent, as they rely on specific oligonucleotide primers or
antibodies. Therefore, these techniques may be unsuitable for situations when
the causative agent of an outbreak is unknown.

Methodology/Principal Findings

In this study we explored the use of high-throughput pyrosequencing for
surveillance of arthropod-borne RNA viruses. Dengue virus, a member of the
positive strand RNA Flavivirus family that is transmitted by several members
of the Aedes genus of mosquitoes, was used as a model. Aedes aegypti
mosquitoes experimentally infected with dengue virus type 1 (DENV-1) were
pooled with noninfected mosquitoes to simulate samples derived from ongoing
arbovirus surveillance programs. Using random-primed methods, total RNA was
reverse-transcribed and resulting cDNA subjected to 454 pyrosequencing.

Conclusions/Significance

In two types of samples, one with 5 adult mosquitoes infected with DENV-1- and
the other with 1 DENV-1 infected mosquito and 4 noninfected mosquitoes, we
identified DENV-1 DNA sequences. DENV-1 sequences were not detected in an
uninfected control pool of 5 adult mosquitoes. We calculated the proportion of
the Ae. aegypti metagenome contributed by each infecting Dengue virus genome
(pIP), which ranged from 2.75×10−8 to 1.08×10−7. DENV-1 RNA was sufficiently
concentrated in the mosquito that its detection was feasible using current
high-throughput sequencing instrumentation. We also identified some of the
components of the mosquito microflora on the basis of the sequence of
expressed RNA. This included members of the bacterial genera Pirellula and
Asaia, various fungi, and a potentially uncharacterized mycovirus.

Author Summary

Traditional methods for virus detection often rely on specific attributes,
such as DNA sequences, of the viruses and therefore they not only require a
priori knowledge of the agent in question, but they also are generally very
specific in nature, capable of detecting viruses only from within a specific
family, for example. Nextgen sequencing shows much promise for
detection/diagnostic applications because of its ever-increasing throughput,
decreasing cost, and unbiased nature. We investigated the applicability of 454
pyrosequencing for viral surveillance of insect populations, using Aedes
aegypti mosquitoes experimentally inoculated with Dengue virus type 1 (DENV-1)
and calculated what proportion of the total nucleic acid from crushed
mosquitoes was contributed by the virus. We concluded that 454 pyrosequencing
is capable of detecting even very small amounts of a known virus from within a
pool of infected and noninfected mosquitoes, but for the amount of sequencing
reads required to detect the virus, this technique may currently be too cost-
prohibitive for use in large-scale surveillance efforts. Interesting
byproducts of our study included a glimpse into what symbi</field></doc>
