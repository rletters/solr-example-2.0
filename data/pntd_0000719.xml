<doc>
<field name="uid">doi:10.1371/journal.pntd.0000719</field>
<field name="doi">10.1371/journal.pntd.0000719</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Neil D. Young, Bronwyn E. Campbell, Ross S. Hall, Aaron R. Jex, Cinzia Cantacessi, Thewarach Laha, Woon-Mok Sohn, Banchob Sripa, Alex Loukas, Paul J. Brindley, Robin B. Gasser</field>
<field name="title">Unlocking the Transcriptomes of Two Carcinogenic Parasites, Clonorchis sinensis and Opisthorchis viverrini</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">6</field>
<field name="pages">e719</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

The two parasitic trematodes, Clonorchis sinensis and Opisthorchis viverrini,
have a major impact on the health of tens of millions of humans throughout
Asia. The greatest impact is through the malignant cancer ( =
cholangiocarcinoma) that these parasites induce in chronically infected
people. Therefore, both C. sinensis and O. viverrini have been classified by
the World Health Organization (WHO) as Group 1 carcinogens. Despite their
impact, little is known about these parasites and their interplay with the
host at the molecular level. Recent advances in genomics and bioinformatics
provide unique opportunities to gain improved insights into the biology of
parasites as well as their relationships with their hosts at the molecular
level. The present study elucidates the transcriptomes of C. sinensis and O.
viverrini using a platform based on next-generation (high throughput)
sequencing and advanced in silico analyses. From 500,000 sequences, &gt;50,000
sequences were assembled for each species and categorized as biologically
relevant based on homology searches, gene ontology and/or pathway mapping. The
results of the present study could assist in defining molecules that are
essential for the development, reproduction and survival of liver flukes
and/or that are linked to the development of cholangiocarcinoma. This study
also lays a foundation for future genomic and proteomic research of C.
sinensis and O. viverrini and the cancers that they are known to induce, as
well as novel intervention strategies.

Author Summary

The parasitic worms, Clonorchis sinensis and Opisthorchis viverrini, have a
serious impact on the health of tens of millions of people throughout Asia.
The greatest impact, however, is through the malignant, untreatable cancer
(cholangiocarcinoma) that these parasites induce in chronically infected
people. These liver flukes are officially classified by the World Health
Organization (WHO) as Group 1 carcinogens. In spite of their massive impact on
human health, little is known about these parasites and their relationship
with the host at the molecular level. Here, we provide the first detailed
insight into the transcriptomes of these flukes, providing a solid foundation
for all of the molecular/-omic work required to understand their biology, but,
more importantly, to elucidate key aspects of the induction of
cholangiocarcinoma. Although our focus has been on the parasites, the
implications will extend far beyond the study of parasitic disease.
Importantly, insights into the pathogenesis of the infection are likely to
have major implications for the study and understanding of other cancers.

Introduction

Liver flukes (Platyhelminthes: Digenea) include important food-borne
eukaryotic pathogens of humans [1]–[5]. For example, the liver flukes
Clonorchis sinensis and Opisthorchis viverrini, which cause the diseases
clonorchiasis and opisthorchiasis, respectively, represent a substantial
public health problem in many parts of Asia [2], [3], [6]. Clonorchis sinensis
is endemic predominantly in regions of China (including Hong Kong and Taiwan),
Korea and North Vietnam [2], [6], whilst O. viverrini is endemic throughout
Thailand, the Lao Peop</field></doc>
