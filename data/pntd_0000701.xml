<doc>
<field name="uid">doi:10.1371/journal.pntd.0000701</field>
<field name="doi">10.1371/journal.pntd.0000701</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Iain D. Kerr, Peng Wu, Rachael Marion-Tsukamaki, Zachary B. Mackey, Linda S. Brinen</field>
<field name="title">Crystal Structures of TbCatB and Rhodesain, Potential Chemotherapeutic Targets and Major Cysteine Proteases of Trypanosoma brucei</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">6</field>
<field name="pages">e701</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Trypanosoma brucei is the etiological agent of Human African Trypanosomiasis,
an endemic parasitic disease of sub-Saharan Africa. TbCatB and rhodesain are
the sole Clan CA papain-like cysteine proteases produced by the parasite
during infection of the mammalian host and are implicated in the progression
of disease. Of considerable interest is the exploration of these two enzymes
as targets for cysteine protease inhibitors that are effective against T.
brucei.

Methods and Findings

We have determined, by X-ray crystallography, the first reported structure of
TbCatB in complex with the cathepsin B selective inhibitor CA074. In addition
we report the structure of rhodesain in complex with the vinyl-sulfone K11002.

Conclusions

The mature domain of our TbCat•CA074 structure contains unique features for a
cathepsin B-like enzyme including an elongated N-terminus extending 16
residues past the predicted maturation cleavage site. N-terminal Edman
sequencing reveals an even longer extension than is observed amongst the
ordered portions of the crystal structure. The TbCat•CA074 structure confirms
that the occluding loop, which is an essential part of the substrate-binding
site, creates a larger prime side pocket in the active site cleft than is
found in mammalian cathepsin B-small molecule structures. Our data further
highlight enhanced flexibility in the occluding loop main chain and structural
deviations from mammalian cathepsin B enzymes that may affect activity and
inhibitor design. Comparisons with the rhodesain•K11002 structure highlight
key differences that may impact the design of cysteine protease inhibitors as
anti-trypanosomal drugs.

Author Summary

Proteases are ubiquitous in all forms of life and catalyze the enzymatic
degradation of proteins. These enzymes regulate and coordinate a vast number
of cellular processes and are therefore essential to many organisms. While
serine proteases dominate in mammals, parasitic organisms commonly rely on
cysteine proteases of the Clan CA family throughout their lifecycle. Clan CA
cysteine proteases are therefore regarded as promising targets for the
selective design of drugs to treat parasitic diseases, such as Human African
Trypanosomiasis caused by Trypanosoma brucei. The genomes of kinetoplastids
such as Trypanosoma spp. and Leishmania spp. encode two Clan CA C1 family
cysteine proteases and in T. brucei these are represented by rhodesain and
TbCatB. We have determined three-dimensional structures of these two enzymes
as part of our ongoing efforts to synthesize more effective anti-trypanosomal
drugs.

Introduction

The protozoan parasite Trypanosoma brucei is the cause of Human African
Trypanosomiasis (HAT, sleeping sickness) in humans and nagana in domestic
livestock [1], [2], [3]. With over 60 million people at risk and 50,000–70,000
infected, new drugs are required to control the spread of disease and
associated mortality. Only four drugs are approved for treatment [4], however,
most are limited by parasite resistance [4] and marked host toxicity [5], [6],
[7], [8]. A new combination therapy for treating HAT has been approved
recently by the World Health Organization (WHO), however no newly developed
drugs are on the horizon [9], [10]

Clan CA cystei</field></doc>
