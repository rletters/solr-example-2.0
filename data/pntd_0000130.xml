<doc>
<field name="uid">doi:10.1371/journal.pntd.0000130</field>
<field name="doi">10.1371/journal.pntd.0000130</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Bennett J. D. Datu, Robin B. Gasser, Shivashankar H. Nagaraj, Eng K. Ong, Peter O'Donoghue, Russell McInnes, Shoba Ranganathan, Alex Loukas</field>
<field name="title">Transcriptional Changes in the Hookworm, Ancylostoma caninum, during the Transition from a Free-Living to a Parasitic Larva</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">1</field>
<field name="pages">e130</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Third-stage larvae (L3) of the canine hookworm, Ancylostoma caninum, undergo
arrested development preceding transmission to a host. Many of the mRNAs up-
regulated at this stage are likely to encode proteins that facilitate the
transition from a free-living to a parasitic larva. The initial phase of
mammalian host invasion by A. caninum L3 (herein termed “activation”) can be
mimicked in vitro by culturing L3 in serum-containing medium.

Methodology/Principal Findings

The mRNAs differentially transcribed between activated and non-activated L3
were identified by suppression subtractive hybridisation (SSH). The analysis
of these mRNAs on a custom oligonucleotide microarray printed with the SSH
expressed sequence tags (ESTs) and publicly available A. caninum ESTs (non-
subtracted) yielded 602 differentially expressed mRNAs, of which the most
highly represented sequences encoded members of the pathogenesis-related
protein (PRP) superfamily and proteases. Comparison of these A. caninum mRNAs
with those of Caenorhabditis elegans larvae exiting from developmental (dauer)
arrest demonstrated unexpectedly large differences in gene ontology profiles.
C. elegans dauer exiting L3 up-regulated expression of mostly intracellular
molecules involved in growth and development. Such mRNAs are virtually absent
from activated hookworm larvae, and instead are over-represented by mRNAs
encoding extracellular proteins with putative roles in host-parasite
interactions.

Conclusions/Significance

Although this should not invalidate C. elegans dauer exit as a model for
hookworm activation, it highlights the limitations of this free-living
nematode as a model organism for the transition of nematode larvae from a
free-living to a parasitic state.

Author Summary

Hookworms are soil-transmitted nematodes that parasitize hundreds of millions
of people in developing countries. Here we describe the genes expressed when
hookworm larvae make the transition from a developmentally arrested free-
living form to a tissue-penetrating parasitic stage. Ancylostoma caninum can
be “tricked” into thinking it has penetrated host skin by incubating free-
living larvae in host serum – this is called “activation”. To comprehensively
identify genes involved in activation, we used suppressive subtractive
hybridization to clone genes that were up- or down-regulated in activated
larvae, with a particular focus on up-regulated genes. The subtracted genes,
as well as randomly sequenced (non-subtracted) genes from public databases
were then printed on a microarray to further explore differential expression.
We compared predicted gene functions between activated hookworms and the free-
living nematode, Caenorhabditis elegans, exiting developmental arrest (dauer),
and found enormous differences in the types of genes expressed. Genes encoding
secreted proteins involved in parasitism were over-represented in activated
hookworms whereas genes involved in growth and development dominated in C.
elegans exiting dauer. Our data implies that C. elegans dauer exit is not a
reliable model for exit from developmental arrest of hookworm larvae. Many of
these genes likely play critical roles in host-parasite intera</field></doc>
