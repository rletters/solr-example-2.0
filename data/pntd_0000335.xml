<doc>
<field name="uid">doi:10.1371/journal.pntd.0000335</field>
<field name="doi">10.1371/journal.pntd.0000335</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Ruth Macklin, Ethan Cowan</field>
<field name="title">Conducting Research in Disease Outbreaks</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">4</field>
<field name="pages">e335</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Conducting research in an emergency situation, such as an outbreak of disease,
poses ethical challenges. These challenges differ according to the type of
research: epidemiologic or clinical, and for the latter, whether the disease
outbreak can be anticipated in advance. We address these three situations,
proposing different potential solutions for each.

In an outbreak situation, public health authorities undertake a rapid response
in an effort to document the existence and magnitude of a public health
problem in the community and to implement appropriate measures to address the
problem [1]. This rapid response will in some cases preclude the possibility
of clearance by a research ethics committee since the time required to develop
and submit a detailed research protocol and respond to any requested
modifications by the committee, followed by re-review, would thwart the very
purpose of the response. According to one prominent view, to require a full
written protocol and submission to an ethics review board would not be in the
interests of the individuals or the community because the resulting delays
would frequently cause excess disease and death [2]. These authors suggest,
however, that emergency response consent forms could be developed and used in
these situations. Other individuals engaged in public health practice have
voiced concern that subjecting their work to the routinely required
“regulatory constraints imposed on research” would prevent flexible and timely
approaches to situations such as disease outbreaks [3]. They argue that
timeliness is a major requirement that would have to be counterbalanced with
other ethical concerns. An example is that of pandemic influenza outbreaks, in
which it is alleged that the review process would impair the ability of public
health agencies to react in a timely manner.

For researchers and public health agencies, therefore, the question is how to
comply with the ethical requirement that research be approved by a properly
constituted, independent ethical review committee (ERC) but still enable a
prompt response when an outbreak occurs. If we concede that existing methods
of ethical review are too protracted to be useful in outbreak situations, can
alternative mechanisms be employed to ensure that such investigations undergo
some type of ethical review? How can the rights and welfare of individuals be
protected during investigations of disease outbreaks, and at the same time
enable such investigations to be carried out expeditiously?

In addition to review by a research ethics committee, a fundamental ethical
requirement in research is to obtain informed consent from participants.
Although not all research requires the informed consent of individual
subjects, the vast majority of clinical research, most social and behavioral
research, and some epidemiologic research must be carried out with the
voluntary, informed consent of participants or their legally authorized
representatives. In contrast, in many instances of public health practice,
collection and use of information or human biological specimens can be
conducted without a written informed consent document and without obtaining
permission to store the samples for future use. However, a problem could arise
if an investigator wants to use these samples, collected initially for public
health purposes, for research. Current research practice calls for obtaining
consent for the </field></doc>
