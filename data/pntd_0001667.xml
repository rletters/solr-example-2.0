<doc>
<field name="uid">doi:10.1371/journal.pntd.0001667</field>
<field name="doi">10.1371/journal.pntd.0001667</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Tetyana Kobets, Helena Havelková, Igor Grekov, Valeriya Volkova, Jarmila Vojtíšková, Martina Slapničková, Iryna Kurey, Yahya Sohrabi, Milena Svobodová, Peter Demant, Marie Lipoldová</field>
<field name="title">Genetics of Host Response to Leishmania tropica in Mice – Different Control of Skin Pathology, Chemokine Reaction, and Invasion into Spleen and Liver</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">6</field>
<field name="pages">e1667</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Leishmaniasis is a disease caused by protozoan parasites of genus Leishmania.
The frequent involvement of Leishmania tropica in human leishmaniasis has been
recognized only recently. Similarly as L. major, L. tropica causes cutaneous
leishmaniasis in humans, but can also visceralize and cause systemic illness.
The relationship between the host genotype and disease manifestations is
poorly understood because there were no suitable animal models.

Methods

We studied susceptibility to L. tropica, using BALB/c-c-STS/A (CcS/Dem)
recombinant congenic (RC) strains, which differ greatly in susceptibility to
L. major. Mice were infected with L. tropica and skin lesions, cytokine and
chemokine levels in serum, and parasite numbers in organs were measured.

Principal Findings

Females of BALB/c and several RC strains developed skin lesions. In some
strains parasites visceralized and were detected in spleen and liver.
Importantly, the strain distribution pattern of symptoms caused by L. tropica
was different from that observed after L. major infection. Moreover, sex
differently influenced infection with L. tropica and L. major. L. major-
infected males exhibited either higher or similar skin pathology as females,
whereas L. tropica-infected females were more susceptible than males. The
majority of L. tropica-infected strains exhibited increased levels of
chemokines CCL2, CCL3 and CCL5. CcS-16 females, which developed the largest
lesions, exhibited a unique systemic chemokine reaction, characterized by
additional transient early peaks of CCL3 and CCL5, which were not present in
CcS-16 males nor in any other strain.

Conclusion

Comparison of L. tropica and L. major infections indicates that the strain
patterns of response are species-specific, with different sex effects and
largely different host susceptibility genes.

Author Summary

Several hundred million people are exposed to the risk of leishmaniasis, a
disease caused by intracellular protozoan parasites of several Leishmania
species and transmitted by phlebotomine sand flies. In humans, L. tropica
causes cutaneous form of leishmaniasis with painful and long-persisting
lesions in the site of the insect bite, but the parasites can also penetrate
to internal organs. The relationship between the host genes and development of
the disease was demonstrated for numerous infectious diseases. However, the
search for susceptibility genes in the human population could be a difficult
task. In such cases, animal models may help to discover the role of different
genes in interactions between the parasite and the host. Unfortunately, the
literature contains only a few publications about the use of animals for L.
tropica studies. Here, we report an animal model suitable for genetic,
pathological and drug studies in L. tropica infection. We show how the host
genotype influences different disease symptoms: skin lesions, parasite
dissemination to the lymph nodes, spleen and liver, and increase of levels of
chemokines CCL2, CCL3 and CCL5 in serum.

Introduction

Several hundred million people in 88 countries are living in areas where they
can contract leishma</field></doc>
