<doc>
<field name="uid">doi:10.1371/journal.pntd.0001624</field>
<field name="doi">10.1371/journal.pntd.0001624</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Luana de Borba, Daisy M. Strottmann, Lucia de Noronha, Peter W. Mason, Claudia N. Duarte dos Santos</field>
<field name="title">Synergistic Interactions between the NS3hel and E Proteins Contribute to the Virulence of Dengue Virus Type 1</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">4</field>
<field name="pages">e1624</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Dengue includes a broad range of symptoms, ranging from fever to hemorrhagic
fever and may occasionally have alternative clinical presentations. Many
possible viral genetic determinants of the intrinsic virulence of dengue virus
(DENV) in the host have been identified, but no conclusive evidence of a
correlation between viral genotype and virus transmissibility and
pathogenicity has been obtained.

Methodology/Principal Findings

We used reverse genetics techniques to engineer DENV-1 viruses with subsets of
mutations found in two different neuroadapted derivatives. The mutations were
inserted into an infectious clone of DENV-1 not adapted to mice. The
replication and viral production capacity of the recombinant viruses were
assessed in vitro and in vivo. The results demonstrated that paired mutations
in the envelope protein (E) and in the helicase domain of the NS3 (NS3hel)
protein had a synergistic effect enhancing viral fitness in human and mosquito
derived cell lines. E mutations alone generated no detectable virulence in the
mouse model; however, the combination of these mutations with NS3hel
mutations, which were mildly virulent on their own, resulted in a highly
neurovirulent phenotype.

Conclusions/Significance

The generation of recombinant viruses carrying specific E and NS3hel proteins
mutations increased viral fitness both in vitro and in vivo by increasing RNA
synthesis and viral load (these changes being positively correlated with
central nervous system damage), the strength of the immune response and animal
mortality. The introduction of only pairs of amino acid substitutions into the
genome of a non-mouse adapted DENV-1 strain was sufficient to alter viral
fitness substantially. Given current limitations to our understanding of the
molecular basis of dengue neuropathogenesis, these results could contribute to
the development of attenuated strains for use in vaccinations and provide
insights into virus/host interactions and new information about the mechanisms
of basic dengue biology.

Author Summary

Dengue virus constitutes a significant public health problem in tropical
regions of the world. Despite the high morbidity and mortality of this
infection, no effective antiviral drugs or vaccines are available for the
treatment or prevention of dengue infections. The profile of clinical signs
associated with dengue infection has changed in recent years with an increase
in the number of episodes displaying unusual signs. We use reverse genetics
technology to engineer DENV-1 viruses with subsets of mutations previously
identified in highly neurovirulent strains to provide insights into the
molecular mechanisms underlying dengue neuropathogenesis. We found that single
mutations affecting the E and NS3hel proteins, introduced in a different
genetic context, had a synergistic effect increasing DENV replication capacity
in human and mosquito derived cells in vitro. We also demonstrated
correlations between the presence of these mutations and viral replication
efficiency, viral loads, the induction of innate immune response genes and
pathogenesis in a mouse model. These results should improve our understanding
of the DENV-host cell interaction and contribute to the development of
effective antiviral strategi</field></doc>
