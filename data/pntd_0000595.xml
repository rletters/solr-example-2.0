<doc>
<field name="uid">doi:10.1371/journal.pntd.0000595</field>
<field name="doi">10.1371/journal.pntd.0000595</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Susan M. Moore, Cathleen A. Hanlon</field>
<field name="title">Rabies-Specific Antibodies: Measuring Surrogates of Protection against a Fatal Disease</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">3</field>
<field name="pages">e595</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Antibodies play a central role in prophylaxis against many infectious agents.
While neutralization is a primary function of antibodies, the Fc- and
complement-dependent activities of these multifunctional proteins may also be
critical in their ability to provide protection against most viruses.
Protection against viral pathogens in vivo is complex, and while virus
neutralization—the ability of antibody to inactivate virus infectivity, often
measured in vitro—is important, it is often only a partial contributor in
protection. The rapid fluorescent focus inhibition test (RFFIT) remains the
“gold standard” assay to measure rabies virus–neutralizing antibodies. In
addition to neutralization, the rabies-specific antigen-binding activity of
antibodies may be measured through enzyme-linked immunosorbent assays
(ELISAs), as well as other available methods. For any disease, in selecting
the appropriate assay(s) to use to assess antibody titers, assay validation
and how they are interpreted are important considerations—but for a fatal
disease like rabies, they are of paramount importance. The innate limitations
of a one-dimensional laboratory test for rabies antibody measurement, as well
as the validation of the method of choice, must be carefully considered in the
selection of an assay method and for the interpretation of results that might
be construed as a surrogate of protection.

Introduction

Whether an animal control worker wants to determine if a rabies vaccine
booster is necessary to establish an acceptable pre-exposure status, or a
physician is considering the causes of encephalitis in a child, or the owner
of an immunologically compromised dog is worried that the dog&apos;s response to
rabies vaccination will not be sufficient to pass a serological test allowing
them to travel to a rabies-free area, or a researcher is trying to determine
if the rabies vaccine-bait response is adequate in a raccoon population, or
one needs to assign a potency value to a rabies immune globulin product, all
demand an accurate assessment based on the measurement of circulating
antibodies. In each of these situations the measurement or simply the
detection of rabies-specific virus-neutralizing or other antibodies will help
resolve the question. However, just as the circumstances in each of these
scenarios are different, the specifics of the method chosen to measure
antibodies, the regulatory requirements of the testing, and the purpose of
testing in each of these situations are different. Antibodies arise from the
humoral immune response to rabies antigens, the process of which is controlled
by many factors, including the amount of antigen, route of delivery, the
expression and involvement of major histocompatibility complex (MHC) genes,
and the health status of the individual, among others. An understanding of the
host immune response, including the immunoglobulin (Ig) subclass (type) and
the kinetics and longevity of the response, is necessary to obtain the measure
or degree of rabies immunity.

Initially, measurement of rabies virus neutralizing antibodies (RVNA) was
performed in vivo using the mouse neutralization test (MNT). Subsequently, the
rapid fluorescent focus inhibition test (RFFIT) was established as the “gold
standard” in vitro test [1]. Methods for measuring rabies immunity vary with
regard to the hum</field></doc>
