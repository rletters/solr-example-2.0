<doc>
<field name="uid">doi:10.1371/journal.pntd.0001237</field>
<field name="doi">10.1371/journal.pntd.0001237</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Fred S. Sarfo, Fabien Le Chevalier, N'Guetta Aka, Richard O. Phillips, Yaw Amoako, Ivo G. Boneca, Pascal Lenormand, Mireille Dosso, Mark Wansbrough-Jones, Romain Veyron-Churlet, Laure Guenin-Macé, Caroline Demangel</field>
<field name="title">Mycolactone Diffuses into the Peripheral Blood of Buruli Ulcer Patients - Implications for Diagnosis and Disease Monitoring</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">7</field>
<field name="pages">e1237</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Mycobacterium ulcerans, the causative agent of Buruli ulcer (BU), is unique
among human pathogens in its capacity to produce a polyketide-derived
macrolide called mycolactone, making this molecule an attractive candidate
target for diagnosis and disease monitoring. Whether mycolactone diffuses from
ulcerated lesions in clinically accessible samples and is modulated by
antibiotic therapy remained to be established.

Methodology/Principal Finding

Peripheral blood and ulcer exudates were sampled from patients at various
stages of antibiotic therapy in Ghana and Ivory Coast. Total lipids were
extracted from serum, white cell pellets and ulcer exudates with organic
solvents. The presence of mycolactone in these extracts was then analyzed by a
recently published, field-friendly method using thin layer chromatography and
fluorescence detection. This approach did not allow us to detect mycolactone
accurately, because of a high background due to co-extracted human lipids. We
thus used a previously established approach based on high performance liquid
chromatography coupled to mass spectrometry. By this means, we could identify
structurally intact mycolactone in ulcer exudates and serum of patients, and
evaluate the impact of antibiotic treatment on the concentration of
mycolactone.

Conclusions/Significance

Our study provides the proof of concept that assays based on mycolactone
detection in serum and ulcer exudates can form the basis of BU diagnostic
tests. However, the identification of mycolactone required a technology that
is not compatible with field conditions and point-of-care assays for
mycolactone detection remain to be worked out. Notably, we found mycolactone
in ulcer exudates harvested at the end of antibiotic therapy, suggesting that
the toxin is eliminated by BU patients at a slow rate. Our results also
indicated that mycolactone titres in the serum may reflect a positive response
to antibiotics, a possibility that it will be interesting to examine further
through longitudinal studies.

Author Summary

Mycolactone is a diffusible cytotoxin produced by Mycobacterium ulcerans, the
causative agent of the skin disease Buruli ulcer. In a previous study using
animal models, we reported that mycolactone released by cutaneous foci of
infection gains access to the peripheral blood. Here we investigated whether
mycolactone circulates in human patients and is detectable in clinically
accessible samples. Using a combination of solvent extraction, high
performance liquid chromatography and mass spectrometry analysis, we found
that structurally intact mycolactone was present in ulcer exudates obtained
from wound swabs and in the serum of patients. Unexpectedly, high titres of
mycolactone were detected in ulcer exudates after completion of antibiotic
treatment. In contrast, mycolactone could only be detected in the serum of
newly diagnosed patients. Our results demonstrate that mycolactone detection
in serum and ulcer exudates may be used to diagnose BU. Moreover, they suggest
that the kinetics of mycolactone concentration in serum may be indicative of
the clinical response of patients to ant</field></doc>
