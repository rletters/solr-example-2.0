<doc>
<field name="uid">doi:10.1371/journal.pntd.0001402</field>
<field name="doi">10.1371/journal.pntd.0001402</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Delphin Mavinga Phanzu, Patrick Suykerbuyk, Désiré Bofunga B. Imposo, Philippe Ngwala Lukanu, Jean-Bedel Masamba Minuku, Linda F. Lehman, Paul Saunderson, Bouke C. de Jong, Pascal Tshindele Lutumba, Françoise Portaels, Marleen Boelaert</field>
<field name="title">Effect of a Control Project on Clinical Profiles and Outcomes in Buruli Ulcer: A Before/After Study in Bas-Congo, Democratic Republic of Congo</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">12</field>
<field name="pages">e1402</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Buruli ulcer (BU) is a necrotizing bacterial infection of skin, subcutaneous
tissue and bone caused by Mycobacterium ulcerans. Although the functional
impairment caused by BU results in severe suffering and in socio-economic
problems, the disease remains largely neglected in Africa. The province of
Bas-Congo in Democratic Republic of Congo contains one of the most important
BU foci of the country, i.e. the Songololo Territory in the District of
Cataractes. This study aims to assess the impact of a BU control project
launched in 2004 in the Songololo Territory.

Methods

We used a comparative non-randomized study design, comparing clinical profiles
and outcomes of the group of patients admitted at the General Reference
Hospital (GRH) of the “Institut Médical Evangélique” (IME) of Kimpese 3 years
before the start of the project (2002–2004) with those admitted during the 3
years after the start of the project (2005–2007).

Results

The BU control project was associated with a strong increase in the number of
admitted BU cases at the GRH of IME/Kimpese and a fundamental change in the
profile of those patients; more female patients presented with BU, the
proportion of relapse cases amongst all admissions reduced, the proportion of
early lesions and simple ulcerative forms increased, more patients healed
without complications and the case fatality rate decreased substantially. The
median duration since the onset of first symptoms however remained high, as
well as the proportion of patients with osteomyelitis or limitations of joint
movement, suggesting that the diagnostic delay remains substantial.

Conclusion

Implementing a specialized program for BU may be effective in improving
clinical profiles and outcomes in BU. Despite these encouraging results, our
study highlights the need of considering new strategies to better improve BU
control in a low resources setting.

Author Summary

Buruli ulcer (BU), which is caused by Mycobacterium ulcerans, is an important
disabling skin disease. However, BU has been neglected in many endemic African
countries, including in the Democratic Republic of Congo. The province of Bas-
Congo contains one of the most important BU foci of the country, i.e. the
Songololo Territory in the District of Cataractes. In 2004 a specialized BU
control program was launched in that area. The present study aims to evaluate
the impact of the above-mentioned program, by comparing clinical profiles and
outcomes of the group of patients admitted at the General Reference Hospital
(GRH) of the “Institut Médical Evangélique” (IME) of Kimpese 3 years before
the start of the project (2002–2004) with those admitted during the 3 years
after the start of the project (2005–2007). The project implementation was
associated with a strong increase in the number of admitted BU cases at the
GRH and a fundamental change in the profile of those BU patients. Despite
these encouraging results, our study provides some limitations of such
program, and highlights the need of considering new strategies to better
improve BU control in a lo</field></doc>
