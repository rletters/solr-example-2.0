<doc>
<field name="uid">doi:10.1371/journal.pntd.0001291</field>
<field name="doi">10.1371/journal.pntd.0001291</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Wendel Coura-Vital, Marcos José Marques, Vanja Maria Veloso, Bruno Mendes Roatt, Rodrigo Dian de Oliveira Aguiar-Soares, Levi Eduardo Soares Reis, Samuel Leôncio Braga, Maria Helena Franco Morais, Alexandre Barbosa Reis, Mariângela Carneiro</field>
<field name="title">Prevalence and Factors Associated with Leishmania infantum Infection of Dogs from an Urban Area of Brazil as Identified by Molecular Methods</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">8</field>
<field name="pages">e1291</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Various factors contribute to the urbanization of the visceral leishmaniasis
(VL), including the difficulties of implementing control measures relating to
the domestic reservoir. The aim of this study was to determine the prevalence
of canine visceral leishmaniasis in an urban endemic area in Brazil and the
factors associated with Leishmania infantum infection among seronegative and
PCR-positive dogs.

Methodology

A cross-sectional study was conducted in Belo Horizonte, Minas Gerais, Brazil.
Blood samples were collected from 1,443 dogs. Serology was carried out by
using two enzyme-linked immunosorbent assays (Biomanguinhos/FIOCRUZ/RJ and “in
house”), and molecular methods were developed, including PCR-RFLP. To identify
the factors associated with early stages of infection, only seronegative (n =
1,213) animals were evaluated. These animals were divided into two groups:
PCR-positive (n = 296) and PCR-negative (n = 917) for L. infantum DNA. A
comparison of these two groups of dogs taking into consideration the
characteristics of the animals and their owners was performed. A mixed
logistic regression model was used to identify factors associated with L.
infantum infection.

Principal Findings

Of the 1,443 dogs examined, 230 (15.9%) were seropositive in at least one
ELISA, whereas PCR-RFLP revealed that 356 animals (24.7%) were positive for L.
infantum DNA. Results indicated that the associated factors with infection
were family income&lt;twice the Brazilian minimum salary (OR 2.3; 95%CI 1.4–3.8),
knowledge of the owner regarding the vector (OR 1.9; 95%CI 1.1–3.4), the dog
staying predominantly in the backyard (OR 2.2; 95%CI 1.1–4.1), and a lack of
previous serological examination for VL (OR 1.5; 95%CI 1.1–2.3).

Conclusions

PCR detected a high prevalence of L. infantum infection in dogs in an area
under the Control Program of VL intervention. Socioeconomic variables, dog
behavior and the knowledge of the owner regarding the vector were factors
associated with canine visceral leishmaniasis (CVL). The absence of previous
serological examination conducted by the control program was also associated
with L. infantum infection. It is necessary to identify the risk factors
associated with CVL to understand the expansion and urbanization of VL.

Author Summary

Visceral leishmaniasis (VL) is a disease caused by the parasite Leishmania
infantum, and dogs are the most important domestic reservoirs of the agent.
During recent decades, VL has expanded to large Brazilian urban centers. In
the present work, we have demonstrated by using molecular techniques that the
rate of canine infection as detected by serology has been considerably
underestimated. Two groups of seronegative dogs (infected and non-infected
according to molecular methods) were further evaluated from data obtained
through interviews with owners of the animals. The factors associated with
Leishmania infection in dogs were a family income of less than two minimum
salaries, the knowledge of the owner regarding the vector, the dog spending
most of its time in the backy</field></doc>
