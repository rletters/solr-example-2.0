<doc>
<field name="uid">doi:10.1371/journal.pntd.0001533</field>
<field name="doi">10.1371/journal.pntd.0001533</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Edith A. Fernández-Figueroa, Claudia Rangel-Escareño, Valeria Espinosa-Mateos, Karol Carrillo-Sánchez, Norma Salaiza-Suazo, Georgina Carrada-Figueroa, Santiago March-Mifsut, Ingeborg Becker</field>
<field name="title">Disease Severity in Patients Infected with Leishmania mexicana Relates to IL-1β</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">5</field>
<field name="pages">e1533</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Leishmania mexicana can cause both localized (LCL) and diffuse (DCL) cutaneous
leishmaniasis, yet little is known about factors regulating disease severity
in these patients. We analyzed if the disease was associated with single
nucleotide polymorphisms (SNPs) in IL-1β (−511), CXCL8 (−251) and/or the
inhibitor IL-1RA (+2018) in 58 Mexican mestizo patients with LCL, 6 with DCL
and 123 control cases. Additionally, we analyzed the in vitro production of
IL-1β by monocytes, the expression of this cytokine in sera of these patients,
as well as the tissue distribution of IL-1β and the number of parasites in
lesions of LCL and DCL patients. Our results show a significant difference in
the distribution of IL-1β (−511 C/T) genotypes between patients and controls
(heterozygous OR), with respect to the reference group CC, which was estimated
with a value of 3.23, 95% CI = (1.2, 8.7) and p-value = 0.0167), indicating
that IL-1β (−511 C/T) represents a variable influencing the risk to develop
the disease in patients infected with Leishmania mexicana. Additionally, an
increased in vitro production of IL-1β by monocytes and an increased serum
expression of the cytokine correlated with the severity of the disease, since
it was significantly higher in DCL patients heavily infected with Leishmania
mexicana. The distribution of IL-1β in lesions also varied according to the
number of parasites harbored in the tissues: in heavily infected LCL patients
and in all DCL patients, the cytokine was scattered diffusely throughout the
lesion. In contrast, in LCL patients with lower numbers of parasites in the
lesions, IL-1β was confined to the cells. These data suggest that IL-1β
possibly is a key player determining the severity of the disease in DCL
patients. The analysis of polymorphisms in CXCL8 and IL-1RA showed no
differences between patients with different disease severities or between
patients and controls.

Author Summary

Leishmania mexicana is an intracellular parasite that causes two polarly
opposed diseases: One is a self-limited disease, characterized by ulcerative
lesions associated with a low infectious load, as found in patients with
localized cutaneous leishmaniasis (LCL). And the other pole is characterized
by a progressive disease where abundant parasites spread uncontrollably
throughout the skin inside heavily infected phagocytic cells, as occurs in
patients with diffuse cutaneous leishmaniasis (DCL). The cause of this severe
form of the disease is unknown, although the early encounter between the
parasite and the inflammatory response of the host possibly plays a decisive
role in the disease outcome. We here show that polymorphism in the gene
encoding IL-1β (−511 C/T) represents a variable influencing the risk to
develop the disease for patients infected with Leishmania mexicana. In vitro
experiments showed that monocytes of DCL patients secreted significantly
higher levels of the proinflammatory cytokine IL-1β as compared to LCL
patients. DCL patients also had augmented levels of IL-1β in serum, and the
cytokine was diffusely distributed throughout lesions, which was correlated
with the numbers of parasites in the lesions. We propose that</field></doc>
