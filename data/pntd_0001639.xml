<doc>
<field name="uid">doi:10.1371/journal.pntd.0001639</field>
<field name="doi">10.1371/journal.pntd.0001639</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Mary B. Crabtree, Rebekah J. Kent Crockett, Brian H. Bird, Stuart T. Nichol, Bobbie Rae Erickson, Brad J. Biggerstaff, Kalanthe Horiuchi, Barry R. Miller</field>
<field name="title">Infection and Transmission of Rift Valley Fever Viruses Lacking the NSs and/or NSm Genes in Mosquitoes: Potential Role for NSm in Mosquito Infection</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">5</field>
<field name="pages">e1639</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Rift Valley fever virus is an arthropod-borne human and animal pathogen
responsible for large outbreaks of acute and febrile illness throughout Africa
and the Arabian Peninsula. Reverse genetics technology has been used to
develop deletion mutants of the virus that lack the NSs and/or NSm virulence
genes and have been shown to be stable, immunogenic and protective against
Rift Valley fever virus infection in animals. We assessed the potential for
these deletion mutant viruses to infect and be transmitted by Aedes
mosquitoes, which are the principal vectors for maintenance of the virus in
nature and emergence of virus initiating disease outbreaks, and by Culex
mosquitoes which are important amplification vectors.

Methodology and Principal Findings

Aedes aegypti and Culex quinquefasciatus mosquitoes were fed bloodmeals
containing the deletion mutant viruses. Two weeks post-exposure mosquitoes
were assayed for infection, dissemination, and transmission. In Ae. aegypti,
infection and transmission rates of the NSs deletion virus were similar to
wild type virus while dissemination rates were significantly reduced.
Infection and dissemination rates for the NSm deletion virus were lower
compared to wild type. Virus lacking both NSs and NSm failed to infect Ae.
aegypti. In Cx. quinquefasciatus, infection rates for viruses lacking NSm or
both NSs and NSm were lower than for wild type virus.

Conclusions/Significance

In both species, deletion of NSm or both NSs and NSm reduced the infection and
transmission potential of the virus. Deletion of both NSs and NSm resulted in
the highest level of attenuation of virus replication. Deletion of NSm alone
was sufficient to nearly abolish infection in Aedes aegypti mosquitoes,
indicating an important role for this protein. The double deleted viruses
represent an ideal vaccine profile in terms of environmental containment due
to lack of ability to efficiently infect and be transmitted by mosquitoes.

Author Summary

Rift Valley fever virus is transmitted mainly by mosquitoes and causes disease
in humans and animals throughout Africa and the Arabian Peninsula. The impact
of disease is large in terms of human illness and mortality, and economic
impact on the livestock industry. For these reasons, and because there is a
risk of this virus spreading to Europe and North America, it is important to
develop a vaccine that is stable, safe and effective in preventing infection.
Potential vaccine viruses have been developed through deletion of two genes
(NSs and NSm) affecting virus virulence. Because this virus is normally
transmitted by mosquitoes we must determine the effects of the deletions in
these vaccine viruses on their ability to infect and be transmitted by
mosquitoes. An optimal vaccine virus would not infect or be transmitted. The
viruses were tested in two mosquito species: Aedes aegypti and Culex
quinquefasciatus. Deletion of the NSm gene reduced infection of Ae. aegypti
mosquitoes indicating a role for the NSm protein in mosquito infection. The
virus with deletion of both NSs and NSm genes was the best vaccine candidate
since it did not infect Ae. aegypti and showed reduced infection</field></doc>
