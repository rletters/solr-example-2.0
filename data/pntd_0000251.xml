<doc>
<field name="uid">doi:10.1371/journal.pntd.0000251</field>
<field name="doi">10.1371/journal.pntd.0000251</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Sandra J. Laney, Caitlin J. Buttaro, Sabato Visconti, Nils Pilotte, Reda M. R. Ramzy, Gary J. Weil, Steven A. Williams</field>
<field name="title">A Reverse Transcriptase-PCR Assay for Detecting Filarial Infective Larvae in Mosquitoes</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">6</field>
<field name="pages">e251</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Existing molecular assays for filarial parasite DNA in mosquitoes cannot
distinguish between infected mosquitoes that contain any stage of the parasite
and infective mosquitoes that harbor third stage larvae (L3) capable of
establishing new infections in humans. We now report development of a
molecular L3-detection assay for Brugia malayi in vectors based on RT-PCR
detection of an L3-activated gene transcript.

Methodology/Principal Findings

Candidate genes identified by bioinformatics analysis of EST datasets across
the B. malayi life cycle were initially screened by PCR using cDNA libraries
as templates. Stage-specificity was confirmed using RNA isolated from infected
mosquitoes. Mosquitoes were collected daily for 14 days after feeding on
microfilaremic cat blood. RT-PCR was performed with primer sets that were
specific for individual candidate genes. Many promising candidates with strong
expression in the L3 stage were excluded because of low-level transcription in
less mature larvae. One transcript (TC8100, which encodes a particular form of
collagen) was only detected in mosquitoes that contained L3 larvae. This assay
detects a single L3 in a pool of 25 mosquitoes.

Conclusions/Significance

This L3-activated gene transcript, combined with a control transcript (tph-1,
accession # U80971) that is constitutively expressed by all vector-stage
filarial larvae, can be used to detect filarial infectivity in pools of
mosquito vectors. This general approach (detection of stage-specific gene
transcripts from eukaryotic pathogens) may also be useful for detecting
infective stages of other vector-borne parasites.

Author Summary

The Global Programme for the Elimination of Lymphatic Filariasis (GPELF) was
launched in the year 1998 with the goal of eliminating lymphatic filariasis by
2020. As the success of mass drug administration (MDA) in the global program
drives the rates of infection in endemic populations to very low levels, the
development of new, highly sensitive methods are required for monitoring
transmission by screening mosquitoes for the presence of L3 infective larvae.
The current method of mosquito dissection to identify L3 larvae is laborious
and insensitive and is not amenable to screening large numbers of mosquitoes.
Existing molecular assays for the detection of filarial parasite DNA in
mosquitoes are sensitive and can easily screen large numbers of vectors.
However, current PCR-based methods cannot distinguish between infected
mosquitoes that contain any stage of the parasite and infective mosquitoes
that harbor third stage larvae (L3) capable of establishing new infections in
humans. This paper reports the first development of a molecular L3-detection
assay for a filarial parasite in mosquitoes based on RT-PCR detection of an
L3-activated gene transcript. This strategy of detecting stage-specific
messenger RNA from filarial parasites may also prove useful for detecting
infective stages of other vector-borne pathogens.

Introduction

Lymphatic filariasis (LF) is a disabling tropical disease that is caused by
filarial nematode parasites that are transmitted by mosquitoes. Brugia malayi
and B. timori account for approximately 10% of the global LF burden of 120
million infected individuals [1]. The Globa</field></doc>
