<doc>
<field name="uid">doi:10.1371/journal.pntd.0001285</field>
<field name="doi">10.1371/journal.pntd.0001285</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Jennifer Keiser, Hanan Sayed, Maged El-Ghanam, Hoda Sabry, Saad Anani, Aly El-Wakeel, Christoph Hatz, Jürg Utzinger, Sayed Seif el-Din, Walaa El-Maadawy, Sanaa Botros</field>
<field name="title">Efficacy and Safety of Artemether in the Treatment of Chronic Fascioliasis in Egypt: Exploratory Phase-2 Trials</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">9</field>
<field name="pages">e1285</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Fascioliasis is an emerging zoonotic disease of considerable veterinary and
public health importance. Triclabendazole is the only available drug for
treatment. Laboratory studies have documented promising fasciocidal properties
of the artemisinins (e.g., artemether).

Methodology

We carried out two exploratory phase-2 trials to assess the efficacy and
safety of oral artemether administered at (i) 6×80 mg over 3 consecutive days,
and (ii) 3×200 mg within 24 h in 36 Fasciola-infected individuals in Egypt.
Efficacy was determined by cure rate (CR) and egg reduction rate (ERR) based
on multiple Kato-Katz thick smears before and after drug administration.
Patients who remained Fasciola-positive following artemether dosing were
treated with single 10 mg/kg oral triclabendazole. In case of treatment
failure, triclabendazole was re-administered at 20 mg/kg in two divided doses.

Principal Findings

CRs achieved with 6×80 mg and 3×200 mg artemether were 35% and 6%,
respectively. The corresponding ERRs were 63% and nil, respectively.
Artemether was well tolerated. A high efficacy was observed with
triclabendazole administered at 10 mg/kg (16 patients; CR: 67%, ERR: 94%) and
20 mg/kg (4 patients; CR: 75%, ERR: 96%).

Conclusions/Significance

Artemether, administered at malaria treatment regimens, shows no or only
little effect against fascioliasis, and hence does not represent an
alternative to triclabendazole. The role of artemether and other artemisinin
derivatives as partner drug in combination chemotherapy remains to be
elucidated.

Author Summary

Fasciola hepatica and F. gigantica are two liver flukes that parasitize
herbivorous large size mammals (e.g., sheep and cattle), as well as humans. A
single drug is available to treat infections with Fasciola flukes, namely,
triclabendazole. Recently, laboratory studies and clinical trials in sheep and
humans suffering from acute fascioliasis have shown that artesunate and
artemether (drugs that are widely used against malaria) also show activity
against fascioliasis. Hence, we were motivated to assess the efficacy and
safety of oral artemether in patients with chronic Fasciola infections. The
study was carried out in Egypt and artemether administered according to two
different malaria treatment regimens. Cure rates observed with 6×80 mg and
3×200 mg artemether were 35% and 6%, respectively. In addition, high efficacy
was observed when triclabendazole, the current drug of choice against human
fascioliasis, was administered to patients remaining Fasciola positive
following artemether treatment. Concluding, monotherapy with artemether does
not represent an alternative to triclabendazole against fascioliasis, but its
role in combination chemotherapy regimen remains to be investigated.

Introduction

Fascioliasis, a zoonotic disease caused by a liver fluke infection of the
species Fasciola hepatica and F. gigantica, is of considerable veterinary and
public health importance [1], [2]. Owing to global changes, infections with
Fasciola spp. appear to be emerging or re-emerging in several parts of the
world [1]. An estimated 91 million people are at risk of fascioliasis, whereas
the estimated number of inf</field></doc>
