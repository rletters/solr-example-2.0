<doc>
<field name="uid">doi:10.1371/journal.pntd.0000615</field>
<field name="doi">10.1371/journal.pntd.0000615</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Krishnamoorthy Gopinath, Sarman Singh</field>
<field name="title">Non-Tuberculous Mycobacteria in TB-Endemic Countries: Are We Neglecting the Danger?</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">4</field>
<field name="pages">e615</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Introduction

There are more than 120 members of the genus Mycobacterium, which are diverse
in pathogenicity, in vivo adaptation, virulence, response to drugs, and growth
characteristics. Mycobacteria other than M. tuberculosis complex and M.
leprosy are known as Non-Tuberculous Mycobacteria (NTM) and are known by
various acronyms. They attracted abrupt attention only after the AIDS
epidemic, but most of the reports were published from TB non-endemic countries
[1] and only rarely from TB-endemic countries. This is probably because the
chances of missing NTM species are higher in TB-endemic countries, which are
poorly equipped and overburdened with other diseases (Box 1). The information
regarding their true incidence and prevalence in these countries is scarce
[2]. In the absence of such authentic information, the current dogma has been
that the NTM are of the least consequence. However, we do not agree with this
myth and wish to present our viewpoint on this important aspect and emphasize
the need for a fresh look at this neglected aspect.

Box 1. Possible Factors for Under-reporting of NTM from TB-Endemic Countries

  * NTM infections are not reportable in any country.

  * Awareness is lacking among treating physicians and microbiologists.

  * Laboratory infrastructure is lacking for culture and identification of non-tuberculous mycobacteria.

  * High burden of TB and HIV attracts the bulk of the attention of the health care system; governmental fiscal inputs toward the costs of these neglected infections continue to be neglected.

  * Standardized or accepted criteria to define NTM respiratory disease are lacking.

Prevalence of NTM Infections before and after the AIDS Epidemic

We searched methodological search terms and phrases such as “non-tuberculous
mycobacteria and AIDS” in Medline records and found that 3,020 articles were
published between 1981 and 2009. Using the same phrase, only 59 articles were
published between 1900 and 1981, indicating a clear upsurge of NTM disease in
the post-AIDS era. However, most of these publications were from TB non-
endemic countries [1]–[5], but not much significance could be adhered to these
isolations [2]. The disseminated NTM infection is typically seen when the CD4+
T lymphocyte number falls below 50 µl. For this reason, it is argued that in
TB-HIV co-endemic countries, AIDS patients usually die of tuberculosis or
other infections before their CD4+ count falls low enough for NTM to cause a
disease (Box 2). Nevertheless, we feel that besides this argument, in a
majority of the patients, the diagnosis of NTM disease gets missed in these
countries.

Box 2. Facts about Non-tuberculous Mycobacterial Disease

  * AIDS patients are significantly more vulnerable to NTM infections due to severe T cell immunodeficiency.

  * Solid organ transplant patients, even though immunocompromised, are not at as high a risk as their HIV-positive counterparts.

  * Although some genetic and anatomical factors predispose to NTM, no proven associations have been proven among geographical, occupational, or ethnic factors and NTM infections.

  * Anatomical abnormalities and other co-morbidities such as chronic obstructive pulmonary disease (COPD), bronchiectasis, cystic fibrosis (CF), pneumoconiosis, past history of TB, pulmonary alveolar proteinosis, and esophageal motility disorders are </field></doc>
