<doc>
<field name="uid">doi:10.1371/journal.pntd.0000650</field>
<field name="doi">10.1371/journal.pntd.0000650</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Bruno B. Andrade, Antonio Reis-Filho, Sebastião Martins Souza-Neto, Imbroinise Raffaele-Netto, Luis M. A. Camargo, Aldina Barral, Manoel Barral-Netto</field>
<field name="title">Plasma Superoxide Dismutase-1 as a Surrogate Marker of Vivax Malaria Severity</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">4</field>
<field name="pages">e650</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Severe outcomes have been described for both Plasmodium falciparum and P.
vivax infections. The identification of sensitive and reliable markers of
disease severity is fundamental to improving patient care. An intense pro-
inflammatory response with oxidative stress and production of reactive oxygen
species is present in malaria. Inflammatory cytokines such as tumor necrosis
factor-alpha (TNF-alpha) and antioxidant agents such as superoxide dismutase-1
(SOD-1) are likely candidate biomarkers for disease severity. Here we tested
whether plasma levels of SOD-1 could serve as a biomarker of severe vivax
malaria.

Methodology/Principal Findings

Plasma samples were obtained from residents of the Brazilian Amazon with a
high risk for P. vivax transmission. Malaria diagnosis was made by both
microscopy and nested PCR. A total of 219 individuals were enrolled: non-
infected volunteers (n = 90) and individuals with vivax malaria: asymptomatic
(n = 60), mild (n = 50) and severe infection (n = 19). SOD-1 was directly
associated with parasitaemia, plasma creatinine and alanine amino-transaminase
levels, while TNF-alpha correlated only with the later enzyme. The predictive
power of SOD-1 and TNF-alpha levels was compared. SOD-1 protein levels were
more effective at predicting vivax malaria severity than TNF-alpha. For
discrimination of mild infection, elevated SOD-1 levels showed greater
sensitivity than TNF-alpha (76% vs. 30% respectively; p&lt;0.0001), with higher
specificity (100% vs. 97%; p&lt;0.0001). In predicting severe vivax malaria,
SOD-1 levels exhibited higher sensitivity than TNF-alpha (80% vs. 56%,
respectively; p&lt;0.0001; likelihood ratio: 7.45 vs. 3.14; p&lt;0.0001). Neither
SOD-1 nor TNF-alpha could discriminate P. vivax infections from those caused
by P. falciparum.

Conclusion

SOD-1 is a powerful predictor of disease severity in individuals with
different clinical presentations of vivax malaria.

Author Summary

Despite being considered a relatively benign disease, Plasmodium vivax
infection has been associated with fatal outcomes due to treatment failure or
inadequate health care. The identification of sensitive and reliable markers
of disease severity is important to improve the quality of patient care.
Although not imperative, a good marker should have a close causative
relationship with the disease pathogenesis. During acute malaria, an intense
inflammatory response and a well-documented oxidative burst are noted. Among
the free radicals released, superoxide anions account for the great majority.
The present study aimed to evaluate the reliability of using an antioxidant
enzyme, responsible for the clearance of superoxide anions, as a marker of
vivax malaria severity. Thus, we investigated individuals from an Amazonian
region highly endemic for vivax malaria with the goal of predicting infection
severity by measuring superoxide dismutase-1 (SOD-1) plasma levels. In
addition, we compared the predictive power SOD-1 to that of the tumor necrosis
factor (TNF)-alpha. SOD-1 was a more powerful predictor of disease severity
than TNF-alpha in individuals with different clinical presentations of vivax
malaria. This finding opens up new approaches in the initial screening of
</field></doc>
