<doc>
<field name="uid">doi:10.1371/journal.pntd.0001708</field>
<field name="doi">10.1371/journal.pntd.0001708</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Amy F. Savage, Gustavo C. Cerqueira, Sandesh Regmi, Yineng Wu, Najib M. El Sayed, Serap Aksoy</field>
<field name="title">Transcript Expression Analysis of Putative Trypanosoma brucei GPI-Anchored Surface Proteins during Development in the Tsetse and Mammalian Hosts</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">6</field>
<field name="pages">e1708</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Human African Trypanosomiasis is a devastating disease caused by the parasite
Trypanosoma brucei. Trypanosomes live extracellularly in both the tsetse fly
and the mammal. Trypanosome surface proteins can directly interact with the
host environment, allowing parasites to effectively establish and maintain
infections. Glycosylphosphatidylinositol (GPI) anchoring is a common
posttranslational modification associated with eukaryotic surface proteins. In
T. brucei, three GPI-anchored major surface proteins have been identified:
variant surface glycoproteins (VSGs), procyclic acidic repetitive protein
(PARP or procyclins), and brucei alanine rich proteins (BARP). The objective
of this study was to select genes encoding predicted GPI-anchored proteins
with unknown function(s) from the T. brucei genome and characterize the
expression profile of a subset during cyclical development in the tsetse and
mammalian hosts. An initial in silico screen of putative T. brucei proteins by
Big PI algorithm identified 163 predicted GPI-anchored proteins, 106 of which
had no known functions. Application of a second GPI-anchor prediction
algorithm (FragAnchor), signal peptide and trans-membrane domain prediction
software resulted in the identification of 25 putative hypothetical proteins.
Eighty-one gene products with hypothetical functions were analyzed for stage-
regulated expression using semi-quantitative RT-PCR. The expression of most of
these genes were found to be upregulated in trypanosomes infecting tsetse
salivary gland and proventriculus tissues, and 38% were specifically expressed
only by parasites infecting salivary gland tissues. Transcripts for all of the
genes specifically expressed in salivary glands were also detected in
mammalian infective metacyclic trypomastigotes, suggesting a possible role for
these putative proteins in invasion and/or establishment processes in the
mammalian host. These results represent the first large-scale report of the
differential expression of unknown genes encoding predicted T. brucei surface
proteins during the complete developmental cycle. This knowledge may form the
foundation for the development of future novel transmission blocking
strategies against metacyclic parasites.

Author Summary

Human African Trypanosomiasis (HAT) is a fatal disease caused by African
trypanosomes and transmitted by an infected tsetse fly. Presently, there are
no vaccines to prevent mammalian infections. Proteins expressed on the
trypanosome surface can influence the host environment and allow for their
transmission. Potentially accessible to the adaptive immune systems of
vertebrate hosts, these proteins could serve as future vaccine targets.
Identification and characterization of these currently unknown proteins can
help us develop strategies to alter the host environment, making it
inhospitable for the parasite, thereby reducing disease transmission. While
there is extensive knowledge about trypanosome development in the mammalian
host, less is known about the molecular events in the tsetse fly, particularly
the salivary gland stages. We used an in silico approach to identify putative
surface proteins from the known genome sequence of Trypanosoma brucei, and we
describe the stage specific </field></doc>
