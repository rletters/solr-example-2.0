<doc>
<field name="uid">doi:10.1371/journal.pntd.0000295</field>
<field name="doi">10.1371/journal.pntd.0000295</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Jennifer F. Friedman, Luz P. Acosta</field>
<field name="title">Toward Comprehensive Interventions to Improve the Health of Women of Reproductive Age</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">9</field>
<field name="pages">e295</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Background

In lesser-developed countries (LDCs), the causes of anaemia during pregnancy
are multi-factorial, yet much of the aetiological fraction of disease is
attributable to a few entities. Iron deficiency is the most common cause of
anaemia among pregnant women, resulting from both dietary insufficiency of
iron as well as losses through the gastrointestinal tract. These losses are
largely due to hookworm infection, but schistosomiasis at higher intensities
of infection may also lead to blood loss [1].

The argument for hookworm treatment during pregnancy as proposed by Brooker et
al. [2] and the WHO [3] is based largely on expected consequences of hookworm-
related iron deficiency anaemia for both mother and newborn. It is beyond the
scope of this commentary to address the complex relationship between
hemoglobin levels assessed at different stages of pregnancy and peri-natal
morbidity [4]. However, the relationship between iron status early in
pregnancy and birth outcomes is clearer and more relevant here. It is fairly
well established that iron deficiency anaemia present during the first
trimester of pregnancy is associated with a 2-fold risk of low birth weight
[5]. This risk is much lower among women with non–iron deficiency anaemia
during the first trimester, arguing for an important mechanistic role for iron
per se, discussed in greater detail in recent reviews [6]. In addition to its
effects on birth weight, transfer of iron to the developing fetus is
compromised among women with depleted iron stores [7]. Maternal iron
deficiency is related to decreased newborn and infant iron stores, as well as
increased risk of anaemia during infancy [4]. Given the established
relationship between hookworm and iron deficiency, hookworm treatment is
likely to positively affect maternal and infant health, though the timing of
treatment as well as provision of micro-nutrient supplementation are key
factors discussed further below.

A Systematic Review of Hookworm-Related Anaemia among Pregnant Women

Brooker and colleagues have conducted a timely and informative meta-analysis
examining the burden of hookworm among pregnant women. In this meta-analysis,
they have included cross-sectional, observational, and randomized controlled
trials to estimate the contribution of hookworm infection to maternal anaemia.
All studies that provided quantitative data on both hookworm intensity of
infection and hemoglobin were included, yielding 19 for inclusion. Overall,
hookworm infection during pregnancy was related to a standardized mean
difference in hemoglobin of −0.24 g/dL in comparing uninfected to lightly
infected women, and −0.57 g/dL in comparing lightly infected to heavily
infected women.

Strengths and Limitations of the Study

The limitations of this study are common to many meta-analyses, whereby the
quality of summary estimates are driven largely by the quality of the studies
included. This is particularly challenging in the case of meta-analyses of
cross-sectional and observational studies where bias and confounding are more
likely to play a role than in well-executed randomized controlled trials [8].
Approaches to this issue are to either judge the quality of studies and then
exclude based on a particular quality score, or include all studies with
careful consideration of the potential for confounding or bias in
interpretat</field></doc>
