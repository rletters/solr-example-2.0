<doc>
<field name="uid">doi:10.1371/journal.pntd.0000575</field>
<field name="doi">10.1371/journal.pntd.0000575</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Fernando Lucas de Melo, Joana Carvalho Moreira de Mello, Ana Maria Fraga, Kelly Nunes, Sabine Eggers</field>
<field name="title">Syphilis at the Crossroad of Phylogenetics and Paleopathology</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">1</field>
<field name="pages">e575</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

The origin of syphilis is still controversial. Different research avenues
explore its fascinating history. Here we employed a new integrative approach,
where paleopathology and molecular analyses are combined. As an exercise to
test the validity of this approach we examined different hypotheses on the
origin of syphilis and other human diseases caused by treponemes
(treponematoses). Initially, we constructed a worldwide map containing all
accessible reports on palaeopathological evidences of treponematoses before
Columbus&apos;s return to Europe. Then, we selected the oldest ones to calibrate
the time of the most recent common ancestor of Treponema pallidum subsp.
pallidum, T. pallidum subsp. endemicum and T. pallidum subsp. pertenue in
phylogenetic analyses with 21 genetic regions of different T. pallidum strains
previously reported. Finally, we estimated the treponemes&apos; evolutionary rate
to test three scenarios: A) if treponematoses accompanied human evolution
since Homo erectus; B) if venereal syphilis arose very recently from less
virulent strains caught in the New World about 500 years ago, and C) if it
emerged in the Americas between 16,500 and 5,000 years ago. Two of the
resulting evolutionary rates were unlikely and do not explain the existent
osseous evidence. Thus, treponematoses, as we know them today, did not emerge
with H. erectus, nor did venereal syphilis appear only five centuries ago.
However, considering 16,500 years before present (yBP) as the time of the
first colonization of the Americas, and approximately 5,000 yBP as the oldest
probable evidence of venereal syphilis in the world, we could not entirely
reject hypothesis C. We confirm that syphilis seems to have emerged in this
time span, since the resulting evolutionary rate is compatible with those
observed in other bacteria. In contrast, if the claims of precolumbian
venereal syphilis outside the Americas are taken into account, the place of
origin remains unsolved. Finally, the endeavor of joining paleopathology and
phylogenetics proved to be a fruitful and promising approach for the study of
infectious diseases.

Author Summary

Syphilis is a reemerging disease burden. Although it has been studied for five
centuries, its origin and spread is still controversial. Did it accompany the
evolution of the genus Homo and does it date back to more than a million years
or did it emerge only after Columbus&apos;s return to Europe? Initially, to test
the validity of a new interdisciplinary approach we constructed a worldwide
map showing precolumbian human skeletons with lesions of syphilis and other
related diseases (also caused by different treponemes). Then, we selected the
oldest cases to estimate the timing of the treponemes&apos; history, using their
DNA sequences and computer simulations. This resulted in treponeme
evolutionary rates, and in temporal intervals during which these
microorganisms could have emerged. Based on comparisons with other bacteria,
we concluded that treponematoses did not emerge before our own species
originated and that syphilis did not start affecting mankind only from 1492
onwards. Instead, it seems to have emerged in the time span between 16,500 and
5,000 years ago. Where syphilis emerged, however, remains unsolved. Finally,
the endeavor of joining as distinct fields a</field></doc>
