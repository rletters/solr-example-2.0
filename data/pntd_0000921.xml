<doc>
<field name="uid">doi:10.1371/journal.pntd.0000921</field>
<field name="doi">10.1371/journal.pntd.0000921</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Maria Angela Bianconcini Trindade, Gil Benard, Somei Ura, Cássio Cesar Ghidella, João Carlos Regazzi Avelleira, Francisco Reis Vianna, Alfredo Bolchat Marques, Ben Naafs, Raul Negrão Fleury</field>
<field name="title">Granulomatous Reactivation during the Course of a Leprosy Infection: Reaction or Relapse</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">12</field>
<field name="pages">e921</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Leprosy is a chronic granulomatous infectious disease and is still endemic in
many parts of the world. It causes disabilities which are the consequence of
nerve damage. This damage is in most cases the result of immunological
reactions.

Objectives

To investigate the differences between a type 1 leprosy (reversal) reaction
and relapse on using histopathology.

Methods

The histopathological changes in 167 biopsies from 66 leprosy patients were
studied. The patients were selected when their sequential biopsies
demonstrated either different patterns or maintained the same pattern of
granulomatous reaction over more than two years during or after the treatment
of leprosy.

Results

In 57 of the patients studied, a reactivation was seen which coincided with a
decrease in the bacteriological index (BI), suggesting that this reactivation
(reversal reaction or type 1 leprosy reaction) coincides with an effective
capacity for bacteriological clearance. In nine patients, an increase of the
bacteriologic index (IB) or persistence of solid bacilli occurred during the
reactivation, indicating proliferative activity, suggestive of a relapse. The
histopathological aspects of the granulomas were similar in both groups.

Conclusion

Bacterioscopy provided the only means to differentiate a reversal reaction
from a relapse in patients with granulomatous reactivation. The type 1 leprosy
reaction may be considered as a part effective immune reconstitution
(reversal, upgrading reaction) or as a mere hypersensitivity reaction
(downgrading reaction) in a relapse.

Author Summary

Leprosy is a serious infectious disease whose treatment still poses some
challenges. Patients are usually treated with a combination of antimicrobial
drugs called multidrug therapy. Although this treatment is effective against
Mycobacterium leprae, the bacillus that causes leprosy, patients may develop
severe inflammatory reactions during treatment. These reactions may be either
attributed to an improvement in the immunological reactivity of the patient
along with the treatment, or to relapse of the disease due to the
proliferation of remaining bacilli. In certain patients these two conditions
may be difficult to differentiate. The present study addresses the
histopathology picture of and the M. leprae bacilli in sequential biopsies
taken from lesions of patients who presented such reactions aiming to improve
the differentiation of the two conditions. This is important because these
reactions are one of the major causes of the disabilities of the patients with
leprosy, and should be treated early and appropriately. Our results show that
the histopathology picture alone is not sufficient, and that bacilli&apos;s
counting is necessary.

Introduction

Leprosy is a chronic infectious disease caused by Mycobacterium leprae and is
still endemic in many parts of the world. Circa 250 000 new cases were
reported in 2009 [1]. It affects nerves and skin, may cause deformities and
may evolve with acute exacerbations. The disease is the result of a
granulomatous reaction to bacilli living inside phagocytes; the host therefore
depends on cell mediated immunity for bacterial elimination. This response
</field></doc>
