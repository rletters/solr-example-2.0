<doc>
<field name="uid">doi:10.1371/journal.pntd.0000722</field>
<field name="doi">10.1371/journal.pntd.0000722</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Paul R. Torgerson, Krista Keller, Mellissa Magnotta, Natalie Ragland</field>
<field name="title">The Global Burden of Alveolar Echinococcosis</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">6</field>
<field name="pages">e722</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Human alveolar echinococcosis (AE) is known to be common in certain rural
communities in China whilst it is generally rare and sporadic elsewhere. The
objective of this study was to provide a first estimate of the global
incidence of this disease by country. The second objective was to estimate the
global disease burden using age and gender stratified incidences and estimated
life expectancy with the disease from previous results of survival analysis.
Disability weights were suggested from previous burden studies on
echinococcosis.

Methodology/Principal Findings

We undertook a detailed review of published literature and data from other
sources. We were unable to make a standardised systematic review as the
quality of the data was highly variable from different countries and hence if
we had used uniform inclusion criteria many endemic areas lacking data would
not have been included. Therefore we used evidence based stochastic techniques
to model uncertainty and other modelling and estimating techniques,
particularly in regions where data quality was poor. We were able to make an
estimate of the annual global incidence of disease and annual disease burden
using standard techniques for calculation of DALYs. Our studies suggest that
there are approximately 18,235 (CIs 11,900–28,200) new cases of AE per annum
globally with 16,629 (91%) occurring in China and 1,606 outside China. Most of
these cases are in regions where there is little treatment available and
therefore will be fatal cases. Based on using disability weights for hepatic
carcinoma and estimated age and gender specific incidence we were able to
calculate that AE results in a median of 666,434 DALYs per annum (CIs
331,000-1.3 million).

Conclusions/Significance

The global burden of AE is comparable to several diseases in the neglected
tropical disease cluster and is likely to be one of the most important
diseases in certain communities in rural China on the Tibetan plateau.

Author Summary

Human alveolar echinococcosis (AE), caused by the larval stage of the fox
tapeworm Echinococcus multilocularis, is amongst the world&apos;s most dangerous
zoonoses. Transmission to humans is by consumption of parasite eggs which are
excreted in the faeces of the definitive hosts: foxes and, increasingly, dogs.
Transmission can be through contact with the definitive host or indirectly
through contamination of food or possibly water with parasite eggs. We made an
intensive search of English, Russian, Chinese and other language databases. We
targeted data which could give country specific incidence or prevalence of
disease and searched for data from every country we believed to be endemic for
AE. We also used data from other sources (often unpublished). From this
information we were able to make an estimate of the annual global incidence of
disease and disease burden using standard techniques for calculation of DALYs.
Our studies suggest that AE results in a median of 18,235 cases globally with
a burden of 666,433 DALYs per annum. This is the first estimate of the global
burden of AE both in terms of global incidence and DALYs and demonstrates the
burden of AE is comparable to several diseases in the neglected tropical
disease cluster.

Introduction

Human alveolar echinococcosis (AE) is caused by the larval stage of the fox
tapeworm Echinococcus multilocularis. </field></doc>
