<doc>
<field name="uid">doi:10.1371/journal.pntd.0000228</field>
<field name="doi">10.1371/journal.pntd.0000228</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Renato B. Reis, Guilherme S. Ribeiro, Ridalva D. M. Felzemburgh, Francisco S. Santana, Sharif Mohr, Astrid X. T. O. Melendez, Adriano Queiroz, Andréia C. Santos, Romy R. Ravines, Wagner S. Tassinari, Marília S. Carvalho, Mitermayer G. Reis, Albert I. Ko</field>
<field name="title">Impact of Environment and Social Gradient on Leptospira Infection in Urban Slums</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">4</field>
<field name="pages">e228</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Leptospirosis has become an urban health problem as slum settlements have
expanded worldwide. Efforts to identify interventions for urban leptospirosis
have been hampered by the lack of population-based information on Leptospira
transmission determinants. The aim of the study was to estimate the prevalence
of Leptospira infection and identify risk factors for infection in the urban
slum setting.

Methods and Findings

We performed a community-based survey of 3,171 slum residents from Salvador,
Brazil. Leptospira agglutinating antibodies were measured as a marker for
prior infection. Poisson regression models evaluated the association between
the presence of Leptospira antibodies and environmental attributes obtained
from Geographical Information System surveys and indicators of socioeconomic
status and exposures for individuals. Overall prevalence of Leptospira
antibodies was 15.4% (95% confidence interval [CI], 14.0–16.8). Households of
subjects with Leptospira antibodies clustered in squatter areas at the bottom
of valleys. The risk of acquiring Leptospira antibodies was associated with
household environmental factors such as residence in flood-risk regions with
open sewers (prevalence ratio [PR] 1.42, 95% CI 1.14–1.75) and proximity to
accumulated refuse (1.43, 1.04–1.88), sighting rats (1.32, 1.10–1.58), and the
presence of chickens (1.26, 1.05–1.51). Furthermore, low income and black race
(1.25, 1.03–1.50) were independent risk factors. An increase of US$1 per day
in per capita household income was associated with an 11% (95% CI 5%–18%)
decrease in infection risk.

Conclusions

Deficiencies in the sanitation infrastructure where slum inhabitants reside
were found to be environmental sources of Leptospira transmission. Even after
controlling for environmental factors, differences in socioeconomic status
contributed to the risk of Leptospira infection, indicating that effective
prevention of leptospirosis may need to address the social factors that
produce unequal health outcomes among slum residents, in addition to improving
sanitation.

Author Summary

Leptospirosis, a life-threatening zoonotic disease, has become an important
urban slum health problem. Epidemics of leptospirosis now occur in cities
throughout the developing world, as the growth of slum settlements has
produced conditions for rat-borne transmission of this disease. In this
prevalence survey of more than 3,000 residents from a favela slum community in
Brazil, Geographical Information System (GIS) and modeling approaches
identified specific deficiencies in the sanitation infrastructure of slum
environments—open sewers, refuse, and inadequate floodwater drainage—that
serve as sources for Leptospira transmission. In addition to the environmental
attributes of the slum environment, low socioeconomic status was found to
independently contribute to the risk of infection. These findings indicate
that effective prevention of leptospirosis will need to address the social
factors that produce unequal health outcomes among slum residents, in addition
to improving sanitation.

Introduction

At present, one billi</field></doc>
