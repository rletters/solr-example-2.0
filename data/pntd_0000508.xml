<doc>
<field name="uid">doi:10.1371/journal.pntd.0000508</field>
<field name="doi">10.1371/journal.pntd.0000508</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Krisztian Magori, Mathieu Legros, Molly E. Puente, Dana A. Focks, Thomas W. Scott, Alun L. Lloyd, Fred Gould</field>
<field name="title">Skeeter Buster: A Stochastic, Spatially Explicit Modeling Tool for Studying Aedes aegypti Population Replacement and Population Suppression Strategies</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">9</field>
<field name="pages">e508</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Dengue is the most important mosquito-borne viral disease affecting humans.
The only prevention measure currently available is the control of its vectors,
primarily Aedes aegypti. Recent advances in genetic engineering have opened
the possibility for a new range of control strategies based on genetically
modified mosquitoes. Assessing the potential efficacy of genetic (and
conventional) strategies requires the availability of modeling tools that
accurately describe the dynamics and genetics of Ae. aegypti populations.

Methodology/Principal findings

We describe in this paper a new modeling tool of Ae. aegypti population
dynamics and genetics named Skeeter Buster. This model operates at the scale
of individual water-filled containers for immature stages and individual
properties (houses) for adults. The biology of cohorts of mosquitoes is
modeled based on the algorithms used in the non-spatial Container Inhabiting
Mosquitoes Simulation Model (CIMSiM). Additional features incorporated into
Skeeter Buster include stochasticity, spatial structure and detailed
population genetics. We observe that the stochastic modeling of individual
containers in Skeeter Buster is associated with a strongly reduced temporal
variation in stage-specific population densities. We show that heterogeneity
in container composition of individual properties has a major impact on
spatial heterogeneity in population density between properties. We detail how
adult dispersal reduces this spatial heterogeneity. Finally, we present the
predicted genetic structure of the population by calculating FST values and
isolation by distance patterns, and examine the effects of adult dispersal and
container movement between properties.

Conclusions/Significance

We demonstrate that the incorporated stochasticity and level of spatial detail
have major impacts on the simulated population dynamics, which could
potentially impact predictions in terms of control measures. The capacity to
describe population genetics confers the ability to model the outcome of
genetic control methods. Skeeter Buster is therefore an important tool to
model Ae. aegypti populations and the outcome of vector control measures.

Author Summary

Dengue is a viral disease that affects approximately 50 million people
annually, and is estimated to result in 12,500 fatalities. Dengue viruses are
vectored by mosquitoes, predominantly by the species Aedes aegypti. Because
there is currently no vaccine or specific treatment, the only available
strategy to reduce dengue transmission is to control the populations of these
mosquitoes. This can be achieved by traditional approaches such as
insecticides, or by recently developed genetic methods that propose the
release of mosquitoes genetically engineered to be unable to transmit dengue
viruses. The expected outcome of different control strategies can be compared
by simulating the population dynamics and genetics of mosquitoes at a given
location. Development of optimal control strategies can then be guided by the
modeling approach. To that end, we introduce a new modeling tool called
Skeeter Buster. This model describes the dynamics and the genetics of Ae.
aegypti populations at a very fine scale, </field></doc>
