<doc>
<field name="uid">doi:10.1371/journal.pntd.0000671</field>
<field name="doi">10.1371/journal.pntd.0000671</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Rebekah J. Kent, Mary B. Crabtree, Barry R. Miller</field>
<field name="title">Transmission of West Nile Virus by Culex quinquefasciatus Say Infected with Culex Flavivirus Izabal</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">5</field>
<field name="pages">e671</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The natural history and potential impact of mosquito-specific flaviviruses on
the transmission efficiency of West Nile virus (WNV) is unknown. The objective
of this study was to determine whether or not prior infection with Culex
flavivirus (CxFV) Izabal altered the vector competence of Cx. quinquefasciatus
Say for transmission of a co-circulating strain of West Nile virus (WNV) from
Guatemala.

Methods and Findings

CxFV-negative Culex quinquefasciatus and those infected with CxFV Izabal by
intrathoracic inoculation were administered WNV-infectious blood meals.
Infection, dissemination, and transmission of WNV were measured by plaque
titration on Vero cells of individual mosquito bodies, legs, or saliva,
respectively, two weeks following WNV exposure. Additional groups of Cx.
quinquefasciatus were intrathoracically inoculated with WNV alone or WNV+CxFV
Izabal simultaneously, and saliva collected nine days post inoculation. Growth
of WNV in Aedes albopictus C6/36 cells or Cx. quinquefasciatus was not
inhibited by prior infection with CxFV Izabal. There was no significant
difference in the vector competence of Cx. quinquefasciatus for WNV between
mosquitoes uninfected or infected with CxFV Izabal across multiple WNV blood
meal titers and two colonies of Cx. quinquefasciatus (p&gt;0.05). However,
significantly more Cx. quinquefasciatus from Honduras that were co-inoculated
simultaneously with both viruses transmitted WNV than those inoculated with
WNV alone (p = 0.0014). Co-inoculated mosquitoes that transmitted WNV also
contained CxFV in their saliva, whereas mosquitoes inoculated with CxFV alone
did not contain virus in their saliva.

Conclusions

In the sequential infection experiments, prior infection with CxFV Izabal had
no significant impact on WNV replication, infection, dissemination, or
transmission by Cx. quinquefasciatus, however WNV transmission was enhanced in
the Honduras colony when mosquitoes were inoculated simultaneously with both
viruses.

Author Summary

Unlike most known flaviviruses (Family, Flaviviridae: Genus, Flavivirus),
insect-only flaviviruses are a unique group of flaviviruses that only infect
invertebrates. The study of insect-only flaviviruses has increased in recent
years due to the discovery and characterization of numerous novel flaviviruses
from a diversity of mosquito species around the world. The widespread
discovery of these viruses has prompted questions regarding flavivirus
evolution and the potential impact of these viruses on the transmission of
flaviviruses of public health importance such as WNV. Therefore, we tested the
effect of Culex flavivirus Izabal (CxFV Izabal), an insect-only flavivirus
isolated from Culex quinquefasciatus mosquitoes in Guatemala, on the growth
and transmission of a strain of WNV isolated concurrently from the same
mosquito species and location. Prior infection of C6/36 (Aedes albopictus
mosquito) cells or Cx. quinquefasciatus with CxFV Izabal did not alter the
replication kinetics of WNV, nor did it significantly affect WNV infection,
dissemination, or transmission rates in two different colonies of mosquitoes
that were fed blood meals containing varying concentrations of WNV. These data
demonstrate that CxFV probably does not have a significant effect on WNV
transmission efficiency in nature.

Intro</field></doc>
