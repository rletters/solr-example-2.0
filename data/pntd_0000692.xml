<doc>
<field name="uid">doi:10.1371/journal.pntd.0000692</field>
<field name="doi">10.1371/journal.pntd.0000692</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Philippe Solano, Dramane Kaba, Sophie Ravel, Naomi A. Dyer, Baba Sall, Marc J. B. Vreysen, Momar T. Seck, Heather Darbyshir, Laetitia Gardes, Martin J. Donnelly, Thierry De Meeûs, Jérémy Bouyer</field>
<field name="title">Population Genetics as a Tool to Select Tsetse Control Strategies: Suppression or Eradication of Glossina palpalis gambiensis in the Niayes of Senegal</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">5</field>
<field name="pages">e692</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The Government of Senegal has initiated the “Projet de lutte contre les
glossines dans les Niayes” to remove the trypanosomosis problem from this area
in a sustainable way. Due to past failures to sustainably eradicate Glossina
palpalis gambiensis from the Niayes area, controversies remain as to the best
strategy implement, i.e. “eradication” versus “suppression.” To inform this
debate, we used population genetics to measure genetic differentiation between
G. palpalis gambiensis from the Niayes and those from the southern tsetse belt
(Missira).

Methodology/Principal Findings

Three different markers (microsatellite DNA, mitochondrial CO1 DNA, and
geometric morphometrics of the wings) were used on 153 individuals and
revealed that the G. p. gambiensis populations of the Niayes were genetically
isolated from the nearest proximate known population of Missira. The genetic
differentiation measured between these two areas (θ = 0.12 using
microsatellites) was equivalent to a between-taxa differentiation. We also
demonstrated that within the Niayes, the population from Dakar – Hann was
isolated from the others and had probably experienced a bottleneck.

Conclusion/Significance

The information presented in this paper leads to the recommendation that an
eradication strategy for the Niayes populations is advisable. This kind of
study may be repeated in other habitats and for other tsetse species to (i)
help decision on appropriate tsetse control strategies and (ii) find other
possible discontinuities in tsetse distribution.

Author Summary

Tsetse flies transmit trypanosomes to humans (sleeping sickness) and animals
(nagana). Controlling these vectors is a very efficient way to contain these
diseases. There are several strategies and methods that can be used for
control, each being more or less efficient depending on several factors. The
Government of Senegal wants to sustainably eliminate trypanosomosis from the
Niayes region by controlling the tsetse vector, Glossina palpalis gambiensis.
To reach this objective, two different strategies may be used: suppression
(decrease in tsetse densities) or eradication (remove all the tsetse in the
region until last one). For eradication, the approach has to be area-wide,
i.e. the control effort targets an entire pest population within a
circumscribed area, to avoid any possible reinvasion. Three different tools
(microsatellite DNA, mitochondrial DNA and morphometrics) were used, and all
showed an absence of gene flow between G. p. gambiensis from the Niayes and
from the nearest known population in the south east of the country (Missira).
This genetic isolation of the target population leads to the recommendation
that an eradication strategy for the Niayes populations is advisable. This
kind of study may be extended to other areas on other tsetse species.

Introduction

The Niayes of Senegal harbours the most northern and western population of
Glossina palpalis gambiensis Vanderplank, which is a major vector of the
debilitating diseases Human African Trypanosomosis (HAT) or sleeping sickness,
and African Animal Trypanosomosis (A</field></doc>
