<doc>
<field name="uid">doi:10.1371/journal.pntd.0001039</field>
<field name="doi">10.1371/journal.pntd.0001039</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Roshan Ramanathan, Sudhir Varma, José M. C. Ribeiro, Timothy G. Myers, Thomas J. Nolan, David Abraham, James B. Lok, Thomas B. Nutman</field>
<field name="title">Microarray-Based Analysis of Differential Gene Expression between Infective and Noninfective Larvae of Strongyloides stercoralis</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">5</field>
<field name="pages">e1039</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Differences between noninfective first-stage (L1) and infective third-stage
(L3i) larvae of parasitic nematode Strongyloides stercoralis at the molecular
level are relatively uncharacterized. DNA microarrays were developed and
utilized for this purpose.

Methods and Findings

Oligonucleotide hybridization probes for the array were designed to bind 3,571
putative mRNA transcripts predicted by analysis of 11,335 expressed sequence
tags (ESTs) obtained as part of the Nematode EST project. RNA obtained from S.
stercoralis L3i and L1 was co-hybridized to each array after labeling the
individual samples with different fluorescent tags. Bioinformatic predictions
of gene function were developed using a novel cDNA Annotation System software.
We identified 935 differentially expressed genes (469 L3i-biased; 466
L1-biased) having two-fold expression differences or greater and microarray
signals with a p value&lt;0.01. Based on a functional analysis, L1 larvae have a
larger number of genes putatively involved in transcription (p = 0.004), and
L3i larvae have biased expression of putative heat shock proteins (such as
hsp-90). Genes with products known to be immunoreactive in S. stercoralis-
infected humans (such as SsIR and NIE) had L3i biased expression. Abundantly
expressed L3i contigs of interest included S. stercoralis orthologs of
cytochrome oxidase ucr 2.1 and hsp-90, which may be potential chemotherapeutic
targets. The S. stercoralis ortholog of fatty acid and retinol binding
protein-1, successfully used in a vaccine against Ancylostoma ceylanicum, was
identified among the 25 most highly expressed L3i genes. The sperm-containing
glycoprotein domain, utilized in a vaccine against the nematode Cooperia
punctata, was exclusively found in L3i biased genes and may be a valuable S.
stercoralis target of interest.

Conclusions

A new DNA microarray tool for the examination of S. stercoralis biology has
been developed and provides new and valuable insights regarding differences
between infective and noninfective S. stercoralis larvae. Potential
therapeutic and vaccine targets were identified for further study.

Author Summary

Strongyloides stercoralis is a soil-transmitted helminth that affects an
estimated 30–100 million people worldwide. Chronically infected persons who
are exposed to corticosteroids can develop disseminated disease, which carries
a high mortality (87–100%) if untreated. Despite this, little is known about
the fundamental biology of this parasite, including the features that enable
infection. We developed the first DNA microarray for this parasite and used it
to compare infective third-stage larvae (L3i) with non-infective first stage
larvae (L1). Using this method, we identified 935 differentially expressed
genes. Functional characterization of these genes revealed L3i biased
expression of heat shock proteins and genes with products that have previously
been shown to be immunoreactive in infected humans. Genes putatively involved
in transcription were found to have L1 biased expression. Potential
chemotherapeutic and vaccine targets such as far-1, ucr 2.1 and hsp-90 were
identified for further study.

Introduction

Strongyloides stercoralis is </field></doc>
