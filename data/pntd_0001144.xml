<doc>
<field name="uid">doi:10.1371/journal.pntd.0001144</field>
<field name="doi">10.1371/journal.pntd.0001144</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Mariángela Vargas, Alvaro Segura, María Herrera, Mauren Villalta, Ricardo Estrada, Maykel Cerdas, Owen Paiva, Teatulohi Matainaho, Simon D. Jensen, Kenneth D. Winkel, Guillermo León, José María Gutiérrez, David J. Williams</field>
<field name="title">Preclinical Evaluation of Caprylic Acid-Fractionated IgG Antivenom for the Treatment of Taipan (Oxyuranus scutellatus) Envenoming in Papua New Guinea</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">5</field>
<field name="pages">e1144</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Snake bite is a common medical emergency in Papua New Guinea (PNG). The
taipan, Oxyuranus scutellatus, inflicts a large number of bites that, in the
absence of antivenom therapy, result in high mortality. Parenteral
administration of antivenoms manufactured in Australia is the current
treatment of choice for these envenomings. However, the price of these
products is high and has increased over the last 25 years; consequently the
country can no longer afford all the antivenom it needs. This situation
prompted an international collaborative project aimed at generating a new,
low-cost antivenom against O. scutellatus for PNG.

Methodology/Principal Findings

A new monospecific equine whole IgG antivenom, obtained by caprylic acid
fractionation of plasma, was prepared by immunising horses with the venom of
O. scutellatus from PNG. This antivenom was compared with the currently used
F(ab&apos;)2 monospecific taipan antivenom manufactured by CSL Limited, Australia.
The comparison included physicochemical properties and the preclinical
assessment of the neutralisation of lethal neurotoxicity and the myotoxic,
coagulant and phospholipase A2 activities of the venom of O. scutellatus from
PNG. The F(ab&apos;)2 antivenom had a higher protein concentration than whole IgG
antivenom. Both antivenoms effectively neutralised, and had similar potency,
against the lethal neurotoxic effect (both by intraperitoneal and intravenous
routes of injection), myotoxicity, and phospholipase A2 activity of O.
scutellatus venom. However, the whole IgG antivenom showed a higher potency
than the F(ab&apos;)2 antivenom in the neutralisation of the coagulant activity of
O. scutellatus venom from PNG.

Conclusions/Significance

The new whole IgG taipan antivenom described in this study compares favourably
with the currently used F(ab&apos;)2 antivenom, both in terms of physicochemical
characteristics and neutralising potency. Therefore, it should be considered
as a promising low-cost candidate for the treatment of envenomings by O.
scutellatus in PNG, and is ready to be tested in clinical trials.

Author Summary

Snake bite envenoming represents an important public health hazard in Papua
New Guinea (PNG). In the southern lowlands of the country the majority of
envenomings are inflicted by the taipan, Oxyuranus scutellatus. The only
currently effective treatment for these envenomings is the administration of
antivenoms manufactured in Australia. However, the price of these products in
PNG is very high and has steadily increased over the last 25 years, leading to
chronic antivenom shortages in this country. As a response to this situation,
an international partnership between PNG, Australia and Costa Rica was
initiated, with the aim of generating a new, low-cost antivenom for the
treatment of PNG taipan envenoming. Horses were immunised with the venom of O.
scutellatus from PNG and whole IgG was purified from the plasma of these
animals by caprylic acid precipitation of non-immunoglobulin proteins. The new
antivenom, manufactured by Instituto Clodomiro Picado (Costa Rica), was
compa</field></doc>
