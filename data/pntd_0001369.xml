<doc>
<field name="uid">doi:10.1371/journal.pntd.0001369</field>
<field name="doi">10.1371/journal.pntd.0001369</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Ranjan Ramasamy, Sinnathamby N. Surendran, Pavilupillai J. Jude, Sangaralingam Dharshini, Muthuladchumy Vinobaba</field>
<field name="title">Larval Development of Aedes aegypti and Aedes albopictus in Peri-Urban Brackish Water and Its Implications for Transmission of Arboviral Diseases</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">11</field>
<field name="pages">e1369</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Aedes aegypti (Linnaeus) and Aedes albopictus Skuse mosquitoes transmit
serious human arboviral diseases including yellow fever, dengue and
chikungunya in many tropical and sub-tropical countries. Females of the two
species have adapted to undergo preimaginal development in natural or
artificial collections of freshwater near human habitations and feed on human
blood. While there is an effective vaccine against yellow fever, the control
of dengue and chikungunya is mainly dependent on reducing freshwater
preimaginal development habitats of the two vectors. We show here that Ae.
aegypti and Ae. albopictus lay eggs and their larvae survive to emerge as
adults in brackish water (water with &lt;0.5 ppt or parts per thousand, 0.5–30
ppt and &gt;30 ppt salt are termed fresh, brackish and saline respectively).
Brackish water with salinity of 2 to 15 ppt in discarded plastic and glass
containers, abandoned fishing boats and unused wells in coastal peri-urban
environment were found to contain Ae. aegypti and Ae. albopictus larvae.
Relatively high incidence of dengue in Jaffna city, Sri Lanka was observed in
the vicinity of brackish water habitats containing Ae. aegypti larvae. These
observations raise the possibility that brackish water-adapted Ae. aegypti and
Ae. albopictus may play a hitherto unrecognized role in transmitting dengue,
chikungunya and yellow fever in coastal urban areas. National and
international health authorities therefore need to take the findings into
consideration and extend their vector control efforts, which are presently
focused on urban freshwater habitats, to include brackish water larval
development habitats.

Author Summary

Aedes aegypti and Aedes albopictus mosquitoes transmit arboviral disease like
dengue and chikungunya that are of international concern. Control of dengue
and chikungunya presently focuses on eliminating freshwater larval development
habitats of the two mosquitoes in urban surroundings. We investigated the
ability of the two mosquito species to lay eggs and undergo development into
larvae, pupae and adults in brackish water, and examined brackish water
collections in the peri-urban environment for the presence of larvae. The
results confirmed their ability to lay eggs and for the eggs to develop into
adults in brackish water. Their larvae were found in brackish water in
discarded food/beverage containers and abandoned boats as well as disused
wells. Such brackish water collections with larvae in Jaffna city, Sri Lanka
were found near areas of high dengue incidence. This hitherto unappreciated
potential contribution to arboviral disease transmission in urban areas is of
global significance. National and international health authorities need to
take these new findings into consideration in developing appropriate
strategies for controlling diseases transmitted by the two mosquito species.

Introduction

Aedes aegypti (Linnaeus) (Diptera: Culicidae) is the principal tropical
mosquito vector of arboviruses causing yellow fever, dengue and chikungunya
[1]–[3]. The related Aedes albopictus Skuse is a secondary vector of dengue
and chikungunya [1]–[4]. Unlike Ae. aegypti, Ae. albopictus possesses a
diapausing egg stage to survive winter</field></doc>
