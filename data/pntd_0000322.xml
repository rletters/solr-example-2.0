<doc>
<field name="uid">doi:10.1371/journal.pntd.0000322</field>
<field name="doi">10.1371/journal.pntd.0000322</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Peter Steinmann, Xiao-Nong Zhou, Zun-Wei Du, Jin-Yong Jiang, Shu-Hua Xiao, Zhong-Xing Wu, Hui Zhou, Jürg Utzinger</field>
<field name="title">Tribendimidine and Albendazole for Treating Soil-Transmitted Helminths, Strongyloides stercoralis and Taenia spp.: Open-Label Randomized Trial</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">10</field>
<field name="pages">e322</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Tribendimidine is an anthelminthic drug with a broad spectrum of activity. In
2004 the drug was approved by Chinese authorities for human use. The efficacy
of tribendimidine against soil-transmitted helminths (Ascaris lumbricoides,
hookworm, and Trichuris trichiura) has been established, and new laboratory
investigations point to activity against cestodes and Strongyloides ratti.

Methodology/Principal Findings

In an open-label randomized trial, the safety and efficacy of a single oral
dose of albendazole or tribendimidine (both drugs administered at 200 mg for
5- to 14-year-old children, and 400 mg for individuals ≥15 years) against
soil-transmitted helminths, Strongyloides stercoralis, and Taenia spp. were
assessed in a village in Yunnan province, People&apos;s Republic of China. The
analysis was on a per-protocol basis and the trial is registered with
controlled-trials.com (number ISRCTN01779485). Both albendazole and
tribendimidine were highly efficacious against A. lumbricoides and,
moderately, against hookworm. The efficacy against T. trichiura was low. Among
57 individuals who received tribendimidine, the prevalence of S. stercoralis
was reduced from 19.3% to 8.8% (observed cure rate 54.5%, p = 0.107), and that
of Taenia spp. from 26.3% to 8.8% (observed cure rate 66.7%, p = 0.014).
Similar prevalence reductions were noted among the 66 albendazole recipients.
Taking into account “new” infections discovered at treatment evaluation, which
were most likely missed pre-treatment due to the lack of sensitivity of
available diagnostic approaches, the difference between the drug-specific net
Taenia spp. cure rates was highly significant in favor of tribendimidine (p =
0.001). No significant adverse events of either drug were observed.

Conclusions/Significance

Our results suggest that single-dose oral tribendimidine can be employed in
settings with extensive intestinal polyparasitism, and its efficacy against A.
lumbricoides and hookworm was confirmed. The promising results obtained with
tribendimidine against S. stercoralis and Taenia spp. warrant further
investigations. In a next step, multiple-dose schedules should be evaluated.

Author Summary

More than a billion people are infected with intestinal worms and, in the
developing world, many individuals harbor several kinds of worms concurrently.
There are only a handful of drugs available for treatment, and drug efficacy
varies according to the worm species. We compared the efficacy of a single
oral dose of tribendimidine, a new broad-spectrum worm drug from China, with
the standard drug albendazole for the treatment of hookworm, large roundworm
(Ascaris lumbricoides), whipworm (Trichuris trichiura) and, for the first
time, Strongyloides stercoralis and tapeworm (Taenia spp.). Our single-blind
randomized trial was conducted in a village in Yunnan province, southwest
China. Both drugs showed high efficacy against A. lumbricoides and a moderate
efficacy against hookworm. Among 57 tribendimidine recipients, the prevalence
of S. stercoralis was reduced from 19.3% to 8.8%, and that of Taenia spp. from
26.3% to 8.8%. Similar prevalence reductions were noted among the 66
albendazole recipients. Taking </field></doc>
