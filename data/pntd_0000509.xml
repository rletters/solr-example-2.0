<doc>
<field name="uid">doi:10.1371/journal.pntd.0000509</field>
<field name="doi">10.1371/journal.pntd.0000509</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Christiane Giroud, Florence Ottones, Virginie Coustou, Denis Dacheux, Nicolas Biteau, Benjamin Miezan, Nick Van Reet, Mark Carrington, Felix Doua, Théo Baltz</field>
<field name="title">Murine Models for Trypanosoma brucei gambiense Disease Progression—From Silent to Chronic Infections and Early Brain Tropism</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">9</field>
<field name="pages">e509</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Human African trypanosomiasis (HAT) caused by Trypanosoma brucei gambiense
remains highly prevalent in west and central Africa and is lethal if left
untreated. The major problem is that the disease often evolves toward chronic
or asymptomatic forms with low and fluctuating parasitaemia producing
apparently aparasitaemic serological suspects who remain untreated because of
the toxicity of the chemotherapy. Whether the different types of infections
are due to host or parasite factors has been difficult to address, since T. b.
gambiense isolated from patients is often not infectious in rodents thus
limiting the variety of isolates.

Methodology/Principal findings

T. b. gambiense parasites were outgrown directly from the cerebrospinal fluid
of infected patients by in vitro culture and analyzed for their molecular
polymorphisms. Experimental murine infections showed that these isolates could
be clustered into three groups with different characteristics regarding their
in vivo infection properties, immune response and capacity for brain invasion.
The first isolate induced a classical chronic infection with a fluctuating
blood parasitaemia, an invasion of the central nervous system (CNS), a
trypanosome specific-antibody response and death of the animals within 6–8
months. The second group induced a sub-chronic infection resulting in a single
wave of parasitaemia after infection, followed by a low parasitaemia with no
parasites detected by microscope observations of blood but detected by PCR,
and the presence of a specific antibody response. The third isolate induced a
silent infection characterised by the absence of microscopically detectable
parasites throughout, but infection was detectable by PCR during the whole
course of infection. Additionally, specific antibodies were barely detectable
when mice were infected with a low number of this group of parasites. In both
sub-chronic and chronic infections, most of the mice survived more than one
year without major clinical symptoms despite an early dissemination and growth
of the parasites in different organs including the CNS, as demonstrated by
bioluminescent imaging.

Conclusions/Significance

Whereas trypanosome characterisation assigned all these isolates to the
homogeneous Group I of T. b. gambiense, they clearly induce very different
infections in mice thus mimicking the broad clinical diversity observed in HAT
due to T. b. gambiense. Therefore, these murine models will be very useful for
the understanding of different aspects of the physiopathology of HAT and for
the development of new diagnostic tools and drugs.

Author Summary

Trypanosoma brucei gambiense is responsible for more than 90% of reported
cases of human African trypanosomosis (HAT). Infection can last for months or
even years without major signs or symptoms of infection, but if left
untreated, sleeping sickness is always fatal. In the present study, different
T. b. gambiense field isolates from the cerebrospinal fluid of patients with
HAT were adapted to growth in vitro. These isolates belong to the homogeneous
Group 1 of T. b. gambiense, which is known to induce a chronic infection in
humans. In spite of this, these isola</field></doc>
