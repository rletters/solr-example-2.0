<doc>
<field name="uid">doi:10.1371/journal.pntd.0001495</field>
<field name="doi">10.1371/journal.pntd.0001495</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Michael J. Lehane, Serap Aksoy</field>
<field name="title">Control Using Genetically Modified Insects Poses Problems for Regulators</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">1</field>
<field name="pages">e1495</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Insects are the pre-eminent form of metazoan life on land, with as many as
1018 individuals alive at any one instant and over three-quarters of a million
species described. Although it is estimated that there are as many as 14,000
species that are blood feeders [1], only three to 400 species regularly
attract our attention [2]. Some of these are of immense importance to us, as
vector-borne diseases still form a huge burden on both the human population
(Table 1) and our domesticated animals.

Table 1

Vector-borne disease still forms a huge burden on humankind.

Prevalence

At Risk

DALYs

Major Vectors

Malaria

247 M

3.3 B

39 M

Anopheline mosquitoes

Leishmaniasis

12 M

350 M

2 M

Phlebotomine sandflies

Dengue

50 M

2.5 B

616 K

Culicine mosquitoes

Lymphatic filariasis

120 M

1.3 B

5.8 M

Mosquitoes

Sleeping sickness

30 K

70 M

1.5 M

Tsetse flies

Chagas disease

10 M

25 M

667 K

Triatomine bugs

An indication of the importance of some of the vector-borne diseases
afflicting man can be seen from these WHO-derived estimates
(http://www.who.int/mediacentre/factsheets/en/, accessed 3 October 2011; DALYs
[19]).

B, billion; K, thousand; M, million.

Much progress has been achieved in the control of some of these vector-borne
diseases by targeting the vector. The following are two good examples. First,
insecticide-treated mosquito nets (ITNs) have had a major impact in the
control of malaria, even in some of the most difficult control settings. The
evidence from large-scale assessments shows that households possessing ITNs
show a 20% reduction in prevalence of Plasmodium falciparum infection in
children under 5 and a 23% reduction in all-cause child mortality, findings
that were consistent across a range of transmission settings [3]. Second, the
Southern Cone Initiative has used indoor residual spraying against the
domesticated triatomine vectors of Chagas disease to immense effect [4]. As a
result, the overall distribution of Triatoma infestans in the Southern Cone
region has been reduced from well over 6 million km2 (1990 estimates) to
around 750,000 km2 mainly in the Chaco of northeast Argentina and Bolivia,
while Rhodnius prolixus has been almost entirely eliminated from Central
America, with all countries there now certified by the World Health
Organization (WHO) and Pan American Health Organization (PAHO) as free of
transmission due to this vector.

However, the emergence and spread of insecticide resistance [5] represents a
challenge to these successes and to other vector control activities, the vast
majority of which depend in one way or another on the use of insecticides. The
need for new insecticides (or novel means to use those we already have) and
for other non-insecticidal means of vector control is quite clear. A good
example of our need for new means of controlling insects is seen in dengue.
Without a vaccine or drugs, disease control efforts are centred on control of
the vector. But, because of the life histories of the vectors involved, the
methods we currently have are inadequate [6].

One non-insecticidal method of vector control, which incidentally shows much
promise for dengue control, is the use of genetically modified (GM) insects.
Serious discussion of whether GM insects could be used in control began as
soon as transgenic insects were first produced in the 1980s [7], and a range
of means by which th</field></doc>
