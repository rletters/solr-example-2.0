<doc>
<field name="uid">doi:10.1371/journal.pntd.0000844</field>
<field name="doi">10.1371/journal.pntd.0000844</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Geremew Tasew, Susanne Nylén, Thorsten Lieke, Befekadu Lemu, Hailu Meless, Nicolas Ruffin, Dawit Wolday, Abraham Asseffa, Hideo Yagita, Sven Britton, Hannah Akuffo, Francesca Chiodi, Liv Eidsmo</field>
<field name="title">Systemic FasL and TRAIL Neutralisation Reduce Leishmaniasis Induced Skin Ulceration</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">10</field>
<field name="pages">e844</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Cutaneous leishmaniasis (CL) is caused by Leishmania infection of dermal
macrophages and is associated with chronic inflammation of the skin. L.
aethiopica infection displays two clinical manifestations, firstly ulcerative
disease, correlated to a relatively low parasite load in the skin, and
secondly non-ulcerative disease in which massive parasite infiltration of the
dermis occurs in the absence of ulceration of epidermis. Skin ulceration is
linked to a vigorous local inflammatory response within the skin towards
infected macrophages. Fas ligand (FasL) and Tumor necrosis factor-related
apoptosis-inducing ligand (TRAIL) expressing cells are present in dermis in
ulcerative CL and both death ligands cause apoptosis of keratinocytes in the
context of Leishmania infection. In the present report we show a differential
expression of FasL and TRAIL in ulcerative and non-ulcerative disease caused
by L. aethiopica. In vitro experiments confirmed direct FasL- and TRAIL-
induced killing of human keratinocytes in the context of Leishmania-induced
inflammatory microenvironment. Systemic neutralisation of FasL and TRAIL
reduced ulceration in a model of murine Leishmania infection with no effect on
parasitic loads or dissemination. Interestingly, FasL neutralisation reduced
neutrophil infiltration into the skin during established infection, suggesting
an additional proinflammatory role of FasL in addition to direct keratinocyte
killing in the context of parasite-induced skin inflammation. FasL signalling
resulting in recruitment of activated neutrophils into dermis may lead to
destruction of the basal membrane and thus allow direct FasL mediated killing
of exposed keratinocytes in vivo. Based on our results we suggest that
therapeutic inhibition of FasL and TRAIL could limit skin pathology during CL.

Author Summary

Cutaneous leishmaniases are associated with parasite-induced inflammatory
lesions of the skin. The degree of clinical pathology is not associated with
parasitic burden; on the contrary, ulcerative lesions are associated with low
infectious load, and non-ulcerative lesions are associated with an abundant
parasite infiltration. Leishmania are intracellular parasites in mammalian
hosts and reside in macrophages in the deep layers of the skin, the dermis.
The exact mechanism of ulceration in CL is not known and Leishmania parasites
do not directly induce destruction of keratinocytes in the most superficial
layer of the skin, the epidermis. In this study we investigated if ulcerated
lesions were associated with higher expression of FasL- and TRAIL-induced
cell-death of keratinocytes. We found a higher expression of FasL and TRAIL in
human skin samples from ulcerative as compared to non-ulcerative
leishmaniasis. In a mouse model of ulcerative leishmaniasis neutralisation of
FasL and TRAIL reduced ulceration. We suggest that FasL and TRAIL participate
in the ulcer formation during leishmaniasis both as a chemoattractant of
activated neutrophils leading to tissue destruction and through direct killing
of keratinocytes. Possible approaches to use this concept in therapeutical
interventions with the aim to reduce immunopathology associated with
leishmaniasis are disc</field></doc>
