<doc>
<field name="uid">doi:10.1371/journal.pntd.0000659</field>
<field name="doi">10.1371/journal.pntd.0000659</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Elizabeth R. Sharlow, Todd A. Lyda, Heidi C. Dodson, Gabriela Mustata, Meredith T. Morris, Stephanie S. Leimgruber, Kuo-Hsiung Lee, Yoshiki Kashiwada, David Close, John S. Lazo, James C. Morris</field>
<field name="title">A Target-Based High Throughput Screen Yields Trypanosoma brucei Hexokinase Small Molecule Inhibitors with Antiparasitic Activity</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">4</field>
<field name="pages">e659</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The parasitic protozoan Trypanosoma brucei utilizes glycolysis exclusively for
ATP production during infection of the mammalian host. The first step in this
metabolic pathway is mediated by hexokinase (TbHK), an enzyme essential to the
parasite that transfers the γ-phospho of ATP to a hexose. Here we describe the
identification and confirmation of novel small molecule inhibitors of
bacterially expressed TbHK1, one of two TbHKs expressed by T. brucei, using a
high throughput screening assay.

Methodology/Principal Findings

Exploiting optimized high throughput screening assay procedures, we
interrogated 220,233 unique compounds and identified 239 active compounds from
which ten small molecules were further characterized. Computation chemical
cluster analyses indicated that six compounds were structurally related while
the remaining four compounds were classified as unrelated or singletons. All
ten compounds were ∼20-17,000-fold more potent than lonidamine, a previously
identified TbHK1 inhibitor. Seven compounds inhibited T. brucei blood stage
form parasite growth (0.03≤EC50&lt;3 µM) with parasite specificity of the
compounds being demonstrated using insect stage T. brucei parasites,
Leishmania promastigotes, and mammalian cell lines. Analysis of two
structurally related compounds, ebselen and SID 17387000, revealed that both
were mixed inhibitors of TbHK1 with respect to ATP. Additionally, both
compounds inhibited parasite lysate-derived HK activity. None of the compounds
displayed structural similarity to known hexokinase inhibitors or human
African trypanosomiasis therapeutics.

Conclusions/Significance

The novel chemotypes identified here could represent leads for future
therapeutic development against the African trypanosome.

Author Summary

African sleeping sickness is a disease found in sub-Saharan Africa that is
caused by the single-celled parasite Trypanosoma brucei. The drugs used widely
now to treat infections are 50 years old and notable for their toxicity,
emphasizing the need for development of new therapeutics. In the search for
potential drug targets, researchers typically focus on enzymes or proteins
that are essential to the survival of the infectious agent while being
distinct enough from the host to avoid accidental targeting of the host
enzyme. This work describes our research on one such trypanosome enzyme,
hexokinase, which is a protein that the parasite requires to make energy. Here
we describe the results of our search for inhibitors of the parasite enzyme.
By screening 220,223 compounds for anti-hexokinase activity, we have
identified new inhibitors of the parasite enzyme. Some of these are toxic to
trypanosomes while having no effect on mammalian cells, suggesting that they
may hold promise for the development of new anti-parasitic compounds.

Introduction

African sleeping sickness conjures historical images of disease-induced fatal
slumbering striking down men, women, and children, consequently decimating
villages of colonial Africa. Unfortunately, people living in many countries of
sub-Saharan Africa today know that African sleeping sickness is not a disease
of hist</field></doc>
