<doc>
<field name="uid">doi:10.1371/journal.pntd.0000771</field>
<field name="doi">10.1371/journal.pntd.0000771</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Claudia List, Weihong Qi, Eva Maag, Bruno Gottstein, Norbert Müller, Ingrid Felger</field>
<field name="title">Serodiagnosis of Echinococcus spp. Infection: Explorative Selection of Diagnostic Antigens by Peptide Microarray</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">8</field>
<field name="pages">e771</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Production of native antigens for serodiagnosis of helminthic infections is
laborious and hampered by batch-to-batch variation. For serodiagnosis of
echinococcosis, especially cystic disease, most screening tests rely on crude
or purified Echinococcus granulosus hydatid cyst fluid. To resolve limitations
associated with native antigens in serological tests, the use of standardized
and highly pure antigens produced by chemical synthesis offers considerable
advantages, provided appropriate diagnostic sensitivity and specificity is
achieved.

Methodology/Principal Findings

Making use of the growing collection of genomic and proteomic data, we applied
a set of bioinformatic selection criteria to a collection of protein sequences
including conceptually translated nucleotide sequence data of two related
tapeworms, Echinococcus multilocularis and Echinococcus granulosus. Our
approach targeted alpha-helical coiled-coils and intrinsically unstructured
regions of parasite proteins potentially exposed to the host immune system.
From 6 proteins of E. multilocularis and 5 proteins of E. granulosus, 45
peptides between 24 and 30 amino acids in length were designed. These peptides
were chemically synthesized, spotted on microarrays and screened for
reactivity with sera from infected humans. Peptides reacting above the cut-off
were validated in enzyme-linked immunosorbent assays (ELISA). Peptides
identified failed to differentiate between E. multilocularis and E. granulosus
infection. The peptide performing best reached 57% sensitivity and 94%
specificity. This candidate derived from Echinococcus multilocularis antigen
B8/1 and showed strong reactivity to sera from patients infected either with
E. multilocularis or E. granulosus.

Conclusions/Significance

This study provides proof of principle for the discovery of diagnostically
relevant peptides by bioinformatic selection complemented with screening on a
high-throughput microarray platform. Our data showed that a single peptide
cannot provide sufficient diagnostic sensitivity whereas pooling several
peptide antigens improved sensitivity; thus combinations of several peptides
may lead the way to new diagnostic tests that replace, or at least complement
conventional immunodiagnosis of echinococcosis. Our strategy could prove
useful for diagnostic developments in other pathogens.

Author Summary

Crude or purified, somatic or metabolic extracts of native antigens are
routinely used for the serodiagnosis of human helminthic infections. These
antigens are often cross-reactive, i.e., recognized by sera from patients
infected with heterologous helminth species. To overcome limitations in
antigen production, test sensitivity and specificity, chemically synthesized
peptides offer a pure and standardized alternative, provided they yield
acceptable operative characteristics. Ongoing genome and proteome work create
new resources for the identification of antigens. Making use of the growing
amount of genomic and proteomic data available in public databases, we tested
a bioinformatic procedure for the selection of potentially antigenic peptides
from a collection of protein sequences including conceptually translated
nucleotide sequence data of Echinococcus multilocularis and E. granulosus
(Plathyhelmin</field></doc>
