<doc>
<field name="uid">doi:10.1371/journal.pntd.0001324</field>
<field name="doi">10.1371/journal.pntd.0001324</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Sumudu Britton, Andrew F. van den Hurk, Russell J. Simmons, Alyssa T. Pyke, Judith A. Northill, James McCarthy, Joe McCormack</field>
<field name="title">Laboratory-Acquired Dengue Virus Infection—A Case Report</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">11</field>
<field name="pages">e1324</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Introduction

The WHO estimates there may be 50 million dengue virus (DENV) infections
worldwide every year, with the disease being endemic in more than 100
countries [1]. There has been a dramatic rise in the incidence of dengue in
recent decades, making this an arbovirus of major international public health
concern. Dengue viruses belong to the family Flaviviridae and are transmitted
between humans via infected female Aedes mosquitoes, particularly Aedes
aegypti. In the state of Queensland, Australia, infected travellers from
overseas have facilitated numerous DENV outbreaks [2], [3]. However, these
outbreaks are limited to the far north of the state, the only area of
Australia where Ae. aegypti occurs [4].

There have been case reports of non-vector, healthcare-associated transmission
of DENVs—four cases of percutaneous transmission via needlestick injuries,
mucocutaneous transmission through a blood splash to the face, vertical
transmission, and transmission via bone marrow transplant (summarised in [5]).
We report the first case to our knowledge of DENV infection acquired by a
laboratory scientist conducting mosquito infection and transmission
experiments.

The Case

The patient, a scientist at a research laboratory, was referred to a public
hospital emergency department by a general practitioner after presenting with
fever, myalgia, and a rash. The patient resided in an area of Australia where
Ae.aegypti has not been reported since the mid-1950 s [4]. The patient had
travelled to Argentina 4 weeks earlier but did not have recent contact with
similarly unwell persons or pets and had no other medical history of clinical
significance. Ten days prior to hospital admission, the patient had performed
a routine laboratory experiment involving the primary infection of colony
mosquitoes with DENV-type 2 (DENV-2) via an artificial membrane feeding
apparatus. During the procedure the patient had worn personal protective
equipment commensurate with what is required for working with DENV in
Australian laboratories, including gown, gloves, and eye protection [6]. The
patient reported a bite from an escaped non-bloodfed mosquito during that day
but denied needlestick injury or mucocutaneous contact with the blood/virus
mixture. Four days later, the patient developed high fever associated with
marked lethargy and fatigue, which progressed to myalgias and severe back pain
over the subsequent 48 hours. Three days after the onset of fever, a fine,
macular, blanching rash developed that was generalised and pruritic. Later
findings following hospital admission demonstrated evidence of neutropenia
(neutrophil count 0.7×109/L) and thrombocytopenia (platelet count 79×109/L).
Results of liver function tests also revealed elevated levels of alanine
aminotransferase (578 U/L) and aspartate aminotransferase (630 U/L). Ten days
following the onset of fever, DENV infection was confirmed by detection of
specific DENV-2 nucleic acid by real-time TaqMan reverse transcriptase
polymerase chain reaction (G. Smith, unpublished data) and anti-DENV-2 IgM
antibodies [7] in the patient&apos;s serum. Subsequent testing of a convalescent
phase sample collected 17 days after the first specimen further demonstrated
the presence of anti-DENV IgM antibodies. Of note, seroconversion of anti-
fl</field></doc>
