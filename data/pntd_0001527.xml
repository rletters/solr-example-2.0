<doc>
<field name="uid">doi:10.1371/journal.pntd.0001527</field>
<field name="doi">10.1371/journal.pntd.0001527</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Cathy Steel, Sudhir Varma, Thomas B. Nutman</field>
<field name="title">Regulation of Global Gene Expression in Human Loa loa Infection Is a Function of Chronicity</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">2</field>
<field name="pages">e1527</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Human filarial infection is characterized by downregulated parasite-antigen
specific T cell responses but distinct differences exist between patients with
longstanding infection (endemics) and those who acquired infection through
temporary residency or visits to filarial-endemic regions (expatriates).

Methods and Findings

To characterize mechanisms underlying differences in T cells, analysis of
global gene expression using human spotted microarrays was conducted on CD4+
and CD8+ T cells from microfilaremic Loa loa-infected endemic and expatriate
patients. Assessment of unstimulated cells showed overexpression of genes
linked to inflammation and caspase-associated cell death, particularly in
endemics, and enrichment of the Th1/Th2 canonical pathway in endemic CD4+
cells. However, pathways within CD8+ unstimulated cells were most
significantly enriched in both patient groups. Antigen (Ag)-driven gene
expression was assessed to microfilarial Ag (MfAg) and to the nonparasite Ag
streptolysin O (SLO). For MfAg-driven cells, the number of genes differing
significantly from unstimulated cells was greater in endemics compared to
expatriates (p&lt;0.0001). Functional analysis showed a differential increase in
genes associated with NFkB (both groups) and caspase activation (endemics).
While the expatriate response to MfAg was primarily a CD4+ pro-inflammatory
one, the endemic response included CD4+ and CD8+ cells and was linked to
insulin signaling, histone complexes, and ubiquitination. Unlike the
enrichment of canonical pathways in CD8+ unstimulated cells, both groups
showed pathway enrichment in CD4+ cells to MfAg. Contrasting with the
divergent responses to MfAg seen between endemics and expatriates, the CD4+
response to SLO was similar; however, CD8+ cells differed strongly in the
nature and numbers (156 [endemics] vs 36 [expatriates]) of genes with
differential expression.

Conclusions

These data suggest several important pathways are responsible for the
different outcomes seen among filarial-infected patients with varying levels
of chronicity and imply an important role for CD8+ cells in some of the global
changes seen with lifelong exposure.

Author Summary

Infection with the filarial parasite Loa loa causes a parasite-specific
downregulation of T cell responses. However, differences exist (clinical and
immunologic) between patients born and living in filarial endemic regions
(endemics) and those who become infected during travel or short-term residency
(expatriates). T cell responses are more depressed in endemics while
expatriates have more clinical “allergic-type” symptoms. In this study, we
showed that these differences reflect transcriptional differences within the T
cell compartment. Using microarrays, we examined global gene expression in
both CD4+ and CD8+ T cells of microfilaremic endemic and expatriate patients
and found differences not only ex vivo, but also to parasite and, for CD8+
cells, to nonparasite antigens. Functional analysis showed that endemic
patients expressed genes linked to inflammatory disease and caspase associated
cell death at homeostasis while expatriates tended to have a more activation-
induced gene profile at homeostasis and a CD4+ inflammatory response to
parasite antigen. Patient groups were similar in their CD4+ response to
nonparasite antigen</field></doc>
