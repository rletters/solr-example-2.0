<doc>
<field name="uid">doi:10.1371/journal.pntd.0000635</field>
<field name="doi">10.1371/journal.pntd.0000635</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Sandeep P. Kishore, Gloria Tavera, Peter J. Hotez</field>
<field name="title">The Global Health Crisis and Our Nation's Research Universities</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">2</field>
<field name="pages">e635</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
On September 14th, 2009, the presidents of five United States
universities—Boston University, Brown, Duke, Johns Hopkins, and the University
of Washington—and representatives of over 50 North American institutions
convened for the first meeting of the Consortium of Universities for Global
Health (http://www.cugh.org). The meeting was in response to the demonstrated
passion and interest of students in the field of global health and the
responses needed from universities to cope with increasing student interest in
this field. Of 37 institutions surveyed that feature global heath programs,
the number of undergraduate and master&apos;s level students studying in the field
has doubled since 2006. In this arena, growing student movements have helped
lead the way. Organizations such as Clinton Global Initiative Universities
have also successfully tapped into university student interest in global
public health outreach and research. To be sure, universities are well poised
to lead such a movement for global health: They are independent organizations,
boast central missions to promote public welfare, and possess copious
resources and knowledge to share with partner institutions globally [1].

All the while, what remains overlooked in this rapidly expanding global health
movement is real innovation for prevention and treatment of the diseases of
poverty; existing drugs, some more than 50 years old, accrue microbial
resistance and, on the whole, exist only in unadjusted dosages for pediatric
patients [2]. What&apos;s more, some drugs (e.g., the arsenicals and pentamadine)
exhibit toxicities that we would consider unacceptable if they were widely
used in the developed world.

The innovation gap for the diseases of poverty is growing at a frightening
pace. For instance, some estimates indicate that the total research and
development funding for diabetes is more than 15 times that of malaria, and
more than 100 times that of other parasitic infections such as hookworm,
elephantiasis, and schistosomiasis. Because these diseases almost exclusively
afflict the world&apos;s poorest people we must look to universities to provide
some of the leadership on this issue. Other funding bodies—including the US
National Institutes of Health (NIH)—are providing fresh capital for basic
science research, but more funds are needed to cope with the global burden of
neglected diseases [3]. Certainly, Dr. Francis Collins, the new NIH Director,
has affirmed a strong commitment to global health in his strategic vision. but
while we feel that he has made almost heroic efforts to ensure prioritization
of a global health research agenda, the harsh reality is that the NIH budget
has been essentially flat since 2003 [4]. The American Recovery and
Reinvestment Act committed to $10 billion of stimulus funds to the NIH, US$8.2
billion of which is to be directed to scientific research priorities [5].

Treating the full range of diseases prominent in the global health arena is
important. While we advocate an increase in the overall investment in global
health disease research, we specifically call for a new and prominent focus on
research for neglected tropical diseases (NTDs), a group of infections that
together rival the disease burdens of more widely known global health
epidemics, yet receive especially limited research and development [6]. The
most recent global </field></doc>
