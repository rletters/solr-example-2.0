<doc>
<field name="uid">doi:10.1371/journal.pntd.0000473</field>
<field name="doi">10.1371/journal.pntd.0000473</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Patrick William Woodburn, Lawrence Muhangi, Stephen Hillier, Juliet Ndibazza, Proscovia Bazanya Namujju, Moses Kizza, Christine Ameke, Nicolas Emojong Omoding, Mark Booth, Alison Mary Elliott</field>
<field name="title">Risk Factors for Helminth, Malaria, and HIV Infection in Pregnancy in Entebbe, Uganda</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">6</field>
<field name="pages">e473</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Infections during pregnancy may have serious consequences for both mother and
baby. Assessment of risk factors for infections informs planning of
interventions and analysis of the impact of infections on health outcomes.

Objectives

To describe risk factors for helminths, malaria and HIV in pregnant Ugandan
women before intervention in a trial of de-worming in pregnancy.

Methods

The trial recruited 2,507 pregnant women between April 2003 and November 2005.
Participants were interviewed and blood and stool samples obtained; location
of residence at enrolment was mapped. Demographic, socioeconomic, behavioral
and other risk factors were modelled using logistic regression.

Results

There was a high prevalence of helminth, malaria and HIV infection, as
previously reported. All helminths and malaria parasitemia were more common in
younger women, and education was protective against every infection. Place of
birth and/or tribe affected all helminths in a pattern consistent with the
geographical distribution of helminth infections in Uganda. Four different
geohelminths (hookworm, Trichuris, Ascaris and Trichostrongylus) showed a
downwards trend in prevalence during the enrolment period. There was a
negative association between hookworm and HIV, and between hookworm and low
CD4 count among HIV-positive women. Locally, high prevalence of
schistosomiasis and HIV occurred in lakeshore communities.

Conclusions

Interventions for helminths, malaria and HIV need to target young women both
in and out of school. Antenatal interventions for malaria and HIV infection
must continue to be promoted. Women originating from a high risk area for a
helminth infection remain at high risk after migration to a lower-risk area,
and vice versa, but overall, geohelminths seem to be becoming less common in
this population. High risk populations, such as fishing communities, require
directed effort against schistosomiasis and HIV infection.

Author Summary

Infections in pregnancy can cause miscarriage, stillbirth, maternal mortality,
and low birth weight and have other long-term complications for mother and
baby, although the full impact of many infections, particularly worm
infections, is not yet fully understood. There is a high burden of infectious
disease in many developing countries. In this analysis, we identified which
factors put pregnant women in Entebbe, Uganda, at particular risk for worm
infections, malaria, HIV, and, where possible, rarer infections including
syphilis. The women in this study, and their children, will be followed up to
determine the long-term effects of exposure of the fetus to these maternal
infections on health during childhood. The findings of this baseline analysis
will help in the interpretation of the long-term outcomes. The findings also
highlight which groups are most at risk of each infection, and this may help
in targeting interventions to prevent, treat, or mitigate the impact of
infections in pregnancy.

Introduction

Approximately one third of the world&apos;s population is infected with helminths
and, in recent years, helminth control has been the focus of renewed interest
on account of the concomitant direct disease burden [1],[</field></doc>
