<doc>
<field name="uid">doi:10.1371/journal.pntd.0000456</field>
<field name="doi">10.1371/journal.pntd.0000456</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Martin Montes, Cesar Sanchez, Kristien Verdonck, Jordan E. Lake, Elsa Gonzalez, Giovanni Lopez, Angelica Terashima, Thomas Nolan, Dorothy E. Lewis, Eduardo Gotuzzo, A. Clinton White Jr</field>
<field name="title">Regulatory T Cell Expansion in HTLV-1 and Strongyloidiasis Co-infection Is Associated with Reduced IL-5 Responses to Strongyloides stercoralis Antigen</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">6</field>
<field name="pages">e456</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Human strongyloidiasis varies from a chronic but limited infection in normal
hosts to hyperinfection in patients treated with corticosteroids or with
HTLV-1 co-infection. Regulatory T cells dampen immune responses to infections.
How human strongyloidiasis is controlled and how HTLV-1 infection affects this
control are not clear. We hypothesize that HTLV-1 leads to dissemination of
Strongyloides stercoralis infection by augmenting regulatory T cell numbers,
which in turn down regulate the immune response to the parasite.

Objective

To measure peripheral blood T regulatory cells and Strongyloides stercoralis
larval antigen-specific cytokine responses in strongyloidiasis patients with
or without HTLV-1 co-infection.

Methods

Peripheral blood mononuclear cells (PBMCs) were isolated from newly diagnosed
strongyloidiasis patients with or without HTLV-1 co-infection. Regulatory T
cells were characterized by flow cytometry using intracellular staining for
CD4, CD25 and FoxP3. PBMCs were also cultured with and without Strongyloides
larval antigens. Supernatants were analyzed for IL-5 production.

Results

Patients with HTLV-1 and Strongyloides co-infection had higher parasite
burdens. Eosinophil counts were decreased in the HTLV-1 and Strongyloides co-
infected subjects compared to strongyloidiasis-only patients (70.0 vs. 502.5
cells/mm3, p = 0.09, Mann-Whitney test). The proportion of regulatory T cells
was increased in HTLV-1 positive subjects co-infected with strongyloidiasis
compared to patients with only strongyloidiasis or asymptomatic HTLV-1
carriers (median = 17.9% vs. 4.3% vs. 5.9 p&lt;0.05, One-way ANOVA).
Strongyloides antigen-specific IL-5 responses were reduced in
strongyloidiasis/HTLV-1 co-infected patients (5.0 vs. 187.5 pg/ml, p = 0.03,
Mann-Whitney test). Reduced IL-5 responses and eosinophil counts were
inversely correlated to the number of CD4+CD25+FoxP3+ cells.

Conclusions

Regulatory T cell counts are increased in patients with HTLV-1 and
Strongyloides stercoralis co-infection and correlate with both low circulating
eosinophil counts and reduced antigen-driven IL-5 production. These findings
suggest a role for regulatory T cells in susceptibility to Strongyloides
hyperinfection.

Author Summary

Human strongyloidiasis varies from a mild, controlled infection to a severe
frequently fatal disseminated infection depending on the hosts. Patients
infected with the retrovirus HTLV-1 have more frequent and more severe forms
of strongyloidiasis. It is not clear how human strongyloidiasis is controlled
by the immune system and how HTLV-1 infection affects this control. We
hypothesize that HTLV-1 leads to dissemination of Strongyloides stercoralis by
augmenting regulatory T cell numbers, which in turn down regulate the immune
response to the parasite. In our study, patients with HTLV-1 and Strongyloides
co-infection had higher parasite burdens than patients with only
strongyloidiasis. Eosinophils play an essential role in control of
strongyloidiasis in animal models, and eosinophil counts were decreased in the
HTLV-1 and Strongyloides stercoralis co-infected subjects compa</field></doc>
