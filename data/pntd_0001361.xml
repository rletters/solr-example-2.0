<doc>
<field name="uid">doi:10.1371/journal.pntd.0001361</field>
<field name="doi">10.1371/journal.pntd.0001361</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Julie Perez, Fabrice Brescia, Jérôme Becam, Carine Mauron, Cyrille Goarant</field>
<field name="title">Rodent Abundance Dynamics and Leptospirosis Carriage in an Area of Hyper-Endemicity in New Caledonia</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">10</field>
<field name="pages">e1361</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Widespread but particularly incident in the tropics, leptospirosis is
transmitted to humans directly or indirectly by virtually any Mammal species.
However, rodents are recognized as the most important reservoir. In endemic
regions, seasonal outbreaks are observed during hot rainy periods. In such
regions, hot spots can be evidenced, where leptospirosis is “hyper-endemic”,
its incidence reaching 500 annual cases per 100,000. A better knowledge of how
rodent populations and their Leptospira prevalence respond to seasonal and
meteorological fluctuations might help implement relevant control measures.

Methodology/Principal Findings

In two tribes in New Caledonia with hyper-endemic leptospirosis, rodent
abundance and Leptospira prevalence was studied twice a year, in hot and cool
seasons for two consecutive years. Highly contrasted meteorological
situations, particularly rainfall intensities, were noted between the two hot
seasons studied. Our results show that during a hot and rainy period, both the
rodent populations and their Leptospira carriage were higher. This pattern was
more salient in commensal rodents than in the sylvatic rats.

Conclusions/Significance

The dynamics of rodents and their Leptospira carriage changed during the
survey, probably under the influence of meteorology. Rodents were both more
numerous and more frequently carrying (therefore disseminating) leptospires
during a hot rainy period, also corresponding to a flooding period with higher
risks of human exposure to waters and watered soils. The outbreaks of
leptospirosis in hyper-endemic areas could arise from meteorological
conditions leading to both an increased risk of exposure of humans and an
increased volume of the rodent reservoir. Rodent control measures would
therefore be most effective during cool and dry seasons, when rodent
populations and leptospirosis incidence are low.

Author Summary

In this study, we surveyed rodents and their Leptospira carriage in an area
where human leptospirosis is hyper-endemic. We evidenced the well-known
associations between specific rodent species and particular leptospires in
both mice and rats. Overall, the observed Leptospira prevalence was in the
range 18–47% depending on species, similar to other descriptions. However,
significant variations were observed both in the abundance of rodents and
their Leptospira carriage, one hot period with heavy rain being associated
with both a highest abundance and an increased prevalence. Similar
meteorological conditions could lead to increased leptospires dispersal by the
rodent reservoir and increased exposure of humans to risk situations (e.g.
flood, mud). Because rodent control measures were demonstrated elsewhere to be
cost-effective if correctly planned and implemented, this contribution to a
better knowledge of rodent and leptospires dynamics provides useful
information and may in turn allow to develop relevant rodent control actions
aimed at reducing the burden of human leptospirosis.

Introduction

Leptospirosis is an endemic bacterial disease in many tropical and sub-
tropical areas. Various Leptospira strains, maintained in different animal
species, are excreted in the urine of asymptomatic chronically infected
individuals [1]–[3]. Humans get infected when abraded skin </field></doc>
