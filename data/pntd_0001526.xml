<doc>
<field name="uid">doi:10.1371/journal.pntd.0001526</field>
<field name="doi">10.1371/journal.pntd.0001526</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Irene Zornetta, Paola Caccin, Julián Fernandez, Bruno Lomonte, José María Gutierrez, Cesare Montecucco</field>
<field name="title">Envenomations by Bothrops and Crotalus Snakes Induce the Release of Mitochondrial Alarmins</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">2</field>
<field name="pages">e1526</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Skeletal muscle necrosis is a common manifestation of viperid snakebite
envenomations. Venoms from snakes of the genus Bothrops, such as that of B.
asper, induce muscle tissue damage at the site of venom injection, provoking
severe local pathology which often results in permanent sequelae. In contrast,
the venom of the South American rattlesnake Crotalus durissus terrificus,
induces a clinical picture of systemic myotoxicity, i.e., rhabdomyolysis,
together with neurotoxicity. It is known that molecules released from damaged
muscle might act as ‘danger’ signals. These are known as ‘alarmins’, and
contribute to the inflammatory reaction by activating the innate immune
system. Here we show that the venoms of B. asper and C. d. terrificus release
the mitochondrial markers mtDNA (from the matrix) and cytochrome c (Cyt c)
from the intermembrane space, from ex vivo mouse tibialis anterior muscles.
Cyt c was released to a similar extent by the two venoms whereas B. asper
venom induced the release of higher amounts of mtDNA, thus reflecting hitherto
some differences in their pathological action on muscle mitochondria. At
variance, injection of these venoms in mice resulted in a different time-
course of mtDNA release, with B. asper venom inducing an early onset increment
in plasma levels and C. d. terrificus venom provoking a delayed release. We
suggest that the release of mitochondrial ‘alarmins’ might contribute to the
local and systemic inflammatory events characteristic of snakebite
envenomations.

Author Summary

Every year, hundreds of thousands of people in tropical and sub-tropical areas
of the world are bitten by poisonous snakes and may develop permanent damages.
This is a major tropical disease which is largely neglected by scientific and
clinical investigators. Snakes of Bothrops and Crotalus genus are responsible
of most cases in Latin America. Here for the first time, we have shown that
these venoms cause the release of both mitochondrial DNA and cytochrome c, two
well known alarmins. Moreover, the kinetic of these processes are in agreement
with the different pathophysiological profiles exhibited by Bothrops and
Crotalus envenomations. These elements suggest a correlation between snake
evenomations and sterile inflammatory syndrome. Alarmins are reported to have
a fundamental role in innate immune response and inflammation; they might
contribute to the local and systemic inflammatory events characteristic of
these envenomations opening a new prospective in the study of these complex
pathologies.

Introduction

Snakebite envenomation is a neglected tropical disease that affects each year
hundreds of thousands of individuals in tropical and sub-tropical areas of the
world [1][2]. In addition to death, many snake bitten patients develop
permanent physical and psychological sequelae which greatly affect their
quality of life [3][4][5][6].

In the Americas, species of the family Viperidae are responsible for the vast
majority of snakebite envenomations [7][5][8]. In Latin America, most cases
are inflicted by species of the genus Bothrops, among which the lance-head
vipers B. asper and B. atrox are very important in Central and South America,
respectively [7]. In addition, the rattlesnake Crotalus durissus is notorious
in So</field></doc>
