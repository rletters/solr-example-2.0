<doc>
<field name="uid">doi:10.1371/journal.pntd.0000537</field>
<field name="doi">10.1371/journal.pntd.0000537</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Hugh J. W. Sturrock, Diana Picon, Anthony Sabasio, David Oguttu, Emily Robinson, Mounir Lado, John Rumunu, Simon Brooker, Jan H. Kolaczinski</field>
<field name="title">Integrated Mapping of Neglected Tropical Diseases: Epidemiological Findings and Control Implications for Northern Bahr-el-Ghazal State, Southern Sudan</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">10</field>
<field name="pages">e537</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

There are few detailed data on the geographic distribution of most neglected
tropical diseases (NTDs) in post-conflict Southern Sudan. To guide
intervention by the recently established national programme for integrated NTD
control, we conducted an integrated prevalence survey for schistosomiasis,
soil-transmitted helminth (STH) infection, lymphatic filariasis (LF), and
loiasis in Northern Bahr-el-Ghazal State. Our aim was to establish which
communities require mass drug administration (MDA) with preventive
chemotherapy (PCT), rather than to provide precise estimates of infection
prevalence.

Methods and Findings

The integrated survey design used anecdotal reports of LF and proximity to
water bodies (for schistosomiasis) to guide selection of survey sites. In
total, 86 communities were surveyed for schistosomiasis and STH; 43 of these
were also surveyed for LF and loiasis. From these, 4834 urine samples were
tested for blood in urine using Hemastix reagent strips, 4438 stool samples
were analyzed using the Kato-Katz technique, and 5254 blood samples were
tested for circulating Wuchereria bancrofti antigen using
immunochromatographic card tests (ICT). 4461 individuals were interviewed
regarding a history of ‘eye worm’ (a proxy measure for loiasis) and 31 village
chiefs were interviewed regarding the presence of clinical manifestations of
LF in their community. At the village level, prevalence of Schistosoma
haematobium and S. mansoni ranged from 0 to 65.6% and from 0 to 9.3%,
respectively. The main STH species was hookworm, ranging from 0 to 70% by
village. Infection with LF and loiasis was extremely rare, with only four
individuals testing positive or reporting symptoms, respectively.
Questionnaire data on clinical signs of LF did not provide a reliable
indication of endemicity. MDA intervention thresholds recommended by the World
Health Organization were only exceeded for urinary schistosomiasis and
hookworm in a few, yet distinct, communities.

Conclusion

This was the first attempt to use an integrated survey design for this group
of infections and to generate detailed results to guide their control over a
large area of Southern Sudan. The approach proved practical, but could be
further simplified to reduce field work and costs. The results show that only
a few areas need to be targeted with MDA of PCT, thus confirming the
importance of detailed mapping for cost-effective control.

Author Summary

Integrated control of neglected tropical diseases (NTDs) is being scaled up in
a number of developing countries, because it is thought to be more cost-
effective than stand-alone control programmes. Under this approach, treatments
for onchocerciasis, lymphatic filariasis (LF), schistosomiasis, soil-
transmitted helminth (STH) infection, and trachoma are administered through
the same delivery structure and at about the same time. A pre-requisite for
implementation of integrated NTD control is information on where the targeted
diseases are endemic and to what extent they overlap. This information is
generated through surveys that can be labour-intensive and expensive. In
Southern Sudan, all of the above diseases except onchocerciasis</field></doc>
