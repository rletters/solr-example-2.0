<doc>
<field name="uid">doi:10.1371/journal.pntd.0000406</field>
<field name="doi">10.1371/journal.pntd.0000406</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Nicolas Praet, Niko Speybroeck, Rafael Manzanedo, Dirk Berkvens, Denis Nsame Nforninwe, André Zoli, Fabrice Quet, Pierre-Marie Preux, Hélène Carabin, Stanny Geerts</field>
<field name="title">The Disease Burden of Taenia solium Cysticercosis in Cameroon</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">3</field>
<field name="pages">e406</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Taenia solium cysticercosis is an important zoonosis in many developing
countries. Human neurocysticercosis is recognised as an important cause of
epilepsy in regions where the parasite occurs. However, it is largely
underreported and there is a lack of data about the disease burden. Because a
body of information on human and porcine cysticercosis in Cameroon is becoming
available, the present study was undertaken to calculate the impact of this
neglected zoonosis.

Methods

Both the cost and Disability Adjusted Life Year (DALY) estimations were
applied. All necessary parameters were collected and imported in R software.
Different distributions were used according to the type of information
available for each of the parameters.

Findings

Based on a prevalence of epilepsy of 3.6%, the number of people with
neurocysticercosis-associated epilepsy was estimated at 50,326 (95% CR
37,299–65,924), representing 1.0% of the local population, whereas the number
of pigs diagnosed with cysticercosis was estimated at 15,961 (95% CR
12,320–20,044), which corresponds to 5.6% of the local pig population. The
total annual costs due to T. solium cysticercosis in West Cameroon were
estimated at 10,255,202 Euro (95% CR 6,889,048–14,754,044), of which 4.7% were
due to losses in pig husbandry and 95.3% to direct and indirect losses caused
by human cysticercosis. The monetary burden per case of cysticercosis amounts
to 194 Euro (95% CR 147–253). The average number of DALYs lost was 9.0 per
thousand persons per year (95% CR 2.8–20.4).

Interpretation

This study provides an estimation of the costs due to T. solium cysticercosis
using country-specific parameters and including the human as well as the
animal burden of the zoonotic disease. A comparison with a study in South
Africa indicates that the cost of inactivity, influenced by salaries, plays a
predominant role in the monetary burden of T. solium cysticercosis. Therefore,
knowing the salary levels and the prevalence of the disease might allow a
rapid indication of the total cost of T. solium cysticercosis in a country.
Ascertaining this finding with additional studies in cysticercosis-endemic
countries could eventually allow the estimation of the global disease burden
of cysticercosis. The estimated number of DALYs lost due to the disease was
higher than estimates already available for some other neglected tropical
diseases. The total estimated cost and number of DALYs lost probably
underestimate the real values because the estimations have been based on
epilepsy as the only symptom of cysticercosis.

Author Summary

Taenia solium cysticercosis is a zoonotic disease occurring in many developing
countries. A relatively high prevalence in humans and pigs has been reported
in several parts of the world, but insufficient data are available on the
disease burden. Disease impact assessment needs detailed information on well-
defined epidemiological and economic parameters. Our work conducted in West
Cameroon over several years allowed us to collect the necessary information to
estimate the impact of the parasite on the human and animal populations in
this area using both cost and Disability Adjusted Life Year (DALY)
estimations. This study identified the prof</field></doc>
