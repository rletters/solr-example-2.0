<doc>
<field name="uid">doi:10.1371/journal.pntd.0000421</field>
<field name="doi">10.1371/journal.pntd.0000421</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Minoarisoa Rajerison, Sylvie Dartevelle, Lalao A. Ralafiarisoa, Idir Bitam, Dinh Thi Ngoc Tuyet, Voahangy Andrianaivoarimanana, Faridabano Nato, Lila Rahalison</field>
<field name="title">Development and Evaluation of Two Simple, Rapid Immunochromatographic Tests for the Detection of Yersinia pestis Antibodies in Humans and Reservoirs</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">4</field>
<field name="pages">e421</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Tools for plague diagnosis and surveillance are not always available and
affordable in most of the countries affected by the disease. Yersinia pestis
isolation for confirmation is time-consuming and difficult to perform under
field conditions. Serologic tests like ELISA require specific equipments not
always available in developing countries. In addition to the existing rapid
test for antigen detection, a rapid serodiagnostic assay may be useful for
plague control.

Methods/Principal Findings

We developed two rapid immunochromatography-based tests for the detection of
antibodies directed against F1 antigen of Y. pestis. The first test, SIgT,
which detects total Ig (IgT) anti-F1 in several species (S) (human and
reservoirs), was developed in order to have for the field use an alternative
method to ELISA. The performance of the SIgT test was evaluated with samples
from humans and animals for which ELISA was used to determine the presumptive
diagnosis of plague. SIgT test detected anti-F1 Ig antibodies in humans with a
sensitivity of 84.6% (95% CI: 0.76–0.94) and a specificity of 98% (95% CI:
0.96–1). In evaluation of samples from rodents and other small mammals, the
SlgT test had a sensitivity of 87.8% (95% CI: 0.80–0.94) and a specificity of
90.3% (95% CI: 0.86–0.93). Improved performance was obtained with samples from
dogs, a sentinel animal, with a sensitivity of 93% (95% CI: 0.82–1) and a
specificity of 98% (95% CI: 0.95–1.01). The second test, HIgM, which detects
human (H) IgM anti-F1, was developed in order to have another method for
plague diagnosis. Its sensitivity was 83% (95% CI: 0.75–0.90) and its
specificity about 100%.

Conclusion/Significance

The SIgT test is of importance for surveillance because it can detect Ig
antibodies in a range of reservoir species. The HIgM test could facilitate the
diagnosis of plague during outbreaks, particularly when only a single serum
sample is available.

Author Summary

Plague is due to the bacterium Yersinia pestis. It is accidentally transmitted
to humans by the bite of infected fleas. Currently, approximately 20
developing countries with very limited infrastructure are still affected. A
plague case was defined according to clinical, epidemiological and biological
features. Rapid diagnosis and surveillance of the disease are essential for
its control. Indeed, the delay of treatment is often rapidly fatal for
patients and outbreaks may occur. Bubo aspirate is the most appropriate
specimen in case of bubonic plague, but its collection is not always feasible.
The main current biological approaches for the diagnosis of human plague are
F1 antigen detection, serology for antibody detection by ELISA and Y. pestis
isolation. The biological diagnosis of plague remains a challenge because the
clinical signs are not specific. In this study, we developed some simple,
rapid and affordable tests able to detect specific plague antibodies. These
tests can be used as alternative methods for plague diagnosis in the field and
for plague surveillance.

Introduction

Plague, a bacterial infection caused by Yersinia pestis, is essentially a
zoonosis of small mammals such as</field></doc>
