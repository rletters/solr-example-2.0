<doc>
<field name="uid">doi:10.1371/journal.pntd.0000361</field>
<field name="doi">10.1371/journal.pntd.0000361</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Atupele P. Kapito-Tembo, Victor Mwapasa, Steven R. Meshnick, Young Samanyika, Dan Banda, Cameron Bowie, Sarah Radke</field>
<field name="title">Prevalence Distribution and Risk Factors for Schistosoma hematobium Infection among School Children in Blantyre, Malawi</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">1</field>
<field name="pages">e361</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Schistosomiasis is a public health problem in Malawi but estimates of its
prevalence vary widely. There is need for updated information on the extent of
disease burden, communities at risk and factors associated with infection at
the district and sub-district level to facilitate effective prioritization and
monitoring while ensuring ownership and sustainability of prevention and
control programs at the local level.

Methods and Findings

We conducted a cross-sectional study between May and July 2006 among pupils in
Blantyre district from a stratified random sample of 23 primary schools.
Information on socio-demographic factors, schistosomiasis symptoms and other
risk factors was obtained using questionnaires. Urine samples were examined
for Schistosoma hematobium ova using filtration method. Bivariate and multiple
logistic regressions with robust estimates were used to assess risk factors
for S. hematobium. One thousand one hundred and fifty (1,150) pupils were
enrolled with a mean age of 10.5 years and 51.5% of them were boys. One
thousand one hundred and thirty-nine (1,139) pupils submitted urine and S.
hematobium ova were detected in 10.4% (95%CI 5.43–15.41%). Male gender (OR
1.81; 95% CI 1.06–3.07), child&apos;s knowledge of an existing open water source
(includes river, dam, springs, lake, etc.) in the area (OR 1.90; 95% CI
1.14–3.46), history of urinary schistosomiasis in the past month (OR 3.65; 95%
CI 2.22–6.00), distance of less than 1 km from school to the nearest open
water source (OR 5.39; 95% CI 1.67–17.42) and age 8–10 years (OR 4.55; 95% CI
1.53–13.50) compared to those 14 years or older were associated with
infection. Using urine microscopy as a gold standard, the sensitivity and
specificity of self-reported hematuria was 68.3% and 73.6%, respectively.
However, the positive predictive value was low at 23.9% and was associated
with age.

Conclusion

The study provides an important update on the status of infection in this part
of sub-Saharan Africa and exemplifies the success of deliberate national
efforts to advance active participation in schistosomiasis prevention and
control activities at the sub-national or sub-district levels. In this
population, children who attend schools close to open water sources are at an
increased risk of infection and self-reported hematuria may still be useful in
older children in this region.

Author Summary

Schistosoma hematobium infection is a parasitic infection endemic in Malawi.
Schistosomiasis usually shows a focal distribution of infection and it is
important to identify communities at high risk of infection and assess
effectiveness of control programs. We conducted a survey in one district in
Malawi to determine prevalence and factors associated with S. hematobium
infection among primary school pupils. Using a questionnaire, information on
history of passing bloody urine and known risk factors associated with
infection was collected. Urine samples were collected and examined for S.
hematobium eggs. One thousand one hundred and fifty (1,150) pupils were
interviewed, and out of 1,139 pupils who submitted urine samples, 10.4% were
infected. Our data showed that male gender, child&apos;s knowledge of an existing
open wate</field></doc>
