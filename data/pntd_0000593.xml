<doc>
<field name="uid">doi:10.1371/journal.pntd.0000593</field>
<field name="doi">10.1371/journal.pntd.0000593</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Kristine J. Kines, Gabriel Rinaldi, Tunika I. Okatcha, Maria E. Morales, Victoria H. Mann, Jose F. Tort, Paul J. Brindley</field>
<field name="title">Electroporation Facilitates Introduction of Reporter Transgenes and Virions into Schistosome Eggs</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">2</field>
<field name="pages">e593</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The schistosome egg represents an attractive developmental stage at which to
target transgenes because of the high ratio of germ to somatic cells, because
the transgene might be propagated and amplified by infecting snails with the
miracidia hatched from treated eggs, and because eggs can be readily obtained
from experimentally infected rodents.

Methods/Findings

We investigated the utility of square wave electroporation to deliver
transgenes and other macromolecules including fluorescent (Cy3) short
interference (si) RNA molecules, messenger RNAs, and virions into eggs of
Schistosoma mansoni. First, eggs were incubated in Cy3-labeled siRNA with and
without square wave electroporation. Cy3-signals were detected by fluorescence
microscopy in eggs and miracidia hatched from treated eggs. Second,
electroporation was employed to introduce mRNA encoding firefly luciferase
into eggs. Luciferase activity was detected three hours later, whereas
luciferase was not evident in eggs soaked in the mRNA. Third, schistosome eggs
were exposed to Moloney murine leukemia virus virions (MLV) pseudotyped with
vesicular stomatitis virus glycoprotein (VSVG). Proviral transgenes were
detected by PCR in genomic DNA from miracidia hatched from virion-exposed
eggs, indicating the presence of transgenes in larval schistosomes that had
been either soaked or electroporated. However, quantitative PCR (qPCR)
analysis determined that electroporation of virions resulted in 2–3 times as
many copies of provirus in these schistosomes compared to soaking alone. In
addition, relative qPCR indicated a copy number for the proviral luciferase
transgene of ∼20 copies for 100 copies of a representative single copy
endogenous gene (encoding cathepsin D).

Conclusions

Square wave electroporation facilitates introduction of transgenes into the
schistosome egg. Electroporation was more effective for the transduction of
eggs with pseudotyped MLV than simply soaking the eggs in virions. These
findings underscore the potential of targeting the schistosome egg for germ
line transgenesis.

Author Summary

The genome sequences of two of the three major species of schistosomes are now
available. Molecular tools are needed to determine the importance of these new
genes. With this in mind, we investigated introduction of reporter transgenes
into schistosome eggs, with the longer-term aim of manipulation of schistosome
genes and gene functions. The egg is a desirable developmental stage for
genome manipulation, not least because it contains apparently accessible germ
cells. Introduction of transgenes into the germ cells of schistosome eggs
might result in transgenic schistosomes. However, the egg is surrounded by a
thick shell which might block access to entry of transgenes. We cultured eggs
in the presence of three types of reporter transgenes of increasing molecular
size, and in addition we tried to produce transient holes in the eggs by
electroporation to investigate whether the transgenes would more easily enter
the eggs. Electroporation of eggs appeared to allow entry of two larger types
of transgenes into cultured schistosome eggs, messenger RNA encoding firefly
luciferase, and retroviral virions. We anticipate that this approach,
electroporation </field></doc>
