<doc>
<field name="uid">doi:10.1371/journal.pntd.0001354</field>
<field name="doi">10.1371/journal.pntd.0001354</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Alejandra Nóbrega Martinez, Marcelo Ribeiro-Alves, Euzenir Nunes Sarno, Milton Ozório Moraes</field>
<field name="title">Evaluation of qPCR-Based Assays for Leprosy Diagnosis Directly in Clinical Specimens</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">10</field>
<field name="pages">e1354</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

The increased reliability and efficiency of the quantitative polymerase chain
reaction (qPCR) makes it a promising tool for performing large-scale screening
for infectious disease among high-risk individuals. To date, no study has
evaluated the specificity and sensitivity of different qPCR assays for leprosy
diagnosis using a range of clinical samples that could bias molecular results
such as difficult-to-diagnose cases. In this study, qPCR assays amplifying
different M. leprae gene targets, sodA, 16S rRNA, RLEP and Ag 85B were
compared for leprosy differential diagnosis. qPCR assays were performed on
frozen skin biopsy samples from a total of 62 patients: 21 untreated
multibacillary (MB), 26 untreated paucibacillary (PB) leprosy patients, as
well as 10 patients suffering from other dermatological diseases and 5 healthy
donors. To develop standardized protocols and to overcome the bias resulted
from using chromosome count cutoffs arbitrarily defined for different assays,
decision tree classifiers were used to estimate optimum cutoffs and to
evaluate the assays. As a result, we found a decreasing sensitivity for Ag 85B
(66.1%), 16S rRNA (62.9%), and sodA (59.7%) optimized assay classifiers, but
with similar maximum specificity for leprosy diagnosis. Conversely, the RLEP
assay showed to be the most sensitive (87.1%). Moreover, RLEP assay was
positive for 3 samples of patients originally not diagnosed as having leprosy,
but these patients developed leprosy 5–10 years after the collection of the
biopsy. In addition, 4 other samples of patients clinically classified as non-
leprosy presented detectable chromosome counts in their samples by the RLEP
assay suggesting that those patients either had leprosy that was misdiagnosed
or a subclinical state of leprosy. Overall, these results are encouraging and
suggest that RLEP assay could be useful as a sensitive diagnostic test to
detect M. leprae infection before major clinical manifestations.

Author Summary

Leprosy is a chronic infectious disease caused by Mycobacterium leprae an
obligate intracellular pathogen that can infect cells in skin and nerves.
Leprosy still affects 211,903 individuals per year worldwide and lead to
permanent nerve injury that is generally associated with late diagnosis. The
mechanisms of interaction between pathogen and the human host that leads to
active disease are complex and there is no gold standard to detect M. leprae
or host response that could early identify patients preventing severe forms of
the disease, extensive nerve damage and disabilities. Thus, diagnosis relies
mainly on clinical parameters and histopathological and bacteriological
sometimes help to ascertain clinical form of patients. But, recently, advances
in the genome of the pathogen provided extended information towards new
targets to design novel genetic or immunological markers. Also novel molecular
biology methods exhibiting higher sensitivity along with easy to handle
apparatus based on nucleic acid detection are available. Here, we test and
compare different assays for quantitative PCR (qPCR) designed to amplify
specific M. leprae targets enriching the test sample with difficult-to-
diagnose leprosy cases. Our results suggest that qPCR specially the one
targeting repetitive element (RLEP) could be used to </field></doc>
