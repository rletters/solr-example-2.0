<doc>
<field name="uid">doi:10.1371/journal.pntd.0001557</field>
<field name="doi">10.1371/journal.pntd.0001557</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Norbert Heinrich, Elmar Saathoff, Nina Weller, Petra Clowes, Inge Kroidl, Elias Ntinginya, Harun Machibya, Leonard Maboko, Thomas Löscher, Gerhard Dobler, Michael Hoelscher</field>
<field name="title">High Seroprevalence of Rift Valley Fever and Evidence for Endemic Circulation in Mbeya Region, Tanzania, in a Cross-Sectional Study</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">3</field>
<field name="pages">e1557</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The Rift Valley fever virus (RVFV) is an arthropod-borne phlebovirus. RVFV
mostly causes outbreaks among domestic ruminants with a major economic impact.
Human infections are associated with these events, with a fatality rate of
0.5–2%. Since the virus is able to use many mosquito species of temperate
climates as vectors, it has a high potential to spread to outside Africa.

Methodology/Principal Findings

We conducted a stratified, cross-sectional sero-prevalence survey in 1228
participants from Mbeya region, southwestern Tanzania. Samples were selected
from 17,872 persons who took part in a cohort study in 2007 and 2008. RVFV IgG
status was determined by indirect immunofluorescence. Possible risk factors
were analyzed using uni- and multi-variable Poisson regression models. We
found a unique local maximum of RVFV IgG prevalence of 29.3% in a study site
close to Lake Malawi (N = 150). The overall seroprevalence was 5.2%.
Seropositivity was significantly associated with higher age, lower socio-
economic status, ownership of cattle and decreased with distance to Lake
Malawi. A high vegetation density, higher minimum and lower maximum
temperatures were found to be associated with RVFV IgG positivity. Altitude of
residence, especially on a small scale in the high-prevalence area was
strongly correlated (PR 0.87 per meter, 95% CI = 0.80–0.94). Abundant surface
water collections are present in the lower areas of the high-prevalence site.
RVF has not been diagnosed clinically, nor an outbreak detected in the high-
prevalence area.

Conclusions

RVFV is probably circulating endemically in the region. The presence of
cattle, dense vegetation and temperate conditions favour mosquito propagation
and virus replication in the vector and seem to play major roles in virus
transmission and circulation. The environmental risk-factors that we
identified could serve to more exactly determine areas at risk for RVFV
endemicity.

Author Summary

We describe a high seropositivity rate for Rift Valley fever virus, in up to
29.3% of tested individuals from the shore of Lake Malawi in southwestern
Tanzania, and much lower rates from areas distant to the lake. Rift Valley
fever disease or outbreaks have not been observed there in the past, which
suggests that the virus is circulating under locally favorable conditions and
is either a non-pathogenic strain, or that occasional occurrence of disease is
missed. We were able to identify a low socio-economic status and cattle
ownership as possible socio-economic risk factors for an individual to be
seropositive. Environmental risk factors associated with seropositivity
include dense vegetation, and ambient land surface temperatures which may be
important for breeding success of the mosquitoes which transmit Rift Valley
fever, and for efficient multiplication of the virus in the mosquito. Low
elevation of the home, and proximity to Lake Malawi probably lead to abundant
surface water collections, which serve as breeding places for mosquitoes.
These findings will inform patient care in the areas close to Lake Malawi, and
may help to design models which predict low-level virus circulation.

</field></doc>
