<doc>
<field name="uid">doi:10.1371/journal.pntd.0000685</field>
<field name="doi">10.1371/journal.pntd.0000685</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Elizabeth J. Carlton, Michelle Hsiang, Yi Zhang, Sarah Johnson, Alan Hubbard, Robert C. Spear</field>
<field name="title">The Impact of Schistosoma japonicum Infection and Treatment on Ultrasound-Detectable Morbidity: A Five-Year Cohort Study in Southwest China</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">5</field>
<field name="pages">e685</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Ultrasonography allows for non-invasive examination of the liver and spleen
and can further our understanding of schistosomiasis morbidity.

Methodology/Principal Findings

We followed 578 people in Southwest China for up to five years. Participants
were tested for Schistosoma japonicum infection in stool and seven standard
measures of the liver and spleen were obtained using ultrasound to evaluate
the relationship between schistosomiasis infection and ultrasound-detectable
pathology, and the impact of targeted treatment on morbidity. Parenchymal
fibrosis, a network pattern of the liver unique to S. japonicum, was
associated with infection at the time of ultrasound (OR 1.40, 95% CI:
1.03–1.90) and infection intensity (test for trend, p = 0.002), adjusting for
age, sex and year, and more strongly associated with prior infection status
and intensity (adjusted OR 1.84, 95% CI: 1.30–2.60; test for trend: p&lt;0.001
respectively), despite prompt treatment of infections. While declines in
parenchymal fibrosis over time were statistically significant, only 28% of
individuals with severe parenchymal fibrosis (grades 2 or 3) at enrollment
reversed to normal or grade 1 within five years. Other liver abnormalities
were less consistently associated with S. japonicum infection.

Conclusions/Significance

Parenchymal fibrosis is an appropriate measure of S. japonicum morbidity and
can document reductions in disease following control efforts. Other ultrasound
measures may have limited epidemiological value in regions with similar
infection levels. Because severe fibrosis may not reverse quickly following
treatment, efforts to reduce exposure to S. japonicum should be considered in
combination with treatment to prevent schistosomiasis morbidity.

Author Summary

Schistosomiasis is a water-borne parasite that infects approximately 200
million people worldwide. Schistosoma japonicum, found in Asia, causes disease
by releasing eggs in the liver, leading to fibrosis, anemia, and, in children,
impaired growth. Ultrasound can assess liver pathology from schistosomiasis;
however more information is needed to evaluate the relevance of standard
ultrasound measures. We followed 578 people for up to five years, testing for
schistosomiasis infection and conducting ultrasound examinations to assess the
relationship between infection and seven ultrasound measures and to evaluate
the impact of treatment with anti-schistosomiasis chemotherapy (praziquantel)
on morbidity. All infections were promptly treated. Fibrosis of the liver
parenchyma, pathology unique to S. japonicum, was associated with
schistosomiasis infection, and was most advanced in people with high worm
burdens. Liver fibrosis declined significantly following treatment, but
reversal of severe liver fibrosis was rare. Other ultrasound measures were not
consistently related to schistosomiasis infection or treatment. These findings
suggest parenchymal fibrosis can be used to measure morbidity attributable to
S. japonicum and evaluate the impact of disease control efforts. Because
reversal of severe fibrosis was limited, disease control efforts will be most
effective if they can not only treat existing infections but also prevent new
infections.

Introd</field></doc>
