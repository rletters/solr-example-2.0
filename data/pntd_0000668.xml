<doc>
<field name="uid">doi:10.1371/journal.pntd.0000668</field>
<field name="doi">10.1371/journal.pntd.0000668</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">David G. Addiss, Jacky Louis-Charles, Jacquelin Roberts, Frederic LeConte, Joyanna M. Wendt, Marie Denise Milord, Patrick J. Lammie, Gerusa Dreyer</field>
<field name="title">Feasibility and Effectiveness of Basic Lymphedema Management in Leogane, Haiti, an Area Endemic for Bancroftian Filariasis</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">4</field>
<field name="pages">e668</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Approximately 14 million persons living in areas endemic for lymphatic
filariasis have lymphedema of the leg. Clinical studies indicate that repeated
episodes of bacterial acute dermatolymphangioadenitis (ADLA) lead to
progression of lymphedema and that basic lymphedema management, which
emphasizes hygiene, skin care, exercise, and leg elevation, can reduce ADLA
frequency. However, few studies have prospectively evaluated the effectiveness
of basic lymphedema management or assessed the role of compressive bandaging
for lymphedema in resource-poor settings.

Methodology/Principal Findings

Between 1995 and 1998, we prospectively monitored ADLA incidence and leg
volume in 175 persons with lymphedema of the leg who enrolled in a lymphedema
clinic in Leogane, Haiti, an area endemic for Wuchereria bancrofti. During the
first phase of the study, when a major focus of the program was to reduce leg
volume using compression bandages, ADLA incidence was 1.56 episodes per
person-year. After March 1997, when hygiene and skin care were systematically
emphasized and bandaging discouraged, ADLA incidence decreased to 0.48
episodes per person-year (P&lt;0.0001). ADLA incidence was significantly
associated with leg volume, stage of lymphedema, illiteracy, and use of
compression bandages. Leg volume decreased in 78% of patients; over the entire
study period, this reduction was statistically significant only for legs with
stage 2 lymphedema (P = 0.01).

Conclusions/Significance

Basic lymphedema management, which emphasized hygiene and self-care, was
associated with a 69% reduction in ADLA incidence. Use of compression bandages
in this setting was associated with an increased risk of ADLA. Basic
lymphedema management is feasible and effective in resource-limited areas that
are endemic for lymphatic filariasis.

Author Summary

Lymphatic filariasis is a parasitic disease that is spread by mosquitoes. In
tropical countries where lymphatic filariasis occurs, approximately 14 million
people suffer from chronic swelling of the leg, known as lymphedema. Repeated
episodes of bacterial skin infection (acute attacks) cause lymphedema to
progress to its disfiguring form, elephantiasis. To help achieve the goal of
eliminating lymphatic filariasis globally, the World Health Organization
recommends basic lymphedema management, which emphasizes hygiene, skin care,
exercise, and leg elevation. Its effectiveness in reducing acute attack
frequency, as well as the role of compressive bandaging, have not been
adequately evaluated in filariasis-endemic areas. Between 1995 and 1998, we
studied 175 people with lymphedema of the leg in Leogane, Haiti. During Phase
I of the study, when compression bandaging was used to reduce leg volume, the
average acute attack rate was 1.56 episodes per year; it was greater in people
who were illiterate and those who used compression bandages. After March 1997,
when hygiene and skin care were emphasized and bandaging discouraged, acute
attack frequency significantly decreased to 0.48 episodes per year. This study
highlights the effectiveness of hygiene and skin care, as well as limitations
of compressive bandaging, in managing lymphedema in filariasis-endem</field></doc>
