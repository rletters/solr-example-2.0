<doc>
<field name="uid">doi:10.1371/journal.pntd.0001573</field>
<field name="doi">10.1371/journal.pntd.0001573</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Melissa D. Conrad, Andrew W. Gorman, Julia A. Schillinger, Pier Luigi Fiori, Rossana Arroyo, Nancy Malla, Mohan Lal Dubey, Jorge Gonzalez, Susan Blank, William E. Secor, Jane M. Carlton</field>
<field name="title">Extensive Genetic Diversity, Unique Population Structure and Evidence of Genetic Exchange in the Sexually Transmitted Parasite Trichomonas vaginalis</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">3</field>
<field name="pages">e1573</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Trichomonas vaginalis is the causative agent of human trichomoniasis, the most
common non-viral sexually transmitted infection world-wide. Despite its
prevalence, little is known about the genetic diversity and population
structure of this haploid parasite due to the lack of appropriate tools. The
development of a panel of microsatellite makers and SNPs from mining the
parasite&apos;s genome sequence has paved the way to a global analysis of the
genetic structure of the pathogen and association with clinical phenotypes.

Methodology/Principal Findings

Here we utilize a panel of T. vaginalis-specific genetic markers to genotype
235 isolates from Mexico, Chile, India, Australia, Papua New Guinea, Italy,
Africa and the United States, including 19 clinical isolates recently
collected from 270 women attending New York City sexually transmitted disease
clinics. Using population genetic analysis, we show that T. vaginalis is a
genetically diverse parasite with a unique population structure consisting of
two types present in equal proportions world-wide. Parasites belonging to the
two types (type 1 and type 2) differ significantly in the rate at which they
harbor the T. vaginalis virus, a dsRNA virus implicated in parasite
pathogenesis, and in their sensitivity to the widely-used drug, metronidazole.
We also uncover evidence of genetic exchange, indicating a sexual life-cycle
of the parasite despite an absence of morphologically-distinct sexual stages.

Conclusions/Significance

Our study represents the first robust and comprehensive evaluation of global
T. vaginalis genetic diversity and population structure. Our identification of
a unique two-type structure, and the clinically relevant phenotypes associated
with them, provides a new dimension for understanding T. vaginalis
pathogenesis. In addition, our demonstration of the possibility of genetic
exchange in the parasite has important implications for genetic research and
control of the disease.

Author Summary

The human parasite Trichomonas vaginalis causes trichomoniasis, the world&apos;s
most common non-viral sexually transmitted infection. Research on T. vaginalis
genetic diversity has been limited by a lack of appropriate genotyping tools.
To address this problem, we recently published a panel of T. vaginalis-
specific genetic markers; here we use these markers to genotype isolates
collected from ten regions around the globe. We detect high levels of genetic
diversity, infer a two-type population structure, identify clinically relevant
differences between the two types, and uncover evidence of genetic exchange in
what was believed to be a clonal organism. Together, these results greatly
improve our understanding of the population genetics of T. vaginalis and
provide insights into the possibility of genetic exchange in the parasite,
with implications for the epidemiology and control of the disease. By taking
into account the existence of different types and their unique
characteristics, we can improve understanding of the wide range of symptoms
that patients manifest and better implement appropriate drug treatment. In
addition, by recogniz</field></doc>
