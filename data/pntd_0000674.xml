<doc>
<field name="uid">doi:10.1371/journal.pntd.0000674</field>
<field name="doi">10.1371/journal.pntd.0000674</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Marcos A. Rossi, Herbert B. Tanowitz, Lygia M. Malvestio, Mara R. Celes, Erica C. Campos, Valdecir Blefari, Cibele M. Prado</field>
<field name="title">Coronary Microvascular Disease in Chronic Chagas Cardiomyopathy Including an Overview on History, Pathology, and Other Proposed Pathogenic Mechanisms</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">8</field>
<field name="pages">e674</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

This review focuses on the short and bewildered history of Brazilian scientist
Carlos Chagas&apos;s discovery and subsequent developments, the anatomopathological
features of chronic Chagas cardiomyopathy (CCC), an overview on the
controversies surrounding theories concerning its pathogenesis, and studies
that support the microvascular hypothesis to further explain the pathological
features and clinical course of CCC. It is our belief that knowledge of this
particular and remarkable cardiomyopathy will shed light not only on the
microvascular involvement of its pathogenesis, but also on the pathogenetic
processes of other cardiomyopathies, which will hopefully provide a better
understanding of the various changes that may lead to an end-stage heart
disease with similar features. This review is written to celebrate the 100th
anniversary of the discovery of Chagas disease.

Introduction

Abnormalities of microcirculation have been demonstrated in several different
cardiomyopathies [1], [2], and microvascular spasm was proposed as a common
pathogenic mechanism for the development of the characteristic focal
myocytolytic necrosis in these cardiomyopathies [3]–[6].

We suggested that alterations in the microvasculature contributed to the
pathogenesis of experimental chronic Chagas cardiomyopathy (CCC) [7], [8].
Mice infected with Trypanosoma cruzi (T. cruzi) developed a chronic
cardiomyopathy similar to that observed in the chronic phase of Chagas disease
in humans. Aggregated platelets forming transient occlusive thrombi were found
in small epicardial and intramyocardial vessels associated with foci of
myocytolytic necrosis and degeneration with an inflammatory mononuclear
infiltrate and interstitial fibrosis. Soon afterwards, areas of focal vascular
constrictions, microaneurysm formation, and dilatation were demonstrated in
mice acutely infected with T. cruzi [9]. In 1990, we proposed the
participation of microcirculation via transient ischemia in the pathogenesis
of CCC [10]. At that time, two hypotheses regarding the pathogenesis of CCC
were being intensely investigated: parasympathetic intrinsic denervation as
the mechanism of cardioneuropathy [11]–[13] and the participation of
autoimmune mechanisms in the genesis of chronic fibrosing myocarditis
[14]–[16].

This review focuses on the short and bewildered history of Chagas&apos;s discovery
and subsequent developments, the anatomopathological features of CCC, an
overview of the controversies surrounding theories concerning the pathogenesis
of CCC, and studies that support the microvascular hypothesis that further
explains the pathology and clinical course of CCC. It is our belief that
knowledge of this particular and remarkable cardiomyopathy will shed light not
only on the microvascular involvement of its pathogenesis, but also on the
pathogenetic processes of other cardiomyopathies, which will hopefully provide
a better understanding of the various changes that may lead to an end-stage
heart disease with similar features. As stressed by Factor [17], “… the
similarity of Chagas disease to other dilated congestive cardiomyopathies,
particularly those due to viral etiology, should make awareness of the S</field></doc>
