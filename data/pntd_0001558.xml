<doc>
<field name="uid">doi:10.1371/journal.pntd.0001558</field>
<field name="doi">10.1371/journal.pntd.0001558</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Sebastian Ziewer, Marc P. Hübner, Bettina Dubben, Wolfgang H. Hoffmann, Odile Bain, Coralie Martin, Achim Hoerauf, Sabine Specht</field>
<field name="title">Immunization with L. sigmodontis Microfilariae Reduces Peripheral Microfilaraemia after Challenge Infection by Inhibition of Filarial Embryogenesis</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">3</field>
<field name="pages">e1558</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Lymphatic filariasis and onchocerciasis are two chronic diseases mediated by
parasitic filarial worms causing long term disability and massive
socioeconomic problems. Filariae are transmitted by blood-feeding mosquitoes
that take up the first stage larvae from an infected host and deliver it after
maturation into infective stage to a new host. After closure of vector control
programs, disease control relies mainly on mass drug administration with drugs
that are primarily effective against first stage larvae and require many years
of annual/biannual administration. Therefore, there is an urgent need for
alternative treatment ways, i.e. other effective drugs or vaccines.

Methodology/Principal Findings

Using the Litomosoides sigmodontis murine model of filariasis we demonstrate
that immunization with microfilariae together with the adjuvant alum prevents
mice from developing high microfilaraemia after challenge infection.
Immunization achieved 70% to 100% protection in the peripheral blood and in
the pleural space and furthermore strongly reduced the microfilarial load in
mice that remained microfilaraemic. Protection was associated with the
impairment of intrauterine filarial embryogenesis and with local and systemic
microfilarial-specific host IgG, as well as IFN-γ secretion by host cells from
the site of infection. Furthermore immunization significantly reduced adult
worm burden.

Conclusions/Significance

Our results present a tool to understand the immunological basis of vaccine
induced protection in order to develop a microfilariae-based vaccine that
reduces adult worm burden and prevents microfilaraemia, a powerful weapon to
stop transmission of filariasis.

Author Summary

Lymphatic filariasis is caused by parasitic filarial worms that are
transmitted by mosquitoes, requiring uptake of larvae and distribution into
the blood of the host. More than 120 million people are infected and about 30%
of these individuals suffer from clinical symptoms. Reduction in transmission
currently depends on mass drug administration, which has significantly reduced
transmission rates over the past years. However, despite repetitive rounds of
administration, transmission has not been eliminated completely from endemic
areas. In some infected individuals the immune system can partially control
the parasite, such that a proportion of infected individuals remain
microfilaria-negative, despite the presence of adult worms. Therefore
mechanisms must exist that are able to combat microfilaraemia. Identifying
such mechanisms would help to design vaccines against disease transmitting
microfilarial stages. Using the Litomosoides sigmodontis murine model of
filariasis research we show a successful immunization against the blood-
circulating larval stage that is responsible for arthropod-dependent
transmission of the disease. Reduced microfilaraemia was associated with
impairment of worm embryogenesis, with systemic and local microfilarial-
specific host IgG and with IFN-γ secretion by host cells at the site of
infection. These results raise hope for developing a microfilariae-based
vaccine, being a pivotal step towards eradicating filariasis.

Introduction

Infections</field></doc>
