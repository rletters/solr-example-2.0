<doc>
<field name="uid">doi:10.1371/journal.pntd.0000386</field>
<field name="doi">10.1371/journal.pntd.0000386</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Tore Lier, Gunnar S. Simonsen, Tianping Wang, Dabing Lu, Hanne H. Haukland, Birgitte J. Vennervald, Maria V. Johansen</field>
<field name="title">Low Sensitivity of the Formol-Ethyl Acetate Sedimentation Concentration Technique in Low-Intensity Schistosoma japonicum Infections</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">2</field>
<field name="pages">e386</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The endemic countries are in a diagnostic dilemma concerning Schistosoma
japonicum with increasing difficulties in diagnosing the infected individuals.
The formol-ethyl acetate sedimentation concentration technique is preferred by
many clinical microbiology laboratories for the detection of parasites in
stool samples. It is potentially more sensitive than the diagnostic methods
traditionally used.

Methodology/Principal Findings

We evaluated the technique for detection of low-intensity S. japonicum
infections in 106 stool samples from China and used a commercial kit, Parasep
Midi Faecal Parasite Concentrator. One stool sample and one serum sample were
collected from each person. As reference standard we used persons positive by
indirect hemagglutination in serum and positive by Kato-Katz thick smear
microscopy (three slides from a single stool), and/or the hatching test. We
found the sedimentation technique to have a sensitivity of only 28.6% and
specificity of 97.4%.

Conclusion/Significance

This study indicates that the sedimentation technique has little to offer in
the diagnosis of low-intensity S. japonicum infections, at least when only a
single stool sample is examined.

Author Summary

Schistosoma japonicum is parasitic fluke (worm) found in China, Indonesia and
the Philippines. A lot of effort has been put into combating the parasite, and
the result has been a large drop in the number of infected people over the
last decades. The average infected person also now has few worms, and hence
excretes few eggs in stool. This has made it increasingly difficult to get a
correct diagnosis by the diagnostic tests traditionally used. Tests based on
detecting eggs in stool can be false-negative and tests detecting antibodies
can be false-positive due to persisting antibodies or antibodies from other
worm infections. Hence there is a need for new diagnostic strategies. Formol-
ethyl acetate sedimentation concentration is a technique for detecting eggs in
stool by microscopy, but has not to our knowledge been evaluated for S.
japonicum. We compared the technique, using a single stool sample and a
commercial preparation kit, with three tests traditionally used in the endemic
countries (Kato-Katz thick smear, hatching test and indirect hemagglutination
antibody detection). The sedimentation technique detected disappointing few
positives and seems not to be an advantage in the diagnosis of low-intensity
S. japonicum infection, compared to the traditionally used tests.

Introduction

Schistosomiasis japonica is still a major public health problem, especially in
China, despite great achievements during the past 50 years in controlling this
parasitic disease. Diagnosis is a key for decision-making, both on individual
and community levels. The current epidemiologic situation in the Schistosoma
japonicum-endemic countries (China, the Philippines and Indonesia) has made
the use of many common diagnostic assays problematic [1]. Low-intensity
infections reduce the sensitivity of tests which demonstrate eggs in stool
such as the Kato-Katz thick smear [2] and the hatching test [3], especially
when only a single stool sample is available for examination. Antibody
detection tests suffer from </field></doc>
