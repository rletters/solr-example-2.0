<doc>
<field name="uid">doi:10.1371/journal.pntd.0001593</field>
<field name="doi">10.1371/journal.pntd.0001593</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Colin M. Fitzsimmons, Frances M. Jones, Alex Stearn, Iain W. Chalmers, Karl F. Hoffmann, Jakub Wawrzyniak, Shona Wilson, Narcis B. Kabatereine, David W. Dunne</field>
<field name="title">The Schistosoma mansoni Tegumental-Allergen-Like (TAL) Protein Family: Influence of Developmental Expression on Human IgE Responses</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">4</field>
<field name="pages">e1593</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

A human IgE response to Sm22.6 (a dominant IgE target in Schistosoma mansoni)
is associated with the development of partial immunity. Located inside the
tegument, the molecule belongs to a family of proteins from parasitic
platyhelminths, the Tegument-Allergen-Like proteins (TALs). In addition to
containing dynein-light-chain domains, these TALs also contain EF-hand domains
similar to those found in numerous EF-hand allergens.

Methodology/Principal Findings

S. mansoni genome searches revealed 13 members (SmTAL1-13) within the species.
Recent microarray data demonstrated they have a wide range of life-cycle
transcriptional profiles. We expressed SmTAL1 (Sm22.6), SmTAL2, 3, 4, 5 and 13
as recombinant proteins and measured IgE and IgG4 in 200 infected males (7–60
years) from a schistosomiasis endemic region in Uganda. For SmTAL1 and 3
(transcribed in schistosomula through adult-worms and adult-worms,
respectively) and SmTAL5 (transcribed in cercariae through adult-worms),
detectable IgE responses were rare in 7–9 year olds, but increased with age.
At all ages, IgE to SmTAL2 (expressed constitutively), was rare while anti-
SmTAL2 IgG4 was common. Levels of IgE and IgG4 to SmTAL4 and 13 (transcribed
predominantly in the cercariae/skin stage) were all low.

Conclusions

We have not measured SmTAL protein abundance or exposure in live parasites,
but the antibody data suggests to us that, in endemic areas, there is priming
and boosting of IgE to adult-worm SmTALs by occasional death of long-lived
worms, desensitization to egg SmTALs through continuous exposure to dying eggs
and low immunogenicity of larval SmTALs due to immunosuppression in the skin
by the parasite. Of these, it is the gradual increase in IgE to the worm
antigens that parallels age-dependent immunity seen in endemic areas.

Author Summary

Examining the genome of the parasitic worm Schistosoma mansoni, we have
identified and defined the structure of a family of 13 allergen-like proteins
from the outer layer (tegument) of the organism. We term these molecules the
S. mansoni Tegument-Allergen-Like proteins (SmTALs). During S. mansoni
infection the human host is exposed to skin-invading larvae, adult-worms
(living in the blood) and to parasite eggs. These life-stages have very
different sizes, tissue composition and gene expression. We have produced 6
SmTAL proteins with different life-cycle transcriptional patterns and measured
IgE antibody responses to them in 200 infected males from an S. mansoni
endemic area. The binding of IgE to foreign proteins is important in allergy
but also in defence against parasitic worms. Our results suggest that, in
these endemic areas, there is priming and boosting of IgE responses to adult-
worm SmTALs by the occasional death of long-lived worms, desensitization to
egg SmTALs due to continuous exposure to dying eggs and low immunogenicity of
larval SmTALs perhaps due to immunosuppression in the skin by the parasite.
Schistosome infection is a major health problem in many countries. Our work
provides insight into what provokes and controls the antibody responses
associated with human immunity to this parasite.

Introduction

Schistosomias</field></doc>
