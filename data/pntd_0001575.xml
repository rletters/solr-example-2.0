<doc>
<field name="uid">doi:10.1371/journal.pntd.0001575</field>
<field name="doi">10.1371/journal.pntd.0001575</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Xiangguo Qiu, Lisa Fernando, P. Leno Melito, Jonathan Audet, Heinz Feldmann, Gary Kobinger, Judie B. Alimonti, Steven M. Jones</field>
<field name="title">Ebola GP-Specific Monoclonal Antibodies Protect Mice and Guinea Pigs from Lethal Ebola Virus Infection</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">3</field>
<field name="pages">e1575</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Ebola virus (EBOV) causes acute hemorrhagic fever in humans and non-human
primates with mortality rates up to 90%. So far there are no effective
treatments available. This study evaluates the protective efficacy of 8
monoclonal antibodies (MAbs) against Ebola glycoprotein in mice and guinea
pigs. Immunocompetent mice or guinea pigs were given MAbs i.p. in various
doses individually or as pools of 3–4 MAbs to test their protection against a
lethal challenge with mouse- or guinea pig-adapted EBOV. Each of the 8 MAbs
(100 µg) protected mice from a lethal EBOV challenge when administered 1 day
before or after challenge. Seven MAbs were effective 2 days post-infection
(dpi), with 1 MAb demonstrating partial protection 3 dpi. In the guinea pigs
each MAb showed partial protection at 1 dpi, however the mean time to death
was significantly prolonged compared to the control group. Moreover, treatment
with pools of 3–4 MAbs completely protected the majority of animals, while
administration at 2–3 dpi achieved 50–100% protection. This data suggests that
the MAbs generated are capable of protecting both animal species against
lethal Ebola virus challenge. These results indicate that MAbs particularly
when used as an oligoclonal set are a potential therapeutic for post-exposure
treatment of EBOV infection.

Author Summary

Ebola virus (EBOV) causes acute hemorrhagic fever in humans and non-human
primates with mortality rates up to 90%. So far there are no effective
treatments available. This study evaluates the protective efficacy of 8
monoclonal antibodies (MAbs) against the Ebola virus surface glycoprotein, in
mice and guinea pigs. Various combinations and doses of the neutralizing and
non-neutralizing MAbs were tested, and a post-exposure treatment protocol was
determined. There was 100% survival when guinea pigs received a mix of 3
neutralizing MAbs two days after a challenge with 1,000 LD50 of guinea pig-
adapted EBOV. This data suggests that the MAbs generated are effective as a
post-exposure therapeutic for a lethal Ebola virus infection. Development of a
post-exposure therapeutic for an Ebola virus infection is vital due to the
high lethality of the disease, the relative speed in which it kills, and the
fact that no vaccine has been approved for human use. Additionally, is it
unlikely that preventative vaccines will be employed, because Ebola virus
infections occur primarily in Africa, and to date have only killed
approximately 2,300 people making it financially unfeasible for a mass
vaccination. Therefore, having an effective therapy in the event of an
outbreak would be extremely beneficial.

Introduction

Ebola virus (EBOV) is a filovirus causing severe viral haemorrhagic fever in
humans and non-human primates (NHPs) [1]. There are five species of EBOV:
Zaire ebolavirus (ZEBOV), Sudan ebolavirus (SEBOV), Cote d&apos;Ivoire ebolavirus
(CIEBOV), Reston ebolavirus (REBOV), and Bundibugyo ebolavirus (BEBOV) [2].
ZEBOV has the highest virulence with a case fatality rate of 60–90% [1], [3].
Although several attempts have been made to treat EBOV infections [4]–[8],
there are currently no commercially approved vaccines or effective therapies,
therefore new treatments are needed. Several studies have bee</field></doc>
