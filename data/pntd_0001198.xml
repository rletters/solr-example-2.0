<doc>
<field name="uid">doi:10.1371/journal.pntd.0001198</field>
<field name="doi">10.1371/journal.pntd.0001198</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Joanna R. Santos-Oliveira, Eduardo G. Regis, Cássia R. B. Leal, Rivaldo V. Cunha, Patrícia T. Bozza, Alda M. Da-Cruz</field>
<field name="title">Evidence That Lipopolisaccharide May Contribute to the Cytokine Storm and Cellular Activation in Patients with Visceral Leishmaniasis</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">7</field>
<field name="pages">e1198</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Visceral leishmaniasis (VL) is characterized by parasite-specific
immunosuppression besides an intense pro-inflammatory response.
Lipopolisaccharide (LPS) has been implicated in the immune activation of
T-cell deficient diseases such as HIV/AIDS and idiopathic lymphocytopenia. The
source of LPS is gram-negative bacteria that enter the circulation because of
immunological mucosal barrier breakdown. As gut parasitization also occurs in
VL, it was hypothesized that LPS may be elevated in leishmaniasis,
contributing to cell activation.

Methodology/Principal Findings

Flow cytometry analysis and immunoassays (ELISA and luminex micro-beads
system) were used to quantify T-cells and soluble factors. Higher LPS and
soluble CD14 levels were observed in active VL in comparison to healthy
subjects, indicating that LPS was bioactive; there was a positive correlation
between these molecules (r = 0.61;p&lt;0.05). Interestingly, LPS was negatively
correlated with CD4+ (r = −0.71;p&lt;0.01) and CD8+ T-cells (r = −0.65;p&lt;0.05).
Moreover, higher levels of activation-associated molecules (HLA-DR, CD38,
CD25) were seen on T lymphocytes, which were positively associated with LPS
levels. Pro-inflammatory cytokines and macrophage migration inhibitory factor
(MIF) were also augmented in VL patients. Consistent with the higher immune
activation status, LPS levels were positively correlated with the inflammatory
cytokines IL-6 (r = 0.63;p&lt;0.05), IL-8 (r = 0.89;p&lt;0.05), and MIF (r =
0.64;p&lt;0.05). Also, higher plasma intestinal fatty acid binding protein
(IFABP) levels were observed in VL patients, which correlated with LPS levels
(r = 0.57;p&lt;0.05).

Conclusions/Significance

Elevated levels of LPS in VL, in correlation with T-cell activation and
elevated pro-inflammatory cytokines and MIF indicate that this bacterial
product may contribute to the impairment in immune effector function. The
cytokine storm and chronic immune hyperactivation status may contribute to the
observed T-cell depletion. LPS probably originates from microbial
translocation as suggested by IFABP levels and, along with Leishmania antigen-
mediated immune suppression, may play a role in the immunopathogenesis of VL.
These findings point to possible benefits of antimicrobial prophylaxis in
conjunction with anti-Leishmania therapy.

Author Summary

Visceral leishmaniasis (VL) affects organs rich in lymphocytes, being
characterized by intense Leishmania-induced T-cell depletion and reduction in
other hematopoietic cells. In other infectious and non-infectious diseases in
which the immune system is affected, such as HIV-AIDS and inflammatory bowel
disease, damage to gut-associated lymphocyte tissues occurs, enabling luminal
bacteria to enter into the circulation. Lipopolisaccharide (LPS) is a
bacterial product that stimulates macrophages, leading to the production of
pro-inflammatory cytokines and other soluble factors such as MIF, which in
turn activate lymphocytes. Continuous and exaggerated stimulation causes
exhaustion of the T-cell compartment, contributing to immunosuppression.

Herein, we show that an increment in LPS plasma levels also occurs in VL; the
higher the LPS levels, th</field></doc>
