<doc>
<field name="uid">doi:10.1371/journal.pntd.0000877</field>
<field name="doi">10.1371/journal.pntd.0000877</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Márcia Jaqueline Alves de Queiroz Sampaio, Nara Vasconcelos Cavalcanti, João Guilherme Bezerra Alves, Mário Jorge Costa Fernandes Filho, Jailson B. Correia</field>
<field name="title">Risk Factors for Death in Children with Visceral Leishmaniasis</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">11</field>
<field name="pages">e877</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Despite the major public health importance of visceral leishmaniasis (VL) in
Latin America, well-designed studies to inform diagnosis, treatment and
control interventions are scarce. Few observational studies address prognostic
assessment in patients with VL. This study aimed to identify risk factors for
death in children aged less than 15 years admitted for VL treatment in a
referral center in northeast Brazil.

Methodology/Principal Findings

In a retrospective cohort, we reviewed 546 records of patients younger than 15
years admitted with the diagnosis of VL at the Instituto de Medicina Integral
Professor Fernando Figueira between May 1996 and June 2006. Age ranged from 4
months to 13.7 years, and 275 (50%) were male. There were 57 deaths, with a
case-fatality rate of 10%. In multivariate logistic regression, the
independent predictors of risk of dying from VL were (adjusted OR, 95% CI):
mucosal bleeding (4.1, 1.3–13.4), jaundice (4.4, 1.7–11.2), dyspnea (2.8,
1.2–6.1), suspected or confirmed bacterial infections (2.7, 1.2–6.1),
neutrophil count &lt;500/mm3 (3.1, 1.4–6.9) and platelet count &lt;50,000/mm3 (11.7,
5.4–25.1). A prognostic score was proposed and had satisfactory sensitivity
(88.7%) and specificity (78.5%).

Conclusions/Significance

Prognostic and severity markers can be useful to inform clinical decisions
such as whether a child with VL can be safely treated in the local healthcare
facility or would potentially benefit from transfer to referral centers where
advanced life support facilities are available. High risk patients may benefit
from interventions such as early use of extended-spectrum antibiotics or
transfusion of blood products. These baseline risk-based supportive
interventions should be assessed in clinical trials.

Author Summary

Visceral leishmaniasis (VL) is a deadly disease caused by a protozoan called
Leishmania. It is transmitted to humans from infected animals by a sandfly
bite. Most people actually manage to control the infection and do not get
sick, while others develop a range of symptoms. VL impairs the production of
blood components and causes the immune system to malfunction, thus anemia,
bleeding, and bacterial infections often complicate the disease and can lead
to death. To identify risk factors for death from VL, the authors studied 546
children in a referral center in Recife, Brazil. They looked at clinical
history, physical examination and full blood counts on the assumption these
could be easily assessed in peripheral health facilities. They found that the
presence of fast breathing, jaundice, mucosal (e.g. gum) bleeding and
bacterial infections would each increase the risk of death in three to four-
fold. The presence of very low counts of neutrophils and platelets would
increase the risk of death in three and 12-fold respectively. This knowledge
can help clinicians to anticipate the use of antibiotics or transfusion of
blood products in high risk patients, who would potentially benefit from
transfer to centers with advanced life support facilities.

Introduction

In Latin America, visceral leishmaniasis (VL) cases occur from Mexico to
Argentina, but around 90% of reported cases come from Brazil [1]. Despite
substantial underreporting in s</field></doc>
