<doc>
<field name="uid">doi:10.1371/journal.pntd.0001041</field>
<field name="doi">10.1371/journal.pntd.0001041</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Stephen L. Walker, Peter G. Nicholls, Sushmita Dhakal, Rachel A. Hawksworth, Murdo Macdonald, Kishori Mahat, Shudan Ruchal, Sushma Hamal, Deanna A. Hagge, Kapil D. Neupane, Diana N. J. Lockwood</field>
<field name="title">A Phase Two Randomised Controlled Double Blind Trial of High Dose Intravenous Methylprednisolone and Oral Prednisolone versus Intravenous Normal Saline and Oral Prednisolone in Individuals with Leprosy Type 1 Reactions and/or Nerve Function Impairment</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">4</field>
<field name="pages">e1041</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Leprosy Type 1 reactions are a major cause of nerve damage and the preventable
disability that results. Type 1 reactions are treated with oral
corticosteroids and there are few data to support the optimal dose and
duration of treatment. Type 1 reactions have a Th1 immune profile: cells in
cutaneous and neural lesions expressing interferon-γ and interleukin-12.
Methylprednisolone has been used in other Th1 mediated diseases such as
rheumatoid arthritis in an attempt to switch off the immune response and so we
investigated the efficacy of three days of high dose (1 g) intravenous
methylprednisolone at the start of prednisolone therapy in leprosy Type 1
reactions and nerve function impairment.

Results

Forty-two individuals were randomised to receive methylprednisolone followed
by oral prednisolone (n = 20) or oral prednisolone alone (n = 22). There were
no significant differences in the rate of adverse events or clinical
improvement at the completion of the study. However individuals treated with
methylprednisolone were less likely than those treated with prednisolone alone
to experience deterioration in sensory function between day 29 and day 113 of
the study. The study also demonstrated that 50% of individuals with Type 1
reactions and/or nerve function impairment required additional prednisolone
despite treatment with 16 weeks of corticosteroids.

Conclusions

The study lends further support to the use of more prolonged courses of
corticosteroid to treat Type 1 reactions and the investigation of risk factors
for the recurrence of Type 1 reaction and nerve function impairment during and
after a corticosteroid treatment.

Trial Registration

Controlled-Trials.comISRCTN31894035

Author Summary

Leprosy is caused by a bacterium and is curable with a combination of
antibiotics known as multi-drug therapy that patients take for six or 12
months. However a significant proportion of leprosy patients experience
inflammation in their skin and/or nerves, which may occur even after
successful completion of multi-drug therapy. These episodes of inflammation
are called leprosy Type 1 reactions. Type 1 reactions are an important
complication of leprosy because they may result in nerve damage that leads to
disability and deformity. Type 1 reactions require treatment with
immunosuppressive agents such as corticosteroids. The optimum dose and
duration of corticosteroid therapy remains unclear. We conducted a study to
see if it would be safe to use a large dose of a corticosteroid called
methylprednisolone for three days at the start of a 16 week corticosteroid
treatment regime of prednisolone in patients with leprosy Type 1 reactions and
leprosy patients with nerve damage present for less than six months. We did
this by comparing individuals who were given methylprednisolone followed by
prednisolone and those who received just prednisolone. In this small study we
did not see any significant difference in the frequency of adverse events due
to corticosteroid treatment in the two groups. W</field></doc>
