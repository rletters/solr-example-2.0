<doc>
<field name="uid">doi:10.1371/journal.pntd.0000742</field>
<field name="doi">10.1371/journal.pntd.0000742</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Van Thi Phan, Annette Kjær Ersbøll, Khue Viet Nguyen, Henry Madsen, Anders Dalsgaard</field>
<field name="title">Farm-Level Risk Factors for Fish-Borne Zoonotic Trematode Infection in Integrated Small-Scale Fish Farms in Northern Vietnam</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">7</field>
<field name="pages">e742</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Northern Vietnam is an endemic region for fish-borne zoonotic trematodes
(FZT), including liver and intestinal flukes. Humans acquire the FZT infection
by eating raw or inadequately cooked fish. The production of FZT-free fish in
aquaculture is a key component in establishing a sustainable program to
prevent and control the FZT transmission to humans. Interventions in
aquaculture should be based on knowledge of the main risk factors associated
with FZT transmission.

Methodology/Principal Findings

A longitudinal study was carried out from June 2006 to May 2007 in Nam Dinh
province, Red River Delta to investigate the development and risk factors of
FZT infections in freshwater cultured fish. A total of 3820 fish were sampled
six times at two-month intervals from 96 fish farms. Logistic analysis with
repeated measurements was used to evaluate potential risk factors based on
information collected through questionnaire interviews with 61 fish farm
owners. The results showed that the FZT infections significantly increased
from first sampling in June to July 2006 (65%) to sixth sampling in April to
May, 2007 (76%). The liver fluke, Clonorchis sinensis and different zoonotic
intestinal flukes including Haplochis pumilio, H. taichui, H. yokogawai,
Centrocestus formosanus and Procerovum varium were found in sampled fish.
Duration of fish cultured (sampling times), mebendazole drug self-medication
of household members, presence of snails in the pond, and feeding fish with
green vegetation collected outside fish farms all had a significant effect on
the development of FZT prevalence in the fish.

Conclusions/Significance

The FZT prevalence in fish increased by 11 percentage points during a one-year
culture period and the risk factors for the development of infection were
identified. Results also highlight that the young fish are already highly
infected when stocked into the grow-out systems. This knowledge should be
incorporated into control programs of FZT transmission in integrated small-
scale aquaculture nursery and grow-out systems in Vietnam.

Author Summary

Fish are the second intermediate host for fish-borne zoonotic trematodes
(FZT). Humans acquire the FZT by eating raw or inadequately cooked fish.
Therefore any sustainable program to prevent and control human FZT infection
should consider how to minimize the FZT prevalence in fish. Understanding the
development in prevalence of FZT in fish and the risk factors involved are of
key importance in order to plan and implement an FZT prevention program
successfully. During a one-year production cycle in integrated small-scale
aquaculture grow-out systems, the FZT prevalence in fish increased by 11
percentage points. Three risk factors associated with the development of the
prevalence of FZT infection in fish were identified including presence of
snails in the pond, feeding fish with green vegetation collected outside fish
farms, and mebendazole medication by the household members. Aquaculture
management solutions addressing these three risk factors as well as the high
level of FZT infection in juvenile fish stocked in grow-out systems should be
found in order to produce fish free of FZT, which are safe for human
consumption.

Introduction

Fish-borne zoonotic trema</field></doc>
