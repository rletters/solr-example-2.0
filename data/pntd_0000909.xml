<doc>
<field name="uid">doi:10.1371/journal.pntd.0000909</field>
<field name="doi">10.1371/journal.pntd.0000909</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Julie Vallée, Thaksinaporn Thaojaikong, Catrin E. Moore, Rattanaphone Phetsouvanh, Allen L. Richards, Marc Souris, Florence Fournet, Gérard Salem, Jean-Paul J. Gonzalez, Paul N. Newton</field>
<field name="title">Contrasting Spatial Distribution and Risk Factors for Past Infection with Scrub Typhus and Murine Typhus in Vientiane City, Lao PDR</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">12</field>
<field name="pages">e909</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The aetiological diagnostic of fevers in Laos remains difficult due to limited
laboratory diagnostic facilities. However, it has recently become apparent
that both scrub and murine typhus are common causes of previous undiagnosed
fever. Epidemiological data suggests that scrub typhus would be more common in
rural areas and murine typhus in urban areas, but there is very little recent
information on factors involved in scrub and murine typhus transmission,
especially where they are sympatric - as is the case in Vientiane, the capital
of the Lao PDR.

Methodology and Principal Findings

We therefore determined the frequency of IgG seropositivity against scrub
typhus (Orientia tsutsugamushi) and murine typhus (Rickettsia typhi), as
indices of prior exposure to these pathogens, in randomly selected adults in
urban and peri-urban Vientiane City (n = 2,002, ≥35 years). Anti-scrub and
murine typhus IgG were detected by ELISA assays using filter paper elutes. We
validated the accuracy of ELISA of these elutes against ELISA using serum
samples. The overall prevalence of scrub and murine typhus IgG antibodies was
20.3% and 20.6%, respectively. Scrub typhus seropositivity was significantly
higher among adults living in the periphery (28.4%) than in the central zone
(13.1%) of Vientiane. In contrast, seroprevalence of murine typhus IgG
antibodies was significantly higher in the central zone (30.8%) as compared to
the periphery (14.4%). In multivariate analysis, adults with a longer
residence in Vientiane were at significant greater risk of past infection with
murine typhus and at lower risk for scrub typhus. Those with no education,
living on low incomes, living on plots of land with poor sanitary conditions,
living in large households, and farmers were at higher risk of scrub typhus
and those living in neighborhoods with high building density and close to
markets were at greater risk for murine typhus and at lower risk of scrub
typhus past infection.

Conclusions

This study underscores the intense circulation of both scrub and murine typhus
in Vientiane city and underlines difference in spatial distribution and risk
factors involved in the transmission of these diseases.

Author Summary

Scrub typhus and murine typhus are neglected but important treatable causes of
fever, morbidity and mortality in South-East Asia. Epidemiological data
suggests that scrub typhus would be more common in rural areas and murine
typhus in urban areas but there are very few comparative data from places
where both diseases occur, as is the case in Vientiane, the capital of the Lao
PDR. We therefore determined the frequency of IgG antibody seropositivity
against scrub typhus and murine typhus, as indices of prior exposure to these
pathogens, in a randomly selected population of 2,002 adults living in
different neighbourhoods in Vientiane. The overall prevalence of IgG against
these two pathogens was ∼20%. However, within the city, the spatial
distribution of IgG against these two diseases was radically different - past
exposure to murine typhus being more frequent in urbanized areas while past
exposure to scrub typhus more frequen</field></doc>
