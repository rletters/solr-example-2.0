<doc>
<field name="uid">doi:10.1371/journal.pntd.0000915</field>
<field name="doi">10.1371/journal.pntd.0000915</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Sofía Ocaña-Mayorga, Martin S. Llewellyn, Jaime A. Costales, Michael A. Miles, Mario J. Grijalva</field>
<field name="title">Sex, Subdivision, and Domestic Dispersal of Trypanosoma cruzi Lineage I in Southern Ecuador</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">12</field>
<field name="pages">e915</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Molecular epidemiology at the community level has an important guiding role in
zoonotic disease control programmes where genetic markers are suitably
variable to unravel the dynamics of local transmission. We evaluated the
molecular diversity of Trypanosoma cruzi, the etiological agent of Chagas
disease, in southern Ecuador (Loja Province). This kinetoplastid parasite has
traditionally been a paradigm for clonal population structure in pathogenic
organisms. However, the presence of naturally occurring hybrids, mitochondrial
introgression, and evidence of genetic exchange in the laboratory question
this dogma.

Methodology/Principal Findings

Eighty-one parasite isolates from domiciliary, peridomiciliary, and sylvatic
triatomines and mammals were genotyped across 10 variable microsatellite loci.
Two discrete parasite populations were defined: one predominantly composed of
isolates from domestic and peridomestic foci, and another predominantly
composed of isolates from sylvatic foci. Spatial genetic variation was absent
from the former, suggesting rapid parasite dispersal across our study area.
Furthermore, linkage equilibrium between loci, Hardy-Weinberg allele
frequencies at individual loci, and a lack of repeated genotypes are
indicative of frequent genetic exchange among individuals in the
domestic/peridomestic population.

Conclusions/Significance

These data represent novel population-level evidence of an extant capacity for
sex among natural cycles of T. cruzi transmission. As such they have dramatic
implications for our understanding of the fundamental genetics of this
parasite. Our data also elucidate local disease transmission, whereby passive
anthropogenic domestic mammal and triatomine dispersal across our study area
is likely to account for the rapid domestic/peridomestic spread of the
parasite. Finally we discuss how this, and the observed subdivision between
sympatric sylvatic and domestic/peridomestic foci, can inform efforts at
Chagas disease control in Ecuador.

Author Summary

Trypanosoma cruzi is transmitted by blood sucking insects known as
triatomines. This protozoan parasite commonly infects wild and domestic
mammals in South and Central America. However, triatomines also transmit the
parasite to people, and human infection with T. cruzi is known as Chagas
disease, a major public health concern in Latin America. Understanding the
complex dynamics of parasite spread between wild and domestic environments is
essential to design effective control measures to prevent the spread of Chagas
disease. Here we describe T. cruzi genetic diversity and population dynamics
in southern Ecuador. Our findings indicate that the parasite circulates in two
largely independent cycles: one corresponding to the sylvatic environment and
one related to the domestic/peridomestic environment. Furthermore, our data
indicate that human activity might promote parasite dispersal among
communties. This information is the key for the design of control programmes
in Southern Ecuador. Finally, we have encountered evidence of a sexual
reproductive mode in the domestic T. cruzi population, which constitutes a new
and intriguing finding with regards to the biology of this parasite.

Introduction

Chagas disease, caused by the protozoan Trypanosoma</field></doc>
