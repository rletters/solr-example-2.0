<doc>
<field name="uid">doi:10.1371/journal.pntd.0000752</field>
<field name="doi">10.1371/journal.pntd.0000752</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Piengchan Sonthayanon, Sharon J. Peacock, Wirongrong Chierakul, Vanaporn Wuthiekanun, Stuart D. Blacksell, Mathew T. G. Holden, Stephen D. Bentley, Edward J. Feil, Nicholas P. J. Day</field>
<field name="title">High Rates of Homologous Recombination in the Mite Endosymbiont and Opportunistic Human Pathogen Orientia tsutsugamushi</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">7</field>
<field name="pages">e752</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Orientia tsutsugamushi is an intracellular α-proteobacterium which resides in
trombiculid mites, and is the causative agent of scrub typhus in East Asia.
The genome sequence of this species has revealed an unprecedented number of
repeat sequences, most notably of the genes encoding the conjugative
properties of a type IV secretion system (T4SS). Although this observation is
consistent with frequent intragenomic recombination, the extent of homologous
recombination (gene conversion) in this species is unknown. To address this
question, and to provide a protocol for the epidemiological surveillance of
this important pathogen, we have developed a multilocus sequence typing (MLST)
scheme based on 7 housekeeping genes (gpsA, mdh, nrdB, nuoF, ppdK, sucD,
sucB). We applied this scheme to the two published genomes, and to DNA
extracted from blood taken from 84 Thai scrub typhus patients, from 20
cultured Thai patient isolates, 1 Australian patient sample, and from 3
cultured type strains. These data demonstrated that the O. tsutsugamushi
population was both highly diverse [Simpson&apos;s index (95% CI) = 0.95
(0.92–0.98)], and highly recombinogenic. These results are surprising given
the intracellular life-style of this species, but are broadly consistent with
results obtained for Wolbachia, which is an α-proteobacterial reproductive
parasite of arthropods. We also compared the MLST data with ompA sequence data
and noted low levels of consistency and much higher discrimination by MLST.
Finally, twenty-five percent of patients in this study were simultaneously
infected with multiple sequence types, suggesting multiple infection caused by
either multiple mite bites, or multiple strains co-existing within individual
mites.

Author Summary

Scrub typhus, the rickettsial infectious disease caused by the obligate
intracellular bacterium Orientia tsutsugamushi, is endemic across the Asia
Pacific region. The bacterium is transmitted by the bite of larval stages of
trombiculid mites (“chiggers”; Leptotrombidium spp.), which more typically
feed on small rodents. Clinical features include fever, headache, myalgia,
lymphadenopathy and an eschar at the site of the bite. Despite the importance
of this pathogen, little is known of the population diversity or the role of
homologous recombination in driving the microevolution of this species. Here,
we describe the development and application of a multilocus sequence typing
(MLST) scheme that can be applied directly to blood samples, and that was
applied to 108 O. tsutsugamushi isolates. We found that this organism
demonstrated a high rate of homologous recombination, a surprising finding
given the intracellular life-style of this species. We also found that 25% of
patients in our study were simultaneously infected with multiple sequence
types, suggesting multiple infection caused by either multiple mite bites, or
multiple strains co-existing within individual mites.

Introduction

Scrub typhus is a zoonotic disease endemic in Southeast Asia caused by
Orientia tsutsugamushi, a Gram-negative obligate intracellular coccobacillus.
The number of new cases in East Asia has been estimated at approximate</field></doc>
