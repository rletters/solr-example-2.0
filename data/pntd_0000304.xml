<doc>
<field name="uid">doi:10.1371/journal.pntd.0000304</field>
<field name="doi">10.1371/journal.pntd.0000304</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Nguyen Thi Phuong Lan, Mihoko Kikuchi, Vu Thi Que Huong, Do Quang Ha, Tran Thi Thuy, Vo Dinh Tham, Ha Manh Tuan, Vo Van Tuong, Cao Thi Phi Nga, Tran Van Dat, Toshifumi Oyama, Kouichi Morita, Michio Yasunami, Kenji Hirayama</field>
<field name="title">Protective and Enhancing HLA Alleles, HLA-DRB1*0901 and HLA-A*24, for Severe Forms of Dengue Virus Infection, Dengue Hemorrhagic Fever and Dengue Shock Syndrome</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">10</field>
<field name="pages">e304</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Dengue virus (DV) infection is one of the most important mosquito-borne
diseases in the tropics. Recently, the severe forms, dengue hemorrhagic fever
(DHF) and dengue shock syndrome (DSS), have become the leading cause of death
among children in Southern Vietnam. Protective and/or pathogenic T cell
immunity is supposed to be important in the pathogenesis of DHF and DSS.

Methodology/Principal Findings

To identify HLA alleles controlling T cell immunity against dengue virus (DV),
we performed a hospital-based case control study at Children&apos;s Hospital No.2,
Ho Chi Minh City (HCMC), and Vinh Long Province Hospital (VL) in Southern
Vietnam from 2002 to 2005. A total of 211 and 418 patients with DHF and DSS,
respectively, diagnosed according to the World Health Organization (WHO)
criteria, were analyzed for their characteristic HLA-A, -B and -DRB1 alleles.
Four hundred fifty healthy children (250 from HCMC and 200 from VL) of the
same Kinh ethnicity were also analyzed as population background. In HLA class
I, frequency of the HLA-A*24 showed increased tendency in both DHF and DSS
patients, which reproduced a previous study. The frequency of A*24 with
histidine at codon 70 (A*2402/03/10), based on main anchor binding site
specificity analysis in DSS and DHF patients, was significantly higher than
that in the population background groups (HCMC 02-03 DSS: OR = 1.89, P =
0.008, DHF: OR = 1.75, P = 0.033; VL 02-03 DSS: OR = 1.70, P = 0.03, DHF: OR =
1.46, P = 0.38; VL 04-05 DSS: OR = 2.09, P = 0.0075, DHF: OR = 2.02, P =
0.038). In HLA class II, the HLA-DRB1*0901 frequency was significantly
decreased in secondary infection of DSS in VL 04-05 (OR = 0.35, P = 0.0025, Pc
= 0.03). Moreover, the frequency of HLA-DRB1*0901 in particular was
significantly decreased in DSS when compared with DHF in DEN-2 infection (P =
0.02).

Conclusion

This study improves our understanding of the risk of HLA-class I for severe
outcome of DV infection in the light of peptide anchor binding site and
provides novel evidence that HLA-class II may control disease severity (DHF to
DSS) in DV infection.

Author Summary

Dengue has become one of the most common viral diseases transmitted by
infected mosquitoes (with any of the four dengue virus serotypes: DEN-1, -2,
-3, or -4). It may present as asymptomatic or illness, ranging from mild to
severe disease. Recently, the severe forms, dengue hemorrhagic fever (DHF) and
dengue shock syndrome (DSS), have become the leading cause of death among
children in Southern Vietnam. The pathogenesis of DHF/DSS, however, is not yet
completely understood. The immune response, virus virulence, and host genetic
background are considered to be risk factors contributing to disease severity.
Human leucocyte antigens (HLA) expressed on the cell surface function as
antigen presenting molecules and those polymorphism can change individuals&apos;
immune response. We investigated the HLA-A, -B (class I), and -DRB1 (class II)
polymorphism in Vietnamese children with different severity (DHF/DSS) by</field></doc>
