<doc>
<field name="uid">doi:10.1371/journal.pntd.0001379</field>
<field name="doi">10.1371/journal.pntd.0001379</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Molouk Beiromvand, Lame Akhlaghi, Seyed Hossein Fattahi Massom, Iraj Mobedi, Ahmad Reza Meamar, Hormozd Oormazdi, Abbas Motevalian, Elham Razmjou</field>
<field name="title">Detection of Echinococcus multilocularis in Carnivores in Razavi Khorasan Province, Iran Using Mitochondrial DNA</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">11</field>
<field name="pages">e1379</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Echinococcus multilocularis is the source of alveolar echinococcosis, a
potentially fatal zoonotic disease. This investigation assessed the presence
of E. multilocularis infection in definitive hosts in the Chenaran region of
Razavi Khorasan Province, northeastern Iran.

Methodology/Principal Findings

Fecal samples from 77 domestic and stray dogs and 14 wild carnivores were
examined using the flotation/sieving method followed by multiplex PCR of
mitochondrial genes. The intestinal scraping technique (IST) and the
sedimentation and counting technique (SCT) revealed adult Echinococcus in the
intestines of five of 10 jackals and of the single wolf examined. Three
jackals were infected only with E. multilocularis but two, and the wolf, were
infected with both E. multilocularis and E. granulosus. Multiplex PCR revealed
E. multilocularis, E. granulosus, and Taenia spp. in 19, 24, and 28 fecal
samples, respectively. Echinococcus multilocularis infection was detected in
the feces of all wild carnivores sampled including nine jackals, three foxes,
one wolf, one hyena, and five dogs (6.5%). Echinococcus granulosus was found
in the fecal samples of 16.9% of dogs, 66.7% of jackals, and all of the foxes,
the wolf, and the hyena. The feces of 16 (21.8%) dogs, 7 of 9 (77.8%) jackals,
and all three foxes, one wolf and one hyena were infected with Taenia spp.

Conclusions/Significance

The prevalence of E. multilocularis in wild carnivores of rural areas of the
Chenaran region is high, indicating that the life cycle is being maintained in
northeastern Iran with the red fox, jackal, wolf, hyena, and dog as definitive
hosts.

Author Summary

Echinococcus multilocularis causes alveolar echinococcosis, a serious zoonotic
disease present in many areas of the world. The parasite is maintained in
nature through a life cycle in which adult worms in the intestine of
carnivores transmit infection to small mammals, predominantly rodents, via
eggs in the feces. Humans may accidentally ingest eggs of E. multilocularis
through contact with the definitive host or by direct ingestion of
contaminated water or foods, causing development of a multivesicular cyst in
the viscera, especially liver and lung. We found adult E. multilocularis in
the intestine and/or eggs in feces of all wild carnivores examined and in some
stray and domestic dogs in villages of Chenaran region, northeastern Iran. The
life cycle of E. multilocularis is being maintained in this area by wild
carnivores, and the local population and visitors are at risk of infection
with alveolar echinococcosis. Intensive health initiatives for control of the
parasite and diagnosis of this potentially fatal disease in humans, in this
area of Iran, are needed.

Introduction

Echinococcus multilocularis is the agent of alveolar echinococcosis, a
potentially fatal zoonotic disease [1], [2]. The life cycle of E.
multilocularis is sylvatic; adult worms are found in wild carnivores,
principally foxes, and in the raccoon dog, wolf, coyote, and jackal, while
their metacestodes develop in small mammals, predominantly rodents such as
Cricetidae, Arvicolidae, and Muridae [3], [4], [5], [6], [7]. In some rural
areas domestic dogs and sometimes cats can</field></doc>
