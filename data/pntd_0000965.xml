<doc>
<field name="uid">doi:10.1371/journal.pntd.0000965</field>
<field name="doi">10.1371/journal.pntd.0000965</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Diego L. Costa, Vanessa Carregaro, Djalma S. Lima-Júnior, Neide M. Silva, Cristiane M. Milanezi, Cristina R. Cardoso, Ângela Giudice, Amélia R. de Jesus, Edgar M. Carvalho, Roque P. Almeida, João S. Silva</field>
<field name="title">BALB/c Mice Infected with Antimony Treatment Refractory Isolate of Leishmania braziliensis Present Severe Lesions due to IL-4 Production</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">3</field>
<field name="pages">e965</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Leishmania braziliensis is the main causative agent of cutaneous leishmaniasis
in Brazil. Protection against infection is related to development of Th1
responses, but the mechanisms that mediate susceptibility are still poorly
understood. Murine models have been the most important tools in understanding
the immunopathogenesis of L. major infection and have shown that Th2 responses
favor parasite survival. In contrast, L. braziliensis–infected mice develop
strong Th1 responses and easily resolve the infection, thus making the study
of factors affecting susceptibility to this parasite difficult.

Methodology/Principal Findings

Here, we describe an experimental model for the evaluation of the mechanisms
mediating susceptibility to L. braziliensis infection. BALB/c mice were
inoculated with stationary phase promastigotes of L. braziliensis, isolates
LTCP393(R) and LTCP15171(S), which are resistant and susceptible to antimony
and nitric oxide (NO), respectively. Mice inoculated with LTCP393(R) presented
larger lesions that healed more slowly and contained higher parasite loads
than lesions caused by LTCP15171(S). Inflammatory infiltrates in the lesions
and production of IFN-γ, TNF-α, IL-10 and TGF-β were similar in mice
inoculated with either isolate, indicating that these factors did not
contribute to the different disease manifestations observed. In contrast, IL-4
production was strongly increased in LTCP393(R)-inoculated animals and also
arginase I (Arg I) expression. Moreover, anti-IL-4 monoclonal antibody (mAb)
treatment resulted in decreased lesion thickness and parasite burden in
animals inoculated with LTCP393(R), but not in those inoculated with
LTCP15171(S).

Conclusion/Significance

We conclude that the ability of L. braziliensis isolates to induce Th2
responses affects the susceptibility to infection with these isolates and
contributes to the increased virulence and severity of disease associated with
them. Since these data reflect what happens in human infection, this model
could be useful to study the pathogenesis of the L. braziliensis infection, as
well as to design new strategies of therapeutic intervention.

Author Summary

Leishmaniasis is a neglected disease that affects more than 12 million people
worldwide. In Brazil, the cutaneous disease is more prevalent with about
28,000 new cases reported each year, and L. braziliensis is the main causative
agent. The interesting data about the infection with this parasite is the wide
variety of clinical manifestations that ranges from single ulcerated lesions
to mucocutaneous and disseminated disease. However, experimental models to
study the infection with this parasite are difficult to develop due to high
resistance of most mouse strains to the infection, and the mechanisms
underlying the distinct manifestations remain poorly understood. Here, the
authors use a mouse experimental model of infection with different L.
braziliensis isolates, known to induce diseases with distinct severity in the
human hosts, to elucidate immune mechanisms that may be involved in the
different manifestations. They showed that distinct p</field></doc>
