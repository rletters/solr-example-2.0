<doc>
<field name="uid">doi:10.1371/journal.pntd.0000572</field>
<field name="doi">10.1371/journal.pntd.0000572</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Alexis Cambanis</field>
<field name="title">Pulmonary Loiasis and HIV Coinfection in Rural Cameroon</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">3</field>
<field name="pages">e572</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Presentation of Case

A 38-year-old man presented to a rural hospital in Northwest Cameroon with a
one-month history of dyspnea that worsened upon exertion. He reported
occasional cough, which was nonproductive. Review of systems revealed a fall
in weight from 81 kg to 71 kg over the last year, but no other problems were
noted. The patient initially sought care at a health center in Douala (10
hours away by public transport), where he was treated for typhoid with no
improvement.

On physical examination, temperature was 37.8°C, blood pressure 110/70 mmHg,
pulse 88 beats per minute, and respiratory rate 28 breaths per minute. He was
noncachectic without thrush or lymphadenopathy. Lung auscultation revealed no
bronchial breath sounds or wheezes, but decreased breath sounds and dullness
to percussion were noted at both lung bases, and a chest radiograph showed
dense bibasilar opacities (Figure 1). Diagnostic thoracentesis confirmed the
presence of exudative pleural effusion; cytological examination yielded 435
white blood cells/mm3 (84% lymphocytes, 10% neutrophils, and 6% eosinophils),
240 red blood cells/mm3, and numerous motile microfilariae (mff) throughout
the specimen (see Video S1). A smear was made of the pleural aspirate and the
filarial species identified as Loa loa based on morphological features (Figure
2). Other laboratory investigations included hemoglobin of 9.3 mg/dl, white
blood cells 9,800/mm3, a thick blood film negative for malaria parasites, and
a positive HIV serology test. His CD4 count was 954 cells/µl. Peripheral blood
was not examined for microfilariae.

Figure 1

Resolution of pleural effusions after treatment for loiasis.

Chest radiographs showing bilateral basilar densities at the time of admission
(left) and complete resolution at 3-week follow-up after one dose of
ivermectin (right).

Figure 2

Loa loa found in pleural aspirate of HIV-positive man in Cameroon.

Microfilaria of Loa loa seen in Giemsa-stained preparation of pleural fluid
showing morphological characteristics including sheath and terminal nuclei
(×1,000). Note that Giemsa does not specifically stain the sheath, but its
outline is clearly visible in this preparation. Several lymphocytes can also
be seen.

After discussing these results with his physician, the patient received a
single dose of ivermectin (150 µg/kg body weight) and all other medications
that had been ordered by the admitting team—including multiple broad-spectrum
antibiotics, ventolin, and amodiaquine—were stopped. A short course of
prednisone was initiated to reduce potential reaction to parasite antigens.
The next day, he left the hospital in stable condition. Follow-up examination
three weeks later showed normalization of his vital signs and total resolution
of the pleural effusions on repeat radiography (Figure 1). The patient was
encouraged to participate in the annual ivermectin campaigns common in the
area and to monitor his HIV infection at his local medical center.

Case Discussion

Loiasis in sub-Saharan Africa

Loa loa is a filarial nematode transmitted by Chrysops tabanid flies and is
limited to West and Central Africa. Most infected people remain asymptomatic,
but after a latency period from 6 months to several years, clinical symptoms
may develop, most often intermittent edematous lesions in the extremities
(Calabar swellings) or passage of the adult worm under the conjunctiva [1]. It
is e</field></doc>
