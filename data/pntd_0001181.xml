<doc>
<field name="uid">doi:10.1371/journal.pntd.0001181</field>
<field name="doi">10.1371/journal.pntd.0001181</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Carla Maia, Veronika Seblova, Jovana Sadlova, Jan Votypka, Petr Volf</field>
<field name="title">Experimental Transmission of Leishmania infantum by Two Major Vectors: A Comparison between a Viscerotropic and a Dermotropic Strain</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">6</field>
<field name="pages">e1181</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

We quantified Leishmania infantum parasites transmitted by natural vectors for
the first time. Both L. infantum strains studied, dermotropic CUK3 and
viscerotropic IMT373, developed well in Phlebotomus perniciosus and Lutzomyia
longipalpis. They produced heavy late-stage infection and colonized the
stomodeal valve, which is a prerequisite for successful transmission. Infected
sand fly females, and especially those that transmit parasites, feed
significantly longer on the host (1.5–1.8 times) than non-transmitting
females. Quantitative PCR revealed that P. perniciosus harboured more CUK3
strain parasites, while in L. longipalpis the intensity of infection was
higher for the IMT373 strain. However, in both sand fly species the parasite
load transmitted was higher for the strain with dermal tropism (CUK3). All but
one sand fly female infected by the IMT373 strain transmitted less than 600
promastigotes; in contrast, 29% of L. longipalpis and 14% of P. perniciosus
infected with the CUK3 strain transmitted more than 1000 parasites. The
parasite number transmitted by individual sand flies ranged from 4 up to
4.19×104 promastigotes; thus, the maximal natural dose found was still about
250 times lower than the experimental challenge dose used in previous studies.
This finding emphasizes the importance of determining the natural infective
dose for the development of an accurate experimental model useful for the
evaluation of new drugs and vaccines.

Author Summary

Leishmaniasis is a disease caused by protozoan parasites which are transmitted
through the bites of infected insects called sand flies. The World Health
Organization has estimated that leishmaniases cause 1.6 million new cases
annually, of which an estimated 1.1 million are cutaneous or mucocutaneous,
and 500,000 are visceral, the most severe form of the disease and fatal if
left untreated. The development of a more natural model is crucial for the
evaluation of new drugs or vaccine candidates against leishmaniases. The main
aim of this study was to quantify the number of Leishmania infantum parasites
transmitted by a single sand fly female into the skin of a vertebrate host
(mouse). Two L. infantum strains, viscerotropic IMT373 and dermotropic CUK3,
were compared in two natural sand fly vectors: Phlebotomus perniciosus and
Lutzomyia longipalpis. We found that the parasite number transmitted by
individual sand flies ranged from 4 up to 4.19×104. The maximal natural
infective dose found in our experiments was about 250 times lower than the
experimental challenge dose used in most previous studies.

Introduction

Leishmania are intracellular protozoan parasites that establish infection in
mammalian hosts following transmission through the bite of an infected
phlebotomine sand fly. Visceral leishmaniasis, caused by Leishmania donovani
in the Old World and L. infantum in both the Old and New World, invariably
leads to death if left untreated [1]. Despite the fact that parasites from the
L. donovani complex are mainly associated with disseminated infection of the
spleen and liver, it has been shown that L. infantum can also cause cutaneous
lesions [2]–[5]. A novel focus of cutaneous leishmaniasis caused by L.
infantum was recently described in the Cukurova region in Turkey [6].
</field></doc>
