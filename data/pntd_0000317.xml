<doc>
<field name="uid">doi:10.1371/journal.pntd.0000317</field>
<field name="doi">10.1371/journal.pntd.0000317</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Eric A. Ottesen, Pamela J. Hooper, Mark Bradley, Gautam Biswas</field>
<field name="title">The Global Programme to Eliminate Lymphatic Filariasis: Health Impact after 8 Years</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">10</field>
<field name="pages">e317</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

In its first 8 years, the Global Programme to Eliminate Lymphatic Filariasis
(GPELF) achieved an unprecedentedly rapid scale-up: &gt;1.9 billion treatments
with anti-filarial drugs (albendazole, ivermectin, and diethylcarbamazine)
were provided via yearly mass drug administration (MDA) to a minimum of 570
million individuals living in 48 of the 83 initially identified LF-endemic
countries.

Methodology

To assess the health impact that this massive global effort has had, we
analyzed the benefits accrued first from preventing or stopping the
progression of LF disease, and then from the broader anti-parasite effects
(‘beyond-LF’ benefits) attributable to the use of albendazole and ivermectin.
Projections were based on demographic and disease prevalence data from
publications of the Population Reference Bureau, The World Bank, and the World
Health Organization.

Result

Between 2000 and 2007, the GPELF prevented LF disease in an estimated 6.6
million newborns who would otherwise have acquired LF, thus averting in their
lifetimes nearly 1.4 million cases of hydrocele, 800,000 cases of lymphedema
and 4.4 million cases of subclinical disease. Similarly, 9.5 million
individuals—previously infected but without overt manifestations of
disease—were protected from developing hydrocele (6.0 million) or lymphedema
(3.5 million). These LF-related benefits, by themselves, translate into 32
million DALYs (Disability Adjusted Life Years) averted. Ancillary, ‘beyond-LF’
benefits from the &gt;1.9 billion treatments delivered by the GPELF were also
enormous, especially because of the &gt;310 million treatments to the children
and women of childbearing age who received albendazole with/without ivermectin
(effectively treating intestinal helminths, onchocerciasis, lice, scabies, and
other conditions). These benefits can be described but remain difficult to
quantify, largely because of the poorly defined epidemiology of these latter
infections.

Conclusion

The GPELF has earlier been described as a ‘best buy’ in global health; this
present tally of attributable health benefits from its first 8 years
strengthens this notion considerably.

Author Summary

Lymphatic filariasis (LF) is a vector-borne, chronically disabling parasitic
infection causing elephantiasis, lymphedema, and hydrocele. The infection is
endemic in 83 countries worldwide, with more than 1.2 billion people at risk
and 120 million already infected. Since 1998, the Global Programme to
Eliminate Lymphatic Filariasis (GPELF) has targeted elimination of LF by 2020.
In its first 8 operational years, the program has scaled-up to provide more
than 1.9 billion treatments through annual, single-dose mass drug
administration (MDA) to ∼570 million individuals living in 48 LF-endemic
countries. Not only do the GPELF drugs prevent the spread of LF, they also
stop the progression of disease in those already infected. In addition, since
two of the three drugs used for LF elimination have broad anti-parasite
properties, treated populations are freed from both intestinal worms and from
skin infections with onchocerca, lice, and scabies. To better understand the
public health benefit of this ongoing global health initiative, we undertook
an analysis of Programme data made available to WHO by participating
countries. Our conservativ</field></doc>
