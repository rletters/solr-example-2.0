<doc>
<field name="uid">doi:10.1371/journal.pntd.0001605</field>
<field name="doi">10.1371/journal.pntd.0001605</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Louise A. Kelly-Hope, Moses J. Bockarie, David H. Molyneux</field>
<field name="title">Loa loa Ecology in Central Africa: Role of the Congo River System</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">6</field>
<field name="pages">e1605</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Background

Loa loa is a parasite that causes tropical eye worm, or Calabar swelling, a
disease confined to tropical forests of Africa where it is transmitted by
Tabanid flies of the genus Chrysops [1], [2]. Recent disease prevalence maps
published by Zouré et al. [3] in PLoS Neglected Tropical Diseases highlight an
unusual geographical distribution in the central African region, which may be
related to distinct environmental or topographical features of the region.
Using geographical information systems (GIS) and remotely sensed satellite
data, we examined the broad geographical and ecological parameters and
specific climate variables of L. loa to highlight factors that could affect or
influence the distribution of Chrysops vectors, and the potential for
transmission, and to explain this unique epidemiological pattern.

The filaria parasite L. loa has assumed increasing importance in recent years,
as it is associated with severe adverse events (SAEs) when some individuals
receive ivermectin during mass drug distribution programmes for the control of
onchocerciasis [4]. Individuals with high L. loa microfilaremia in excess of
30,000 microfilaria/ml of blood have a higher risk of these adverse events,
which are life-threatening unless treatment and care is available [5].
Patients display symptoms of a dysfunctional central nervous system manifested
as coma as a result of encephalopathy, presumptively as a result of the rapid
death of L. loa microfilariae [4]–[6], although the precise pathology remains
unclear. Proper care usually results in full recovery, but in more remote
areas access to effective care by formal health workers is often difficult
with serious consequences [6].

L. loa was a neglected parasitic infection until the observations of its
association with adverse events came to the attention of the African Programme
for Onchocerciasis Control (APOC), the donor of ivermectin (Mectizan) (Merck &amp;
Co., Inc.), and the Mectizan Donation Programme. The recent paper by Zouré et
al. [3] has defined the areas of high SAE risk following extensive surveys
using the Rapid Assessment Procedure for Loiasis (RAPLOA) method, which is
based on village surveys of a history of eye worm and assesses the potential
risk of post-treatment L. loa encephalopathy [7]. High risk areas are those
with a prevalence of over 40% of eye worm history. Earlier studies [8], [9]
developed a spatial model to define high endemicity, which was based on
ecological parameters, in particular the degree of forest cover given the
association of the main vectors Chrysops silacea and C. dimidiata with moist
broad leaf tropical forest habitat [1], [2], [10], as well as other potential
environmental drivers of Chrysops ecology such as elevation and soil type.

The high prevalence of loiasis and the risks of SAEs [3] has been the major
impediment to scaling up both the onchocerciasis [11] and lymphatic filariasis
(LF) programmes [12] as both use ivermectin, which acts as a microfilaricide.
The challenges of co-endemicity of the three filarial infections—Onchocerca
volvulus, Wuchereria bancrofti, and L. loa—are especially important in
countries such as the Democratic Republic of Congo (DRC) given its size and
the number of people at risk. A recent paper [13] has reviewed the earlier
literature on loiasis with particular reference to the DRC, and</field></doc>
