<doc>
<field name="uid">doi:10.1371/journal.pntd.0000836</field>
<field name="doi">10.1371/journal.pntd.0000836</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Sahotra Sarkar, Stavana E. Strutz, David M. Frank, Chissa–Louise Rivaldi, Blake Sissel, Victor Sánchez–Cordero</field>
<field name="title">Chagas Disease Risk in Texas</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">10</field>
<field name="pages">e836</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Chagas disease, caused by Trypanosoma cruzi, remains a serious public health
concern in many areas of Latin America, including México. It is also endemic
in Texas with an autochthonous canine cycle, abundant vectors (Triatoma
species) in many counties, and established domestic and peridomestic cycles
which make competent reservoirs available throughout the state. Yet, Chagas
disease is not reportable in Texas, blood donor screening is not mandatory,
and the serological profiles of human and canine populations remain unknown.
The purpose of this analysis was to provide a formal risk assessment,
including risk maps, which recommends the removal of these lacunae.

Methods and Findings

The spatial relative risk of the establishment of autochthonous Chagas disease
cycles in Texas was assessed using a five–stage analysis. 1. Ecological risk
for Chagas disease was established at a fine spatial resolution using a
maximum entropy algorithm that takes as input occurrence points of vectors and
environmental layers. The analysis was restricted to triatomine vector species
for which new data were generated through field collection and through
collation of post–1960 museum records in both México and the United States
with sufficiently low georeferenced error to be admissible given the spatial
resolution of the analysis (1 arc–minute). The new data extended the
distribution of vector species to 10 new Texas counties. The models predicted
that Triatoma gerstaeckeri has a large region of contiguous suitable habitat
in the southern United States and México, T. lecticularia has a diffuse
suitable habitat distribution along both coasts of the same region, and T.
sanguisuga has a disjoint suitable habitat distribution along the coasts of
the United States. The ecological risk is highest in south Texas. 2.
Incidence–based relative risk was computed at the county level using the
Bayesian Besag–York–Mollié model and post–1960 T. cruzi incidence data. This
risk is concentrated in south Texas. 3. The ecological and incidence–based
risks were analyzed together in a multi–criteria dominance analysis of all
counties and those counties in which there were as yet no reports of parasite
incidence. Both analyses picked out counties in south Texas as those at
highest risk. 4. As an alternative to the multi–criteria analysis, the
ecological and incidence–based risks were compounded in a multiplicative
composite risk model. Counties in south Texas emerged as those with the
highest risk. 5. Risk as the relative expected exposure rate was computed
using a multiplicative model for the composite risk and a scaled population
county map for Texas. Counties with highest risk were those in south Texas and
a few counties with high human populations in north, east, and central Texas
showing that, though Chagas disease risk is concentrated in south Texas, it is
not restricted to it.

Conclusions

For all of Texas, Chagas disease should be designated as reportable, as it is
in Arizona and Massachusetts. At least for south Texas, lower than N, blood
donor screening should be mandatory, and the serological profiles of human and
canine populations should be established. It is also recommended that a joint
initiative be undertaken by the United States and México to combat Chagas
disease in the trans</field></doc>
