<doc>
<field name="uid">doi:10.1371/journal.pntd.0001412</field>
<field name="doi">10.1371/journal.pntd.0001412</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Solomon Nwaka, Dominique Besson, Bernadette Ramirez, Louis Maes, An Matheeussen, Quentin Bickle, Nuha R. Mansour, Fouad Yousif, Simon Townson, Suzanne Gokool, Fidelis Cho-Ngwa, Moses Samje, Shailja Misra-Bhattacharya, P. K. Murthy, Foluke Fakorede, Jean-Marc Paris, Clive Yeates, Robert Ridley, Wesley C. Van Voorhis, Timothy Geary</field>
<field name="title">Integrated Dataset of Screening Hits against Multiple Neglected Disease Pathogens</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">12</field>
<field name="pages">e1412</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

New chemical entities are desperately needed that overcome the limitations of
existing drugs for neglected diseases. Screening a diverse library of 10,000
drug-like compounds against 7 neglected disease pathogens resulted in an
integrated dataset of 744 hits. We discuss the prioritization of these hits
for each pathogen and the strong correlation observed between compounds active
against more than two pathogens and mammalian cell toxicity. Our work suggests
that the efficiency of early drug discovery for neglected diseases can be
enhanced through a collaborative, multi-pathogen approach.

Author Summary

The search for new drugs for human neglected diseases accelerated in the past
decade, based on the recognition that addressing these infections was
necessary for global poverty reduction. The expansion of discovery and
development programmes was supported by donor investment, increasing
participation of the industry and the creation of Product Development
Partnership (PDP) enterprises. Despite these efforts, major discovery gaps
remain as, apart from some repurposed drugs and a few new molecules for
malaria, no new candidate has been recently transitioned from discovery into
development for the major Neglected Tropical Diseases (NTDs). In this
publication, we present a collaborative network model for drug discovery based
on coordinated North-South partnerships. This network carried out low-to-
medium throughput whole-organism screening assays against seven NTDs (malaria,
leishmaniasis, human African trypanosomiasis [HAT], Chagas&apos; disease,
schistosomiasis, onchocerciasis and lymphatic filariasis) together with an
early assessment of compound toxicity in mammalian cells. We describe a
screening campaign of 10,000 molecules, its outcome and the implications of
this strategy for enhancing the efficiency and productivity of drug discovery
for NTDs.

Introduction

The search for new antiparasitic drugs for use in humans has accelerated in
the past decade, based partly on the growing recognition that addressing these
widespread infections is necessary for poverty reduction. There is a consensus
that the drugs available for these pathogens are far from optimal, plagued by
susceptibility to resistance, lack of activity against key species (or stages
of the life cycle), lack of adequate efficacy in field-compatible delivery
regimens, and reliant on single agents for control programmes [1], [2].
Expansion of programs for discovery and development of new compounds has been
fueled by investment from donor organizations (such as the Bill and Melinda
Gates Foundation and the Wellcome Trust) and increasing participation of the
pharmaceutical industry. Some companies have established drug discovery
centers for a select set of diseases. For example, the Novartis Institute in
Singapore is focusing on malaria, dengue and tuberculosis and the
GlaxoSmithKline (GSK) facility in Tres Cantos, Spain, is expanding its efforts
to include a number of neglected diseases. We have also witnessed drug
donation programmes essential for filariasis cont</field></doc>
