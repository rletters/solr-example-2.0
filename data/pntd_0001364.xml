<doc>
<field name="uid">doi:10.1371/journal.pntd.0001364</field>
<field name="doi">10.1371/journal.pntd.0001364</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Tiaoying Li, Akira Ito, Renqing Pengcuo, Yasuhito Sako, Xingwang Chen, Dongchuan Qiu, Ning Xiao, Philip S. Craig</field>
<field name="title">Post-Treatment Follow-Up Study of Abdominal Cystic Echinococcosis in Tibetan Communities of Northwest Sichuan Province, China</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">10</field>
<field name="pages">e1364</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Human cystic echinococcosis (CE), caused by the larval stage of Echinococcus
granulosus, with the liver as the most frequently affected organ, is known to
be highly endemic in Tibetan communities of northwest Sichuan Province.
Antiparasitic treatment with albendazole remains the primary choice for the
great majority of patients in this resource-poor remote area, though surgery
is the most common approach for CE therapy that has the potential to remove
cysts and lead to complete cure. The current prospective study aimed to assess
the effectiveness of community based use of cyclic albendazole treatment in
Tibetan CE cases, and concurrently monitor the changes of serum specific
antibody levels during treatment.

Methodology/Principal Findings

Ultrasonography was applied for diagnosis and follow-up of CE cases after
cyclic albendazole treatment in Tibetan communities of Sichuan Province during
2006 to 2008, and serum specific IgG antibody levels against Echinococcus
granulosus recombinant antigen B in ELISA was concurrently monitored in these
cases. A total of 196 CE cases were identified by ultrasound, of which 37
(18.9%) showed evidence of spontaneous healing/involution of hepatic cyst(s)
with CE4 or CE5 presentations. Of 49 enrolled CE cases for treatment follow-
up, 32.7% (16) were considered to be cured based on B-ultrasound after 6
months to 30 months regular albendazole treatment, 49.0% (24) were improved,
14.3% (7) remained unchanged, and 4.1% (2) became aggravated. In general,
patients with CE2 type cysts (daughter cysts present) needed a longer
treatment course for cure (26.4 months), compared to cases with CE1
(univesicular cysts) (20.4 months) or CE3 type (detached cyst membrane or
partial degeneration of daughter cysts) (9 months). In addition, the curative
duration was longer in patients with large (&gt;10 cm) cysts (22.3 months),
compared to cases with medium (5–10 cm) cysts (17.3 months) or patients with
small (&lt;5 cm) cysts (6 months). At diagnosis, seven (53.8%) of 13 cases with
CE1 type cysts without any previous intervention showed negative specific IgG
antibody response to E. granulosus recombinant antigen B (rAgB). However,
following 3 months to 18 months albendazole therapy, six of these 7 initially
seronegative CE1 cases sero-converted to be specific IgG antibody positive,
and concurrently ultrasound scan showed that cysts changed to CE3a from CE1
type in all the six CE cases. Two major profiles of serum specific IgG
antibody dynamics during albendazole treatment were apparent in CE cases: (i)
presenting as initial elevation followed by subsequent decline, or (ii) a
persistent decline. Despite a decline, however, specific antibody levels
remained positive in most improved or cured CE cases.

Conclusions

This was the first attempt to follow up community-screened cystic
echinococcosis patients after albendazole therapy using ultrasonography and
serology in an endemic Tibetan region. Cyclic albendazole treatment proved to
be effective in the great majority of CE cases in this resource-poor area, but
periodic abdominal ultrasound examination was necessary to guide appropriate
treatment. Oral albendazole for over 18 months was more likely to result in CE
cure. P</field></doc>
