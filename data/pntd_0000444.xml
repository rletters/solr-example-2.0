<doc>
<field name="uid">doi:10.1371/journal.pntd.0000444</field>
<field name="doi">10.1371/journal.pntd.0000444</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Sophie La Vincente, Therese Kearns, Christine Connors, Scott Cameron, Jonathan Carapetis, Ross Andrews</field>
<field name="title">Community Management of Endemic Scabies in Remote Aboriginal Communities of Northern Australia: Low Treatment Uptake and High Ongoing Acquisition</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">5</field>
<field name="pages">e444</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Scabies and skin infections are endemic in many Australian Aboriginal
communities. There is limited evidence for effective models of scabies
treatment in high prevalence settings. We aimed to assess the level of
treatment uptake amongst clinically diagnosed scabies cases and amongst their
household contacts. In addition, we aimed to determine the likelihood of
scabies acquisition within these households over the 4 weeks following
treatment provision.

Methods and Findings

We conducted an observational study of households in two scabies-endemic
Aboriginal communities in northern Australia in which a community-based skin
health program was operating. Permethrin treatment was provided for all
householders upon identification of scabies within a household during home
visit. Households were visited the following day to assess treatment uptake
and at 2 and 4 weeks to assess scabies acquisition among susceptible
individuals. All 40 households in which a child with scabies was identified
agreed to participate in the study. Very low levels of treatment uptake were
reported among household contacts of these children (193/440, 44%). Household
contacts who themselves had scabies were more likely to use the treatment than
those contacts who did not have scabies (OR 2.4, 95%CI 1.1, 5.4), whilst males
(OR 0.6, 95%CI 0.42, 0.95) and individuals from high-scabies-burden households
(OR 0.2, 95%CI 0.08, 0.77) were less likely to use the treatment. Among 185
susceptible individuals, there were 17 confirmed or probable new diagnoses of
scabies recorded in the subsequent 4 weeks (9.2%). The odds of remaining
scabies-free was almost 6 times greater among individuals belonging to a
household where all people reported treatment uptake (OR 5.9, 95%CI 1.3, 27.2,
p = 0.02).

Conclusion

There is an urgent need for a more practical and feasible treatment for
community management of endemic scabies. The effectiveness and sustainability
of the current scabies program was compromised by poor treatment uptake by
household contacts of infested children and high ongoing disease transmission.

Author Summary

Like many impoverished areas around the world, Aboriginal communities in
Australia experience an unacceptably high burden of scabies, skin infections,
and secondary complications. Young children are most at risk. Our study
investigated scabies in a remote setting with very high rates of skin disease,
a high level of household overcrowding, and limited infrastructure for
sanitation and preventive health measures. We assessed uptake of scabies
treatment and scabies acquisition following provision of treatment by a
community-based skin program. In a household where scabies was present, we
found that treatment with topical permethrin cream of all close contacts can
significantly reduce a susceptible individual&apos;s risk of infection. Our
findings also demonstrate the challenges of achieving a high level of
treatment participation, with limited permethrin use observed among household
contacts. This suggests an urgent need for a more practical treatment option.
International efforts to reduce childhood morbidity and mortality have
demonstrated the efficacy of numerous child health interventions but have </field></doc>
