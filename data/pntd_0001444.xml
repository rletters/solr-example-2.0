<doc>
<field name="uid">doi:10.1371/journal.pntd.0001444</field>
<field name="doi">10.1371/journal.pntd.0001444</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Mario Del Carpio, Carlos Hugo Mercapide, Juan Carlos Salvitti, Leonardo Uchiumi, José Sustercic, Hector Panomarenko, Jorge Moguilensky, Eduardo Herrero, Gabriel Talmon, Marcela Volpe, Daniel Araya, Guillermo Mujica, Arnoldo Calabro, Sergio Mancini, Carlos Chiosso, Jose Luis Labanchi, Ricardo Saad, Sam Goblirsch, Enrico Brunetti, Edmundo Larrieu</field>
<field name="title">Early Diagnosis, Treatment and Follow-Up of Cystic Echinococcosis in Remote Rural Areas in Patagonia: Impact of Ultrasound Training of Non-Specialists</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">1</field>
<field name="pages">e1444</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Cystic echinococcosis (CE) is a chronic, complex and neglected disease caused
by the larval stage of Echinococcus granulosus. The effects of this neglect
have a stronger impact in remote rural areas whose inhabitants have no chances
of being diagnosed and treated properly without leaving their jobs and
travelling long distances, sometimes taking days to reach the closest referral
center.

Background

In 1980 our group set up a control program in endemic regions with CE in rural
sections of Rio Negro, Argentina. Since 1997, we have used abdominopelvic
ultrasound (US) as a screening method of CE in school children and determined
an algorithm of treatment.

Objectives

To describe the training system of general practitioners in early diagnosis
and treatment of CE and to evaluate the impact of the implementation of the
field program.

Materials and Methods

In 2000, to overcome the shortage of radiologists in the area, we set up a
short training course on Focused Assessment with Sonography for Echinococcosis
(FASE) for general practitioners with no previous experience with US. After
the course, the trainees were able to carry out autonomous ultrasound surveys
under the supervision of the course faculty. From 2000 to 2008, trainees
carried out 22,793 ultrasound scans in children from 6 to 14 years of age, and
diagnosed 87 (0.4%) new cases of CE. Forty-nine (56.4%) were treated with
albendazole, 29 (33.3%) were monitored expectantly and 9 (10.3%) were treated
with surgery.

Discussion

The introduction of a FASE course for general practitioners allowed for the
screening of CE in a large population of individuals in remote endemic areas
with persistent levels of transmission, thus overcoming the barrier of the
great distance from tertiary care facilities. The ability of local
practitioners to screen for CE using US saved the local residents costly
travel time and missed work and proved to be an efficacious and least
expensive intervention tool for both the community and health care system.

Author Summary

Cystic echinococcosis (CE) is an important and widespread disease that affects
sheep, cattle, and humans living in areas where sheep and cattle are raised.
CE is highly endemic in rural sections of Rio Negro, Argentina, where our
group is based. However, it requires continuous monitoring of both populations
with human disease best assessed by means of ultrasound (US) screening. This
is challenging in remote rural areas due to the shortage of imaging
specialists. To overcome this hurdle, we set up a two-day training program of
Focused Assessment with Sonography for Echinococcosis (FASE) on CE for family
medicine practitioners with no previous experience in US. After the course,
they were equipped with portable US scanners and dispatched to remote rural
areas in Rio Negro where they screened patients, located and staged the cysts
and decided on the treatment with the help of surgeons and radiologists in
local tertiary care centers.

The need to tr</field></doc>
