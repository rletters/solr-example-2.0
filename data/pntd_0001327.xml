<doc>
<field name="uid">doi:10.1371/journal.pntd.0001327</field>
<field name="doi">10.1371/journal.pntd.0001327</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Diana N. J. Lockwood, Lavanya Suneetha, Karuna Devi Sagili, Meher Vani Chaduvula, Ismail Mohammed, Wim van Brakel, W. C. Smith, Peter Nicholls, Sujai Suneetha</field>
<field name="title">Cytokine and Protein Markers of Leprosy Reactions in Skin and Nerves: Baseline Results for the North Indian INFIR Cohort</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">12</field>
<field name="pages">e1327</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Previous studies investigating the role of cytokines in the pathogenesis of
leprosy have either been on only small numbers of patients or have not
combined clinical and histological data. The INFIR Cohort study is a
prospective study of 303 new multibacillary leprosy patients to identify risk
factors for reaction and nerve damage. This study characterised the cellular
infiltrate in skin and nerve biopsies using light microscopic and
immunohistochemical techniques to identify any association of cytokine
markers, nerve and cell markers with leprosy reactions.

Methodology/Principal Findings

TNF-α, TGF-β and iNOS protein in skin and nerve biopsies were detected using
monoclonal antibody detection immunohistochemistry techniques in 299 skin
biopsies and 68 nerve biopsies taken from patients at recruitment. The tissues
were stained with hematoxylin and eosin, modified Fite Faraco, CD68 macrophage
cell marker and S100.

Conclusions/Significance

Histological analysis of the biopsies showed that 43% had borderline
tuberculoid (BT) leprosy, 27% borderline lepromatous leprosy, 9% lepromatous
leprosy, 13% indeterminate leprosy types and 7% had no inflammation. Forty-six
percent had histological evidence of a Type 1 Reaction (T1R) and 10% of
Erythema Nodosum Leprosum. TNF-α was detected in 78% of skin biopsies
(181/232), iNOS in 78% and TGF-β in 94%. All three molecules were detected at
higher levels in patients with BT leprosy. TNF-α was localised within
macrophages and epithelioid cells in the granuloma, in the epidermis and in
dermal nerves in a few cases. TNF-α, iNOS and TGF-β were all significantly
associated with T1R (p&lt;0.001). Sixty-eight nerve biopsies were analysed. CD68,
TNF-α and iNOS staining were detectable in 88%, 38% and 28% of the biopsies
respectively. The three cytokines TNF-α, iNOS and TGF-β detected by
immunohistochemistry showed a significant association with the presence of
skin reaction. This study is the first to demonstrate an association of iNOS
and TGF-β with T1R.

Author Summary

Leprosy affects skin and peripheral nerves. Although we have effective
antibiotics to treat the mycobacterial infection, a key part of the disease
process is the accompanying inflammation. This can worsen after starting
antibacterial treatment with episodes of immune mediated inflammation, so
called ‘reactions’. These reactions are associated with worsening of the nerve
damage. We recruited a cohort of 303 newly diagnosed leprosy patients in North
India with the aim of understanding and defining the pathological processes
better. We took skin and nerve biopsies from patients and examined them to
define which molecules and mediators of inflammation were present. We found
high levels of the cytokines Tumour Necrosis Factor alpha, Transforming Growth
Factor beta and inducible Nitric Oxide Synthase in biopsies from patients with
reactions. We also found high levels of bacteria and inflammation in the
nerves. These experiments tell us that we need to determine which other
molecules are present and to explore ways of switching off the production of
these pro-inflammatory molecules.

Introduction

Leprosy is complicated by leprosy reactio</field></doc>
