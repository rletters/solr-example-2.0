<doc>
<field name="uid">doi:10.1371/journal.pntd.0000485</field>
<field name="doi">10.1371/journal.pntd.0000485</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Peter J. Hotez, Alan Fenwick</field>
<field name="title">Schistosomiasis in Africa: An Emerging Tragedy in Our New Global Health Decade</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">9</field>
<field name="pages">e485</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Despite new information that the disease burden of schistosomiasis in Africa
may be equivalent to malaria or HIV/AIDS and a simple annual anthelminthic
treatment for this disease is available for less than 50 cents per person
including delivery costs, we now know that fewer than 5% of the infected
population is receiving coverage. To date, this situation represents one of
the first great failures of the “global health decade” that began in 2000.

Although it has not been officially labeled as such, there are many good
reasons to consider the first years of the 21st century as the global health
decade [1]. Through the President&apos;s Emergency Program for AIDS Relief
(PEPFAR), the President&apos;s Malaria Initiative (PMI), the Global Fund to Fight
AIDS, Tuberculosis, and Malaria, and the sprouting of numerous global health
advocacy organizations, tens of billions of dollars have been committed so far
for HIV/AIDS, tuberculosis, and malaria, i.e., the three major killer
infections of humankind, with millions of people now placed on treatment for
these conditions. Over the same period, there have even been some impressive
results for providing preventive chemotherapy treatments for some of the major
neglected tropical diseases (NTDs). In 2007, an estimated 546 million people
received anthelminthic treatments for lymphatic filariasis (LF), or
approximately 42% of the population at risk, while in 2005, 46% of eligible
populations received ivermectin treatments for onchocerciasis (river
blindness) [2]. With the active involvement of global partnerships for LF and
onchocerciasis, including the Global Programme to Eliminate LF (GPELF), the
African Programme for Onchocerciasis Control (APOC), and the Organization to
Eliminate Onchocerciasis in the Americas (OEPA), together with ongoing
donations from GlaxoSmithKline and Merck &amp; Co., Inc. to provide albendazole
and ivermectin, respectively, there is great optimism that coverage for these
conditions will continue to increase, and that eventually these great scourges
will some day be eliminated as public health problems. Two other NTDs, namely
leprosy and human African trypanosomiasis, are also being targeted for
elimination.

Unfortunately, other NTDs have not fared so well in terms of coverage. Today
it is believed that fewer than 10% of eligible populations living in endemic
regions of Africa, Asia, and the Americas are receiving annual treatments for
their schistosomiasis, intestinal helminth infections, and/or trachoma [2].
The World Health Organization (WHO) and several leading public private
partnerships and non-governmental development organizations are actively
working to correct this situation and to steadily increase global coverage to
the levels of LF and onchocerciasis. Of these, we believe that the single
largest gap in mass drug administration for a serious NTD has to be the almost
non-existent global coverage for schistosomiasis.

There are an estimated 207 million people infected with one of the major
schistosomes [3], with more than 90% of the cases occurring in sub-Saharan
Africa [3],[4]. Through a full consideration of the amount of end-organ
pathologies to the liver (in the case of Schistosoma mansoni and S. japonicum
infections), and to the bladder and kidneys (in the case of S. haematobium
infection) [5], together with the chronic morbidities associated with impair</field></doc>
