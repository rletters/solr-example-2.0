<doc>
<field name="uid">doi:10.1371/journal.pntd.0000310</field>
<field name="doi">10.1371/journal.pntd.0000310</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Daniel J. Carucci, Michael Gottlieb</field>
<field name="title">The Future of TDR: The Need to Adapt to a Changing Global Health Environment</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">11</field>
<field name="pages">e310</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
In this issue of PLoS Neglected Tropical Diseases, Abdallah Daar and
colleagues describe the fourth independent external analysis and evaluation of
the Special Programme for Research and Training in Tropical Diseases (TDR) and
summarize the findings resulting from the review [1]. The panel responsible
for the review and report, which was chaired by Daar, recommends several
critical areas of refocus and reorganization and calls for additional funding
of TDR. In making its recommendations, the panel considers the changes that
have taken place over the last decade. The most notable change was the
unprecedented increase in funding, external to TDR, for research and control
of tropical diseases and the accompanying establishment of product development
partnerships (PDPs).

The panel contends that TDR should increase its focus on research capacity
strengthening, concentrate on neglected populations over neglected diseases,
and strengthen its role in transdisciplinary research, particularly by
augmenting its capacity in social sciences. In spite of TDR&apos;s clear successes
and accomplishments since its inception, the panel concludes that TDR has not
kept pace with a dynamic and changing world of global health research. The
panel also concludes that TDR has not established itself as a credible partner
with other leading funders, and is in danger of being marginalized to the
point of ineffectiveness in addressing potentially critical gaps in tropical
disease research. The review suggests that in its current form, TDR is overly
bureaucratic and poorly aligned with the World Health Organization (WHO), that
it has insufficient funds and flexibility to carry out its mandate, and that
it is not readily able to adapt to the rapidly evolving and dynamic global
health landscape.

In order to ensure that TDR takes its proper role in supporting research and
development (R&amp;D) and training and to capitalize on the tremendous support the
organization has among a broad constituency, especially among scientists in
disease-endemic countries, the report recommends that TDR focus its efforts in
four specific areas: (1) stewardship, (2) expanded interventional research,
(3) research capacity strengthening, and (4) R&amp;D for physical products that
are not otherwise supported. In doing so, and with the recognized need for
substantial increases in funding, TDR must dramatically rethink its objectives
and organization. It should also focus efforts on improved relationships with
its sponsoring organizations and forge new interactions with organizations,
especially public–private partnerships, with complementary interests in R&amp;D
and capacity building.

In their response to Daar and colleagues&apos; external review, Robert Ridley (the
Director of TDR) and colleagues acknowledge many of the shortcomings
identified in the review, and say that TDR has committed to a series of steps
to improve and reorganize based on the evaluation&apos;s recommendations [2]. These
steps include a revised strategic focus on knowledge management, an increased
capacity building effort, and an enhanced focus on neglected areas such as
some aspects of translational research. The external review contributed to
TDR&apos;s new Ten Year Strategy and Business Plan approved by TDR&apos;s Joint
Coordinating Board and endorsed by WHO.

To implement the new strategy, Ridley et al. d</field></doc>
