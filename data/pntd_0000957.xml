<doc>
<field name="uid">doi:10.1371/journal.pntd.0000957</field>
<field name="doi">10.1371/journal.pntd.0000957</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Sue C. Grady, Joseph P. Messina, Paul F. McCord</field>
<field name="title">Population Vulnerability and Disability in Kenya's Tsetse Fly Habitats</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">2</field>
<field name="pages">e957</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Human African Trypanosomiasis (HAT), also referred to as sleeping sickness,
and African Animal Trypanosomaisis (AAT), known as nagana, are highly
prevalent parasitic vector-borne diseases in sub-Saharan Africa. Humans
acquire trypanosomiasis following the bite of a tsetse fly infected with the
protozoa Trypanosoma brucei (T.b.) spp. –i.e., T.b. gambiense in West and
Central Africa and T.b. rhodesiense in East and Southern Africa. Over the last
decade HAT diagnostic capacity to estimate HAT prevalence has improved in
active case-finding areas but enhanced passive surveillance programs are still
lacking in much of rural sub-Saharan Africa.

Methodology/Principal Findings

This retrospective-cross-sectional study examined the use of national census
data (1999) to estimate population vulnerability and disability in Kenya&apos;s 7
tsetse belts to assess the potential of HAT-acquired infection in those areas.
A multilevel study design estimated the likelihood of disability in
individuals, nested within households, nested within tsetse fly habitats of
varying levels of poverty. Residents and recent migrants of working age were
studied. Tsetse fly&apos;s impact on disability was conceptualised via two exposure
pathways: directly from the bite of a pathogenic tsetse fly resulting in HAT
infection or indirectly, as the potential for AAT takes land out of
agricultural production and diseased livestock leads to livestock morbidity
and mortality, contributing to nutritional deficiencies and poverty. Tsetse
belts that were significantly associated with increased disability prevalence
were identified and the direct and indirect exposure pathways were evaluated.

Conclusions/Significance

Incorporating reports on disability from the national census is a promising
surveillance tool that may enhance future HAT surveillance programs in sub-
Saharan Africa. The combined burdens of HAT and AAT and the opportunity costs
of agricultural production in AAT areas are likely contributors to disability
within tsetse-infested areas. Future research will assess changes in the
spatial relationships between high tsetse infestation and human disability
following the release of the Kenya 2009 census at the local level.

Author Summary

The tsetse fly&apos;s influence on human health occurs through direct and indirect
exposure pathways. Directly, the fly is a vector for the disease human African
trypanosomiasis (HAT), which it spreads to nearly 18,000 new victims each
year. Indirectly, the fly is a vector for African Animal Trypanosomaisis (AAT)
also known as nagana, which restricts agricultural production, limiting the
availability of food and contributing to impoverished conditions across rural
sub-Saharan Africa. This historical study used 1999 census data to determine
the prevalence of disability among residents and migrants living within
Kenya&apos;s 7 tsetse fly belts. The results showed that the HAT transmission cycle
may differ for residents and migrants with mechanisms leading to exposures
that are environmentally driven for residents and economically driven for
migrants. The combined burdens of HAT and AAT and the opportunity costs of
agricultural production in AAT areas are potential contributors to disability
within these tsetse-infested areas. Incorporating reports on disability from
the national census appears to be an i</field></doc>
