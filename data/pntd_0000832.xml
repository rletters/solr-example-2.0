<doc>
<field name="uid">doi:10.1371/journal.pntd.0000832</field>
<field name="doi">10.1371/journal.pntd.0000832</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Dinesh Mondal, Kamrul Nahar Nasrin, M. Mamun Huda, Mamun Kabir, Mohammad Shakhawat Hossain, Axel Kroeger, Tania Thomas, Rashidul Haque</field>
<field name="title">Enhanced Case Detection and Improved Diagnosis of PKDL in a Kala-azar-Endemic Area of Bangladesh</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">10</field>
<field name="pages">e832</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Objectives

To support the Bangladesh National Kala-azar Elimination Programme (NKEP), we
investigated the feasibility of using trained village volunteers for detecting
post-kala-azar dermal leishmaniasis (PKDL) cases, using polymerase chain
reaction (PCR) for confirmation of diagnosis and treatment compliance by PKDL
patients in Kanthal union of Trishal sub-district, Mymensingh, Bangladesh.

Methods

In this cross-sectional study, Field Research Assistants (FRAs) conducted
census in the study area, and the research team trained village volunteers on
how to look for PKDL suspects. The trained village volunteers (TVVs) visited
each household in the study area for PKDL suspects and referred the suspected
PKDL cases to the study clinic. The suspected cases underwent physical
examinations by a qualified doctor and rK39 strip testing by the FRAs and, if
positive, slit skin examination (SSE), culture, and PCR of skin specimens and
peripheral buffy coat were done. Those with evidence of Leishmania donovani
(LD) were referred for treatment. All the cases were followed for one year.

Results

The total population of the study area was 29,226 from 6,566 households. The
TVVs referred 52 PKDL suspects. Probable PKDL was diagnosed in 18 of the 52
PKDL suspect cases, and PKDL was confirmed in 9 of the 18 probable PKDL cases.
The prevalence of probable PKDL was 6.2 per 10,000 people in the study area.
Thirteen PKDL suspects self-reported from outside the study area, and probable
and confirmed PKDL was diagnosed in 10 of the 13 suspects and in 5 of 10
probable PKDL cases respectively. All probable PKDL cases had hypopigmented
macules. The median time for PKDL development was 36 months (IQR, 24–48).
Evidence of the LD parasite was documented by SSE and PCR in 3.6% and 64.3% of
the cases, respectively. PCR positivity was associated with gender and
severity of disease. Those who were untreated had an increased risk (odds
ratio = 3.33, 95%CI 1.29–8.59) of having persistent skin lesions compared to
those who were treated. Patients&apos; treatment-seeking behavior and treatment
compliance were poor.

Conclusion

Improved detection of PKDL cases by TVVs is feasible and useful. The NKEP
should promote PCR for the diagnosis of PKDL and should find ways for
improving treatment compliance by patients.

Author Summary

PKDL is a skin disorder which usually develops in 10–20% and about 60% of
patients with visceral leishmaniasis (VL) after treatment respectively in the
Indian subcontinent and Sudan. However, cases among people without prior VL
have also been reported. Except skin lesion, PKDL patients are healthy and
usually do not feel sick. However, persistence of a few PKDL cases is
sufficient to initiate a new epidemic of anthroponotic VL. Thus, identifying
and treating people with PKDL is a key strategy for the elimination of kala-
azar. Diagnosis of PKDL relies upon clinical criteria and a serological test
which is not specific for PKDL. The use of the existing laboratory diagnostic
tools for confirmation of PKDL among PKDL suspects is unknown. In the Indian
subcontinent, PKDL is not self-limited and needs to be treated with sodium
stibogluconate injections for 4–6 months. No data are available relating to
treatm</field></doc>
