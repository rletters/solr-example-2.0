<doc>
<field name="uid">doi:10.1371/journal.pntd.0001417</field>
<field name="doi">10.1371/journal.pntd.0001417</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Phonepasong Ayé Soukhathammavong, Somphou Sayasone, Khampheng Phongluxa, Vilavanh Xayaseng, Jürg Utzinger, Penelope Vounatsou, Christoph Hatz, Kongsap Akkhavong, Jennifer Keiser, Peter Odermatt</field>
<field name="title">Low Efficacy of Single-Dose Albendazole and Mebendazole against Hookworm and Effect on Concomitant Helminth Infection in Lao PDR</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">1</field>
<field name="pages">e1417</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Albendazole and mebendazole are increasingly deployed for preventive
chemotherapy targeting soil-transmitted helminth (STH) infections. We assessed
the efficacy of single oral doses of albendazole (400 mg) and mebendazole (500
mg) for the treatment of hookworm infection in school-aged children in Lao
PDR. Since Opisthorchis viverrini is co-endemic in our study setting, the
effect of the two drugs could also be determined against this liver fluke.

Methodology

We conducted a randomized, open-label, two-arm trial. In total, 200 children
infected with hookworm (determined by quadruplicate Kato-Katz thick smears
derived from two stool samples) were randomly assigned to albendazole (n =
100) and mebendazole (n = 100). Cure rate (CR; percentage of children who
became egg-negative after treatment), and egg reduction rate (ERR; reduction
in the geometric mean fecal egg count at treatment follow-up compared to
baseline) at 21–23 days posttreatment were used as primary outcome measures.
Adverse events were monitored 3 hours post treatment.

Principal Findings

Single-dose albendazole and mebendazole resulted in CRs of 36.0% and 17.6%
(odds ratio: 0.4; 95% confidence interval: 0.2–0.8; P = 0.01), and ERRs of
86.7% and 76.3%, respectively. In children co-infected with O. viverrini,
albendazole and mebendazole showed low CRs (33.3% and 24.2%, respectively) and
moderate ERRs (82.1% and 78.2%, respectively).

Conclusions/Significance

Both albendazole and mebendazole showed disappointing CRs against hookworm,
but albendazole cured infection and reduced intensity of infection with a
higher efficacy than mebendazole. Single-dose administrations showed an effect
against O. viverrini, and hence it will be interesting to monitor potential
ancillary benefits of a preventive chemotherapy strategy that targets STHs in
areas where opisthorchiasis is co-endemic.

Clinical Trial Registration

Current Controlled Trials ISRCTN29126001

Author Summary

Parasitic worms remain a public health problem in developing countries.
Regular deworming with the drugs albendazole and mebendazole is the current
global control strategy. We assessed the efficacies of a single tablet of
albendazole (400 mg) and mebendazole (500 mg) against hookworm in children of
southern Lao PDR. From each child, two stool samples were examined for the
presence and number of hookworm eggs. Two hundred children were found to be
infected. They were randomly assigned to albendazole (n = 100) or mebendazole
(n = 100) treatment. Three weeks later, another two stool samples were
analyzed for hookworm eggs. Thirty-two children who were given albendazole had
no hookworm eggs anymore in their stool, while only 15 children who received
mebendazole were found egg-negative. The total number of hookworm eggs was
reduced by 85.3% in the albendazole and 74.5% in the mebendazole group. About
one third of the children who were co-infected with the Asian liver fluke
Opisthorchis viverrini were cleared from this infection following albendazole
treatment and about one forth in the mebendazole group. Concluding, both
albendazole and mebendazole showed disapp</field></doc>
