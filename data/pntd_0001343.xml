<doc>
<field name="uid">doi:10.1371/journal.pntd.0001343</field>
<field name="doi">10.1371/journal.pntd.0001343</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Burkhard Bauer, Bettina Holzgrefe, Charles Ibrahim Mahama, Maximilian P. O. Baumann, Dieter Mehlitz, Peter-Henning Clausen</field>
<field name="title">Managing Tsetse Transmitted Trypanosomosis by Insecticide Treated Nets - an Affordable and Sustainable Method for Resource Poor Pig Farmers in Ghana</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">10</field>
<field name="pages">e1343</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

An outbreak of tsetse-transmitted trypanosomiasis resulted in more than 50%
losses of domestic pigs in the Eastern Region of Ghana (source: Veterinary
Services, Accra; April 2007). In a control trial from May 4th–October 10th
2007, the efficacy of insecticide-treated mosquito fences to control tsetse
was assessed. Two villages were selected – one serving as control with 14
pigsties and one experimental village where 24 pigsties were protected with
insecticide treated mosquito fences. The 100 cm high, 150denier polyester
fences with 100 mg/m2 deltamethrin and a UV protector were attached to
surrounding timber poles and planks. Bi-monthly monitoring of tsetse densities
with 10 geo-referenced bi-conical traps per village showed a reduction of more
than 90% in the protected village within two months. Further reductions
exceeding 95% were recorded during subsequent months. The tsetse population in
the control village was not affected, only displaying seasonal variations.
Fifty pigs from each village were ear-tagged and given a single curative
treatment with diminazene aceturate (3.5 mg/kg bw) after their blood samples
had been taken. The initial trypanosome prevalence amounted to 76% and 72% of
protected and control animals, respectively, and decreased to 16% in protected
as opposed to 84% in control pigs three months after intervention. After six
months 8% of the protected pigs were infected contrasting with 60% in the
control group.

Author Summary

Sixty million people and more than 70 million livestock live in Africa at risk
of contracting trypanosomiasis. The heads of member states of the African
Union (AU) declared the year 2000 as the beginning of the Pan African Tsetse
and Trypanosomiasis Eradication Campaign to eradicate tsetse flies and the
diseases they transmit from the continent. For the first time the social and
economic consequences of trypanosomiasis were brought to the attention of the
affected populations. Efforts to control the fatal disease in man and
livestock are based on treatment of patients and livestock with trypanocidal
drugs. Resistance-related drug failures are increasing. Methods to control
tsetse flies rely on insecticides. Past tsetse campaigns proved unsustainable
due to the public good character of most control techniques such as aerial and
ground spraying, traps or targets. Treating livestock with insecticides may be
more sustainable and is also controlling ticks, which can transmit
economically important and often fatal diseases. Costs per head of livestock
and tick resistance against insecticides are seen as a major hindrance to
their continuous large-scale use. Insecticide treated nets proved an effective
and affordable means protecting pigs against tsetse transmitted trypanosomoses
in Ghana.

Introduction

The tsetse and trypanosomiasis problem to man and his livestock lies at the
heart of Africa&apos;s poverty. Estimates of gross national per capita income show
that 20 of the world&apos;s 25 poorest countries are affected by tsetse-transmitted
trypanosomoses [1]. An ever-growing demand for meat and milk products has led
to a policy where improvements in productivity are sought by introducing
exotic breeds or by cross-breedi</field></doc>
