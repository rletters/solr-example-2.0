<doc>
<field name="uid">doi:10.1371/journal.pntd.0000883</field>
<field name="doi">10.1371/journal.pntd.0000883</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Nadine Litzba, Christoph S. Klade, Sabine Lederer, Matthias Niedrig</field>
<field name="title">Evaluation of Serological Diagnostic Test Systems Assessing the Immune Response to Japanese Encephalitis Vaccination</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">11</field>
<field name="pages">e883</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

A new commercial anti-Japanese encephalitis virus IgM and IgG indirect
immunofluorescence test (IIFT) was evaluated for the detection of the humoral
immune response after Japanese encephalitis vaccination. The IgM IIFT was
compared to two IgM capture ELISAs and the IgG IIFT was analysed in comparison
to a plaque reduction neutralization test (PRNT50) and an IgG ELISA. Moreover,
the course of the immune reaction after vaccination with an inactivated JEV
vaccine was examined. For the present study 300 serum samples from different
blood withdrawals from 100 persons vaccinated against Japanese encephalitis
were used. For the IgM evaluation, altogether 78 PRNT50 positive samples taken
7 to 56 days after vaccination and 78 PRNT50 negative sera were analyzed with
the Euroimmun anti-JEV IgM IIFT, the Panbio Japanese Encephalitis – Dengue IgM
Combo ELISA and the InBios JE Detect IgM capture ELISA. For the IgG
evaluation, 100 sera taken 56 days after vaccination and 100 corresponding
sera taken before vaccination were tested in the PRNT50, the Euroimmun anti-
JEV IgG IIFT, and the InBios JE Detect IgG ELISA. The Euroimmun IgM IIFT
showed in comparison to the Panbio ELISA a specificity of 95% and a
sensitivity of 86%. With respect to the InBios ELISA, the values were 100% and
83.9%, respectively. The analysis of the Euroimmun IgG IIFT performance and
the PRNT50 results demonstrated a specificity of 100% and a sensitivity of
93.8%, whereas it was not possible to detect more than 6.6% of the PRNT50
positive sera as positive with the InBios JE Detect IgG ELISA. Thus, the IIFT
is a valuable alternative to the established methods in detecting anti-JEV
antibodies after vaccination in travellers and it might prove useful for the
diagnosis of acutely infected persons.

Author Summary

Japanese encephalitis, caused by the Japanese encephalitis virus, is the most
prominent viral encephalitis in Asia. Three billion people live in endemic
areas and at least 50,000 clinical cases occur each year, although reliable
vaccines are available. Concerning the burden caused by this disease, more
should be done to prevent it. Good and reliable diagnostics are one of the
prerequisites for an effective fight against the virus, but it is nearly
impossible to produce and evaluate an in-house assay according to high
standard quality criteria as done for commercial tests. Only a few commercial
assays are available and the thorough evaluation of these assays is of great
importance for diagnostic laboratories. The sensitivity and specificity are
statistical measures to assess the performance of a diagnostic assay. In this
study the Euroimmun IgM indirect immunofluorescence test (IIFT) was compared
to the Panbio and InBios IgM ELISAs. It showed a specificity of 95% and 100%,
and a sensitivity of 86% and 93.8%, respectively. The specificity of the IgG
IIFT in comparison to PRNT50 was 100% and the sensitivity was 93.8%. Overall,
the IIFT showed a comparable performance and could be used as an alternative
to the established assays.

Introduction

Japanese encephalitis virus (JEV), a mosquito-borne pathogen of the genus
Flavivirus, family Flaviviridae, is the main cause of viral encephalitis in
Asia. Three billion people live in the endemic areas and at least 50,000
clinical Japanese encepha</field></doc>
