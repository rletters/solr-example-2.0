<doc>
<field name="uid">doi:10.1371/journal.pntd.0000075</field>
<field name="doi">10.1371/journal.pntd.0000075</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Peter Steinmann, Xiao-Nong Zhou, Zun-Wei Du, Jin-Yong Jiang, Li-Bo Wang, Xue-Zhong Wang, Lan-Hua Li, Hanspeter Marti, Jürg Utzinger</field>
<field name="title">Occurrence of Strongyloides stercoralis in Yunnan Province, China, and Comparison of Diagnostic Methods</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2007</field>
<field name="volume">1</field>
<field name="number">1</field>
<field name="pages">e75</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Strongyloides stercoralis is a neglected soil-transmitted helminth species,
and there is a lack of parasitologic and epidemiologic data pertaining to this
parasite in China and elsewhere. We studied the local occurrence of S.
stercoralis in a village in Yunnan province, China, and comparatively assessed
the performance of different diagnostic methods.

Methodology/Principal Findings

Multiple stool samples from a random population sample were subjected to the
Kato-Katz method, an ether-concentration technique, the Koga agar plate
method, and the Baermann technique. Among 180 participants who submitted at
least 2 stool samples, we found a S. stercoralis prevalence of 11.7%. Males
had a significantly higher prevalence than females (18.3% versus 6.1%, p =
0.011), and infections were absent in individuals &lt;15 years of age. Infections
were only detected by the Baermann (highest sensitivity) and the Koga agar
plate method, but neither with the Kato-Katz nor an ether-concentration
technique. The examination of 3 stool samples rather than a single one
resulted in the detection of 62% and 100% more infections when employing the
Koga agar plate and the Baermann technique, respectively. The use of a
mathematical model revealed a ‘true’ S. stercoralis prevalence in the current
setting of up to 16.3%.

Conclusions/Significance

We conclude that S. stercoralis is endemic in the southern part of Yunnan
province and that differential diagnosis and integrated control of intestinal
helminth infections needs more pointed emphasis in rural China.

Author Summary

An estimated 30 million to 100 million people are infected with the parasitic
worm Strongyloides stercoralis, the causative agent of strongyloidiasis, and
yet this is a neglected tropical disease. The diagnosis of this parasite
requires specialized techniques (e.g. Baermann and Koga agar plate method),
but these are rarely employed in epidemiologic studies. We assessed the
occurrence of S. stercoralis in a rural part of southern Yunnan province,
China, and compared different diagnostic methods. At least two stool samples
were obtained from 180 randomly selected individuals, and examined with four
diagnostic approaches, including the Koga agar plate and the Baermann
technique. Twenty-one individuals were infected with S. stercoralis
(prevalence: 11.7%). Males were more often infected than females (18.3% versus
6.1%, p = 0.011). Infections were absent in children below the age of 15
years. The Baermann technique showed a higher sensitivity than the Koga agar
plate method, and the examination of multiple stool samples improved the
diagnostic performances of both methods. The use of a mathematical model
suggested a ‘true’ S. stercoralis prevalence of 16.3%. There is a need to
further study the epidemiology of strongyloidiasis in other parts of China,
and control measures are required in settings with high prevalences as
observed in this area.

Introduction

Soil-transmitted helminthiases are caused by infections with intestinal
nematodes, of which Ascaris lumbricoides, Trichuris trichiura and the
hookworms (Ancylostoma duodenale and Necator americanus) are the most
widespread species [1–3]. Collectively, these soil-transmitted </field></doc>
