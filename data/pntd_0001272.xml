<doc>
<field name="uid">doi:10.1371/journal.pntd.0001272</field>
<field name="doi">10.1371/journal.pntd.0001272</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Carlos A. Flores-López, Carlos A. Machado</field>
<field name="title">Analyses of 32 Loci Clarify Phylogenetic Relationships among Trypanosoma cruzi Lineages and Support a Single Hybridization prior to Human Contact</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">8</field>
<field name="pages">e1272</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The genetic diversity of Trypanosoma cruzi, the etiological agent of Chagas
disease, has been traditionally divided in two major groups, T. cruzi I and
II, corresponding to discrete typing units TcI and TcII-VI under a recently
proposed nomenclature. The two major groups of T. cruzi seem to differ in
important biological characteristics, and are thus thought to represent a
natural division relevant for epidemiological studies and development of
prophylaxis. To understand the potential connection between the different
manifestations of Chagas disease and variability of T. cruzi strains, it is
essential to have a correct reconstruction of the evolutionary history of T.
cruzi.

Methodology/Principal Findings

Nucleotide sequences from 32 unlinked loci (&gt;26 Kilobases of aligned sequence)
were used to reconstruct the evolutionary history of strains representing the
known genetic variability of T. cruzi. Thorough phylogenetic analyses show
that the original classification of T. cruzi in two major lineages does not
reflect its evolutionary history and that there is only strong evidence for
one major and recent hybridization event in the history of this species.
Furthermore, estimates of divergence times using Bayesian methods show that
current extant lineages of T. cruzi diverged very recently, within the last 3
million years, and that the major hybridization event leading to hybrid
lineages TcV and TcVI occurred less than 1 million years ago, well before the
contact of T. cruzi with humans in South America.

Conclusions/Significance

The described phylogenetic relationships among the six major genetic
subdivisions of T. cruzi should serve as guidelines for targeted
epidemiological and prophylaxis studies. We suggest that it is important to
reconsider conclusions from previous studies that have attempted to uncover
important biological differences between the two originally defined major
lineages of T. cruzi especially if those conclusions were obtained from single
or few strains.

Author Summary

Trypanosoma cruzi is the protozoan parasite that causes Chagas disease, a
major health problem in Latin America. The genetic diversity of this parasite
has been traditionally divided in two major groups: T. cruzi I and II, which
can be further divided in six major genetic subdivisions (subgroups TcI-TcVI).
T. cruzi I and II seem to differ in important biological characteristics, and
are thought to represent a natural division relevant for epidemiological
studies and development of prophylaxis. Having a correct reconstruction of the
evolutionary history of T. cruzi is essential for understanding the potential
connection between the genetic and phenotypic variability of T. cruzi with the
different manifestations of Chagas disease. Here we present results from a
comprehensive phylogenetic analysis of T. cruzi using more than 26 Kb of
aligned sequence data. We show strong evidence that T. cruzi II (TcII-VI) is
not a natural evolutionary group but a paraphyletic lineage and that all major
lineages of T. cruzi evolved recently (&lt;3 million years ago [mya]).
Furthermore, the sequence data is consistent with one major hybridization
event having occurred in this species recently (&lt; 1 mya) but well before T.
cruzi entered in contact with humans in </field></doc>
