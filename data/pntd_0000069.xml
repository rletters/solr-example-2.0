<doc>
<field name="uid">doi:10.1371/journal.pntd.0000069</field>
<field name="doi">10.1371/journal.pntd.0000069</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Gavin Yamey</field>
<field name="title">Research Ethics and Reporting Standards at PLoS Neglected Tropical Diseases</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2007</field>
<field name="volume">1</field>
<field name="number">1</field>
<field name="pages">e69</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
PLoS Neglected Tropical Diseases has been open to submissions since February
2007, and we have been impressed by the number, quality, and range of the
research articles we have received to date—from laboratory studies and
assessments of new diagnostic tests to observational epidemiology, clinical
trials, and qualitative research. But we have also noted that some authors
have omitted crucial information about the ethical conduct of their study, or
have been unaware of international guidance on how to report specific types of
research. We would therefore like to remind authors of our reporting
requirements (more detailed guidance is available at
http://www.plosntds.org/guidelines.php).

Ethical Approval and Informed Consent

Patients with neglected tropical diseases are among the most economically and
socially disadvantaged people on the planet. So when it comes to conducting
clinical research among this vulnerable population, investigators must take
particular precautions to protect the welfare of research participants.

One crucial precaution that all researchers must take is to seek prior
approval for their study from a relevant institutional review board (IRB),
which must be named in the paper. We reserve the right to ask authors to send
a copy of the board&apos;s formal approval. During the study, if the authors
deviated from their protocol—and particularly if the deviation exposed
participants to any additional risks—we expect the researchers to have
immediately informed the board about the deviation and we require
documentation of the board&apos;s response.

Some authors have argued that their study was exempt from IRB approval since
the data were collected as part of the routine diagnosis or treatment of
patients. But because of the potential risks involved in recording data about
patients for research purposes, such as breaches of patient confidentiality,
an IRB should be asked to assess these risks ahead of the study. The IRB may
indeed decide that no specific approval is required, in which case we ask
authors to send a copy of the IRB exemption letter.

We are aware that in some research settings there may not be a local IRB.
Under these circumstances, authors should have made every attempt to find an
ethics committee outside the immediate region to approve the study. In the
very rare event that it was impossible to find such a committee, we expect
authors to have conducted their study according to the principles expressed in
the Declaration of Helsinki (http://www.wma.net/e/policy/b3.htm) and to have
familiarized themselves with accepted international norms and standards, such
as those set out by the International Committee of Medical Journal Editors
(ICMJE; http://www.icmje.org/) and the Committee on Publication Ethics
(http://www.publicationethics.org.uk/).

A second crucial precaution is that investigators must obtain fully informed,
ideally written, consent from all participants in their study. We reserve the
right to ask authors to send a copy of the consent form that the participants
signed. If details about a specific patient are included in a paper (e.g., a
case description or a clinical photograph), authors must send a copy of the
patient&apos;s written informed consent to have these clinical details published in
PLoS Neglected Tropical Diseases (using the consent form at
http://www.plosntds.org/guidelines.php#supporting, availabl</field></doc>
