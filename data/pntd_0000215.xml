<doc>
<field name="uid">doi:10.1371/journal.pntd.0000215</field>
<field name="doi">10.1371/journal.pntd.0000215</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Martijn D. de Kruif, Tatty E. Setiati, Albertus T. A. Mairuhu, Penelopie Koraka, Hella A. Aberson, C. Arnold Spek, Albert D. M. E. Osterhaus, Pieter H. Reitsma, Dees P. M. Brandjes, Augustinus Soemantri, Eric C. M. van Gorp</field>
<field name="title">Differential Gene Expression Changes in Children with Severe Dengue Virus Infections</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">4</field>
<field name="pages">e215</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

The host response to dengue virus infection is characterized by the production
of numerous cytokines, but the overall picture appears to be complex. It has
been suggested that a balance may be involved between protective and
pathologic immune responses. This study aimed to define differential immune
responses in association with clinical outcomes by gene expression profiling
of a selected panel of inflammatory genes in whole blood samples from children
with severe dengue infections.

Methodology/Principal Findings

Whole blood mRNA from 56 Indonesian children with severe dengue virus
infections was analyzed during early admission and at day −1, 0, 1, and 5–8
after defervescence. Levels were related to baseline levels collected at a
1-month follow-up visit. Processing of mRNA was performed in a single reaction
by multiplex ligation-dependent probe amplification, measuring mRNA levels
from genes encoding 36 inflammatory proteins and 14 Toll-like receptor
(TLR)-associated molecules. The inflammatory gene profiles showed up-
regulation during infection of eight genes, including IFNG and IL12A, which
indicated an antiviral response. On the contrary, genes associated with the
nuclear factor (NF)-κB pathway were down-regulated, including NFKB1, NFKB2,
TNFR1, IL1B, IL8, and TNFA. Many of these NF-κB pathway–related genes, but not
IFNG or IL12A, correlated with adverse clinical events such as development of
pleural effusion and hemorrhagic manifestations. The TLR profile showed that
TLRs were differentially activated during severe dengue infections: increased
expression of TLR7 and TLR4R3 was found together with a decreased expression
of TLR1, TLR2, TLR4R4, and TLR4 co-factor CD14.

Conclusions/Significance

These data show that different immunological pathways are differently
expressed and associated with different clinical outcomes in children with
severe dengue infections.

Author Summary

Dengue virus infection is an impressively emerging disease that can be fatal
in severe cases. It is not precisely clear why some patients progress to
severe disease whereas most patients only suffer from a mild infection. In
severe disease, a “cytokine storm” is induced, which indicates the release of
a great number of inflammatory mediators (“cytokines”). Evidence suggested
that a balance could be involved between protective and pathologic cytokine
release patterns. We studied this concept in a cohort of Indonesian children
with severe dengue disease using a gene expression profiling method.

The study showed that the immune response to severe dengue infection was
characterized by up-regulation of interferon pathway–associated cytokines and
down-regulation of nuclear factor (NF)-kappaB pathway–associated cytokines.
Many of the NF-kappaB pathway–related genes, but not the interferon
pathway–associated genes, correlated with adverse clinical events such as
development of pleural effusion and hemorrhagic manifestations. In addition,
the study showed that the expression of specific pathogen recognition
receptors called Toll-like receptors was differentially regulated in patients
with dengue. Together, the data show </field></doc>
