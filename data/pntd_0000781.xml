<doc>
<field name="uid">doi:10.1371/journal.pntd.0000781</field>
<field name="doi">10.1371/journal.pntd.0000781</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Da-Bing Lu, James W. Rudge, Tian-Ping Wang, Christl A. Donnelly, Guo-Ren Fang, Joanne P. Webster</field>
<field name="title">Transmission of Schistosoma japonicum in Marshland and Hilly Regions of China: Parasite Population Genetic and Sibship Structure</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">8</field>
<field name="pages">e781</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

The transmission dynamics of Schistosoma japonicum remain poorly understood,
as over forty species of mammals are suspected of serving as reservoir hosts.
However, knowledge of the population genetic structure and of the full-sibship
structuring of parasites at two larval stages will be useful in defining and
tracking the transmission pattern between intermediate and definitive hosts.
S. japonicum larvae were therefore collected in three marshland and three
hilly villages in Anhui Province of China across three time points: April and
September-October 2006, and April 2007, and then genotyped with six
microsatellite markers. Results from the population genetic and sibling
relationship analyses of the parasites across two larval stages demonstrated
that, within the marshland, parasites from cattle showed higher genetic
diversity than from other species; whereas within the hilly region, parasites
from dogs and humans displayed higher genetic diversity than those from
rodents. Both the extent of gene flow and the estimated proportion of full-sib
relationships of parasites between two larval stages indicated that the
cercariae identified within intermediate hosts in the marshlands mostly came
from cattle, whereas in the hilly areas, they were varied between villages,
coming primarily from rodents, dogs or humans. Such results suggest a
different transmission process within the hilly region from within the
marshlands. Moreover, this is the first time that the sibling relationship
analysis was applied to the transmission dynamics for S. japonicum.

Author Summary

Schistosoma japonicum involves two obligatory host stages, with asexual
reproduction within a molluscan host and sexual reproduction within a
mammalian host. Having over 40 species of mammals suspected of being potential
reservoirs complicates the transmission patterns. Understanding the complex
transmission patterns is further hampered by the ethical and logistical
difficulty in sampling adult worms from mammalian hosts. However, the two
free-swimming larval stages, cercariae (released from a mollusc and then
infective to a mammal) and miracidia (hatched from eggs passed in a mammal&apos;s
faeces, and then infective to a mollusc), are available, and elucidating the
genetic composition of parasites at theses two stages could provide
information of infection processes. Here we sampled cercariae during April
2006, miracidia during September-October 2006, and cercariae during April 2007
in three marshland and three hilly villages in Anhui Province of China, and,
using microsatellite markers, analyzed the population genetic structure and,
for the first time, the familial relationships of parasites at different
stages. We found contrasting population structures of parasites, and host
species-associated diversities and transmission patterns of parasites between
and within two regions. Moreover, we demonstrate that the successful
application of sibship analyses to infection process provides an alternative
approach to the dissection of transmission dynamics.

Introduction

A major challenge to the biomedical science in the 21st century is gaining a
thorough understanding of the dynamics and evolution of multi-host pathogens
[1]. This is especially true for Schisto</field></doc>
