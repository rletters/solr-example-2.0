<doc>
<field name="uid">doi:10.1371/journal.pntd.0000467</field>
<field name="doi">10.1371/journal.pntd.0000467</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Andrew C. Steer, Adam W. J. Jenney, Joseph Kado, Michael R. Batzloff, Sophie La Vincente, Lepani Waqatakirewa, E. Kim Mulholland, Jonathan R. Carapetis</field>
<field name="title">High Burden of Impetigo and Scabies in a Tropical Country</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2009</field>
<field name="volume">3</field>
<field name="number">6</field>
<field name="pages">e467</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Impetigo and scabies are endemic diseases in many tropical countries; however
the epidemiology of these diseases is poorly understood in many areas,
particularly in the Pacific.

Methodology/Principal Findings

We conducted three epidemiological studies in 2006 and 2007 to determine the
burden of disease due to impetigo and scabies in children in Fiji using simple
and easily reproducible methodology. Two studies were performed in primary
school children (one study was a cross-sectional study and the other a
prospective cohort study over ten months) and one study was performed in
infants (cross-sectional). The prevalence of active impetigo was 25.6% (95% CI
24.1–27.1) in primary school children and 12.2% (95% CI 9.3–15.6) in infants.
The prevalence of scabies was 18.5% (95% CI 17.2–19.8) in primary school
children and 14.0% (95% CI 10.8–17.2) in infants. The incidence density of
active impetigo, group A streptococcal (GAS) impetigo, Staphylococcus aureus
impetigo and scabies was 122, 80, 64 and 51 cases per 100 child-years
respectively. Impetigo was strongly associated with scabies infestation (odds
ratio, OR, 2.4, 95% CI 1.6–3.7) and was more common in Indigenous Fijian
children when compared with children of other ethnicities (OR 3.6, 95% CI
2.7–4.7). The majority of cases of active impetigo in the children in our
study were caused by GAS. S. aureus was also a common cause (57.4% in school
aged children and 69% in infants).

Conclusions/Significance

These data suggest that the impetigo and scabies disease burden in children in
Fiji has been underestimated, and possibly other tropical developing countries
in the Pacific. These diseases are more than benign nuisance diseases and
consideration needs to be given to expanded public health initiatives to
improve their control.

Author Summary

Scabies and impetigo are often thought of as nuisance diseases, but have the
potential to cause a great deal of morbidity and even mortality if infection
becomes complicated. Accurate assessments of these diseases are lacking,
particularly in tropical developing countries. We performed a series of
studies in infants and primary school children in Fiji, a tropical developing
country in the South Pacific. Impetigo was very common: more than a quarter of
school-aged children and 12% of infants had active impetigo. Scabies was also
very common affecting 18% of school children and 14% of infants. The group A
streptococcus was the most common infective organism followed by
Staphylococcus aureus. The size of the problem has been underestimated,
particularly in the Pacific. It is time for more concerted public health
efforts in controlling impetigo and scabies.

Introduction

A recent review by the World Health Organization Department of Child and
Adolescent Health and Development indicated that impetigo and scabies are
endemic disease in many tropical and subtropical countries [1]. This review
concluded that more data documenting the burden of skin disease in children
are required at the community level because of gaps in the evidence,
particularly relating to disease burden in infants and the reporting of
incidence data. The authors of the review also concluded that comparisons
between studies of childhood skin disease ar</field></doc>
