<doc>
<field name="uid">doi:10.1371/journal.pntd.0000754</field>
<field name="doi">10.1371/journal.pntd.0000754</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Dominik Glinz, Kigbafori D. Silué, Stefanie Knopp, Laurent K. Lohourignon, Kouassi P. Yao, Peter Steinmann, Laura Rinaldi, Giuseppe Cringoli, Eliézer K. N'Goran, Jürg Utzinger</field>
<field name="title">Comparing Diagnostic Accuracy of Kato-Katz, Koga Agar Plate, Ether-Concentration, and FLOTAC for Schistosoma mansoni and Soil-Transmitted Helminths</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2010</field>
<field name="volume">4</field>
<field name="number">7</field>
<field name="pages">e754</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Infections with schistosomes and soil-transmitted helminths exert a
considerable yet underappreciated economic and public health burden on
afflicted populations. Accurate diagnosis is crucial for patient management,
drug efficacy evaluations, and monitoring of large-scale community-based
control programs.

Methods/Principal Findings

The diagnostic accuracy of four copromicroscopic techniques (i.e., Kato-Katz,
Koga agar plate, ether-concentration, and FLOTAC) for the detection of
Schistosoma mansoni and soil-transmitted helminth eggs was compared using
stool samples from 112 school children in Côte d&apos;Ivoire. Combined results of
all four methods served as a diagnostic ‘gold’ standard and revealed
prevalences of S. mansoni, hookworm, Trichuris trichiura, Strongyloides
stercoralis and Ascaris lumbricoides of 83.0%, 55.4%, 40.2%, 33.9% and 28.6%,
respectively. A single FLOTAC from stool samples preserved in sodium acetate-
acetic acid-formalin for 30 or 83 days showed a higher sensitivity for S.
mansoni diagnosis (91.4%) than the ether-concentration method on stool samples
preserved for 40 days (85.0%) or triplicate Kato-Katz using fresh stool
samples (77.4%). Moreover, a single FLOTAC detected hookworm, A. lumbricoides
and T. trichiura infections with a higher sensitivity than any of the other
methods used, but resulted in lower egg counts. The Koga agar plate method was
the most accurate diagnostic assay for S. stercoralis.

Conclusion/Significance

We have shown that the FLOTAC method holds promise for the diagnosis of S.
mansoni. Moreover, our study confirms that FLOTAC is a sensitive technique for
detection of common soil-transmitted helminths. For the diagnosis of S.
stercoralis, the Koga agar plate method remains the method of choice.

Author Summary

Infections with parasitic worms (e.g., Schistosoma mansoni, hookworm,
roundworm, whipworm, and threadworm) are still widespread in the developing
world. Accurate diagnosis is important for better patient management and for
monitoring of deworming programs. Unfortunately, methods to detect parasite
eggs or larvae in stool samples lack sensitivity, particularly when infection
intensities are low. The most widely used method for the diagnosis of S.
mansoni, hookworm, roundworm and whipworm in epidemiological surveys is the
Kato-Katz technique. Recently, the FLOTAC technique has shown a higher
sensitivity than the Kato-Katz method for the diagnosis of hookworm, roundworm
and whipworm, but no data are available for S. mansoni. We compared the
diagnostic accuracy of the FLOTAC with the Kato-Katz, ether-concentration and
Koga agar plate techniques for S. mansoni and other parasitic worm infections
using stool samples from 112 school children from Côte d&apos;Ivoire. FLOTAC showed
the highest sensitivity for S. mansoni diagnosis. Egg counts, however, were
lower when using FLOTAC, an issue which needs further investigations. The
FLOTAC, Kato-Katz and ether-concentration techniques failed to accurately
detect threadworm larvae, and hence, the Koga agar plate remains the method of
choice for this neglected parasite.

Introduction

Schistosomiasis a</field></doc>
