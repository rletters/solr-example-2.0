<doc>
<field name="uid">doi:10.1371/journal.pntd.0001622</field>
<field name="doi">10.1371/journal.pntd.0001622</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Míriam Maria Costa, Marcos Penido, Mariana Silva dos Santos, Daniel Doro, Eloísa de Freitas, Marilene Susan Marques Michalick, Gabriel Grimaldi, Ricardo Tostes Gazzinelli, Ana Paula Fernandes</field>
<field name="title">Improved Canine and Human Visceral Leishmaniasis Immunodiagnosis Using Combinations of Synthetic Peptides in Enzyme-Linked Immunosorbent Assay</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">5</field>
<field name="pages">e1622</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Zoonotic visceral leishmaniasis (VL) is a severe infectious disease caused by
protozoan parasites of the genus Leishmania and the domestic dogs are the main
urban parasite reservoir hosts. In Brazil, indirect fluorescence antibody
tests (IFAT) and indirect enzyme linked immunosorbent assay (ELISA) using
promastigote extracts are widely used in epidemiological surveys. However,
their sensitivity and specificity have often been compromised by the use of
complex mixtures of antigens, which reduces their accuracy allowing the
maintenance of infected animals that favors transmission to humans. In this
context, the use of combinations of defined peptides appears favorable.
Therefore, they were tested by combinations of five peptides derived from the
previously described Leishmania diagnostic antigens A2, NH, LACK and K39.

Methodology/Principal Findings

Combinations of peptides derived A2, NH, LACK and K39 antigens were used in
ELISA with sera from 44 human patients and 106 dogs. Improved sensitivities
and specificities, close to 100%, were obtained for both sera of patients and
dogs. Moreover, high sensitivity and specificity were observed even for canine
sera presenting low IFAT anti-Leishmania antibody titers or from asymptomatic
animals.

Conclusions/Significance

The use of combinations of B cell predicted synthetic peptides derived from
antigens A2, NH, LACK and K39 may provide an alternative for improved
sensitivities and specificities for immunodiagnostic assays of VL.

Author Summary

Visceral leishmaniasis is endemic in many areas of tropical and subtropical
America where it constitutes a significant public health problem. It is
usually diagnosed by enzyme-linked immunosorbent assays (ELISA) using crude
Leishmania antigens, but a variety of other immunological methods may also be
applied. Although these approaches are useful, historically their sensitivity
and specificity have often been compromised by the use of complex mixtures of
antigens. In this context, the use of combinations of purified, well-
characterized antigens appears preferable and may yield better results. In the
present study, combinations of peptides derived from the previously described
Leishmania diagnostic antigens A2, NH, LACK and K39 were used in ELISA against
sera from 106 dogs and 44 human patients. Improved sensitivities and
specificities, close to 100%, for both sera of patients and dogs was observed
for ELISA using some combinations of the peptides, including the detection of
VL in dogs with low anti-Leishmania antibody titers and asymptomatic
infection. So, the use of combinations of B cell predicted synthetic peptides
derived from antigens A2, NH, LACK and K39 may provide an alternative for
improved sensitivities and specificities for immunodiagnostic assays of VL.

Introduction

Zoonotic visceral leishmaniasis (VL) caused by Leishmania infantum is an
important emerging parasitic disease found in countries around the
Mediterranean basin, in the Middle East, and in Latin America [1], [2]. In
these areas, wild canids constitute major sylvatic reservoirs, and domestic
dogs are the principal urban reservoir</field></doc>
