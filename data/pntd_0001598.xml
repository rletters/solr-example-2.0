<doc>
<field name="uid">doi:10.1371/journal.pntd.0001598</field>
<field name="doi">10.1371/journal.pntd.0001598</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Adele A. Rodrigues, Jasson S. S. Saosa, Grace K. da Silva, Flávia A. Martins, Aline A. da Silva, Cecílio P. da Silva Souza Neto, Catarina V. Horta, Dario S. Zamboni, João S. da Silva, Eloisa A. V. Ferro, Claudio V. da Silva</field>
<field name="title">IFN-γ Plays a Unique Role in Protection against Low Virulent Trypanosoma cruzi Strain</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2012</field>
<field name="volume">6</field>
<field name="number">4</field>
<field name="pages">e1598</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

T. cruzi strains have been divided into six discrete typing units (DTUs)
according to their genetic background. These groups are designated T. cruzi I
to VI. In this context, amastigotes from G strain (T. cruzi I) are highly
infective in vitro and show no parasitemia in vivo. Here we aimed to
understand why amastigotes from G strain are highly infective in vitro and do
not contribute for a patent in vivo infection.

Methodology/Principal Findings

Our in vitro studies demonstrated the first evidence that IFN-γ would be
associated to the low virulence of G strain in vivo. After intraperitoneal
amastigotes inoculation in wild-type and knockout mice for TNF-α, Nod2, Myd88,
iNOS, IL-12p40, IL-18, CD4, CD8 and IFN-γ we found that the latter is crucial
for controlling infection by G strain amastigotes.

Conclusions/Significance

Our results showed that amastigotes from G strain are highly infective in
vitro but did not contribute for a patent infection in vivo due to its
susceptibility to IFN-γ production by host immune cells. These data are useful
to understand the mechanisms underlying the contrasting behavior of different
T. cruzi groups for in vitro and in vivo infection.

Author Summary

Trypanosoma cruzi, an obligate intracellular protozoan, is the etiological
agent of Chagas disease that represents an important public health burden in
Latin America. The infection with this parasite can lead to severe
complications in cardiac and gastrointestinal tissue depending on the strain
of parasite and host genetics. Currently, six genetic groups (T. cruzi I to
VI) have been identified in this highly genetic and diverse parasite.The
majority of published data concerning host immune response has been obtained
from studying T. cruzi II to VI-infected mice, and the genetic differences
between T. cruzi II to VI and T. cruzi I strains are large. Here we aimed to
understand how amastigotes from T. cruzi I G strain are highly infective in
vitro and do not contribute for a patent parasitemia in vivo. Our results
showed that amastigotes from G strain are highly susceptible to IFN-γ
treatment in vitro and secretion by immune cells in vivo. This information may
represent important findings to design novel immune strategies to control
pathology that may be caused by different strains in the same host.

Introduction

Chagas disease is a chronic, systemic, parasitic infection caused by the
protozoan Trypanosoma cruzi. The disease affects about 8 million people in
Latin America, of whom 30–40% either have or will develop cardiomyopathy,
digestive megasyndromes, or both [1]. Knowledge of the pathology and immune
response to T. cruzi infection has been largely obtained from murine models.
These models have shown that the innate and adaptive immune responses play an
important role in parasite control, depending on the combined action of
various cellular types including NK, CD4+ and CD8+ as well as on the
production of antibodies by B cells [2]–[5]. Resistance to T. cruzi infection
has been associated with the production of the pro-inflammatory cytokines
IL-12 and IFN-γ and with the local production of RANTES, MIP-1α, MIP-1β a</field></doc>
