<doc>
<field name="uid">doi:10.1371/journal.pntd.0001153</field>
<field name="doi">10.1371/journal.pntd.0001153</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Gláucia F. Cota, Marcos R. de Sousa, Ana Rabello</field>
<field name="title">Predictors of Visceral Leishmaniasis Relapse in HIV-Infected Patients: A Systematic Review</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2011</field>
<field name="volume">5</field>
<field name="number">6</field>
<field name="pages">e1153</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background and Objectives

Visceral leishmaniasis (VL) is a common complication in AIDS patients living
in Leishmania-endemic areas. Although antiretroviral therapy has changed the
clinical course of HIV infection and its associated illnesses, the prevention
of VL relapses remains a challenge for the care of HIV and Leishmania co-
infected patients. This work is a systematic review of previous studies that
have described predictors of VL relapse in HIV-infected patients.

Review Methods

We searched the electronic databases of MEDLINE, LILACS, and the Cochrane
Central Register of Controlled Trials. Studies were selected if they included
HIV-infected individuals with a VL diagnosis and patient follow-up after the
leishmaniasis treatment with an analysis of the clearly defined outcome of
prediction of relapse.

Results

Eighteen out 178 studies satisfied the specified inclusion criteria. Most
patients were males between 30 and 40 years of age, and HIV transmission was
primarily via intravenous drug use. Previous VL episodes were identified as
risk factors for relapse in 3 studies. Two studies found that baseline CD4+ T
cell count above 100 cells/mL was associated with a decreased relapse rate.
The observation of an increase in CD4+ T cells at patient follow-up was
associated with protection from relapse in 5 of 7 studies. Meta-analysis of
all studies assessing secondary prophylaxis showed significant reduction of VL
relapse rate following prophylaxis. None of the five observational studies
evaluating the impact of highly active antiretroviral therapy use found a
reduction in the risk of VL relapse upon patient follow-up.

Conclusion

Some predictors of VL relapse could be identified: a) the absence of an
increase in CD4+ cells at follow-up; b) lack of secondary prophylaxis; and c)
previous history of VL relapse. CD4+ counts below 100 cells/mL at the time of
primary VL diagnosis may also be a predictive factor for VL relapse.

Author Summary

Visceral leishmaniasis (VL) is the most serious form of an insect-transmitted
parasitic disease prevalent in 70 countries. The disease is caused by species
of the L. donovani complex found in different geographical regions. These
parasites have substantially different clinical, drug susceptibility and
epidemiological characteristics. According to data from the World Health
Organization, the areas where HIV-Leishmania co-infection is distributed are
extensive. HIV infection increases the risk of developing VL, reduces the
likelihood of a therapeutic response, and greatly increases the probability of
relapse. A better understanding of the factors promoting relapses is
essential; therefore we performed a systematic review of articles involving
all articles assessing the predictors of VL relapse in HIV-infected
individuals older than 14 years of age. Out of 178 relevant articles, 18 met
the inclusion criteria and in total, data from 1017 patients were analyzed. We
identified previous episodes of VL relapse, CD4+ lymphocyte count fewer than
100 cells/mL at VL diagnosis, and the absence of an increase in CD4+ counts at
follow-up as major factors associated with VL relapse. Knowledge of relapse
predictors can help to identify patients with different degrees of risk,
facilitate and direct prophylaxis choices, and aid in patient counseling.

Introduction

Visceral </field></doc>
