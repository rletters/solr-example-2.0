<doc>
<field name="uid">doi:10.1371/journal.pntd.0000272</field>
<field name="doi">10.1371/journal.pntd.0000272</field>
<field name="data_source">PLoS Open Access XML</field>
<field name="authors">Asif M. Khan, Olivo Miotto, Eduardo J. M. Nascimento, K. N. Srinivasan, A. T. Heiny, Guang Lan Zhang, E. T. Marques, Tin Wee Tan, Vladimir Brusic, Jerome Salmon, J. Thomas August</field>
<field name="title">Conservation and Variability of Dengue Virus Proteins: Implications for Vaccine Design</field>
<field name="journal">PLoS Neglected Tropical Diseases</field>
<field name="year">2008</field>
<field name="volume">2</field>
<field name="number">8</field>
<field name="pages">e272</field>
<field name="license">Creative Commons Attribution (CC BY)</field>
<field name="license_url">http://creativecommons.org/licenses/by/3.0/</field>
<field name="fulltext">
Abstract

Background

Genetic variation and rapid evolution are hallmarks of RNA viruses, the result
of high mutation rates in RNA replication and selection of mutants that
enhance viral adaptation, including the escape from host immune responses.
Variability is uneven across the genome because mutations resulting in a
deleterious effect on viral fitness are restricted. RNA viruses are thus
marked by protein sites permissive to multiple mutations and sites critical to
viral structure-function that are evolutionarily robust and highly conserved.
Identification and characterization of the historical dynamics of the
conserved sites have relevance to multiple applications, including potential
targets for diagnosis, and prophylactic and therapeutic purposes.

Methodology/Principal Findings

We describe a large-scale identification and analysis of evolutionarily highly
conserved amino acid sequences of the entire dengue virus (DENV) proteome,
with a focus on sequences of 9 amino acids or more, and thus immune-relevant
as potential T-cell determinants. DENV protein sequence data were collected
from the NCBI Entrez protein database in 2005 (9,512 sequences) and again in
2007 (12,404 sequences). Forty-four (44) sequences (pan-DENV sequences),
mainly those of nonstructural proteins and representing ∼15% of the DENV
polyprotein length, were identical in 80% or more of all recorded DENV
sequences. Of these 44 sequences, 34 (∼77%) were present in ≥95% of sequences
of each DENV type, and 27 (∼61%) were conserved in other Flaviviruses. The
frequencies of variants of the pan-DENV sequences were low (0 to ∼5%), as
compared to variant frequencies of ∼60 to ∼85% in the non pan-DENV sequence
regions. We further showed that the majority of the conserved sequences were
immunologically relevant: 34 contained numerous predicted human leukocyte
antigen (HLA) supertype-restricted peptide sequences, and 26 contained T-cell
determinants identified by studies with HLA-transgenic mice and/or reported to
be immunogenic in humans.

Conclusions/Significance

Forty-four (44) pan-DENV sequences of at least 9 amino acids were highly
conserved and identical in 80% or more of all recorded DENV sequences, and the
majority were found to be immune-relevant by their correspondence to known or
putative HLA-restricted T-cell determinants. The conservation of these
sequences through the entire recorded DENV genetic history supports their
possible value for diagnosis, prophylactic and/or therapeutic applications.
The combination of bioinformatics and experimental approaches applied herein
provides a framework for large-scale and systematic analysis of conserved and
variable sequences of other pathogens, in particular, for rapidly mutating
viruses, such as influenza A virus and HIV.

Author Summary

Dengue viruses (DENVs) circulate in nature as a population of 4 distinct
types, each with multiple genotypes and variants, and represent an increasing
global public health issue with no prophylactic and therapeutic formulations
currently available. Viral genomes contain sites that are evolutionarily
stable and therefore highly conserved, presumably because changes in these
sites have deleterious effects on viral fitn</field></doc>
