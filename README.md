# RLetters Solr Example

**NOTE: This version of the RLetters Solr example is designed to be used with RLetters 2.0, versions released prior to May of 2018. It will not work wil newer versions of RLetters. Please see [the new repository](https://codeberg.org/rletters/solr-example-3.0) for updates.**

This repository contains an example Solr instance that can be used with [RLetters.](https://codeberg.org/rletters/rletters)

The scripts are hopefully self-explanatory, but here goes anyway: the `data` folder includes a set of XML documents, the `start` and `stop` scripts bring the Solr server up and down, and the `sync` script clears out the Solr database and reindexes the files in the `data` directory.

We are currently bundling Solr 4.10.4.

The data here is taken from two sources: the [PLoS Neglected Tropical Diseases journal](http://journals.plos.org/plosntds/), available under the [CC-BY license,](https://creativecommons.org/licenses/by/2.0/) and from [Project Gutenberg,](https://www.gutenberg.org/) available under the [Project Gutenberg license.](https://www.gutenberg.org/wiki/Gutenberg:The_Project_Gutenberg_License)  All code here is licensed under the MIT License, see the COPYING file.
